#include "../include/triutils.h"

float fast_sqrt( float number )
{
        long i;
        float x2, y;
        const float threehalfs = 1.5F;
 
        x2 = number * 0.5F;
        y  = number;
        i  = * ( long * ) &y;                       // evil floating point bit level hacking
        i  = 0x5f3759df - ( i >> 1 );               // what the fuck?
        y  = * ( float * ) &i;
        y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration
//      y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed
 
        return y;
}

bool intersection(	const Point2d& pt1, const Point2d& pt2,
					const Point2d& ptA, const Point2d& ptB, 
					Point2d& intersection )
{
	/* intersection between (pt1, pt2) and (ptA, ptB).
		NB: intersection need not be in [pt1,pt2] or [ptA,ptB]
		if (pt1,pt2) and (ptA,ptB) are collinear, returns false.
	*/
	REAL a0 = pt1.y - pt2.y;
	REAL b0 = pt2.x - pt1.x;
	REAL c0 = - ( a0 * pt1.x + b0 * pt1.y );

	REAL a1 = ptA.y - ptB.y;
	REAL b1 = ptB.x - ptA.x;
	REAL c1 = - ( a1 * ptB.x + b1 * ptB.y );

	REAL det = a0*b1-a1*b0;

	if( det == 0. )
		return false;

	intersection( ( b0*c1 - b1*c0 ) / det, ( a1*c0 - a0*c1 ) / det );

	return true;

}

bool segintersecte(	const Point2d& pt1, const Point2d& pt2,
					const Point2d& ptA, const Point2d& ptB)
{	// wether segments [pt1,pt2] and [ptA,ptB] intersect.
	// check that no two points of a segment are on the same side of the other segment.
	return	 ( isdirect(ptA,ptB,pt1) ^ isdirect(ptA,ptB,pt2) ) && ( isdirect(pt1,pt2,ptA) ^ isdirect(pt1,pt2,ptB) );
}
