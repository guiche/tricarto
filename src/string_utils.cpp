#include "string_utils.h"

void string_utils::trim(std::string& str, const std::string& whitespace)
{
	const auto strBegin = str.find_first_not_of(whitespace);
	if (strBegin == std::string::npos)
		str.clear(); // no content
	else
	{
		const auto strEnd = str.find_last_not_of(whitespace);
		const auto strRange = strEnd - strBegin + 1;

		str = str.substr(strBegin, strRange);
	}
}

bool string_utils::isInteger(const std::string & s, bool acceptSign)
{
	if (s.empty() || (!isdigit(s[0]) && (!acceptSign || ((s[0] != '-') && (s[0] != '+')))))
		return false;


	/*	Explanation:
	strtol will parse the string, stopping at the first character that cannot be considered part of an integer.
	If you provide p (as I did above), it sets p right at this first non-integer character.

	if p is not set to the end of the string (the 0 character),
	then there is a non-integer character in the string s, meaning s is not a correct integer.
	*/
	char * p;

	// add a useless if statement to work around gcc 4.3 warning about unused returned value.
	if (strtol(s.c_str(), &p, 10))
		p += 0;

	return (*p == 0);		
}
