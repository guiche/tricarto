#include "triangle.h"
//#include <cmath>

#include "mesh.h"

#include "trigeneral.h"

#include "triexception.h"

#include "triutils.h"
#include <algorithm>

class Mesh;

/*****************************************************************************/
/*                                                                           */
/*  dummyinit()   Initialize the triangle that fills "outer space" and the   */
/*                omnipresent subsegment.                                    */
/*                                                                           */
/*  The triangle that fills "outer space," called `dummytri', is pointed to  */
/*  by every triangle and subsegment on a boundary (be it outer or inner) of */
/*  the triangulation.  Also, `dummytri' points to one of the triangles on   */
/*  the convex hull (until the holes and concavities are carved), making it  */
/*  possible to find a starting triangle for point location.                 */
/*                                                                           */
/*  The omnipresent subsegment, `dummysub', is pointed to by every triangle  */
/*  or subsegment that doesn't have a full complement of real subsegments    */
/*  to point to.                                                             */
/*                                                                           */
/*  `dummytri' and `dummysub' are generally required to fulfill only a few   */
/*  invariants:  their vertices must remain nullptr and `dummytri' must always  */
/*  be bonded (at offset zero) to some triangle on the convex hull of the    */
/*  mesh, via a boundary edge.  Otherwise, the connections of `dummytri' and */
/*  `dummysub' may change willy-nilly.  This makes it possible to avoid      */
/*  writing a good deal of special-case code (in the edge flip, for example) */
/*  for dealing with the boundary of the mesh, places where no subsegment is */
/*  present, and so forth.  Other entities are frequently bonded to          */
/*  `dummytri' and `dummysub' as if they were real mesh entities, with no    */
/*  harm done.                                                               */
/*                                                                           */
/*****************************************************************************/

triangle::triangle(Mesh& mesh):mesh(mesh)
{
	Reset();
}

void triangle::Reset()
{
	verts[0] = nullptr;
	verts[1] = nullptr;
	verts[2] = nullptr;

	otris[0].tri = mesh.dummytri;
	otris[1].tri = mesh.dummytri;
	otris[2].tri = mesh.dummytri;

	osubs[0].ss = mesh.dummysub;
	osubs[1].ss = mesh.dummysub;
	osubs[2].ss = mesh.dummysub;

	attrs[0] = 0;
	attrs[1] = 0;
	attrs[2] = 0;

	index = -1;
}

const otri& triangle::getTri(int idx)const
{
	return this->otris[idx];
}

const osub& triangle::getSeg(int idx)const
{
	return this->osubs[idx];
}

wertex* triangle::getVert(int idx)const
{
	return this->verts[idx];
}

void triangle::setTri(int idx, triangle* tri, int orient)
{
	this->otris[idx].tri = tri;
	this->otris[idx].orient = orient;
}

void triangle::setSeg(int idx, subseg* seg, int orient)
{
	this->osubs[idx].ss = seg;
	this->osubs[idx].ssorient = orient;
}

void triangle::setVert(int idx, wertex* vert)
{
	this->verts[idx] = vert;
}

subseg::subseg(Mesh& mesh):mesh(mesh),type(subseg::WALL),dualSeg(nullptr)
{
	verts[0] = nullptr;
	verts[1] = nullptr;
	verts[2] = nullptr;
	verts[3] = nullptr;

	osubs[0].ss = mesh.dummysub;
	osubs[1].ss = mesh.dummysub;

	otris[0].tri = mesh.dummytri;
	otris[1].tri = mesh.dummytri;

	seg_next[0].ss = mesh.dummysub;
	seg_next[1].ss = mesh.dummysub;

	seg_prev[0].ss = mesh.dummysub;
	seg_prev[1].ss = mesh.dummysub;

	index = -1;
	visited = -1;

	mass = 0.;
	elas = .5;
	frot = .05;
	glis = 0.;

	mark = 0;
}

void subseg::Delete(bool resetDual)
{
	if( index >= 0 )
	{
		osub temp(this);
		this->mesh.deletesegment(temp);
	
		if( resetDual && this->mesh.searchGraph && this->mesh.searchDualDirty )
		{
			this->mesh.searchDual->resetConvexAreas();
		}
	}
}

const otri& subseg::getTri(int idx)const
{
	return this->otris[idx];
}

const osub& subseg::getColinearSeg(int idx)const
{
	return this->osubs[idx];
}

wertex* subseg::getVert(int idx)const
{
	return this->verts[idx];
}

osub subseg::getNextSeg(int idx)const
{
	return this->seg_next[idx];
}

osub subseg::getPrevSeg(int idx)const
{
	return this->seg_prev[idx];
}

void subseg::setPrevSeg(int idx, subseg* seg, int orient)
{
	this->seg_prev[idx].ss = seg;
	this->seg_prev[idx].ssorient = orient;
}

void subseg::setNextSeg(int idx, subseg* seg, int orient)
{
	this->seg_next[idx].ss = seg;
	this->seg_next[idx].ssorient = orient;
}


void subseg::setTri(int idx, triangle* tri, int orient)
{
	this->otris[idx].tri = tri;
	this->otris[idx].orient = orient;
}

void subseg::setColinearSeg(int idx, subseg* seg, int orient)
{
	this->osubs[idx].ss = seg;
	this->osubs[idx].ssorient = orient;
}

void subseg::setVert(int idx, wertex* vert)
{
	this->verts[idx] = vert;
}



/*****************************************************************************/
/*                                                                           */
/*  triangleinit()   Initialize some variables.                              */
/*                                                                           */
/*****************************************************************************/

void wertex::reset()
{	
	index		= -1;

	mass		= 0.; // radius*radius

	w			= 0;

	frot = .05;
	elas = 1.;

	type		= NULLVERTEX;

	otri_.tri	= mesh.dummytri;

	dualVert	= nullptr;
	mark		= 0;
}

void wertex::Delete(bool resetDual)
{
	this->mesh.deleteVertex(*this,resetDual);
}

void wertex::setTri(triangle* tri, int orient)
{
	this->otri_.tri = tri;
	this->otri_.orient = orient;
}

const otri& wertex::getTri()const
{
	if( type == wertex::DEADVERTEX )
		throw TriangleExc("DEAD vertex");
	
	return otri_;
}

wertex::Type wertex::getType()const
{
	return type;
}

int wertex::getMarker()const{
	return vertexmark(this);
}

REAL wertex::distToSeg( const subseg& checkseg )const
{
	return checkseg.distToPoint(coords);
}

void subseg::calcUnitVec(Point2d& u)const
{   // precompute unit vec

	u = this->verts[1]->coords;
	u -= this->verts[0]->coords;
	u.iuni();
}

void subseg::setUnitVec()
{   // precompute unit vec

	calcUnitVec(unitVec);
}

Point2d osub::univec_py()const
{
	Point2d out = ss->unitVec;
	
	if( this->ssorient )
	{
		out.x *= -1;
		out.y *= -1;
	}
	return out;
}

REAL subseg::distToPoint( const Point2d& coords, bool distToLine )const
{
	//
	// distance between a segment and this vertex
	//

	const Point2d& coords1 = this->verts[0]->coords;
	const Point2d& coords2 = this->verts[1]->coords;

	Point2d pointToSeg = coords-coords1;

	if( !distToLine )
	{
		REAL projX = pointToSeg * unitVec;

		if( projX < 0 )
			return coords1.dist(coords);

		if( projX*projX > coords1.distSq(coords2) ) // projX > segSize
			return coords2.dist(coords);
	}

	// projection sur la normale unitaire au segment
	REAL distToSeg = unitVec^pointToSeg;
	return fabs(distToSeg);
}

bool wertex::detectSegContact( const osub& checkseg, 
								bool squareSection,
								REAL const& frontRadius,
								REAL const& sideRadius,
								const Point2d& moveUnit,
								REAL& moveDist, 
								bool checkValid )const
{
	/*	determines contact point along the moveUnit trajectory with a segment.

		distToSeg = max advance along the moveUnit direction before colliding with segment

	*/

	const Point2d& coords1 = svorg(checkseg)->coords;
	const Point2d& coords2 = svdest(checkseg)->coords;

	REAL org_osubY = prodscalN_vp(this->coords, coords1, moveUnit);
	REAL des_osubY = prodscalN_vp(this->coords, coords2, moveUnit);

	REAL orgY = org_osubY;
	REAL desY = des_osubY;

	if( ( orgY <= -sideRadius && desY <= -sideRadius) || // seg ends are both to the left  of the move
		( orgY >=  sideRadius && desY >=  sideRadius) )   // seg ends are both to the right of the move
	{
		return false;
	}

	REAL orgX = prodscal_vp (this->coords, coords1, moveUnit);
	REAL desX = prodscal_vp (this->coords, coords2, moveUnit);
	
	if(	orgX <= 0 && desX <= 0 )
	{	// both seg ends are behind the move
		// such that it can't be reached by advancing by moveDist.
		// No collision.
		return false;
	}

	if( orgX < desX )
	{
		// switch org and dest such that org is further away than dest along the move direction
		REAL temp = desX;
		desX = orgX;
		orgX = temp;

		temp = orgY;
		orgY = desY;
		desY = temp;
	}

	const Point2d& ssUnitVec = checkseg.ss->unitVec;

	REAL contactX;
	REAL avanceX;

	if (squareSection)
	{
		if (abs(desY) < sideRadius){			
			contactX = desX;			
		}
		else
		{
			const Point2d pt1 = this->coords + moveUnit.nml() * (desY > 0 ? sideRadius : -sideRadius);
			const Point2d pt2 = pt1 + moveUnit;
			Point2d contactPoint;
			
			if (!intersection(coords1, coords2, pt1, pt2, contactPoint))
				throw TriangleExc("Intersection expected");
			
			contactX = (contactPoint - coords) * moveUnit;
		}
		avanceX = contactX - frontRadius;
	}
	else 
	{
		// interX is such that coords + moveUnit * interX is the intersection between the trajectory and the segment.
		REAL interX = (orgX*desY - desX*orgY) / (desY - orgY);

		// temp is 1/sin(alpha) = norm(seg)/prodvect(moveUnit,seg)
		// temp = norm(orgCoo, desCoo) / fabs(prodvect_vp(orgCoo, desCoo, moveUnit));
		REAL temp = 1. / fabs(ssUnitVec ^ moveUnit);

		// avanceX is how much vertex can move along the trajectory before hitting the segment.
		avanceX = interX - frontRadius * temp;

		// contactX is the coordinate along moveUnit of the contact point
		contactX = avanceX + frontRadius / temp;
	}

	// Check whether the contact point is outside the segment.
	// If it is we expect to collide with a segment end instead.
	if( contactX < desX || orgX < contactX )
		return false;

	//REAL disttoseg = distToSeg(*checkseg.ss) - frontRadius; // TODO debug

	if( moveDist < 0 )
	{ // negative moveDist is a way of saying we are only interested in the distance to the segment.
		moveDist = avanceX;
		return true;
	}
	else
	{
		if( avanceX < 0 )
		{
			const bool isDirect = isdirect(checkseg.ss->verts[0]->coords,checkseg.ss->verts[1]->coords,coords);
			const REAL v_proj = prodvect_v(checkseg.ss->unitVec,moveUnit);
			// NB: unitVec is directed from coords1 to coords2

			if( ( isDirect && (v_proj >= 0) ) || ( !isDirect && (v_proj <= 0) ) )
			{	// move direction goes away from the segment, we don't care about an overlap
				return false;
			}
			else if( fabs(orgY) < sideRadius || fabs(desY) < sideRadius) // At least one of the segment ends is on the trajectory
			{
				const int sign = isDirect ? -1 : 1;
				Point2d pointTangent = coords; 
				pointTangent.iadd_mul(moveUnit, avanceX).iadd_mul(checkseg.ss->unitVec.nml(), sign*frontRadius);
								
				Point2d vec_tangent_1 = checkseg.ss->verts[0]->coords - pointTangent;
				Point2d vec_tangent_2 = checkseg.ss->verts[1]->coords - pointTangent;
				
				if( vec_tangent_1 * vec_tangent_2 >= 0 )
				{   // it's allright for avanceX to be negative if :
					//	- one of the segments endpoint is in the trajectory box,
					//     as we assume that collision will be detected further on when testing against that end.
					//  - both seg ends are beyond move range.
					return false;
				}
			}

			moveDist = 0; // we can't advance any further, there is already a mini-overlap

			__infoCollision.oseg = checkseg;
			__infoCollision.segAvanceX = avanceX;

			if( avanceX >= -1e-3 )
			{   // allow for numerical imprecision				
				return true;	
			}
			else
			{	// we shouldn't be getting there.
				if( !checkValid )
					return true;

				char message[50];
				sprintf(message,"Overlapping Vertex and Segment by %.4f",avanceX);
				throw TriangleExc_Sub_Vert(message, checkseg, this );
			}
			
		}

		if( __infoCollision.oseg.ss == nullptr || avanceX < __infoCollision.segAvanceX )
		{	// keep track of a blocking seg further away (not the first obstacle)
			__infoCollision.oseg = checkseg;
			if( org_osubY < des_osubY )
				ssymself(__infoCollision.oseg);// orientation convention for segment collided
			__infoCollision.segAvanceX = avanceX;
		}

		if( avanceX < moveDist )
		{
			if( __infoCollision.side_block && __infoCollision.side_block_dist < avanceX ){
				// Go back to first blocking vertex as max advance is reduced
				moveDist = __infoCollision.side_block_dist;
				
			}
			else{
				moveDist = avanceX; 
			}
			// move is constrained.
			return true;
		}
	
		// No collision
		return false;
	}
}

void wertex::detectVertexContact(wertex const& v_check,
	bool squareSection,
	const REAL& projX,
	const REAL& projY,
	REAL const& frontRadius,
	REAL const& sideRadius,	
	const Point2d& moveUnit,
	REAL& moveDist,
	const REAL& frontTolFrac)const
{
	/*
		Raises an error for a big overlap
	*/
	const REAL otherRadius = vertexradius(&v_check);
	REAL newDist = 0;

	const REAL rayons = frontRadius + otherRadius;
	
	if (squareSection)
	{

		const REAL sideDiff = abs(projY) - sideRadius;
		if (sideDiff <= 0){
			newDist = projX - frontRadius - otherRadius;
		}
		else{
			newDist = projX - sqrt(otherRadius*otherRadius - sideDiff*sideDiff) - frontRadius;
		}
		
	}
	else{		
		newDist = projX - sqrt(rayons*rayons - projY*projY);
	}

	const bool withinMoveReach = newDist < moveDist + (frontTolFrac - 1.)*radius;
	
	if(withinMoveReach || !__infoCollision.blocking)
	{
		// ajout du sommet a la liste des obstacles si :
		// 1) il gene effectivement le mouvement ou bien 
		// 2) il est au dela de la portee du mouvement mais aucun obstacle (sans compter la tolerance de cote)
		// n'a encore ete trouve.
		__infoCollision.add_obstacle(v_check, newDist);
	}
	
	if(withinMoveReach)
	{		
		//REAL dist = sqrt(dist2(coords, v_check->coords))-rayons;		

		if( newDist < -radius * 1e-3 )
		{ // Ignore overlaps smaller than 1/1000 radius.

			char message[50];
			sprintf(message,"Overlapping Vertices by %.4f", sqrt(dist2(coords, v_check.coords))-rayons);
			throw TriangleExc_Vert2(message, this, &v_check);
		}
		else if( newDist <= 0 )
		{
			newDist = 0.;
		}

		if ( __infoCollision.sideTolFrac > 0 && 
			newDist < moveDist && 
			fabs( projY ) > __infoCollision.sideTolFrac * rayons )
		{	// the two vertices don't overlap by more than sideTolFrac*rayons.

			Point2d newPos = coords + moveUnit * moveDist;

			REAL dist2ToNewPos = dist2(newPos, v_check.coords);

 			if( dist2ToNewPos > rayons*rayons )
			{
				// If advancing by moveDist takes us clear of v_check
				// ignore the collision

				if( !__infoCollision.side_block || newDist < __infoCollision.side_block_dist ){
					// Keep track of the ignored position in case we need to come back to it,
					// if we find another obstacle preventing from going far enough to that new position.
					
					__infoCollision.side_block = &v_check;
					if( newDist < 0 )
						__infoCollision.side_block_dist = 0;
					else
						__infoCollision.side_block_dist = newDist;
				}
				return;
			}
		}

		if( __infoCollision.side_block && __infoCollision.side_block_dist < newDist ){
			// We attempted ignoring one collision against a vertex on the account it was only sideways, below a tolerance, in passing
			// and not against the final destination. 
			// However there is another above-tolerance collision that prevents reaching that final destination, shortening moveDist.
			// To not risk vertex overlap, we go back to that earlier collision.
	
			if( __infoCollision.side_block_dist < moveDist )
				moveDist = __infoCollision.side_block_dist;

			__infoCollision.blocking = true;
		}
		else
		{
			if( newDist < moveDist )
			{
				moveDist = newDist;
				__infoCollision.blocking = true;
			}
		}
	}

	return;
}

void InfoCollision::add_obstacle(wertex const& vert, const REAL& projX)
{
	// Insertion du nouvel obstacle respectant l'ordre.
	
	if( obstProjXs.empty() )
	{
		obstProjXs.push_back(projX);
		obstacles.push_back(const_cast<wertex*>(&vert));
	}
	else
	{
		// Returns an iterator pointing to the first element in the range [first,last) which does not compare less than val.
		std::vector<REAL>::iterator low = std::lower_bound(obstProjXs.begin(), obstProjXs.end(), projX);

		const size_t i = low - obstProjXs.begin();

		obstProjXs.insert(low, projX);
		obstacles.insert(obstacles.begin()+i, const_cast<wertex*>(&vert));
	}
}

InfoCollision& wertex::firstObstacleLoop( int NCalls,
										const Point2d& move, 
										REAL const& frontRadius,
										REAL const& sideRadius,
										bool relative,
										bool segOnly,
										bool detectFar,
										bool checkResult, 
										const REAL& sideTolFrac,
										const REAL& frontTolFrac)const
{
	for( int i=0; i<NCalls; i++)
		firstObstacle(move, frontRadius, sideRadius, relative, segOnly, detectFar, checkResult, sideTolFrac, frontTolFrac);

	return __infoCollision;
}

InfoCollision& wertex::firstObstacle( const Point2d& move,
										REAL const& frontRadius,
										REAL const& sideRadius_,
										bool relative,
										bool /*segOnly*/,
										bool detectFar,
										bool checkResult, 
										const REAL& sideTolFrac,
										const REAL& frontTolFrac)const
{
	/*
		renvoie donnees de la premiere collision (point de contact, objet bloquant (vertex ou segment))
		sur la segment du sommet courant au point vise move=(x,y).

		relative :
			move : point vise en coordonnees absolues
		sinon :
			move : vecteur deplacement = point vise - position actuelle

		NB: Vertex.vert can be nullptr if no blocking vertex was found
	*/
	
	__infoCollision.Reset(sideTolFrac);

	Point2d moveUnit = move; // Vecteur unitaire pointant vers le point vise

	if( !relative )
		moveUnit -= this->coords;

	if( !moveUnit )
		return __infoCollision;

	const bool squareSection = sideRadius_ != 0;	
	const REAL sideRadius = squareSection ? sideRadius_ : frontRadius;
	
	const REAL moveDistOrig = moveUnit.nor();
	moveUnit.iuni();

	REAL moveDist = moveDistOrig;
	const REAL maxAvanceX = moveDistOrig + frontRadius;
	
	struct otri starttri = getTri();
	struct otri searchtri;

	if (starttri.org() != this)
		throw TriangleExc_Tri_Vert("Inconsistent vertex.otri_ org",starttri,this);

	searchtri = starttri;

	otri countingTri = searchtri;
	
	int turnccw = 1;
	vertex v_dest, v_apex;

	// Start by checking both apex and org as we don't know what triangle starting point is used.
	// all we know is that it has this at its org.
	int checkIndex;
	vertex v_toCheck [2];
	bool checkAccrossSegment = true;

	REAL projYs [2];
	REAL projXs [2];
	//REAL radiuses [2];

	int Apex = 1;
	int Dest = 0;

	Point2d sideNormale; // normale vector to the opposite side of countingTri.

	bool firstPass = true;
	// TODO: first identify the triangle pointed at by the orientation of the move
	// check both vertices ahead then start rotation around both sides triangles alternatively.
	do
	{	//
		// Check for collision against either apex or dest of countingTri
		//

		v_apex = countingTri.apex();
		v_dest = countingTri.dest();

		if( turnccw || !checkAccrossSegment || firstPass ){
			projYs[Apex] = moveUnit.prodscalN(coords, v_apex->coords);
			projXs[Apex] = moveUnit.prodscal_(coords, v_apex->coords);
			v_toCheck[Apex] = v_apex;
		}
		if( !turnccw || !checkAccrossSegment || firstPass ){
			projYs[Dest] = moveUnit.prodscalN(coords, v_dest->coords);
			projXs[Dest] = moveUnit.prodscal_(coords, v_dest->coords);
			v_toCheck[Dest] = v_dest;
		}

		firstPass = false;

		checkIndex = turnccw ? Apex : Dest;
		
		const REAL otherRadius = vertexradius(v_toCheck[checkIndex]);

		// Check collision against another vertex if:
		if(projXs[checkIndex] > 0 && 										// v_check is ahead and not behind
			(detectFar || projXs[checkIndex] <= maxAvanceX + otherRadius) &&// v_check's projection along the move direction is within reach
			fabs(projYs[checkIndex]) < sideRadius + otherRadius)			// sections projected on the move normal direction intersect
		{
			
			detectVertexContact( *v_toCheck[checkIndex], 
								squareSection,
								projXs[checkIndex], projYs[checkIndex], 
								frontRadius, sideRadius,
								moveUnit, moveDist,
								frontTolFrac);
		}

		//
		// Check whether trajectory direction intersects opposite side of countingTri
		//
		if( !checkAccrossSegment ){
			checkAccrossSegment = true;
		}
		else
		{
			if( ( projXs[Apex] > 0 || projXs[Dest] > 0 ) // at least one of the facing segment ends must be ahead of the move.
			   && projYs[Apex] > -sideRadius && projYs[Dest] < sideRadius// and we are not either too far to the right of dest or too far to the left of apex.
			   && !( projXs[Apex] > maxAvanceX && projXs[Dest] > maxAvanceX ) )
			{				
				bool checkThrough = projYs[Apex] > sideRadius && projYs[Dest] < -sideRadius;

				if(!checkThrough)
				{
					// Check whether we move far enough to cross the opposite side 
					// by comparing the projection of the trajectory on the side normal
					// to the projection of the side vertices.

					sideNormale = (v_dest->coords-v_apex->coords).iuni_nml();
					
					auto distToSeg = prodscal_vp(coords, v_apex->coords, sideNormale);
					const REAL moveProj = (sideNormale * moveUnit) * moveDist + frontRadius;

					checkThrough = moveProj > 0 && (moveProj + sideRadius > distToSeg || std::max<REAL>(sideRadius, frontRadius) > distToSeg);
				}

				if(checkThrough)
				{					
					// check opposite Tri
					std::vector<otri> edgesToVisit(1);
					lnext(countingTri,edgesToVisit[0]);
					otri edgetri;
					otri oppotri;
					do
					{
						edgetri = edgesToVisit.back();
						edgesToVisit.pop_back();

						const osub& checkseg = edgetri.apex_s();
						if( checkseg.ss != this->mesh.dummysub )
						{   // side is a segment - we can't move through it.
							detectSegContact(checkseg, squareSection, frontRadius, sideRadius, moveUnit, moveDist, checkResult);
						}
						else
						{	// Check through the side
#ifdef SELF_CHECK
							if( __infoCollision.obstProjXs.size() > 100 )
								throw TriangleExc_Vert("Probable Boucle infinie",this);
#endif
							sym(edgetri,oppotri);

							vertex v_check = oppotri.apex();

							if( oppotri.tri != this->mesh.dummytri )
							{
								bool segContactDetected = false;

								if( segmentend(v_check) )
								{									
									otri pivottri = oppotri;

									Point2d cwLimit, ccwLimit;
									if(moveUnit.isdirect_(coords, v_check->coords))
									{
										cwLimit = moveUnit.nml(); ccwLimit = moveUnit;
									}
									else
									{
										cwLimit = moveUnit; ccwLimit = moveUnit.anml();
									}

									for (int cw = 0; cw <= 1; cw ++)
									{
										lprev(oppotri, pivottri);
										auto& limit = cw ? cwLimit : ccwLimit;
										do
										{ // Rotate around v_pivot cw until we find the segment or the outside

											osub check_seg = cw ? pivottri.apex_s() : pivottri.dest_s();

											if(check_seg.ss != this->mesh.dummysub )
											{
												segContactDetected |= detectSegContact(
													check_seg,
													squareSection, 
													frontRadius, 
													sideRadius, 
													moveUnit, 
													moveDist, 
													checkResult);
												break;	
											}
											
											if(cw){
												oprevself(pivottri);
											}
											else{
												onextself(pivottri);									
											}

											if(pivottri.tri == this->mesh.dummytri)
												break;

										}while(limit.isdirect_(v_check->coords, (cw ? pivottri.dest() : pivottri.apex())->coords));
									}
								}
																
								if( v_check == this )
									continue;

								const REAL projY = moveUnit.prodscalN(coords,v_check->coords);
								const REAL projX = moveUnit.prodscal_(coords,v_check->coords);

								const REAL v_check_radius = vertexradius(v_check);
								const REAL sideClearance = sideRadius + v_check_radius;
								const REAL rayons = frontRadius + v_check_radius;
								// Check collision against another vertex if:
								if(projX > 0 && 								// v_check is ahead and not behind
									(detectFar || projX <= moveDist + rayons ) &&				// v_check's projection along the move direction is within reach
									fabs(projY) < sideClearance)				// sections projected on the move normal direction intersect
								{
									detectVertexContact(
										*v_check, 
										squareSection, 
										projX, 
										projY, 
										frontRadius, 
										sideRadius, 
										moveUnit, 
										moveDist, 
										frontTolFrac);
								}

								if (oppotri.dest_s().ss == this->mesh.dummysub)
								{
									const REAL projX_org = moveUnit.prodscal_(coords, oppotri.org()->coords);

									// Check each side of v_check.
									if ((projY <= 0 && projX > 0) ||
										(projY <= sideRadius && projX_org > 0) ||
										(projX > 0 && projY < sideRadius)) // Check left
									{
										// check through the side
										if (projX <= moveDist + frontRadius ||
											prodscal_vp(coords, oppotri.org()->coords, moveUnit) < moveDist + frontRadius)
										{
											// visit beyond the side
											edgesToVisit.push_back(oppotri);
											lprevself(edgesToVisit.back());
										}
									}
								}

								if (oppotri.org_s().ss == this->mesh.dummysub)
								{
									const REAL projX_des = moveUnit.prodscal_(coords, oppotri.dest()->coords);

									if ((projY > 0 && projX > 0) ||
										(projY > -sideRadius && projX_des > 0) ||
										(projX > 0 && projY > -sideRadius)) // Check right
									{

										if (projX <= moveDist + frontRadius ||
											moveUnit.prodscal_(coords, oppotri.dest()->coords) < moveDist + frontRadius)
										{
											edgesToVisit.push_back(oppotri);
											lnextself(edgesToVisit.back());
										}
									}
								}
							}
						}
					}while(!edgesToVisit.empty());
				}
								
				

			} // end of check through opposite side

		}


		Apex = Dest;
		Dest = 1-Dest;

		if( turnccw ){
			onextself(countingTri);
		}else{
			oprevself(countingTri);
		}

		if( countingTri.tri == this->mesh.dummytri )
		{
			if( turnccw )
			{
				// Switch rotation direction.
				countingTri = searchtri;
				turnccw = false;
				checkAccrossSegment = false;
			}
			else
			{
				// We have reached an edge of the mesh after attemption to rotate in both directions.
				break;
			}
		}
					
	}
	while( !turnccw || countingTri.tri != searchtri.tri ); 
	// if we are not turning ccw, it means we have switched direction after bumping into a mesh edge
	// so we can assume that we will break by bumping into another side of the edge in cw direction.


	__infoCollision.coords.ass_mul(moveUnit,moveDist);
	__infoCollision.blocking  = moveDist < moveDistOrig;
	

	if( __infoCollision.sideTolFrac > 0 && __infoCollision.side_block != nullptr && __infoCollision.side_block_dist < moveDist )
	{   // Removed obstacles we have ignored due to side tolerance
		for( int i = (int)__infoCollision.obstProjXs.size()-1; i>=0; i-- )
		{
			if( __infoCollision.obstProjXs[i] < moveDist )
			{
				__infoCollision.obstProjXs.erase(__infoCollision.obstProjXs.begin()+i);
				__infoCollision.obstacles .erase(__infoCollision.obstacles .begin()+i);
			}
		}
	}

	if( __infoCollision.blocking )
	{
		const REAL moveDistWithTol = moveDist + frontRadius * (frontTolFrac-1);
		for( int i = (int)__infoCollision.obstProjXs.size()-1; i>=0; i-- )
		{
			if( __infoCollision.obstProjXs[i] > moveDistWithTol )
			{
				__infoCollision.obstProjXs.pop_back();
				__infoCollision.obstacles.pop_back();
			}
			else
				break;
		}
	}
	// allow for Numerical precision
	moveDist *= .99999999;

	if( checkResult )
		checkCollide( __infoCollision.coords+coords );

	return __infoCollision;
}


void wertex::checkCollide( const Point2d& newPos, bool fullCheck )const
{
	/*Check neighboring vertices for collisions if this was to move to newPos*/
	
	std::vector<vertex>::const_iterator it;
	std::vector<vertex>::const_iterator end;

	if( fullCheck )
	{
		it = this->mesh.vertices.begin();
		end = this->mesh.vertices.end();
	}
	else
	{
		// Check only neighboring vertices
		otri searchtri;
		searchtri = this->otri_;

		this->mesh.preciselocate(newPos,&searchtri,false);
		std::vector<vertex> vertices;
		vertices.reserve(6);

		vertex tempVert = searchtri.org();
		if( tempVert != this )
			vertices.push_back( tempVert );
		tempVert = searchtri.dest();
		if( tempVert != this )
			vertices.push_back( tempVert );
		tempVert = searchtri.apex();
		if( tempVert != this )
			vertices.push_back( tempVert );
	
		otri temptri;
		sym(searchtri,temptri);

		if( temptri.tri != this->mesh.dummytri )
		{
			tempVert = temptri.apex();
			if( tempVert != this )
				vertices.push_back(tempVert);
		}

		dprev(searchtri,temptri);
		if( temptri.tri != this->mesh.dummytri )
		{
			tempVert = temptri.apex();
			if( tempVert != this )
				vertices.push_back(tempVert);
		}

		onext(searchtri,temptri);
		if( temptri.tri != this->mesh.dummytri )
		{
			tempVert = temptri.apex();
			if( tempVert != this )
				vertices.push_back(tempVert);
		}

		it = vertices.begin();
		end = vertices.end();
	}

	REAL otherradius;
	//const REAL& radius = vertexradius(this);

	for( ; it<end; it++ )
	{
		if( *it != this )
		{
			otherradius = vertexradius(*it);
			if( otherradius >= 0 )
			{
				REAL sumradius = radius + otherradius;

				REAL distsq = dist2( (*it)->coords, newPos );

				if( distsq <= .9999999 * sumradius * sumradius )
				{
					char message[50];
					sprintf(message,"Overlapping Vertices by %.2f to %.2f,%.2f",sumradius-sqrt(distsq),newPos.x,newPos.y);
					throw TriangleExc_Vert2(message,this,*it);
				}
			}
		}
	}


	//
	// Check overlap between this vertex and all subsegments.
	//
	osub seg;
	for(auto sit : this->mesh.subsegs)
	{
		seg.ss = sit;
		if( this == svorg(seg) || this == svdest(seg) )
			continue;

		const REAL segDist = seg.ss->distToPoint(newPos) - vertexradius(this);
		
		if( segDist < -1e-6 )
		{
			char message[50];
			sprintf(message,"Overlapping Vertex and Segment by %.2f moving to %.2f,%.2f",segDist,newPos.x,newPos.y);
			throw TriangleExc_Sub_Vert(message,osub(sit),this);
		}
	}
 }


enum insertvertexresult wertex::move( const Point2d& move, bool relative, bool checkCollisions, bool enforceDelaunay )
{
	Point2d newPos = move;

	if( relative )
	{
		if( !move )
			return SUCCESSFULVERTEX;

		newPos += this->coords;
	}
	else if(newPos == this->coords)
	{
		return SUCCESSFULVERTEX;
	}

	if( checkCollisions )
		checkCollide(newPos);

	struct otri vertextri = this->otri_;
	if(vertextri.org() != this)
		throw TriangleExc_Tri_Vert("Vertex->otri_ has bad origin", this->otri_, this);
	
	otri countingtri = vertextri;

	if( isConvexAndInterior(newPos) )
	{
		// Vertex has moved within one of the triangles it defines and the neighboring vertex define a convex area:
		// we can take a shortcut here and update the coordinates in place,
		// without any triangle deletion/creation/hard retriangulation 
		// as the convexity ensures the move is not creating inverted triangles.
					
		this->coords = newPos;
		
		// We still need to check and restore Delaunay property
		if( enforceDelaunay )
			this->mesh.repairDelaunay(countingtri);

		return SUCCESSFULVERTEX;
	}
	else
	{
		// Vertex has moved further away than its former position convex hole
		// so we must delete it, retriangulate the hole and reinsert it.

		std::vector<wertex *> osubneighbors;
	
		if( vertexradius(this) <= 0 )
		{
			// Check what other vertex we are connected to via subsegs.
			this->mesh.deletevertex(&vertextri, false, &osubneighbors);
		}
		else
			this->mesh.deletevertex(&vertextri, false);

		this->coords = newPos;
		
		insertvertexresult result = this->mesh.insertvertex(this, &vertextri);

		for( auto neighbor: osubneighbors)
		{
			this->Connect(*neighbor);
		}

		if( this->mesh.searchGraph && segmentend(this) )
		{
			// Update searchGraph if the new vertex is a boundary endpoint.
			this->dualVert->move(move,relative,checkCollisions,enforceDelaunay);
			this->mesh.searchDual->resetConvexAreas();
		}

		return result;
	}
}

void wertex::Connect( wertex& other )
{
	assert( segmentend(this) );
	assert( segmentend(&other) );

	this->mesh.insertsegment( this, &other );

	if (this->mesh.searchGraph)
	{
		this->dualVert->Connect(*other.dualVert);
		this->mesh.searchDualDirty = false;
	}

}

void wertex::getNeighbors(std::vector<wertex*>& neighbors, int degree, bool seeThroughWall)
{
	/*
		Liste des voisins du sommet d'ordre degree.
		(degree 0 : voisins directs, i.e. connectes par une arrete)

		NB : commence a parcourir dans le sens indirect.

		NB : fonctionne avec un sommet frontiere.
		Comme cela casse la circularite autour du sommet, il faut
		pouvoir tourner dans les deux sens.
		Cela fait que pour un sommet interieur, les voisins sont dans un ordre
		unidirectionnel de parcours du cercle.
		
	*/

	neighbors.resize(0);

	otri starttri = this->otri_;
	otri countingtri = starttri;

	otri temptri;
	osub tempsub;
  
	bool hasCheckedOtherSide = false;

	do{
  
		if( countingtri.tri==this->mesh.dummytri ){
			if( hasCheckedOtherSide )
				break;
			else{
				// go back to starting tri and walk in the other direction.
				neighbors.push_back( starttri.dest() );
				hasCheckedOtherSide = true;
				oprev(starttri, countingtri);
				continue;
			}
		}

		if( degree >=1 )
		{
			dprev(countingtri, temptri);
			if( temptri.tri != this->mesh.dummytri )
			{	
				tspivot( temptri, tempsub );
				if( seeThroughWall || tempsub.ss == this->mesh.dummysub || tempsub.ss->type != subseg::WALL )
				{// Check that no subseg is hidding neighbors beyond that side.
					vertex tempvert = temptri.apex();
					if( ( !segmentend(temptri.org()) || !isdirect(this->coords, temptri.org()->coords, tempvert->coords) ) 
					&&  ( !segmentend(temptri.org()) ||  isdirect(this->coords, temptri.dest()->coords, tempvert->coords) ))
					{
						// Check there is line of sight if any of the opposite side vertex is a segment end.
						neighbors.push_back( tempvert );
					}
				}
			}
		}

		if( hasCheckedOtherSide ){
			neighbors.push_back( countingtri.dest() );
			oprevself(countingtri);
		}
		else{
			neighbors.push_back( countingtri.apex() );
			onextself(countingtri);
		}

	}while ( starttri != countingtri );

}

std::vector<wertex*> wertex::getNeighbors_py(int degree, bool seeThroughWall)
{
	std::vector<wertex*> out;
	getNeighbors(out,degree,seeThroughWall);
	return out;
}

bool wertex::isConvexAndInterior(const Point2d& newpos)const
{   // Whether the neighbor vertices of this define a convex region.
	// and newpos is in that region and this is an inner vertex.

  struct otri countingtri = this->otri_;
  vertex prevvert;

  do{

	prevvert = countingtri.dest();
	onextself(countingtri);

	if( countingtri.tri == this->mesh.dummytri )
		return false;
	
	if( !(isdirect( prevvert->coords, countingtri.dest()->coords, countingtri.apex()->coords ) ) )
		return false;
	if( !(isdirect(countingtri.dest()->coords, countingtri.apex()->coords, newpos)) )
		return false;

  }while( countingtri != this->otri_ );

  return true;
}

std::vector<osub> wertex::segments()const
{
	// segments ending on this vertex

	std::vector<osub> out;

	struct otri temptri = this->otri_;

	struct otri countingtri;
  
	countingtri = temptri;
	bool hasCheckedOtherSide = false;

	osub tempseg;

	do{
  
		if( hasCheckedOtherSide )
		{
			tempseg = countingtri.apex_s();
			oprevself(countingtri);
		}
		else{
			tempseg = countingtri.dest_s();
			onextself(countingtri);
		}
		if( tempseg.ss != this->mesh.dummysub )
			out.push_back(tempseg);

		if( countingtri.tri==this->mesh.dummytri ){
			if( hasCheckedOtherSide )
				break;
			else{
				hasCheckedOtherSide = true;
				countingtri = temptri;
			}
		}
	
	}while ( hasCheckedOtherSide || temptri != countingtri );

	return out;
}

int wertex::getDegree(bool checkCollisions)const
{
	/*
	- a negative degree if vertex is on a boundary (i.e. has a dummy vertex in its fan)
	- 0 if collision is detected.
	*/
	struct otri temptri = this->otri_;

	struct otri countingtri;
  
	int degree = 0;

	countingtri = temptri;
	bool hasCheckedOtherSide = false;

	do{
  
#ifdef SELF_CHECK
		if( degree > 10000 )
			throw TriangleExc("vertex degree exceeds 10000 - hint of an infinite loop");
#endif

		if( checkCollisions )
		{
			vertex otherVert;
			if( hasCheckedOtherSide )
				otherVert = countingtri.dest();
			else
				otherVert = countingtri.apex();

			REAL sumRadius = vertexradius(otherVert) + vertexradius(this);
			if( this->coords.distSq(otherVert->coords) <= 1.01 * sumRadius*sumRadius )
				return 0;
						
		}

		if( hasCheckedOtherSide ){
			degree++;
			oprevself(countingtri);
		}
		else{
			degree++;
			onextself(countingtri);
		}

		if( countingtri.tri==this->mesh.dummytri ){
			if( hasCheckedOtherSide )
				break;
			else{
				hasCheckedOtherSide = true;
				countingtri = temptri;
			}
		}
	
	}while ( hasCheckedOtherSide || temptri != countingtri );

	if( hasCheckedOtherSide )
		return -degree;
	else
		return degree;
}




std::ostream&  operator<<(std::ostream& out, const wertex& arg)
{
	if( &arg == nullptr )
		out << "vNULL";
	else
		out << "v" << arg.id;
    return out;
}

/*
 * otri::
 */

std::ostream&  operator<<(std::ostream& out, const otri& arg)
{
	if (!arg.tri)
	{
		out << "ot[void]";
	}
	else
	{
		if (arg.isDummy())
			out << "dummy ot[ ";
		else
			out << "ot" << arg.tri->index << " [ ";

		out << *arg.org() << "," << *arg.dest() << "," << *arg.apex() << " ]";
	}
    return out;
}

std::ostream&  operator<<(std::ostream& out, const triangle& arg)
{
	if( &arg == arg.mesh.dummytri )
		out << "dummy t[ " ;
	else
		out << "t" << arg.index << " [ ";

	out << *arg.getVert(0) << "," << *arg.getVert(1) << "," << *arg.getVert(2) << " ]";

    return out;
}

void otri::setorient( int arg ){
	orient = arg%3;
	if( arg < 0 )
		orient += 3;
}

otri wertex::triSecant( const Point2d& point2 )
{
	/*
		Find the first triangle intersected on the path from this->coords to point2 (x,y)
		NB : modifie this->otri_
	*/
	struct otri countingtri = otri_;
  	
	do{
		vertex v_apex = countingtri.apex();
		vertex v_dest = countingtri.dest();

		REAL prodVapex = prodvect(coords, point2, v_apex->coords);
		REAL prodVdest = prodvect(coords, point2, v_dest->coords);

		if( (prodVapex == 0 && prodscal(coords,point2,v_apex->coords) >= 0) || 
			(prodVdest == 0 && prodscal(coords,point2,v_dest->coords) >= 0) || 
			(prodVapex >= 0 && prodVdest <= 0))
		{
			otri_ = countingtri;
			return countingtri;
		}

		if( prodVapex > 0 ){
			oprevself(countingtri);
		}
		else{
			onextself(countingtri);			
		}

		if( countingtri.tri == this->mesh.dummytri ){
			return otri(this->mesh.dummytri);
		}

	}while (true);

	return otri_;//dummy statement for compile warning
}

otri::iterator otri::begin()const { return otri::iterator(*this,true, true); }
otri::iterator otri::end()const { return otri::iterator(*this,true, false); }
otri::iterator otri::rbegin()const { return otri::iterator(*this,false,true); }
otri::iterator otri::rend()const { return otri::iterator(*this,false,false); }

otri_iter_unidir otri::begin_unidir()const { return otri_iter_unidir(*this,true,true); }
otri_iter_unidir otri::end_unidir()const { return otri_iter_unidir(*this,true,false); }
otri_iter_unidir otri::rbegin_unidir()const { return otri_iter_unidir(*this,false,true); }
otri_iter_unidir otri::rend_unidir()const { return otri_iter_unidir(*this,false,false); }

otri_iter & otri_iter::increment(bool forward, bool uni_dir)
{
	const bool isdirect = _is_direct ^ !forward;

	if( isdirect )
	{
		if( _stop_at_seg && (_counter.dest_s().ss != _counter.tri->mesh.dummysub) )
		{

		}
		else
			onextself(_counter);
	}
	else
	{
		oprevself(_counter);
	}

	_is_start = false;

	if( _counter.tri==_counter.tri->mesh.dummytri )
	{
		_counter = _start; // shoud match otri.end() iterator

		if( uni_dir )
		{
			while(true)
			{ // Rewind until we find another edge so we can keep iterating in the same direction.
				if( isdirect )
				{
					if( right_tri(_counter) ==_counter.tri->mesh.dummytri )
						break;
					oprevself(_counter);
				}
				else
				{
					if( left_tri(_counter) ==_counter.tri->mesh.dummytri )
						break;
					onextself(_counter);
				}	
			}
		
			_found_edge = true;
		}
		else
		{
			_is_direct = !_is_direct; // switch iteration direction

			if( !_found_edge )
			{	// go back to _start and rotate in other direction.
				_found_edge = true;
				increment(forward,uni_dir);
			}
		}
	}
	return *this;
}


ETriSidePos otri::orientTri( const Point2d& point1, const Point2d& point2, bool inplace, bool moveToNext )
{
	/* Assuming point1 is contained in this triangle,
	   returns the side crossed by point1->point2.
	  
	  if inplace : transforms this into the adjacent triangle beyond the edge crossed by point1->point2 direction.
	*/

	if( prodvect(point1,point2,this->dest()->coords) >=0 && prodvect(point1,this->org()->coords,point2) >=0 )
	{ // exit triangle through org-dest
		if( inplace && moveToNext ){
			symself(*this);
		}
		return ETriSidePos::opAPX;
	}
	else if( prodvect(point1,point2,this->org()->coords) >=0 && prodvect(point1,this->apex()->coords,point2) >=0 )
	{// exit triangle through org-apex
		if (inplace ){
			if (moveToNext)
			{
				onextself(*this);
			}
			else
			{
				lprevself(*this);
			}
		}
		return ETriSidePos::opDST;
	}
	else
	{// exit triangle through dest-apex
		if( inplace ){
			if (moveToNext)
			{
				dprevself(*this);
			}
			else
			{
				lnextself(*this);
			}
		}
		return ETriSidePos::opORG;
	}
}

otri & otri::operator++(){ onextself(*this); return *this; }
otri & otri::operator--(){ oprevself(*this); return *this; }

wertex* otri::org()const{
#ifdef SELF_CHECK
	if( this->tri == nullptr )
		throw TriangleExc("triangle is NULL");
#endif //SELF_CHECK
	return tri->verts[plus1mod3[orient]];
}
wertex* otri::dest()const{
#ifdef SELF_CHECK
	if( this->tri == nullptr )
		throw TriangleExc("triangle is NULL");
#endif //SELF_CHECK
	return tri->verts[minus1mod3[orient]];
}
wertex* otri::apex()const{
#ifdef SELF_CHECK
	if( this->tri == nullptr )
		throw TriangleExc("triangle is NULL");
#endif //SELF_CHECK
	return tri->verts[orient];
}

Point2d& otri::coords(uint i)const
{ 
	return tri->verts[(orient+1+i)%3]->coords; 
}

wertex* otri::getVert(uint i)const
{
	return tri->verts[(orient + 1 + i) % 3];
}

void otri::setorg(wertex* vert){tri->verts[plus1mod3[orient]] = vert;}
void otri::setdest(wertex* vert){tri->verts[minus1mod3[orient]] = vert;}
void otri::setapex(wertex* vert){tri->verts[orient] = vert;}


const osub& otri::org_s()const
{
#ifdef SELF_CHECK
	if( this->tri == nullptr )
		throw TriangleExc("triangle is NULL");
#endif //SELF_CHECK
	return tri->osubs[plus1mod3[orient]];
}

const osub& otri::dest_s()const
{
#ifdef SELF_CHECK
	if( this->tri == nullptr )
		throw TriangleExc("triangle is NULL");
#endif //SELF_CHECK
	return tri->osubs[minus1mod3[orient]];
}

const osub& otri::apex_s()const
{
#ifdef SELF_CHECK
	if( this->tri == nullptr )
		throw TriangleExc("triangle is NULL");
#endif //SELF_CHECK
	return tri->osubs[orient];
}

otri otri::left_tri_()const{
	otri out(*this);
	out.tri = left_tri(*this);
	return out;
}

otri otri::right_tri_()const{
	otri out(*this);
	out.tri = right_tri(*this);
	return out;
}

std::vector<wertex*> otri::vertices()const
{
	std::vector<wertex*> out(3);
	if( this->tri )
	{
		out[0] = this->org();
		out[1] = this->dest();
		out[2] = this->apex();
	}
	return out;
}

otri otri::lprev_() const{
	otri temp;
	lprev(*this, temp);
	return temp;
}
otri otri::lnext_() const{
	otri temp;
	lnext(*this, temp);
	return temp;
}
void otri::lprevself_(){
	lprevself(*this);
}
void otri::lnextself_(){
	lnextself(*this);
}
otri otri::oprev_() const{
	otri temp;
	oprev(*this, temp);
	return temp;
}
otri otri::onext_() const{
	otri temp;
	onext(*this, temp);
	return temp;
}

otri otri::dnext_() const{
	otri temp;
	dnext(*this, temp);
	return temp;
}
otri otri::dprev_() const{
	otri temp;
	dprev(*this, temp);
	return temp;
}

otri otri::sym_() const{
	otri temp;
	sym(*this, temp);
	return temp;
}

otri otri::rprev_() const{
	otri temp;
	rprev(*this, temp);
	return temp;
}

otri otri::rnext_() const{
	otri temp;
	rnext(*this, temp);
	return temp;
}

osub otri::osegnext()const{
	/* Rotate ccw around the origin until we find a non-dummy seg.
		Ignore any seg between org and apex or org and dest.
		Returned osub has same origin as this.
	*/

	struct otri temptri;
	struct osub tempsub;
	tempsub.ss = tri->mesh.dummysub;

	onext(*this, temptri);

	// rotate clockwise

	while( temptri.tri != tri->mesh.dummytri && temptri.tri != this->tri )
	{
		tempsub = temptri.dest_s(); // subseg on the side opposite dest, oriented from apex to org

		if( tempsub.ss != tri->mesh.dummysub )
		{
			break;
		}

		onextself(temptri);
	}

	return tempsub;
}

osub otri::osegprev()const{
	/*  Rotate clockwise around the origin until we find a non dummy seg.
		Ignore any seg between org and apex or org and dest.
		Returned osub has same origin as this.
	*/

	struct otri temptri;
	struct osub tempsub;
	tempsub.ss = tri->mesh.dummysub;

	oprev(*this,temptri);

	// rotate clockwise

	while( temptri.tri != tri->mesh.dummytri && temptri.tri != this->tri )
	{		
		tempsub = temptri.apex_s(); // subseg on the side opposite apex, oriented from dest to org

		if( tempsub.ss != tri->mesh.dummysub )
		{
			ssymself(tempsub); // we want tempsub to have the same origin as this
			break;
		}

		oprevself(temptri);
	}

	return tempsub;
}

osub otri::dsegnext()const{
	struct otri temptri;
	lnext(*this,temptri);
	return temptri.osegnext();
}
osub otri::dsegprev()const{
	struct otri temptri;
	lnext(*this,temptri);
	return temptri.osegprev();
}
osub otri::asegnext()const{
	struct otri temptri;
	lprev(*this,temptri);
	return temptri.osegnext();
}
osub otri::asegprev()const{
	struct otri temptri;
	lprev(*this,temptri);
	return temptri.osegprev();
}

void otri::bond_(const struct otri& other){ // other.tri is actually changed.
	bond(*this,other);	
}

void otri::flip(){
	tri->mesh.flip(this);
}

void otri::Delete(){
	tri->Delete();
}

void triangle::Delete(){
	this->mesh.triangledealloc(this);
}

bool otri::contains(const Point2d& point, bool strict)const
{
	return sideposition(point, strict) == ETriSidePos::inTRI;
}

ETriSidePos otri::sideposition(const Point2d& point, bool strict)const
{
	/*	returns -1 if point x,y is inside this->tri.
		else returns the index of the first side beyond which the point lies.
		NB: the order in which we check the position relative to a side matters, as a point can be beyond two sides at once.
			So here we check org->dest then org->apex and finally dest->apex. 
		NB: strict = true considers the point is outside if it's exactly on a side.
	*/
	if (tri == nullptr || tri == tri->mesh.dummytri)
		throw TriangleExc("invalid otri");

	vertex v_org  = this->org();
	vertex v_dest = this->dest();
	
	REAL prodv = prodvect(v_org->coords,v_dest->coords,point);
	if( (  strict && prodv <= 0 ) || 
		( !strict && prodv < 0 ) )
		return ETriSidePos::opAPX; // point is to the right of org->dest

	vertex v_apex = this->apex();

	prodv = prodvect(v_apex->coords,v_org->coords,point);
	if( (  strict && prodv <= 0 ) || 
		( !strict && prodv < 0 ) )
		return ETriSidePos::opDST; // point is to the left of org->apex

	prodv = prodvect(v_dest->coords,v_apex->coords,point);
	if( (  strict && prodv <= 0 ) || 
		( !strict && prodv < 0 ) )
		return ETriSidePos::opORG; // point is to the right of dest->apex, beyond opposite side of org.

	// point is inside this triangle.
	return ETriSidePos::inTRI;
}

bool otri::isDirect(const Point2d& point)const{
	// whether point x,y lies to the left of dest-org direction
	return isdirect(org()->coords,dest()->coords,point);
}

 bool otri::isDummy()const{return tri == tri->mesh.dummytri;}

/*
 *	 osub::
 */


void osub::Delete(bool resetDual)
{
	if( this->ss )
		this->ss->Delete(resetDual);
}

wertex* osub::getVert(int index)const
{
	return this->ss->getVert(index);
}

REAL osub::getMass_py()const{ return ss->mass; }
void osub::setMass_py(REAL arg){ ss->mass = arg; }
REAL osub::getElas_py()const{ return ss->elas; }
void osub::setElas_py(REAL arg){ ss->elas = arg; }
REAL osub::getFrot_py()const{ return ss->frot; }
void osub::setFrot_py(REAL arg){ ss->frot = arg; }
REAL osub::getGlis_py()const{ return ss->glis; }
void osub::setGlis_py(REAL arg){ ss->glis = arg; }

std::ostream&  operator<<(std::ostream& out, const osub& arg)
{
	if (!arg.ss)
	{
		out << "os[void]";
	}
	else
	{
		if (arg.isDummy())
			out << "dummy os[ " ;
		else
			out << " os" << arg.ss->index << " [ ";

		out << *arg.sorg_py() << "," << *arg.sdest_py() << " ]";
	}
    return out;
}

std::ostream&  operator<<(std::ostream& out, const subseg& arg)
{
	if( arg.isDummy() )
		out << "dummy[ " ;
	else
		out << "s" << arg.index << " [ ";

	out << *arg.getVert(0) << "," << *arg.getVert(1) << " ]";

    return out;
}

bool subseg::isDummy()const{
	 return this == this->mesh.dummysub;
}

bool osub::isDummy()const{
	 return ss->isDummy();
}

osub osub::ssym_py()const{
	osub out;
	ssym(*this,out);
	return out;
}

osub osub::segnext_py()const
{
	return segnext(*this);
}
osub osub::segprev_py()const
{
	return segprev(*this);
}

osub osub::dsegnext_py()const
{
	osub out = segnext(*this);
	ssymself(out);
	return out;
}
osub osub::dsegprev_py()const
{
	osub out = segprev(*this);
	ssymself(out);
	return out;
}

osub osub::spivot_py()const{
	osub out;
	spivot(*this,out);
	return out;
}
osub osub::snext_py()const{
	osub out;
	snext(*this,out);
	return out;
}
otri osub::stpivot_py()const{
	otri out;
	stpivot(*this,out);
	return out;
}

wertex* osub::sorg_py()const
{
	wertex* vert;
	sorg(*this,vert);
	return vert;
}

wertex* osub::sdest_py()const
{
	wertex* vert;
	sdest(*this,vert);
	return vert;
}

void osub::SetNeighborSegs()
{
	/* populate segnext and segprev, if any (otherwise, set them to dummysegs).

		Routine Called e.g. when inserting / deleting a segment.
		segnext is the next segment found when rotating around the seg origin counter-clockwise
		segprev is the next segment found when rotating around the seg origin clockwise
	*/

	struct otri temptri;
	struct osub tempsub;

	segprev( *this ) = ss->mesh.dummysub;
	segnext( *this ) = ss->mesh.dummysub;

	// Do segprev, rotate clockwise

	stpivot(*this, temptri);
	
	while( temptri.tri != ss->mesh.dummytri )
	{
#ifdef SELF_CHECK
		if(svorg(*this) != temptri.dest())
			throw TriangleExc_Sub_Tri("Bad otri-osub orientation", *this, temptri);
#endif
		tempsub = temptri.org_s(); // subseg on the side opposite org, oriented from apex to dest

		if( tempsub.ss == ss )
			break; // we cycled back to this

		if( tempsub.ss != ss->mesh.dummysub )
		{
			ssymself( tempsub ); // we want tempsub to have the same origin as newsubseg

#ifdef SELF_CHECK
			assert( svorg(tempsub) == svorg(*this) );
#endif
			segprev( *this ) = tempsub;
			segnext( tempsub ) = *this;
			break;
		}
		dprevself(temptri);

	}

	// Do segnext, rotate counter-clockwise

	ssym(*this, tempsub);
	stpivot(tempsub, temptri);
	
	while( temptri.tri != ss->mesh.dummytri ){

		tempsub = temptri.dest_s();//subseg on the side opposite dest, oriented from org to apex

		if( tempsub.ss == ss )
			break; // we cycled back to this
		
		if( tempsub.ss != ss->mesh.dummysub )
		{

#ifdef SELF_CHECK
			assert( svorg(tempsub) == svorg(*this) );
#endif

			segnext( *this ) = tempsub;
			segprev( tempsub ) = *this;
			break;
		}
		onextself(temptri);

	}

}
