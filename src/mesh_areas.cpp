#include "mesh.h"
#include "triutils.h"
#include "triexception.h"
#include <set>
#include <list>
//#include <unordered_set>

bool compare_tri_on_index(const otri& first, const otri& second)
{
	if( first.tri->index != second.tri->index )
		return first.tri->index < second.tri->index;
	else
		return first.orient < second.orient;
}

std::ostream& operator<<(std::ostream& out, const ConvexArea::triplet& arg)
{
	out << arg.tri << " " << arg.oseg << " " << arg.neighbor;
    return out;
}

std::ostream& operator<<(std::ostream& out, const Passage& arg)
{ 
	out << "Passage[" << arg.coords[0] << ", " << arg.coords[1] << " " << arg.sign;
	return out; 
}

template<typename V>
bool comparePathStepPtrs(V lhs, V rhs)
{
	return lhs->distance < rhs->distance;
}

bool ConvexArea::contains( const Point2d& point, bool segOnly )const
{
	size_t maxIndex = boundary.size()-1;
	
	for (uint i = 0; i <= maxIndex; i++)
	{
		if( segOnly && boundary[i].oseg.isDummy() )
			continue;
		
		const otri& temptri = boundary[i].tri;
		// boundary is oriented clock-wise
		if( !isdirect(temptri.org()->coords,temptri.dest()->coords,point) )
		{
			return false;
		}
		
	}
	return true;
}

osub Perimeter::get_oseg()
{
	if( oseg.ss == nullptr )
		throw TriangleExc_Perimeter("oseg is NULL",this);
	return oseg;
}

bool Perimeter::contains( const Point2d& point, bool segOnly )const
{
	PerimeterPtr side = this->next;
	
	do
	{
		if(!segOnly || !side->neighbor || side->prev->neighbor || side->next->neighbor)
		{
			// boundary is oriented clock-wise
			if( !isdirect(side->coords,side->next->coords,point) )
			{
				return false;
			}
		}

		side = side->next;

	}while( side != this->next );

	return true;
}

bool Perimeter::isConvex()const
{
	PerimeterPtr side = this->next;
	
	do
	{
		REAL orient = prodvect(side->coords,side->next->coords,side->next->next->coords);
		REAL angle = orient / sqrt( dist2(side->coords,side->next->coords) ) / sqrt( dist2(side->next->coords,side->next->next->coords) );
		if( angle < -REAL_EPSILON ) // numerical tolerance when two adjacent sides are almost collinear
		{
			return false;
		}
		
		side = side->next;

	}while( side != this->next );

	return true;
}

PerimeterPtr Perimeter::findIntersect(Point2d start, Point2d end)const
{
	PerimeterPtr side = this->next;
	
	do
	{
		if (!isdirect_strict(start, end, side->coords) &&
			isdirect(start, end, side->next->coords))
		{
			return side;
		}
		side = side->next;

	} while (side != this->next);
	
	throw TriangleExc_Perimeter("start point appears not to be in convexArea", this);
}

PerimeterPtr ConvexArea::toPerimeter( std::vector<std::pair<int,PerimeterPtr>>& perimMap ) const
{
	PerimeterPtr startside;
	PerimeterPtr side; 
	PerimeterPtr prevside;

	wertex * triorg;

	std::vector<ConvexArea::triplet>::const_iterator it = boundary.begin();

	for( ; it!=boundary.end(); it++, prevside = side )
	{
		otri temptri = it->tri;
		triorg = temptri.org();

		side = PerimeterPtr( new Perimeter(this->index, triorg->coords, triorg) );
		
		if( startside == nullptr )
		{	
			startside = side;
		}
		else
		{
			connectPerimSides( prevside, side );
		}

		if( it->oseg.isDummy() )
		{	// No segment blocking the way along that side.
			symself(temptri);
			if( temptri.tri != temptri.tri->mesh.dummytri )
			{	// There is a passage to an adjacent perimeter.

				// That neightbor perimeter might not have been created yet
				// so we just map it to the convex area index for later stitching.
				perimMap.emplace_back(it->neighbor->index,side);
			}
		}
		else
		{
			side->oseg = it->oseg;
		}

	}

	// Close the loop
	connectPerimSides(prevside, startside);

	return startside;
}

void Perimeter::setNext(const PerimeterPtr& perim)
{
	next = perim;
	univec[0] = next->coords[0]-coords[0];
	univec[1] = next->coords[1]-coords[1];

	width = norm_v(univec);
	
	univec[0] /= width;
	univec[1] /= width;
}



void Perimeter::setNextPrevPort()
{
	// Setup prevPort / nextPort links for all sides of the perimeter.
	
	PerimeterPtr aPort = this->next;

	bool foundPort = false;
	do 
	{
		if( aPort->neighbor )
		{
			foundPort = true;
			break;
		}

		aPort = aPort->next;

	}while(aPort != this->next);

	if( foundPort )
	{
		const PerimeterPtr startSide = aPort;
		PerimeterPtr side;
		for( side = startSide->next; side != startSide; side=side->next )
		{
			side->prevPort = aPort;
			if( side->neighbor )
			{
				aPort = side;
			}
		}
		startSide->prevPort = aPort;

		aPort = startSide;
		for( side = startSide->prev; side != startSide; side=side->prev )
		{
			side->nextPort = aPort;
			if( side->neighbor )
			{
				aPort = side;
			}
		}
		startSide->nextPort = aPort;
	}

}

void Perimeter::validate(bool /*recursive*/)
{
	// assert this->next->index == this.index
	// assert this->prev->index == this.index

	if( width == 0. )
		throw TriangleExc_Perimeter("width not initialized.", this);

	if( dist2_v(univec) == 0. )
		throw TriangleExc_Perimeter("univec not initialized.", this);

	PerimeterPtr side = this->next;
	do
	{
		if( side != side->next->prev )
			throw TriangleExc_Perimeter("Bad Perimeter link", this);

		//if( !side->neighbor && side->oseg.ss == nullptr)
		//	throw TriangleExc_Perimeter("No port, but no oseg either", this);

	}while( side != this->next );

}

std::ostream& operator<<(std::ostream& out, const Perimeter& arg)
{
	out << "[" << arg.index << "] " << arg.coords[0] << "," << arg.coords[1] << " > ";
	if( arg.next )
		out << arg.next->coords[0] << "," << arg.next->coords[1];
	else
		out << "NULL";

	if( arg.neighbor )
		out << " [>" << arg.neighbor->index;

    return out;
}

std::ostream& operator<<(std::ostream& out, const PerimeterPtr& arg)
{
	if (arg)
		out << *arg;
	else
		out << "NULL";

	return out;
}

PerimeterPtr Mesh::locateConvexArea(const Point2d& pos, otri& searchtri)
{
	if( !searchDual )
		throw TriangleExc("missing searchDual");

	enum locateresult result = searchDual->preciselocate(pos,&searchtri,0);
	if(result == OUTSIDE)
		return PerimeterPtr();

	return searchDual->triToConvexArea[searchtri.tri->index];
}

std::vector<Passage> Mesh::pathFind_py(	
							const Point2d& start, 
							const Point2d& end, 
							enum PathFindAlgo algo,
							const REAL& distMax,
							const REAL& widthMax,
							bool rectify)
{
	REAL distance;
	REAL searchRatio;
	return pathFindTri(start,end,distance,searchRatio,otri(),algo,distMax,widthMax,rectify);
}

std::vector<Passage> Mesh::pathFind(const Point2d& start, // Path starting point
						const Point2d& end, // Path objective
						enum PathFindAlgo algo,
						REAL& distance,
						REAL& searchRatio,
						const REAL& distMax,
						const REAL& widthMax,
						bool rectify)
{
	// Approach based on ConvexAreas is flawed as the tiles are not disjoint
	// which can cause reentry problems. Use pathFindTri instead.

	distance = 0;
	std::vector<Passage> path;
	otri searchtri;
	PerimeterPtr convArea = locateConvexArea(start,searchtri);

	searchRatio = 0;

	if(!convArea)
		return path; // startpoint is outside the mesh - no path

	PerimeterPtr endPointConvArea = locateConvexArea(end,searchtri);
	if( !endPointConvArea )
		return path; // endpoint is outside the mesh - no path

	if (endPointConvArea->index == convArea->index)
	{
		path.emplace_back(end);
		return path;
	}

	if( !isconnexDual( convArea, endPointConvArea ) )
		return path; // points are in different connex areas - no path
	
	std::list<PathStepPtr> pathHeads;   // list of open paths potentially leading to goal in the form of their latest step.
	
	bool endPointReachable = true;
	
	std::set<int> visitedAreas;// keep track of visited areas. TODO: replace with std::unordered_set when nice autoexp.dat support for it.

	const REAL minDist = start.dist(end);

	PathStepPtr curStep(new PathStep(start, convArea, PerimeterPtr(), minDist, 0, minDist));
	PathStepPtr closestStep = curStep;

	while (!curStep->toArea->contains(end))
	{
		Point2d const* prevpoint = &curStep->point;
		
		convArea = curStep->toArea;
	
		if (!hasElem(visitedAreas,convArea->index))
		{	// for the first visit to convex area, try to get right accross in direction of the end point.
			visitedAreas.insert(convArea->index);
			convArea = convArea->findIntersect(*prevpoint, end);
		}

		bool turn_ccw = !isdirect(*prevpoint, end, convArea->coords);

		if (!convArea->neighbor)
			convArea = turn_ccw ? convArea->nextPort : convArea->prevPort;

		PerimeterPtr areaSide = convArea;

		bool foundBetterChildStep = false;

		if (areaSide)
		{
			do // Iter over perimeter of the area, looking for ways to go into neighboring areas
			{
				PerimeterPtr nextArea = areaSide->neighbor;
				if (!nextArea)
					throw TriangleExc_Perimeter("there should be a passage to nextArea on that perim", &*areaSide);

				if (!hasElem(visitedAreas, nextArea->index))
				{	// Crossing into another area is possible.
					// and that area hasn't been visited yet.

					// If we are coming from another area, only check those ports of next area
					// that are beyond the port we come from (that can arise as convexAreas do not overlap).
					if (!curStep->fromArea || !isdirect_strict(curStep->fromArea->coords, curStep->fromArea->next->coords, areaSide->coords)
										   || !isdirect_strict(curStep->fromArea->coords, curStep->fromArea->next->coords, areaSide->next->coords))
					{
						if (areaSide->width >= widthMax)
						{	// Passage is wide enough

							const Point2d& pointRight = areaSide->coords;
							const Point2d& pointLeft  = areaSide->next->coords;
							Point2d point;

							if (isdirect_strict(pointLeft, pointRight, end))
							{//target point in on the other side of passage into next convex area.
								if (isdirect(*prevpoint, end, pointRight))
								{
									point = pointRight;
								}
								else if (!isdirect_strict(*prevpoint, end, pointLeft))
								{
									point = pointLeft;
								}
								else
								{
									intersection(*prevpoint, end, pointLeft, pointRight, point);
								}
							}
							else
							{ // Target point is on the same side of passage into next convex area as prevpoint.
								if (prodscal(pointLeft, pointRight, end) >
									prodscal(pointLeft, pointRight, *prevpoint))
								{
									point = pointRight;
								}
								else
								{
									point = pointLeft;
								}
							}

							const REAL remainingDist = norm(point, end); // shortest remaining distance to end
							const REAL stepDist = prevpoint->dist(point);

							REAL distSoFar = stepDist + curStep->distSoFar;

							if (!distMax || (distSoFar) < distMax)
							{
								REAL distForCompare;
								if (algo == Mesh::MinDist)
									distForCompare = distSoFar + remainingDist;
								else
								{
									distForCompare = remainingDist;
									assert(algo == Mesh::MinDistToEnd);
								}
								PathStepPtr newStep(new PathStep(point, nextArea, areaSide, distForCompare, distSoFar, remainingDist, curStep));
								
								if (newStep->distToEnd < closestStep->distToEnd)
									closestStep = newStep;
								
								REAL stepTolerance = 1.01;
								if (remainingDist + stepDist < stepTolerance*curStep->distToEnd)
								{	// it's good enough that min possible distance worsens by stepTolerance
									// with the new step that we want to continue going that way.

									// we might not be done iterating over curStep.
									// but we want to jump right away over to a more promising path as distance 
									// to end estimate has been reduced
									foundBetterChildStep = true;
									pathHeads.push_back(curStep);
									
									curStep = newStep;
									break;
								}
								else
								{
									pathHeads.push_back(newStep);
								}
							}
						}
					}
				}

				if (turn_ccw)
					areaSide = areaSide->nextPort;
				else
					areaSide = areaSide->prevPort;

			} while (areaSide != convArea);
		}

		if (foundBetterChildStep)
		{
			continue;
		}

		// Determine which path gets closest to endpoint after the step update.

		// Sort paths by min distance to end.
		pathHeads.sort(&comparePathStepPtrs<PathStepPtr>);

		if (pathHeads.empty())
		{ // no path to reach endpoint
			endPointReachable = false;
			break;
		}
		// set curStep to be the path with smallest distance.
		curStep = pathHeads.front();
		// remove curStep from current open paths
		// and add it to dead ends list in case we need to go back to it
		// after failing to find a path to goal.
		pathHeads.pop_front();

	}

	if (endPointReachable) // add end point if it's reachable.
		path.emplace_back(end);
	else
	{	// there is no path connecting startPoint and endPoint.
		// Find the path that gets us closest to endPoint.
		curStep = closestStep;
	}

	REAL numSteps = 0;
	for (PathStepPtr pathStep = curStep; pathStep != nullptr; pathStep = pathStep->prevStep, numSteps++);

	searchRatio = ( numSteps / (REAL)(visitedAreas.size()+1) );
	//printf("searchRatio %.2f\n", searchRatio);

	if (rectify && curStep->fromArea)
	{
		// Now that we have found a path,
		// rectify path to produce a straight, natural trajectory

		Point2d const* refpoint = &end;
		std::vector<std::vector<PerimeterPtr>> posts(2);
		posts[0] = { curStep->fromArea }; // potential trajectory 'hinges'
		posts[1] = { curStep->fromArea }; // potential trajectory 'hinges'

		for (PathStepPtr pathStep = curStep; pathStep->fromArea != nullptr; pathStep = pathStep->prevStep)
		{	// NB: path points go backwards from endpoint towards startpoint
			// but do not include startpoint

			if (!pathStep->prevStep->fromArea)
			{	// No more convarea tile to cross : we got to start point.

				for (uint isRight = 0; isRight <= 1; isRight++) // loop over left and right side
				{
					bool foundOne = false;
					for (auto post : posts[isRight])
					{
						const Point2d& checkPoint = post->getCoords(isRight==1);
						if (isdirect(*refpoint, checkPoint, start) ^ isRight)
						{
							foundOne = true;
							if( checkPoint != *refpoint )
							{
								path.emplace_back(checkPoint, isRight? -1 : 1, post->vert);
								refpoint = &path.back().coords;
							}
						}
						else
							break;
					}
					if (foundOne)
						break;
				}
			}
			else
			{
				PerimeterPtr newPost[] = { pathStep->prevStep->fromArea, pathStep->prevStep->fromArea };

				for (uint isRight = 0; isRight <= 1; isRight++) // loop over left and right side
				{
					auto it = posts[isRight].begin();
					for (; it < posts[isRight].end(); it++)
					{	// next area passage is causing the path to curve on the inside - process all the accumulated rails
						// Add all trajectory 'hinges' that are in the way of getting straight to the previous area.

						const Point2d& checkPoint = newPost[!isRight]->getCoords(isRight==0);
						const Point2d& checkPoint2 = (*it)->getCoords(isRight==0);
						if (checkPoint != *refpoint && (isdirect(*refpoint, checkPoint2, checkPoint) ^ isRight))
						{
							if (checkPoint2 != *refpoint)
							{
								path.emplace_back(checkPoint2, isRight ? -1 : 1, (*it)->vert);
								refpoint = &checkPoint2;
							}
						}
						else
							break;
					}

					if (it != posts[isRight].begin())
					{
						posts[isRight].erase(posts[isRight].begin(), it);
						posts[isRight].push_back(newPost[isRight]);
						posts[!isRight] = { newPost[!isRight] };
						break;
					}

					if (isRight) // gets here if it didn't break above on the left or right side checks
					{	// Accumulate potential hinges which define an increasing curvature
						for (uint is_right = 0; is_right <= 1; is_right++) // loop over left and right side
						{
							if (posts[is_right].back()->getCoords(is_right==1) != newPost[is_right]->getCoords(is_right==1))
							{
								Point2d const* prevPoint = refpoint;

								for (auto iter = posts[is_right].begin(); iter < posts[is_right].end(); ++iter)
								{
									Point2d const* checkPoint = &(*iter)->getCoords(is_right==1);
									if (!isdirect(*prevPoint, *checkPoint, newPost[is_right]->getCoords(is_right==1)) ^ (is_right==1))
									{	// remove hinges if we found a concavity in the accumulated convex boundary
										posts[is_right].erase(iter, posts[is_right].end());
										break;
									}
									prevPoint = checkPoint;
								}
								posts[is_right].push_back(newPost[is_right]);
							}
						}
					}
				}
			}
		}
	}

	return path;
}


REAL getWayPoint( otri steptri, 
				  const Point2d& prevpoint, 
				  const Point2d& end, 
				  Point2d& point, 
				  double widthMax)
{
	const Point2d& pointRight = steptri.dest()->coords;
	const Point2d& pointLeft = steptri.org()->coords;
	const Point2d orgDest = pointLeft - pointRight;
	REAL gauge = 0;

	/*compute narrowest width between a segment on a side and the opposite vertex*/
	if (!steptri.org_s().ss->isDummy())
	{
		if (steptri.dest_s().ss->isDummy())
		{
			gauge = fabs(steptri.org_s().ss->unitVec ^ orgDest);
		}
		else
		{	// both other sides are segments : dead end.
			if (steptri.contains(end))
			{ // we got to the triangle containing end point
				gauge = pointRight.dist(pointLeft); 
				if (widthMax != 0 && gauge < widthMax)
					return -1;
			}
			else
				return 0; // no way through beyond that tri.
		}
	}
	else if (!steptri.dest_s().ss->isDummy())
	{
		gauge = fabs(steptri.dest_s().ss->unitVec ^ orgDest);
	}
	else
	{	// no segment on any side - dist between org and dest is the limiting factor
		gauge = pointRight.dist(pointLeft);
	}

	if (widthMax == 0 || gauge >= widthMax)
	{	// Passage is wide enough

		if (isdirect_strict(pointLeft, pointRight, end))
		{//target point in on the other side of passage into next convex area.
			if (isdirect(prevpoint, end, pointRight))
			{
				point = pointRight;
			}
			else if (!isdirect_strict(prevpoint, end, pointLeft))
			{
				point = pointLeft;
			}
			else
			{
				intersection(prevpoint, end, pointLeft, pointRight, point);
			}
		}
		else
		{ // Target point is on the same side of passage into next convex area as prevpoint.
			if (prodscal(pointLeft, pointRight, end) >
				prodscal(pointLeft, pointRight, prevpoint))
			{
				point = pointRight;
			}
			else
			{
				point = pointLeft;
			}
		}
		return gauge;
	}
	// no way through
	return 0;
}

std::vector<Passage> Mesh::pathFindTri(const Point2d& start, // Path starting point
										const Point2d& end, // Path objective
										REAL& distance,
										REAL& searchRatio,
										otri searchtri,
										enum PathFindAlgo algo,
										const REAL& distMax,
										const REAL& widthMax,
										bool rectify)
{
	distance = 0;
	searchRatio = 0;
	
	std::vector<Passage> path;
	
	PerimeterPtr convArea = locateConvexArea(start, searchtri);

	if (!convArea)
		return path; // startpoint is outside the mesh - no path
	
	otri end_tri;
	PerimeterPtr endPointConvArea = locateConvexArea(end, end_tri);
	if (!endPointConvArea)
		return path; // endpoint is outside the mesh - no path

	if (endPointConvArea->index == convArea->index)
	{
		path.emplace_back(end);
		return path;
	}

	if (!isconnexDual(convArea, endPointConvArea))
		return path; // points are in different connex areas - no path

	std::list<PathStepTriPtr> pathHeads;   // list of open paths potentially leading to goal in the form of their latest step.

	bool endPointReachable = true;

	std::set<triangle const*> visitedAreas{searchtri.tri};// keep track of visited areas. TODO: replace with std::unordered_set when nice autoexp.dat support for it.
	uint seentris = 1;

	otri starttri = searchtri;
	starttri.orientTri(start, end, true, false);

	for (uint i = 0; i <= 2; i++, starttri.lnextself_())
	{	// look at the three adjacent triangles to starttri to setup  initial path heads.
		osub seg = starttri.apex_s();
		if (seg.ss == seg.ss->mesh.dummysub)
		{
			searchtri = starttri.sym_();
			if (searchtri.tri != searchDual->dummytri)
			{
				Point2d wayPoint;
				const REAL gauge = getWayPoint(searchtri, start, end, wayPoint, widthMax);
				if (gauge < 0)
				{
					pathHeads.clear(); // no path to end
					break;
				}
				if (gauge != 0)
				{ 
					REAL startToPoint = start.dist(wayPoint);
					REAL pointToEnd = wayPoint.dist(end);
					visitedAreas.insert(searchtri.tri);
					pathHeads.emplace_back(new PathStepTri(wayPoint, searchtri, startToPoint + pointToEnd, startToPoint, pointToEnd));
					pathHeads.back()->gauge = gauge;
				}
			}
		}
	}
	
	if (pathHeads.empty())
		return path;
	
	pathHeads.sort(&comparePathStepPtrs<PathStepTriPtr>);

	PathStepTriPtr curStep = pathHeads.front();
	pathHeads.pop_front();
	PathStepTriPtr closestStep = curStep;

	while (!curStep->toArea.contains(end))
	{		
		searchtri = curStep->toArea;
		seentris += 1;
		Point2d const* prevpoint = &curStep->point;
		vertex oppo = curStep->toArea.apex();

		bool goRight = isdirect(*prevpoint, end, oppo->coords);
		bool foundBetterChildStep = false;

		for (uint seenSides = 0; seenSides <= 1; seenSides++, goRight = !goRight)
		{
			osub seg = goRight ? searchtri.org_s() : searchtri.dest_s();
			
			if (seg.ss == seg.ss->mesh.dummysub)
			{
				otri proxtri = searchtri;
				if (goRight)
				{
					dprevself(proxtri);
				}
				else
				{
					onextself(proxtri);
				}

				if (!hasElem(visitedAreas,proxtri.tri))
				{	// for the first visit to convex area, try to get right accross in direction of the end point.

					Point2d point;
					const REAL gauge = getWayPoint(proxtri, *prevpoint, end, point, widthMax);
					if (gauge < 0)
					{
						pathHeads.clear();
						break;
					}
					if (gauge != 0)
					{
						// Passage is wide enough

						const REAL remainingDist = norm(point, end); // shortest remaining distance to end
						const REAL stepDist = prevpoint->dist(point);

						REAL distSoFar = stepDist + curStep->distSoFar;

						if (!distMax || (distSoFar) < distMax)
						{
							REAL distForCompare;
							if (algo == Mesh::MinDist)
								distForCompare = distSoFar + remainingDist;
							else
							{
								distForCompare = remainingDist;
								assert(algo == Mesh::MinDistToEnd);
							}

							visitedAreas.insert(proxtri.tri);
							PathStepTriPtr newStep(new PathStepTri(point, proxtri, distForCompare, distSoFar, remainingDist, curStep));
							
							newStep->gauge = gauge;

							if (newStep->distToEnd < closestStep->distToEnd)
								closestStep = newStep;

							REAL stepTolerance = 1.01;
							if (remainingDist + stepDist < stepTolerance*curStep->distToEnd)
							{	// it's good enough that min possible distance worsens by stepTolerance
								// with the new step that we want to continue going that way.

								// we might not be done iterating over curStep.
								// but we want to jump right away over to a more promising path as distance 
								// to end estimate has been reduced
								foundBetterChildStep = true;
								if (seenSides == 0)
									pathHeads.push_back(curStep);

								curStep = newStep;
								break;
							}
							else
							{
								pathHeads.push_back(newStep);
							}
						}
					}
				}

			}
		}

		
		if (foundBetterChildStep)
		{
			continue;
		}

		// Determine which path gets closest to endpoint after the step update.

		// Sort paths by min distance to end.
		pathHeads.sort(&comparePathStepPtrs<PathStepTriPtr>);

		if (pathHeads.empty())
		{ // no path to reach endpoint
			endPointReachable = false;
			break;
		}
		// set curStep to be the path with smallest distance.
		curStep = pathHeads.front();
		// remove curStep from current open paths
		// and add it to dead ends list in case we need to go back to it
		// after failing to find a path to goal.
		pathHeads.pop_front();

	}

	if (endPointReachable) // add end point if it's reachable.
		path.emplace_back(end);
	else
	{	// there is no path connecting startPoint and endPoint.
		// Find the path that gets us closest to endPoint.
		curStep = closestStep;
	}
	/* DEBUG info to assess how many triangles were tried for a given path.
	REAL numSteps = 0;
	for (PathStepTriPtr pathStep = curStep; pathStep != nullptr; pathStep = pathStep->prevStep, numSteps++);

	searchRatio = numSteps / seentris;
	printf("searchRatio %.2f\n", searchRatio);
	*/
	if (rectify)
	{
		// Now that we have found a path,
		// rectify path to produce a straight, natural trajectory

		Point2d const* refpoint = &end;
		std::vector<std::vector<vertex>> posts(2);
		posts[0] = { curStep->toArea.getVert(1) }; // potential trajectory 'hinges'
		posts[1] = { curStep->toArea.getVert(0) }; // potential trajectory 'hinges'

		for (auto const& pathStep: *curStep)
		{	// NB: path points go backwards from endpoint towards startpoint
			// but do not include startpoint

			if (!pathStep.prevStep)
			{	// No more convarea tile to cross : we got to start point.

				for (uint isRight = 0; isRight <= 1; isRight++) // loop over left and right side
				{
					bool foundOne = false;
					for (auto post : posts[isRight])
					{
						const Point2d& checkPoint = post->coords;
						if (isdirect(*refpoint, checkPoint, start) ^ isRight)
						{
							foundOne = true;
							if (&checkPoint != refpoint)
							{
								path.emplace_back(checkPoint, pathStep.gauge  * (isRight ? -1: 1), post);
								refpoint = &checkPoint;
							}
						}
						else
							break;
					}
					if (foundOne)
						break;
				}
			}
			else
			{
				vertex newPosts[] = {pathStep.prevStep->toArea.getVert(1),
								     pathStep.prevStep->toArea.getVert(0)};

				for (uint isRight = 0; isRight <= 1; isRight++) // loop over left and right side
				{
					auto it = posts[isRight].begin();
					for (; it < posts[isRight].end(); it++)
					{	// next area passage is causing the path to curve on the inside - process all the accumulated rails
						// Add all trajectory 'hinges' that are in the way of getting straight to the previous area.

						Point2d const& checkPoint = newPosts[!isRight]->coords;

						if (&checkPoint == refpoint)
							break;

						Point2d const& refPointCandidate = (*it)->coords;
						const int sign = (isRight ? -1 : 1);

						if (widthMax && pathStep.gauge)
						{
							//auto pathNml = (refPointCandidate - checkPoint).uni_nml() * sign * pathStep->gauge / 2;
							//refPointCandidate += pathNml;
						}

						if (isdirect(*refpoint, refPointCandidate, checkPoint) ^ isRight)
						{
							if (&refPointCandidate != refpoint)
							{
								path.emplace_back(refPointCandidate, pathStep.gauge * sign, *it);
								refpoint = &refPointCandidate;
							}
						}
						else
							break;
					}

					if (it != posts[isRight].begin())
					{
						posts[isRight].erase(posts[isRight].begin(), it);
						posts[isRight].push_back(newPosts[isRight]);
						posts[!isRight] = { newPosts[!isRight] };
						break;
					}

					if (isRight) // gets here if it didn't break above on the left or right side checks
					{	// Accumulate potential hinges which define an increasing curvature
						for (uint is_right = 0; is_right <= 1; is_right++) // loop over left and right side
						{
							auto const& newHinge = newPosts[is_right];

							if (posts[is_right].back() != newHinge)
							{
								Point2d const* prevPoint = refpoint;

								for (auto iter = posts[is_right].begin(); iter < posts[is_right].end(); ++iter)
								{
									Point2d const* checkPoint = &(*iter)->coords;
									if (!isdirect(*prevPoint, *checkPoint, newHinge->coords) ^ is_right)
									{	// remove hinges if we found a concavity in the accumulated convex boundary
										posts[is_right].erase(iter, posts[is_right].end());
										break;
									}
									prevPoint = checkPoint;
								}
								posts[is_right].push_back(newPosts[is_right]);
							}
						}
					}
				}
			}
		}
	}
	
	return path;
}

void Mesh::resetConvexAreas()
{
	convexAreasByPerim.clear();	
	triToConvexArea.clear();
	triToConvexArea.resize(triangles.size());

	std::vector<ConvexAreaPtr> triToConvexA(triangles.size());
	std::vector<ConvexAreaPtr> tempConvexAreas;

	// if MinCoverage is false, grow a convex area from every triangle.
	// Results in more convexAreas but also more memory / processing
	bool MinCoverage = true; 

	// Map each triangle to the largest convexArea
	// created by adding neighboring triangles (and extending some sides).
	for( size_t i=0; i<triangles.size(); i++ )
	{
		if(!MinCoverage || !triToConvexA[i])
			triToConvexA[i] = convexAreaForTri(triangles[i], triToConvexA, tempConvexAreas);
	}

	// Connect each convex area to neighboring convex areas
	for(auto& convAreaIt : tempConvexAreas)
	{
		for(auto& portIt : (convAreaIt)->boundary)
		{
			if( portIt.oseg.ss == this->dummysub )
			{
				otri temptri = portIt.tri;
				symself(temptri);

				if( temptri.tri == this->dummytri )
				{
					// opening on the outside of the mesh.
				}else
				{
					// temptri is now such that org->dest is along the boundary but apex is outside of convexAreaIt.
					portIt.neighbor = triToConvexA[temptri.tri->index];
				}
			}
		}
	}

	std::vector<std::pair<int,PerimeterPtr>> perimToConvexAreaIndex;
	std::vector<Perimeter*> triToPerimeter(triangles.size());

	std::vector<PerimeterPtr> convAreaToPerimeter(tempConvexAreas.size());

	// Convert ConvexArea to Perimeters
	for(size_t i=0; i<tempConvexAreas.size(); i++)
	{
		convAreaToPerimeter[i] = tempConvexAreas[i]->toPerimeter(perimToConvexAreaIndex);
		convexAreasByPerim.push_back(convAreaToPerimeter[i]);
	}

	// Connect convex area to neighboring convex areas
	for(size_t i=0; i<perimToConvexAreaIndex.size(); i++)
	{
		perimToConvexAreaIndex[i].second->neighbor = convAreaToPerimeter[perimToConvexAreaIndex[i].first];
	}

	// Map each triangle to a Perimeter 
	for( size_t i=0; i<triangles.size(); i++ )
	{
		triToConvexArea[i] = convAreaToPerimeter[triToConvexA[i]->index];
	}
	
	// Inflate perimeter to maximize convex area.
	
	for(size_t i=0; i<tempConvexAreas.size(); i++)
	{
		ConvexAreaPtr convexArea = tempConvexAreas[i];
		PerimeterPtr start = convAreaToPerimeter[i];

		PerimeterPtr curSide = start;
		for(size_t j=0; j<convexArea->boundary.size(); j++, curSide=curSide->next)
		{

#ifdef SELF_CHECK
			if( !curSide->isConvex() )
				throw TriangleExc("Perimeter is not convex");
#endif

			if( curSide->neighbor )
			{
				otri outsidetri = convexArea->boundary[j].tri;
				symself(outsidetri);
				
				// outside tri is oriented such that 
				// org is curSide->next->coords and dest is curSide->coords.
				assert( curSide->coords[0] == outsidetri.dest()->coords[0] );
				assert( curSide->coords[1] == outsidetri.dest()->coords[1] );
				assert( curSide->next->coords[0] == outsidetri.org()->coords[0] );
				assert( curSide->next->coords[1] == outsidetri.org()->coords[1] );

				if( outsidetri.tri == this->dummytri )
					continue;

				const Point2d& triapexcoords = outsidetri.apex()->coords;
				
				// 
				const REAL prevSideLength	= sqrt( dist2(curSide->prev->coords,curSide->coords) );
				const REAL curSideLength	= sqrt( dist2(curSide->next->coords,curSide->coords) );
				const REAL nextSideLength	= sqrt( dist2(curSide->next->coords,curSide->next->next->coords) );
				
				REAL angleprev = prodvect(curSide->prev->coords,curSide->coords,curSide->next->coords) / curSideLength / prevSideLength;
				REAL anglenext = prodvect(curSide->coords,curSide->next->coords,curSide->next->next->coords) / curSideLength / nextSideLength;

				if( fabs(angleprev) < REAL_EPSILON || fabs(anglenext) < REAL_EPSILON )
					continue; // if sides are already flat enough, no need to extend.

				bool extendRight = prodvect(curSide->prev->coords,curSide->coords,triapexcoords) < 0;
				bool extendLeft = prodvect(curSide->next->next->coords,curSide->next->coords,triapexcoords) > 0;

				Point2d intersecPoint;
				PerimeterPtr nextAreaRight;
				PerimeterPtr nextAreaLeft;
				osub osegLeft;
				osub osegRight;
				nextAreaRight = nextAreaLeft = triToConvexArea[outsidetri.tri->index];
				//bool isBound = true;

				if( extendRight && extendLeft )
				{
					continue;
					//isBound = false;
					//intersection(curSide->coords, curSide->prev->coords, curSide->next->coords, curSide->next->next->coords, intersecPoint);
				}
				else if( extendRight )
				{
					intersection(curSide->coords, curSide->prev->coords, curSide->next->coords, triapexcoords, intersecPoint);
					
					lprevself(outsidetri);
					osub tempsub;
					tspivot(outsidetri,tempsub);

					if( tempsub.ss != this->dummysub )
					{
						osegLeft = tempsub;
						nextAreaLeft = PerimeterPtr();
					}
					else
					{
						continue;
						/*
						isBound = false;
						otri furthertri = outsidetri;
						symself(furthertri);
						if( furthertri.tri == this->dummytri )
							nextAreaLeft = PerimeterPtr();
						else
							nextAreaLeft = triToConvexArea[furthertri.tri->index];
						*/
					}
				}
				else if( extendLeft )
				{
					intersection(curSide->coords, triapexcoords, curSide->next->coords, curSide->next->next->coords, intersecPoint);

					lnextself(outsidetri);
					osub tempsub;
					tspivot(outsidetri,tempsub);

					if( tempsub.ss != this->dummysub )
					{
						osegRight = tempsub;
						nextAreaRight = PerimeterPtr();
					}
					else
					{	
						continue;
						/*
						isBound = false;
						otri furthertri = outsidetri;
						symself(furthertri);
						if( furthertri.tri == this->dummytri )
							nextAreaRight = PerimeterPtr();
						else
							nextAreaRight = triToConvexArea[furthertri.tri->index];
						*/
					}
				}
				else
				{	
					continue;
				}

				REAL existingSideLength = norm(curSide->coords, curSide->next->coords);
				REAL newSideLength1		= norm(curSide->coords, intersecPoint);
				REAL newSideLength2		= norm(curSide->next->coords, intersecPoint);
				REAL sideTol = .1 * existingSideLength;

				if( newSideLength1 < sideTol || newSideLength2 < sideTol )
				{  // One of the new sides is so small it's not worth extending.
					continue;
				}

				PerimeterPtr newSide( new Perimeter(curSide->index,intersecPoint) );

				newSide->neighbor = nextAreaLeft;
				newSide->oseg = osegLeft;
				curSide->neighbor = nextAreaRight;
				curSide->oseg = osegRight;

				connectPerimSides(newSide,curSide->next);
				connectPerimSides(curSide,newSide);

				curSide->validate();
				newSide->validate();
				
				curSide = newSide;
				

				if( !curSide->isConvex() )
				{
					throw TriangleExc("Perimeter is no longer convex");
				}
			}

		}// loop over convex areas

		// setup shortcuts pointers to previous/next ports
		start->setNextPrevPort();
	}
	
	// Connect convex area to neighboring convex areas
	connectConvexAreas();

	if( true )
	{
		for( size_t i=0; i<convexAreasByPerim.size(); i++)
		{
			convexAreasByPerim[i]->validate();
		}
	}

}

ConvexAreaPtr Mesh::convexAreaForTri(triangle* const tri, std::vector<ConvexAreaPtr>& triToConvexA,
														  std::vector<ConvexAreaPtr>& convAreas)
{
	/*
		Grow convex area around tri by adding neighboring triangles,
		such that the resulting area is convex and the boundary is in ccw order
	*/
	osub tempsub;
	
	otri starttri(tri);
	std::list<otri> conv_area_edges; // the edges making up the convex area
	std::vector<triangle*> trisInArea;


	trisInArea.push_back(tri);
	// Intially, edges of the convex area are the 3 sides of starting triangle
	for( uint i = 0; i<3; i++ )
	{
		starttri.orient = i;
		conv_area_edges.push_back(starttri);
	}

	auto it = conv_area_edges.begin();

	while(it != conv_area_edges.end())
	{
		otri temptri = *it;
		tspivot(temptri,tempsub);

		symself(temptri);

		if( temptri.tri == this->dummytri || tempsub.ss != this->dummysub )
		{
			it++; // found a segment or an edge of the mesh
		}
		else
		{
			otri* prevEdge;
			if( it == conv_area_edges.begin() )
			{
				prevEdge = &conv_area_edges.back();
			}
			else
			{
				it--;
				prevEdge = &*it;
				it++;
			}
			
			it++;
			otri* nextEdge = (it == conv_area_edges.end()) ? &conv_area_edges.front() : &*it;
			it--;
			vertex newedgevert = temptri.apex();

			if( isdirect(newedgevert->coords, nextEdge->org()->coords, nextEdge->dest()->coords) && 
				isdirect(prevEdge->org()->coords, prevEdge->dest()->coords, newedgevert->coords) )
			{   // If new triangle preserves convexity,
				// enlarge convex area by replacing one edge by two others coming from adjacent triangles
				// effectively adding a triangle to the area.

				trisInArea.push_back(temptri.tri);

				it = conv_area_edges.erase(it);

				//NB: insert inserts before it
				lnextself(temptri);
				conv_area_edges.insert(it,temptri);
				lnextself(temptri);
				conv_area_edges.insert(it,temptri);
				
				// it now points to the edge after the one that was replaced, or the end.
				it--;
				it--;
				// it now points to the first edge that replaced the one that was deleted
				// as we want to recursively test those new edges for convexity
			}
			else
			{
				it++;
			}
		}
	}

	// Determine whether that area has been created already from a different starttri.

	// Loop through all triangles on the boundary and check their convex area if any.
	// Since a convex area can start at a different edge, we sort them to make them easy to identify.
	std::list<otri> edgesSorted;
	edgesSorted.assign(conv_area_edges.begin(), conv_area_edges.end() );
	edgesSorted.sort(compare_tri_on_index);

	const size_t num_edges = conv_area_edges.size();
	for(const auto& triIt : trisInArea)
	{
		ConvexAreaPtr other = triToConvexA[triIt->index];

		if( other == nullptr || other->boundary.size() != num_edges )
		{	// No ConvexArea setup for that triangle yet, or the number of edges is different.
			continue;
		}
		else
		{
			std::list<otri> otherBoundaryCopy;
			for(const auto& boundary_tri : other->boundary)
				otherBoundaryCopy.emplace_back(boundary_tri.tri);

			otherBoundaryCopy.sort(compare_tri_on_index);

			std::list<otri>::const_iterator sortedIt = edgesSorted.begin();
			std::list<otri>::const_iterator otherIt = otherBoundaryCopy.begin();
			bool foundSame = true;
			for( ; sortedIt != edgesSorted.end() && otherIt != otherBoundaryCopy.end(); ++sortedIt, ++otherIt )
			{
				if( *sortedIt != *otherIt )
				{
					foundSame = false;
					break;
				}
			}

			if( foundSame )
			{ // an existing convex area with the same boundary was found - reuse it.
				return other;
			}

		}
	}

	// A new ConvexArea is needed as none has been found with the same edges.
	ConvexAreaPtr newConvexArea( new ConvexArea( (int)convAreas.size() ) );
	convAreas.emplace_back( newConvexArea );

	for(auto edge_tri : conv_area_edges)
	{		
		tspivot(edge_tri,tempsub);

		newConvexArea->boundary.emplace_back(edge_tri,tempsub);
	}
	
	for(const auto& triIt : trisInArea)
	{
		ConvexAreaPtr& prevArea = triToConvexA[triIt->index];
		if( (prevArea == nullptr) || prevArea->boundary.size() < num_edges )
		{
			triToConvexA[triIt->index] = newConvexArea;
		}
	}

	return newConvexArea;
}

void ConvexArea::validate()
{
	for (const auto& it : boundary)
	{
		if (it.oseg.isDummy() && it.neighbor == nullptr)
		{  // if there is no segment, a neighbor is expected
			otri temptri = it.tri;
			symself( temptri );
			if( temptri.tri != temptri.tri->mesh.dummytri )
				throw TriangleExc("Missing neighbor convexArea information");
		}
	}
	
}

std::ostream& operator<<(std::ostream& out, const ConvexArea& arg)
{
	if( &arg == nullptr )
		out << "ConvexArea(NULL)";
	else
		out << "ConvexArea[" << arg.index <<"]";
    return out;
}

void Mesh::resetDual()
{
	if( searchDual != nullptr )
		deleteDual();

	this->searchDual = new Mesh(!b.noexact, false);

	std::vector<wertex*> boundaryWerts;
	boundaryVertices(boundaryWerts);
	std::vector<wertex*> wertDuals;
	std::vector<Point2d> dualCoords(boundaryWerts.size());

	// Identify all boundary verts
	size_t numDualVerts;
	for( numDualVerts = 0; numDualVerts < boundaryWerts.size(); numDualVerts++ )
	{
		dualCoords[numDualVerts] = boundaryWerts[numDualVerts]->coords;
	}

	// Create mesh from only boundary verts
	searchDual->addvertices(&dualCoords[0], numDualVerts, wertDuals);

	// Map each vertex to its search graph dual
	for( uint i = 0; i < boundaryWerts.size(); i++ )
	{
		wertDuals[i]->dualVert = boundaryWerts[i];
		boundaryWerts[i]->dualVert = wertDuals[i];
	}

	
	// Triangulation of the search graph
	searchDual->delaunay();

	// Add segments in the search graph
	vertex endpoint1, endpoint2;
	for(auto sbseg : subsegs)
	{
		osub tempsub(sbseg);
		endpoint1 = svorg(tempsub);
		endpoint2 = svdest(tempsub);
		// insertsegment can result in more than 1 segment being inserted.
		searchDual->insertsegment(endpoint1->dualVert, endpoint2->dualVert);

	}

	// Map each segments to its search graph dual
	for (auto sbseg : subsegs)
	{
		// Take an endpoint of the segment, go to its dual vertex 
		// then rotate around that vertex in the search graph
		// until we find the dual vertex of the other endpoint
		// which identifies the dual segment.

		//TODO

		osub tempsub(sbseg);
		endpoint1 = svorg(tempsub);
		endpoint2 = svdest(tempsub);

		//otri temptri = endpoint1->otri_;

		//tempsub.ss->dualSeg->dualVert = tempsub.ss;
	}

	this->searchDual->resetConvexAreas();

	searchDualDirty = false;
}

void Mesh::validateSearchGraph()
{
	if(searchDual != nullptr)
	{
		searchDual->checkmesh();

		for(const auto& itVert : vertices)
		{
			if( segmentend(itVert) )
			{
				if( itVert->dualVert != nullptr )
				{
					if( itVert->dualVert->dualVert != itVert )
						throw TriangleExc_Vert2("non-reciprocated searchDual mapping found",itVert,itVert->dualVert);	
				}
				else
				{
					throw TriangleExc_Vert("missing searchDual",itVert);	
				}
			}
			else if( itVert->dualVert != NULL)
			{
				throw TriangleExc_Vert2("searchDual found for non-segment vertex %d",itVert,itVert->dualVert);
			}
		}

		for(const auto& itSeg : subsegs)
		{
			if( itSeg->dualSeg != nullptr )
			{
				if( itSeg->dualSeg->dualSeg != itSeg )
					throw TriangleExc_Sub2("non-receciprocated searchDual mapping found",itSeg, itSeg->dualSeg);	
			}
			else
			{
				// TODO throw TriangleExc_Sub("missing searchDual",*itSeg);	
			}
			
		}

		for(auto& perimPtr : convexAreasByPerim)
		{
			if (perimPtr->index < 0)
				throw TriangleExc_Perimeter("Connex index not set", &*perimPtr);
		}
	
		if(connexAreaIndex.size() != connexAreas.size())
		{
			throw TriangleExc("different number of connexAreas and connexAreaIndex");
		}

		for(uint i=0; i<connexAreaIndex.size(); i++)
		{
			if( connexAreaIndex[i] == -1 )
			{
				char message[50];
				sprintf(message,"convexArea not mapped to a connexArea %d",i);
				throw TriangleExc(message);
			}
		}
	}
}


void Mesh::connectConvexAreas()
{
	connexAreaIndex.clear();
	connexAreaIndex.resize(convexAreasByPerim.size(),-1);
	connexAreas.clear();

	int latestIndex = -1;
	
	for (auto& perimPtr : convexAreasByPerim)
	{
		if (connexAreaIndex[perimPtr->index] < 0)
		{
			latestIndex++;
			int currentIndex = latestIndex;

			connexAreaIndex[perimPtr->index] = currentIndex;
			connexAreas.resize(latestIndex+1);
			
			connexAreas[latestIndex].push_back(perimPtr);

			std::vector<PerimeterPtr> convAreaQueue;
			convAreaQueue.push_back(perimPtr);

			do
			{
				PerimeterPtr perim = convAreaQueue.back();
				convAreaQueue.pop_back();
		
				PerimeterPtr convAreaIter = perim;

				for( convAreaIter = perim->next; convAreaIter!=perim; convAreaIter=convAreaIter->next)
				{
					PerimeterPtr nextArea = convAreaIter->neighbor;

					if( nextArea )
					{
						if( connexAreaIndex[nextArea->index] >= 0 )
						{ 
							if( isconnex(nextArea, perim) )
								continue; // already visited
							else
							{
								if( currentIndex != latestIndex )
								{
									throw TriangleExc_Perim2("bad convexArea connection",&*perim,&*nextArea);
								}
								else
								{	// It might happen that a connex component is not linked to by the other connex components:
									// ports are one way. In that case we don't want to create a new connex area, we just
									// add what we have found so far back to the existing one.
									currentIndex = connexAreaIndex[nextArea->index];
									connexAreas[currentIndex].insert(connexAreas[currentIndex].end(), connexAreas[latestIndex].begin(), connexAreas[latestIndex].end());
									for(const auto& perim_ptr: connexAreas[latestIndex])
									{
										connexAreas[currentIndex].push_back(perim_ptr);
										connexAreaIndex[perim_ptr->index] = currentIndex;
									}

									connexAreas.pop_back();
									latestIndex--;
									continue;
								}
							}								
						}

						connexAreaIndex[nextArea->index] = currentIndex;
						connexAreas[currentIndex].push_back(nextArea);
						convAreaQueue.push_back( nextArea );
					}
				}
			}while( !convAreaQueue.empty() );
		}
	}
}

std::vector<PerimeterPtr> Mesh::getConnexArea( PerimeterPtr perim )const
{
	return connexAreas[connexAreaIndex[perim->index]];
}
