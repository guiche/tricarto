#include "mesh.h"
#include "triexception.h"
#include "triutils.h"
#include <sstream>
#include <algorithm> //std::max

/* On some machines, my exact arithmetic routines might be defeated by the   */
/*   use of internal extended precision floating-point registers.  The best  */
/*   way to solve this problem is to set the floating-point registers to use */
/*   single or double precision internally.  On 80x86 processors, this may   */
/*   be accomplished by setting the CPU86 symbol for the Microsoft C         */
/*   compiler, or the LINUX symbol for the gcc compiler running on Linux.    */
/*                                                                           */
/* An inferior solution is to declare certain values as `volatile', thus     */
/*   forcing them to be stored to memory and rounded off.  Unfortunately,    */
/*   this solution might slow Triangle down quite a bit.  To use volatile    */
/*   values, write "#define INEXACT volatile" below.  Normally, however,     */
/*   INEXACT should be defined to be nothing.  ("#define INEXACT".)          */
/*                                                                           */
/* For more discussion, see http://www.cs.cmu.edu/~quake/robust.pc.html .    */
/*   For yet more discussion, see Section 5 of my paper, "Adaptive Precision */
/*   Floating-Point Arithmetic and Fast Robust Geometric Predicates" (also   */
/*   available as Section 6.6 of my dissertation).                           */

/* #define CPU86 */
/* #define LINUX */

#ifdef CPU86
#include <float.h>
#endif /* CPU86 */
#ifdef LINUX
#include <fpu_control.h>
#endif /* LINUX */

#define INEXACT /* Nothing */
/* #define INEXACT volatile */

/* A number that speaks for itself, every kissable digit.                    */

#define PI 3.141592653589793238462643383279502884197169399375105820974944592308

/* Another fave.                                                             */

#define SQUAREROOTTWO 1.4142135623730950488016887242096980785696718753769480732

/* And here's one for those of you who are intimidated by math.              */

#define ONETHIRD 0.333333333333333333333333333333333333333333333333333333333333

/* Global constants.                                                         */

REAL splitter;       /* Used to split REAL factors for exact multiplication. */
REAL epsilon;                             /* Floating-point machine epsilon. */
REAL resulterrbound;
REAL ccwerrboundA, ccwerrboundB, ccwerrboundC;
REAL iccerrboundA, iccerrboundB, iccerrboundC;
REAL o3derrboundA, o3derrboundB, o3derrboundC;

/*****************************************************************************/
/*                                                                           */
/*  exactinit()   Initialize the variables used for exact arithmetic.        */
/*                                                                           */
/*  `epsilon' is the largest power of two such that 1.0 + epsilon = 1.0 in   */
/*  floating-point arithmetic.  `epsilon' bounds the relative roundoff       */
/*  error.  It is used for floating-point error analysis.                    */
/*                                                                           */
/*  `splitter' is used to split floating-point numbers into two half-        */
/*  length significands for exact multiplication.                            */
/*                                                                           */
/*  I imagine that a highly optimizing compiler might be too smart for its   */
/*  own good, and somehow cause this routine to fail, if it pretends that    */
/*  floating-point arithmetic is too much like real arithmetic.              */
/*                                                                           */
/*  Don't change this routine unless you fully understand it.                */
/*                                                                           */
/*****************************************************************************/

void exactinit()
{
  REAL half;
  REAL check, lastcheck;
  int every_other;
#ifdef LINUX
  int cword;
#endif /* LINUX */

#ifdef CPU86
#ifdef SINGLE
  _control87(_PC_24, _MCW_PC); /* Set FPU control word for single precision. */
#else /* not SINGLE */
  _control87(_PC_53, _MCW_PC); /* Set FPU control word for double precision. */
#endif /* not SINGLE */
#endif /* CPU86 */
#ifdef LINUX
#ifdef SINGLE
  /*  cword = 4223; */
  cword = 4210;                 /* set FPU control word for single precision */
#else /* not SINGLE */
  /*  cword = 4735; */
  cword = 4722;                 /* set FPU control word for double precision */
#endif /* not SINGLE */
  _FPU_SETCW(cword);
#endif /* LINUX */

  every_other = 1;
  half = 0.5;
  epsilon = 1.0;
  splitter = 1.0;
  check = 1.0;
  /* Repeatedly divide `epsilon' by two until it is too small to add to      */
  /*   one without causing roundoff.  (Also check if the sum is equal to     */
  /*   the previous sum, for machines that round up instead of using exact   */
  /*   rounding.  Not that these routines will work on such machines.)       */
  do {
    lastcheck = check;
    epsilon *= half;
    if (every_other) {
      splitter *= 2.0;
    }
    every_other = !every_other;
    check = 1.0 + epsilon;
  } while ((check != 1.0) && (check != lastcheck));
  splitter += 1.0;
  /* Error bounds for orientation and incircle tests. */
  resulterrbound = (3.0 + 8.0 * epsilon) * epsilon;
  ccwerrboundA = (3.0 + 16.0 * epsilon) * epsilon;
  ccwerrboundB = (2.0 + 12.0 * epsilon) * epsilon;
  ccwerrboundC = (9.0 + 64.0 * epsilon) * epsilon * epsilon;
  iccerrboundA = (10.0 + 96.0 * epsilon) * epsilon;
  iccerrboundB = (4.0 + 48.0 * epsilon) * epsilon;
  iccerrboundC = (44.0 + 576.0 * epsilon) * epsilon * epsilon;
  o3derrboundA = (7.0 + 56.0 * epsilon) * epsilon;
  o3derrboundB = (3.0 + 28.0 * epsilon) * epsilon;
  o3derrboundC = (26.0 + 288.0 * epsilon) * epsilon * epsilon;
}

/* Random number seed is not constant, but I've made it global anyway.       */

unsigned long randomseed;                     /* Current random number seed. */

/*****************************************************************************/
/*                                                                           */
/*  parsecommandline()   Read the command line, identify switches, and set   */
/*                       up options and file names.                          */
/*                                                                           */
/*****************************************************************************/

void Mesh::parsecommandline(int argc, const char **argv)
{
  int i, j, k;
  char workstring[1024];

  b.poly = b.refine = b.quality = 0;
  b.vararea = b.fixedarea = b.usertest = 0;
  b.regionattrib = b.convex = b.weighted = b.jettison = 0;
  b.firstnumber = 1;
  b.edgesout = b.voronoi = b.neighbors = b.geomview = 0;
  b.nobound = b.nopolywritten = b.nonodewritten = b.noelewritten = 0;
  b.noiterationnum = 0;
  b.noholes = b.noexact = 0;
  b.incremental = 0;
  b.dwyer = 1;
  b.splitseg = 0;
  b.docheck = 0;
  b.nobisect = 0;
  b.conformdel = 0;
  b.steiner = -1;
  b.order = 1;
  b.minangle = 0.0;
  b.maxarea = -1.0;
  b.quiet = b.verbose = 0;

  for (i = 0; i < argc; i++) {
      for (j = 0; argv[i][j] != '\0'; j++) {
        if (argv[i][j] == 'p') {
          b.poly = 1;
	}

        if (argv[i][j] == 'r') {
          b.refine = 1;
	}
        if (argv[i][j] == 'q') {
          b.quality = 1;
          if (((argv[i][j + 1] >= '0') && (argv[i][j + 1] <= '9')) ||
              (argv[i][j + 1] == '.')) {
            k = 0;
            while (((argv[i][j + 1] >= '0') && (argv[i][j + 1] <= '9')) ||
                   (argv[i][j + 1] == '.')) {
              j++;
              workstring[k] = argv[i][j];
              k++;
            }
            workstring[k] = '\0';
            b.minangle = (REAL) strtod(workstring, (char **) NULL);
	  } else {
            b.minangle = 20.0;
	  }
	}
        if (argv[i][j] == 'a') {
          b.quality = 1;
          if (((argv[i][j + 1] >= '0') && (argv[i][j + 1] <= '9')) ||
              (argv[i][j + 1] == '.')) {
            b.fixedarea = 1;
            k = 0;
            while (((argv[i][j + 1] >= '0') && (argv[i][j + 1] <= '9')) ||
                   (argv[i][j + 1] == '.')) {
              j++;
              workstring[k] = argv[i][j];
              k++;
            }
            workstring[k] = '\0';
            b.maxarea = (REAL) strtod(workstring, (char **) NULL);
            if (b.maxarea <= 0.0) {
              printf("Error:  Maximum area must be greater than zero.\n");
	    }
	  } else {
            b.vararea = 1;
	  }
	}
        if (argv[i][j] == 'u') {
          b.quality = 1;
          b.usertest = 1;
        }

        if (argv[i][j] == 'A') {
          b.regionattrib = 1;
        }
        if (argv[i][j] == 'c') {
          b.convex = 1;
        }
        if (argv[i][j] == 'w') {
          b.weighted = 1;
        }
        if (argv[i][j] == 'W') {
          b.weighted = 2;
        }
        if (argv[i][j] == 'j') {
          b.jettison = 1;
        }
        if (argv[i][j] == 'z') {
          b.firstnumber = 0;
        }
        if (argv[i][j] == 'e') {
          b.edgesout = 1;
	}
        if (argv[i][j] == 'v') {
          b.voronoi = 1;
	}
        if (argv[i][j] == 'n') {
          b.neighbors = 1;
	}
        if (argv[i][j] == 'g') {
          b.geomview = 1;
	}
        if (argv[i][j] == 'B') {
          b.nobound = 1;
	}
        if (argv[i][j] == 'P') {
          b.nopolywritten = 1;
	}
        if (argv[i][j] == 'N') {
          b.nonodewritten = 1;
	}
        if (argv[i][j] == 'E') {
          b.noelewritten = 1;
	}
        if (argv[i][j] == 'O') {
          b.noholes = 1;
	}
        if (argv[i][j] == 'X') {
          b.noexact = 1;
	}
        if (argv[i][j] == 'o') {
          if (argv[i][j + 1] == '2') {
            j++;
            b.order = 2;
          }
	}
        if (argv[i][j] == 'Y') {
          b.nobisect++;
	}
        if (argv[i][j] == 'S') {
          b.steiner = 0;
          while ((argv[i][j + 1] >= '0') && (argv[i][j + 1] <= '9')) {
            j++;
            b.steiner = b.steiner * 10 + (int) (argv[i][j] - '0');
          }
        }

        if (argv[i][j] == 'i') {
          b.incremental = 1;
        }
        if (argv[i][j] == 'l') {
          b.dwyer = 0;
        }
        if (argv[i][j] == 's') {
          b.splitseg = 1;
        }
        if ((argv[i][j] == 'D') || (argv[i][j] == 'L')) {
          b.quality = 1;
          b.conformdel = 1;
        }
        if (argv[i][j] == 'C') {
          b.docheck = 1;
        }
        if (argv[i][j] == 'Q') {
          b.quiet = 1;
        }
        if (argv[i][j] == 'V') {
          b.verbose++;
        }
      }
  }
  b.usesegments = b.poly || b.refine || b.quality || b.convex;
  b.goodangle = cos(b.minangle * PI / 180.0);
  if (b.goodangle == 1.0) {
    b.offconstant = 0.0;
  } else {
    b.offconstant = 0.475 * sqrt((1.0 + b.goodangle) / (1.0 - b.goodangle));
  }
  b.goodangle *= b.goodangle;
  if (b.refine && b.noiterationnum) {
    printf(
      "Error:  You cannot use the -I switch when refining a triangulation.\n");
  }
  /* Be careful not to allocate space for element area constraints that */
  /*   will never be assigned any value (other than the default -1.0).  */
  if (!b.refine && !b.poly) {
    b.vararea = 0;
  }
  /* Be careful not to add an extra attribute to each element unless the */
  /*   input supports it (PSLG in, but not refining a preexisting mesh). */
  if (b.refine || !b.poly) {
    b.regionattrib = 0;
  }
  /* Regular/weighted triangulations are incompatible with PSLGs */
  /*   and meshing.                                              */
  if (b.weighted && (b.poly || b.quality)) {
    //b.weighted = 0;
    if (!b.quiet) {
      printf("Warning:  weighted triangulations (-w, -W) are incompatible\n");
      printf("  with PSLGs (-p) and meshing (-q, -a, -u).  Weights ignored.\n"
             );
    }
  }
  if (b.jettison && b.nonodewritten && !b.quiet) {
    printf("Warning:  -j and -N switches are somewhat incompatible.\n");
    printf("  If any vertices are jettisoned, you will need the output\n");
    printf("  .node file to reconstruct the new node indices.");
  }

}

/*
	 Mesh::
*/

Mesh::Mesh(bool exact, bool searchGraph, const char* switches) :
	infvertex1(*this),
	infvertex2(*this),
	infvertex3(*this)
{
	dummytri = new triangle(*this);
	dummysub = new subseg(*this);

	ResetDummy();

	//const char * triswitches = switches.c_str();
	parsecommandline(1, &switches);
 
	recenttri.tri = nullptr; /* No triangle has been visited yet. */
	undeads = 0;                       /* No eliminated input vertices yet. */
	samples = 1;         /* Point location should take at least one sample. */
	checksegments = 1; 
  
	randomseed = 1;
  
	b.noexact = !exact;
	exactinit();                     /* Initialize exact arithmetic constants. */
    
	createdvertices = 0;

	this->searchGraph = searchGraph;
	searchDualDirty = true;

	if( searchGraph )
	{
		searchDual = new Mesh(exact, false, switches);
		searchDual->searchDual = this;
	}
	else
		searchDual = nullptr;
	
}

void Mesh::ResetDummy()
{
	dummytri->otris[0].tri = this->dummytri;
	dummytri->otris[1].tri = this->dummytri;
	dummytri->otris[2].tri = this->dummytri;

	dummytri->osubs[0].ss = this->dummysub;
	dummytri->osubs[1].ss = this->dummysub;
	dummytri->osubs[2].ss = this->dummysub;

	dummysub->osubs[0].ss = this->dummysub;
	dummysub->osubs[1].ss = this->dummysub;

	dummysub->otris[0].tri = this->dummytri;
	dummysub->otris[1].tri = this->dummytri;
}

Mesh::~Mesh()
{
	// Delete all triangles, vertices and subsegs
	for(triangle* tri : triangles)
	{
		delete tri;
	}
	for(subseg* seg : subsegs)
	{
		delete seg;
	}
	for(wertex* vert : vertices)
	{
		delete vert;
	}
	// Delete dead items
	for(triangle* tri : deadtriangles)
	{
		delete tri;
	}
	for(subseg* seg : deadsubsegs)
	{
		delete seg;
	}
	for(wertex* vert : deadvertices)
	{
		delete vert;
	}

	delete dummytri;
	delete dummysub;

	if( searchGraph )
	{
		deleteDual();
	}
		
}

wertex*	  Mesh::GetVert(int index)const{ return vertices[index]; }
triangle* Mesh::GetTri(int index)const{ return triangles[index]; }
subseg*	  Mesh::GetSeg(int index)const{ return subsegs[index]; }

void Mesh::deleteDual()
{
	delete searchDual;
}

void Mesh::initVertices(int n)
{
	vertex vert;
	this->vertices.reserve(n);
	for(int i=0; i<n; i++){
		this->makevertex(vert);
	}
}

void Mesh::initTriangles(int n)
{
	otri temptri;
	this->triangles.reserve(n);
	for(int i=0; i<n; i++)
	{
		this->maketriangle(&temptri);
	}
}

void Mesh::initSegments(int n)
{
	osub temposub;
	this->subsegs.reserve(n);
	for(int i=0; i<n; i++)
	{
		this->makesubseg(&temposub);
	}
}

void Mesh::resetNeighborSegs()
{ /* Reset the prev/next segment around a given vertex information 
     for all sub segments.*/
	osub temposub;
	for(subseg* seg : subsegs)
	{
		temposub.ss = seg;
		
		temposub.SetNeighborSegs();
		ssymself(temposub);
		temposub.SetNeighborSegs();

		temposub.ss->setUnitVec();
	}
}

/********* Constructors begin here                                   *********/
/**                                                                         **/
/**                                                                         **/

/*****************************************************************************/
/*                                                                           */
/*  maketriangle()   Create a new triangle with orientation zero.            */
/*                                                                           */
/*****************************************************************************/

void Mesh::maketriangle(struct otri* newotri)
{
	if( deadtriangles.empty() )
	{
		newotri->tri = new triangle(*this);
	}
	else
	{
		newotri->tri = deadtriangles.back();
		deadtriangles.pop_back();
	}

	newotri->tri->index = (int)triangles.size();
	triangles.push_back(newotri->tri);
 
	newotri->orient = 0;
}

/*****************************************************************************/
/*                                                                           */
/*  triangledealloc()   Deallocate space for a triangle, marking it dead.    */
/*                                                                           */
/*****************************************************************************/

void Mesh::triangledealloc( triangle *dyingtriangle )
{
	// Put the dying triangle in the deadtriangles list
	uint index = dyingtriangle->index;
	assert( this->triangles[index] == dyingtriangle );

	dyingtriangle->Reset();

	this->deadtriangles.push_back(dyingtriangle);
	
	// replace removed object with last one in the list
	// so as to leave no gap in the sequence.
	if( index < this->triangles.size()-1 )
	{
		this->triangles[index] = this->triangles.back();
		this->triangles[index]->index = index;
	}

	this->triangles.pop_back();
}

/*****************************************************************************/
/*                                                                           */
/*  makevertex()   Create a new vertex										 */
/*                                                                           */
/*****************************************************************************/

void Mesh::makevertex(wertex* &newvertex)
{
	if( deadvertices.empty() )
	{
		newvertex = new wertex(*this);
	}
	else
	{
		newvertex = deadvertices.back();
		deadvertices.pop_back();
	}

	newvertex->id = this->createdvertices;
	this->createdvertices++;

	newvertex->index = (int)vertices.size();
	vertices.push_back(newvertex);
}

/*****************************************************************************/
/*                                                                           */
/*  vertexdealloc()   Deallocate space for a vertex, marking it dead.        */
/*                                                                           */
/*****************************************************************************/

void Mesh::vertexdealloc(wertex *dying)
{
	// Put the dying vertex in the dead vertices list
	uint index = dying->index;
	assert( this->vertices[index] == dying );
	
	dying->reset();

	this->deadvertices.push_back(dying);

	if( index < this->vertices.size() - 1 )
	{
		// put last vertex in the place of the one about to be removed
		// so we can safely pop the last one.
		this->vertices[index] = this->vertices.back();
		this->vertices[index]->index = index;
	}

	this->vertices.pop_back();
}

/*****************************************************************************/
/*                                                                           */
/*  makesubseg()   Create a new subsegment with orientation zero.            */
/*                                                                           */
/*****************************************************************************/

void Mesh::makesubseg(struct osub *newsubseg)
{
	if( deadsubsegs.empty() )
	{
		newsubseg->ss = new subseg(*this);
	}
	else
	{
		newsubseg->ss = deadsubsegs.back();
		deadsubsegs.pop_back();
	}

	newsubseg->ss->index = (int)subsegs.size();
	subsegs.push_back(newsubseg->ss);
 
	newsubseg->ssorient = 0;
}

void Mesh::subsegdealloc(subseg *dying)
{
	// Put the dying subseg in the deadsubsegs list
	uint index = dying->index;
	assert( this->subsegs[index] == dying );
	
	dying->index = -1;

	for(uint i = 0; i<2; i++){
		dying->otris[i].tri = this->dummytri;
		dying->osubs[i].ss = this->dummysub;
	}

	this->deadsubsegs.push_back(dying);

	// replace removed object with last one in the list
	// so as to leave no gap in the sequence.
	if( index < this->subsegs.size() - 1 )
	{
		this->subsegs[index] = this->subsegs.back();
		this->subsegs[index]->index = index;
	}

	this->subsegs.pop_back();
}

/********* Constructors end here                                     *********/

otri Mesh::getRecentTri()const{ 
	if( this->recenttri.tri == nullptr )
		throw TriangleExc("recenttri is NULL");
	return this->recenttri;
}

void Mesh::setRecentTri(const struct otri& arg){ 
	
	this->recenttri = arg;
}

void Mesh::swapVertices(wertex* vert1, wertex* vert2)
{
	vertex temp = vertices[vert2->index];
	temp->index = vert1->index;
	vertices[vert2->index] = vertices[vert1->index];
	vert1->index = vert2->index;
	vertices[vert1->index] = temp;
	
}

std::vector<wertex*>& Mesh::GetVertices()
{
	return this->vertices;
}

std::vector<triangle*>& Mesh::GetTriangles()
{
	return this->triangles;
}

std::vector<subseg*>& Mesh::GetSegments()
{
	return this->subsegs;
}

/*****************************************************************************/
/*                                                                           */
/*  randomnation()   Generate a random number between 0 and `choices' - 1.   */
/*                                                                           */
/*  This is a simple linear congruential random number generator.  Hence, it */
/*  is a bad random number generator, but good enough for most randomized    */
/*  geometric algorithms.                                                    */
/*                                                                           */
/*****************************************************************************/

unsigned long randomnation(uint choices)
{
  randomseed = (randomseed * 1366l + 150889l) % 714025l;
  return randomseed / (714025l / choices + 1);
}

/********* Mesh quality testing routines begin here                  *********/
/**                                                                         **/
/**                                                                         **/

/*****************************************************************************/
/*                                                                           */
/*  checkmesh()   Test the mesh for topological consistency.                 */
/*                                                                           */
/*****************************************************************************/

void Mesh::checkmesh()
{
	struct otri triangleloop;
	struct otri oppotri, oppooppotri;
	vertex checkorg, checkdest, checkapex;
	vertex oppoorg, oppodest;
	int horrors;
	int saveexact;
	osub checksub, testsub;

	std::ostringstream os;

	/* Temporarily turn on exact arithmetic if it's off. */
	saveexact = b.noexact;
	b.noexact = 0;
	if (!b.quiet) {
		printf("  Checking consistency of mesh...\n");
	}
	horrors = 0;
  
	//
	// Check index consistency
    //
    for(size_t i=0; i<vertices.size(); i++)
	{
		vertex vert = vertices[i];
		if( vert->index != (int)i )
		{
			os << "Inconsistent elem index " << *vert << " " << i << " vs " << vert->index;
			throw TriangleExc( os.str().c_str() );
		}
	  
		vert->checkCollide(vert->coords);

		if (vert->otri_.org()!=vert)
			throw TriangleExc_Tri_Vert( "Invalid otri_",vert->otri_,vert );

	}

	osub subsegloop, opposubseg, oppoopposub;
	Point2d unitVec;
	/* Run through the list of segments, checking each one. */
    for(size_t i=0; i<subsegs.size(); i++)
	{
		subsegloop.ss = subsegs[i];

		if( subsegloop.ss->index != (int)i )
		{
			os << "Inconsistent elem index " << subsegloop << " " << i << " vs " << subsegs[i]->index;
			throw TriangleExc( os.str().c_str() );
		}

		if( subsegs[i]->visited >= 0 )
			throw TriangleExc_Sub("segment.visite has not been reset",subsegloop);

		subsegloop.ss->calcUnitVec(unitVec);
		if( !vect_eq(unitVec,subsegloop.ss->unitVec) )
			throw TriangleExc_Sub("Invalid subseg unit vec",subsegloop);

		/* Check all 2 neighboring subsegs. */
		for (subsegloop.ssorient = 0; subsegloop.ssorient < 2;
				subsegloop.ssorient++)
		{

			sorg(subsegloop, checkorg);
	
			if( checkorg == nullptr ){
				throw TriangleExc_Sub("Found NULL vertex in subseg",subsegloop);
			}

			stpivot(subsegloop,oppotri);
			if( !oppotri.isDummy() && svdest(subsegloop) != oppotri.org() )
				throw TriangleExc_Sub_Tri( "otri and subseg org do not match", subsegloop, oppotri);

			/* Check prev / next subseg information */
			if( segnext(subsegloop).ss != this->dummysub && !(segprev(segnext(subsegloop)) == subsegloop) )
			{
				throw TriangleExc_Sub2( "Mismatched subseg next / prev info", subsegloop, segnext(subsegloop) );
			}
		
			if( segprev(subsegloop).ss != this->dummysub && !(segnext(segprev(subsegloop)) == subsegloop) )
			{
				throw TriangleExc_Sub2( "Mismatched subseg prev / next info", subsegloop, segprev(subsegloop) );
			}

			ssym(subsegloop, opposubseg);
			if( opposubseg.ss != this->dummysub )
			{
				/*ssym(opposubseg,oppoopposub); 
			
				if( !osubequal(opposubseg,oppoopposub) )
					throw TriangleExc_Sub2("Assymetric segment-segment bond", subsegloop, opposubseg);
				*/

				/* Check that both subsegs agree on the identities */
				/*   of their shared vertices.                     */
				sdest(opposubseg, oppodest);

				if ( checkorg != oppodest )
				{
					horrors++;
					throw TriangleExc_Sub2( "Mismatched segment edge coordinates", subsegloop, opposubseg );
				}

			}
		}

	}

	/* Run through the list of triangles, checking each one. */
	for(size_t i=0; i<triangles.size(); i++)
	{

		triangleloop.tri = triangles[i];
		if( triangleloop.tri->index != (int)i )
		{
			os << "Inconsistent elem index " << triangleloop << " " << i << " vs " << triangleloop.tri->index;
			throw TriangleExc( os.str().c_str() );
		}

	  
		/* Check all three edges of the triangle. */
		for (triangleloop.orient = 0; triangleloop.orient < 3;
				triangleloop.orient++)
		{
			checkorg = triangleloop.org();
			checkdest = triangleloop.dest();

			if (triangleloop.orient == 0) {       /* Only test for inversion once. */
				/* Test if the triangle is flat or inverted. */
				checkapex = triangleloop.apex();
				if( checkorg == nullptr || checkdest == nullptr || checkapex == nullptr ){
					throw TriangleExc_Tri("Found NULL vertex in triangle",triangleloop);
				}

				if (counterclockwise(checkorg->coords, checkdest->coords, checkapex->coords) <= 0.0) 
				{
					horrors++;
					throw TriangleExc_Tri("counterclockwise triangle found",triangleloop);
				}
			}

			tspivot(triangleloop, testsub);
			if( testsub.ss != this->dummysub )
			{
				stpivot(testsub, oppotri);
				if( triangleloop != oppotri )
					throw TriangleExc_Sub_Tri("Assymmetric triangle-subseg bond", testsub, triangleloop);

				ssymself(testsub);
				if( triangleloop.org()!=svorg(testsub) )
					throw TriangleExc_Sub_Tri("Bad triangle-subseg orientation", testsub, triangleloop);

			}

			/* Find the neighboring triangle on this edge. */
			sym(triangleloop, oppotri);
			if (oppotri.tri != this->dummytri)
			{
				/* Check that the triangle's neighbor knows it's a neighbor. */
				sym(oppotri, oppooppotri);
				if ((triangleloop.tri != oppooppotri.tri)
					|| (triangleloop.orient != oppooppotri.orient))
				{

					std::string message = "Asymmetric triangle-triangle bond";
					if (triangleloop.tri == oppooppotri.tri) {
						message += " (Right triangle, wrong orientation)";
					}
					horrors++;		  
					throw TriangleExc_Tri2(message, triangleloop, oppotri);

				}
				/* Check that both triangles agree on the identities */
				/*   of their shared vertices.                       */
				oppoorg = oppotri.org();
				oppodest = oppotri.dest();

				if ((checkorg != oppodest) || (checkdest != oppoorg))
				{
					horrors++;
					throw TriangleExc_Tri2( "Mismatched triangle edge coordinates", triangleloop, oppotri );
				}

				/* Check that both triangles agree on the identities */
				/*   of their shared subseg.                         */
				tspivot(oppotri,checksub);

				if( checksub.ss != testsub.ss )
				{
					TriangleException exc = TriangleExc_Tri2("Mismatched subseg between two triangles", triangleloop, oppotri);
					exc.osubs.push_back(testsub);
					exc.osubs.push_back(checksub);
					throw exc;
				}
				
			}
		}
	}
	if (horrors == 0) {
		if (!b.quiet) {
			printf("The mesh appears to be consistent.\n");
		}
	} else if (horrors == 1) {
		throw TriangleExc("Precisely one festering wound discovered.");
	} else {
		std::ostringstream message;
		message << horrors << " abominations witnessed.";
		throw TriangleExc(message.str());
	}
	/* Restore the status of exact arithmetic. */
	b.noexact = saveexact;

	checkvertexmap();
  
	/* this->hullsize appears to be set unreliably so we turn off the below

	int computedHullSize = HullSize(true);
  
	if( this->hullsize != computedHullSize ){
		os <<"this->hullsize differs from this->HullSize(): " << this->hullsize << " vs " << computedHullSize;
		throw TriangleExc(os.str().c_str());
	}
	*/

	// Validate dual graph
	if( searchGraph && !searchDualDirty )
		validateSearchGraph();
}

int Mesh::HullSize(bool checkConvexity)
{ 
	// Walks the convex hull and return its size.
	// checkConvexity : raise if hull is not convex

	otri countingtri, prevtri;

	if( triangles.empty() )
		throw TriangleExc("Can't find a single triangle in mesh");

	otri starttri(triangles[0]);

	Point2d outsidepoint(0,-10000);
	while( preciselocate(outsidepoint,&starttri,false) != OUTSIDE )
	{
		outsidepoint[1] *= 10;
	}

	if( starttri.tri == this->dummytri || right_tri(starttri) != this->dummytri )
		throw TriangleExc("Can't find a handle on convex hull from dummytri");

	countingtri = starttri;

	int hullsize = 0;
	int degree;
	do{

		hullsize ++;

		if( checkConvexity )
			prevtri = countingtri;
		 
		degree = 1;
		while(left_tri(countingtri) != this->dummytri){
			onextself(countingtri);
#ifdef SELF_CHECK
			degree++;
			if( degree > abs(countingtri.org()->getDegree())){
				std::ostringstream os;
				os << "we are supposed to be on the hull and yet have circled around " << countingtri.org();
				throw TriangleExc( os.str() );
			}
#endif
		}
		lprevself(countingtri);
		if( checkConvexity )
		{
			if(prodvect(countingtri.org()->coords, countingtri.dest()->coords, prevtri.dest()->coords) < 0){
			  throw TriangleExc("Hull is not Convex");
			}
		}	
	  }while( countingtri != starttri );

	return hullsize;
}

/*****************************************************************************/
/*                                                                           */
/*  checkdelaunay()   Ensure that the mesh is (constrained) Delaunay.        */
/*                                                                           */
/*****************************************************************************/

void Mesh::checkdelaunay()
{
  struct otri triangleloop;
  struct otri oppotri;
  struct osub opposubseg;
  vertex triorg, tridest, triapex;
  vertex oppoapex;
  bool shouldbedelaunay;
  int horrors;
  int saveexact;
  
  /* Temporarily turn on exact arithmetic if it's off. */
  saveexact = b.noexact;
  b.noexact = 0;
  
   // Checking Delaunay property of mesh...");
 
  horrors = 0;
  /* Run through the list of triangles, checking each one. */
  for( triangle* tri : triangles)
  {
	triangleloop.tri = tri;

    /* Check all three edges of the triangle. */
    for (triangleloop.orient = 0; triangleloop.orient < 3;
         triangleloop.orient++) 
	{
      triorg = triangleloop.org();
      tridest = triangleloop.dest();
      triapex = triangleloop.apex();
      sym(triangleloop, oppotri);
      oppoapex = oppotri.apex();
      /* Only test that the edge is locally Delaunay if there is an   */
      /*   adjoining triangle whose pointer is larger (to ensure that */
      /*   each pair isn't tested twice).                             */
      shouldbedelaunay = (oppotri.tri != this->dummytri) &&
            !deadtri(oppotri.tri) && (triangleloop.tri < oppotri.tri) &&
            (triorg != &this->infvertex1) && (triorg != &this->infvertex2) &&
            (triorg != &this->infvertex3) &&
            (tridest != &this->infvertex1) && (tridest != &this->infvertex2) &&
            (tridest != &this->infvertex3) &&
            (triapex != &this->infvertex1) && (triapex != &this->infvertex2) &&
            (triapex != &this->infvertex3) &&
            (oppoapex != &this->infvertex1) && (oppoapex != &this->infvertex2) &&
            (oppoapex != &this->infvertex3);

      if (this->checksegments && shouldbedelaunay) 
	  {
        /* If a subsegment separates the triangles, then the edge is */
        /*   constrained, so no local Delaunay test should be done.  */
        tspivot(triangleloop, opposubseg);
        if (opposubseg.ss != this->dummysub)
		{
          shouldbedelaunay = false;
        }
      }

      if (shouldbedelaunay) 
	  {
        if (nonregular(triorg->coords, tridest->coords, triapex->coords, oppoapex->coords) > 0.0) 
		{
			horrors++;
			if (b.weighted) {
			  throw TriangleExc_Tri2("Non-regular pair of triangles",triangleloop, oppotri);
          } else {
			  throw TriangleExc_Tri_Vert("Non-Delaunay triangle",triangleloop, oppoapex);
          }
        }
      }
    }
   
  }

  if (horrors == 1) 
  {	
	  throw TriangleExc("Precisely one terrifying transgression identified.");
  }else if( horrors > 1 ){
	  std::ostringstream message;
	  message << horrors << " obscenities viewed with horror.";
	  throw TriangleExc(message.str());
  }

  /* Restore the status of exact arithmetic. */
  b.noexact = saveexact;
}

/**                                                                         **/
/**                                                                         **/
/********* Mesh quality testing routines end here                    *********/

/********* Point location routines begin here                        *********/
/**                                                                         **/
/**                                                                         **/

/*****************************************************************************/
/*                                                                           */
/*  makevertexmap()   Construct a mapping from vertices to triangles to      */
/*                    improve the speed of point location for segment        */
/*                    insertion.                                             */
/*                                                                           */
/*  Traverses all the triangles, and provides each corner of each triangle   */
/*  with a pointer to that triangle.  Of course, pointers will be            */
/*  overwritten by other pointers because (almost) each vertex is a corner   */
/*  of several triangles, but in the end every vertex will point to some     */
/*  triangle that contains it.                                               */
/*                                                                           */
/*****************************************************************************/

void Mesh::makevertexmap()
{
	// Constructing mapping from vertices to triangles
	struct otri triangleloop;
	vertex triorg;

	for(triangle* tri : triangles)
	{
		triangleloop.tri = tri;
  
		/* Check all three vertices of the triangle. */
		for (triangleloop.orient = 0; triangleloop.orient < 3;
				triangleloop.orient++) {
			triorg = triangleloop.org();
			setvertex2tri(triorg, triangleloop);
		}

	}
}

void Mesh::checkvertexmap()
{
	struct otri triangleloop;
	;

	for (vertex vertexloop : vertices)
	{
		triangleloop = vertexloop->otri_;
		
		if( triangleloop.tri == nullptr || triangleloop.org() != vertexloop ){
			throw TriangleExc_Tri_Vert( "bad vertex to triangle mapping", triangleloop, vertexloop );
		}
	}
	/*
	for(triangle* tri : triangles)
	{
		triangleloop.tri = tri;
		for( int j=0; j<3; j++ )
		{
			triangleloop.orient = j;
			vertexloop = triangleloop.org();
			
			if( std::find(vertexloop->wotris.begin(), vertexloop->wotris.end(), triangleloop) == vertexloop->wotris.end() ){
				throw TriangleExc_Tri_Vert( "missing triangle mapping on vertex", triangleloop, vertexloop );
			}
			
		}
	}
	*/
}

/*****************************************************************************/
/*                                                                           */
/*  preciselocate()   Find a triangle or edge containing a given point.      */
/*                                                                           */
/*  Begins its search from `searchtri'.  It is important that `searchtri'    */
/*  be a handle with the property that `searchpoint' is strictly to the left */
/*  of the edge denoted by `searchtri' [GG: no longer required], or is collinear with that edge and   */
/*  does not tt that edge.  (In particular, `searchpoint' should not  */
/*  be the origin or destination of that edge.)                              */
/*                                                                           */
/*  These conditions are imposed because preciselocate() is normally used in */
/*  one of two situations:                                                   */
/*                                                                           */
/*  (1)  To try to find the location to insert a new point.  Normally, we    */
/*       know an edge that the point is strictly to the left of.  In the     */
/*       incremental Delaunay algorithm, that edge is a bounding box edge.   */
/*       In Ruppert's Delaunay refinement algorithm for quality meshing,     */
/*       that edge is the shortest edge of the triangle whose circumcenter   */
/*       is being inserted.                                                  */
/*                                                                           */
/*  (2)  To try to find an existing point.  In this case, any edge on the    */
/*       convex hull is a good starting edge.  You must screen out the       */
/*       possibility that the vertex sought is an endpoint of the starting   */
/*       edge before you call preciselocate().                               */
/*                                                                           */
/*  On completion, `searchtri' is a triangle that contains `searchpoint'.    */
/*                                                                           */
/*  This implementation differs from that given by Guibas and Stolfi.  It    */
/*  walks from triangle to triangle, crossing an edge only if `searchpoint'  */
/*  is on the other side of the line containing that edge.  After entering   */
/*  a triangle, there are two edges by which one can leave that triangle.    */
/*  If both edges are valid (`searchpoint' is on the other side of both      */
/*  edges), one of the two is chosen by drawing a line perpendicular to      */
/*  the entry edge (whose endpoints are `forg' and `fdest') passing through  */
/*  `fapex'.  Depending on which side of this perpendicular `searchpoint'    */
/*  falls on, an exit edge is chosen.                                        */
/*                                                                           */
/*  This implementation is empirically faster than the Guibas and Stolfi     */
/*  point location routine (which I originally used), which tends to spiral  */
/*  in toward its target.                                                    */
/*                                                                           */
/*  Returns ONVERTEX if the point lies on an existing vertex.  `searchtri'   */
/*  is a handle whose origin is the existing vertex.                         */
/*                                                                           */
/*  Returns ONEDGE if the point lies on a mesh edge.  `searchtri' is a       */
/*  handle whose primary edge is the edge on which the point lies.           */
/*                                                                           */
/*  Returns INTRIANGLE if the point lies strictly within a triangle.         */
/*  `searchtri' is a handle on the triangle that contains the point.         */
/*                                                                           */
/*  Returns OUTSIDE if the point lies outside the mesh.  `searchtri' is a    */
/*  handle whose primary edge the point is to the right of.  This might      */
/*  occur when the circumcenter of a triangle falls just slightly outside    */
/*  the mesh due to floating-point roundoff error.  It also occurs when      */
/*  seeking a hole or region point that a foolish user has placed outside    */
/*  the mesh.                                                                */
/*                                                                           */
/*  If `stopatsubsegment' is nonzero, the search will stop if it tries to    */
/*  walk through a subsegment, and will return OUTSIDE.                      */
/*                                                                           */
/*  WARNING:  This routine is designed for convex triangulations, and will   */
/*  not generally work after the holes and concavities have been carved.     */
/*  However, it can still be used to find the circumcenter of a triangle, as */
/*  long as the search is begun from the triangle in question.               */
/*                                                                           */
/*****************************************************************************/

std::vector<otri> Mesh::intersectTris_py( const std::vector<Point2d>& coords,
										  bool loop,
										  bool hullstrict,
										  const otri& searchtri )
{

	std::list<otri> otrilist;
	intersectTris( otrilist, coords, loop, hullstrict, searchtri );
	std::vector<otri> out;

	out.assign(otrilist.begin(), otrilist.end());
	
	return out;
}

void Mesh::intersectTris(	std::list<otri>& out, 
							const std::vector<Point2d>& coordsList, 
							bool loop, 
							bool hullstrict, 
							const otri& searchtri )
{
	/* Liste des triangles coupes par les segments joignant la liste de points coords = [ x0,y0, x1,y1, ...].

	   loop : if true, connect first and last items in coordsList to close the polygon.
	   searchTri : if valid, must contain the first point.
	   hullstrict : if true, triangles that are entered and exited through the same sides are not returned.
	*/
	
	size_t numcoords = coordsList.size();
	if( numcoords < 2 )
		throw TriangleExc("2 points at least are needed to define a contour");
	
	//out.resize(1);
	out.push_back(searchtri);

	vertex v_apex;
	
	size_t loopcoords = numcoords;
	if( loop )
		loopcoords += 1;
	
	Point2d const* point;
	Point2d const* prevPoint; // previous point in a different triangle

	std::list<otri>::iterator itTri  = out.begin();
	std::list<otri>::iterator itTri2;

	otri temptri = searchtri;
	bool firstPointOnVertex = false;

	for( uint i=0; i<loopcoords; i++ )
	{
		if(loop && i == numcoords ){
			// connect last point to first
			point = &coordsList[0];
		}
		else{
			point = &coordsList[i];
		}

		if( itTri->tri == nullptr || itTri->tri == this->dummytri )
		{	
			if( itTri != out.begin() )
			{ // start searching from last valid tri encountered 
				itTri2 = itTri;
				itTri2--;
				*itTri = *itTri2; // copy previous item
			}

			locateresult locateInfo = preciselocate(*point, &*itTri, 0); 
			if( locateInfo == ONVERTEX )
			{
				firstPointOnVertex = i == 0;
				// point is on a vertex : in that case preciselocate itTri has org at that vertex.
				//assert( point[0] == itTri->org()->coords[0] && point[1] == itTri->org()->coords[1] );
				// We need to reorient the triangle so we make sure point->next point direction crosses side 
				// opposite org
				if( i < loopcoords-1 )
					*itTri = itTri->org()->triSecant(coordsList[i+1]);
				else
					*itTri = itTri->org()->triSecant(coordsList[0]);
			}

			if( locateInfo == OUTSIDE )
			{   
				// point is Still outside the hull
				itTri->tri = this->dummytri;
			}
			else if( i > 0 )
			{	// we have found a valid tri containing point.
				// prev point is outside the hull : walk back from point to prevPoint and add
				// the triangles found on the way.
				prevPoint = &coordsList[i-1];
				itTri2 = itTri;
				temptri = *itTri;

				auto orient = temptri.orientTri(*point,*prevPoint);
				// orient itTri in the direction of the walk
				// i.e. such that prevPoint->point intersects org->dest
				if(orient == ETriSidePos::opORG)
					lnextself(*itTri);
				else if(orient == ETriSidePos::opDST)
					lprevself(*itTri);

				while( temptri.tri != this->dummytri )
				{
					out.insert(itTri2,temptri);
					itTri2--;
					v_apex = temptri.apex();
					if( prodvect(*point,*prevPoint,v_apex->coords) >= 0 )
					{// exit triangle through dest-apex
						dprevself(temptri);
						// we need to reorient previous tri as we are walking backward
						lnextself(*itTri2);
					}
					else
					{// exit triangle through apex-org
						onextself(temptri);
						// we need to reorient previous tri as we are walking backward
						lprevself(*itTri2);
					}			
				}

			}

			continue;
			
		}
		else if( i==0 ){
#ifdef SELF_CHECK
			assert( itTri->contains(coordsList[0]) );
#endif	
			continue;
		}

		if( !itTri->contains(*point) ) // point is not in itTri
		{
			prevPoint = &coordsList[i-1];
			out.push_back(*itTri);
			itTri++;
			
			if( itTri->orientTri(*prevPoint, *point) == ETriSidePos::opAPX && hullstrict )
			{
				//leaving the tri through the same side we entered it
				//we skip that tri.
				
				itTri2 = itTri;
				itTri2--;
				if( itTri2 != out.begin() )
				{// Do not do that check for first tri as we don't know what side we entered the tri from.
					out.erase(itTri2);
				}
			}

			while( itTri->tri != this->dummytri && !itTri->contains(*point) )
			{
				// Select all the valid triangles intersected by prevPoint->point
				// i.e. stop once we find dummytri or a tri that contains point.

				out.push_back(*itTri);
				itTri++;

				v_apex = itTri->apex();
				if( prodvect(*prevPoint,*point,v_apex->coords) >=0 )
				{// exit triangle through dest-apex
					dprevself(*itTri);
				}
				else
				{// exit triangle through apex-org
					onextself(*itTri);
				}
			}
		}
	}
	// if coords are looping, we will have inserted the start and end tris twice,
	// so here we remove the duplicata.
	// If last triangle encountered is a dummytri, remove it.
	if( itTri->tri == this->dummytri )
	{
		out.pop_back();
	}
	else if( loop )
	{
#ifdef SELF_CHECK
		// since first point wasn't outside, we expect first and last tri to be the same.
		// (unless we started on a vertex)
		if(  !firstPointOnVertex && out.front().tri != out.back().tri )
			throw TriangleExc_Tri2( "different start and end tris", out.front(), out.back() );
#endif

		out.pop_front(); // get rid of front as its orientation might not be consistent with selection path.
		if( hullstrict && !out.empty() )
		{
			if( out.front() == out.back() )
			{ // only one triangle
				out.clear();
			}
			else if( right_tri(out.back()) == out.front().tri )
			{
				// detect case (which was not tested above) of selection path going
				// in and out of first tri through the same side.
				out.pop_back();
			}

		}		
	}
}

subseg* Mesh::firstSegObstacle(	const Point2d& startpoint, 
								const Point2d& endpoint,
								const otri& starttri)
{
	/*
		First segment obstacle when going from startpoint to endpoint in a straight line.
	*/

	otri searchtri = starttri; 
	if( searchtri.isNull() ){
		enum locateresult result = preciselocate(startpoint,&searchtri,0);
		if(result == OUTSIDE)
			throw TriangleExc("start point is outside the mesh");
	}

	// orient searchtri in the direction of the walk
	auto orient = searchtri.orientTri(startpoint,endpoint,false);
	if(orient == ETriSidePos::opAPX)
		lnextself(searchtri);

	osub searchsub;

	while( true )
	{
		if( searchtri.contains(endpoint) )
		{// point found in straight line
			return nullptr;
		}
		else
		{
			// need to cross into another triangle.

			// Check what side is crossed
			if( prodvect(startpoint,endpoint,searchtri.apex()->coords) >= 0 )
			{// exit triangle through dest->apex
				lnextself(searchtri);
			}
			else
			{// exit triangle through org->apex
				lprevself(searchtri);
			}
			
			tspivot(searchtri, searchsub);

			if( searchsub.ss != this->dummysub )
			{	// a segment was found on the path.
				// searchsub is oriented left to right.
				return searchsub.ss;
			}

			symself(searchtri);

			if( searchtri.tri == this->dummytri )
				return nullptr; // point must be outside the mesh.

		}

	} // while

}

std::vector<wertex*> Mesh::verticesInCircle( const REAL /*centre*/ [],
											 const REAL& /*radius*/,
											 const otri& /*searchtri*/ )
{
	std::vector<wertex*> out;
	/*
	std::list<otri> visitedtris;
	otri itTri = searchtri;
	if( itTri.tri == nullptr || itTri.tri == this->dummytri )
	{	

		locateresult locateInfo = preciselocate(centre, &itTri, 0);

		if( locateInfo == ONVERTEX )
		{
			firstPointOnVertex = i == 0;
			// point is on a vertex : in that case preciselocate itTri has org at that vertex.
			//assert( point[0] == itTri->org()->coords[0] && point[1] == itTri->.org()->coords[1] );
			// We need to reorient the triangle so we make sure point->next point direction crosses side 
			// opposite org
			if( i < loopcoords-3 )
				*itTri = itTri->org()->triSecant(coordsList[i+1],coordsList[i+3]);
			else
				*itTri = itTri->org()->triSecant(coordsList[0],coordsList[1]);
		}

		if( locateInfo == OUTSIDE )
		{   
			// point is Still outside the hull
			itTri->tri = this->dummytri;
		}
		else if( i > 0 )
		{
		
		}
	}
	*/
	return out;
}

std::vector<wertex*> Mesh::verticesInPolygon( const std::vector<Point2d>& coordsList, 
												bool includeAll, 
												const otri& searchtri )
{
	std::vector<wertex*> verts;
	std::vector<triangle*> tris;
	vertAndTrisInPolygon(	coordsList,
							includeAll,
							verts,
							tris,
							searchtri );

	return verts;
}

std::vector<triangle*> Mesh::trianglesInPolygon( const std::vector<Point2d>& coordsList, 
											bool includeAll, 
											const otri& searchtri )
{
	std::vector<wertex*> verts;
	std::vector<triangle*> tris;
	vertAndTrisInPolygon(	coordsList,
							includeAll,
							verts,
							tris,
							searchtri );

	return tris;
}

void Mesh::vertAndTrisInPolygon( const std::vector<Point2d>& coordsList,
								  bool includeAll,
								  std::vector<wertex*>& verts,
								  std::vector<triangle*>& visitedtris,
								  const otri& searchtri )
{
	/*
	Vertices and tris found to the right hand side of the boundary defined by coordsList.
	*/

	// Triangles defining the boundary of the area of the selection.
	// They are oriented such that 
	std::list<otri> outerotris;

	intersectTris( outerotris, coordsList, true, true, searchtri );

	if( outerotris.empty() )
		return;

	std::vector<otri> innerotris;
	
	visitedtris.reserve(outerotris.size()*4);

	/*	Mark triangles on the selection boundary.
		such that triangles through which the boundary goes only once are marked with 1
		while triangles through which the boundary goes twice or more have a marker > 1
	*/
	for (const otri& it : outerotris)
	{
		setelemattribute( it, 0, elemattribute(it,0) + 1 );
		visitedtris.push_back(it.tri);
	}

	otri temptri;
	otri temptri2;

	vertex v_org;

	// Loop over triangles defining the boundary of the area
	for(const otri& it : outerotris)
	{
		if( elemattribute(it,0) == 1 )
		{ // found a vertex on a simple boundary (i.e. where the area bound doesn't go in and out through the same triangle side)
			v_org = it.org();
			if( ( includeAll || vertexradius(v_org) > 0 ) && vertexmark(v_org) == 0 )
			{// add vertex on the boundary if it hasn't been marked
				// mark vertex as visited - make sure we mark it back to zero before exiting the routine.
				verts.push_back(v_org);
				setvertexmark(v_org, 1);
			}
		
			onext(it,temptri2);
			
			if( temptri2.tri != this->dummytri && elemattribute(temptri2,0) == 0 )
			{
				// an inner triangle which hasn't been visited
				innerotris.push_back(temptri2);
				setelemattribute( temptri2, 0, 1 );
			}
		}
	}

	while( !innerotris.empty() )
	{
		temptri = innerotris.back();
		visitedtris.push_back(temptri.tri);

		innerotris.pop_back();

		for(int i=0; i<2; i++)
		{ 
			// Loop over the 2 neighbors of the triangle that potentially have not been visited
			
			if( i > 0 )
			{  // Only apex of original temptri might be unmarked.
				v_org = temptri.org();
				if( ( includeAll || vertexradius(v_org) > 0 ) && vertexmark(v_org) == 0 )
				{ // mark visited vertices by flipping their sign
					setvertexmark(v_org, 1);
					verts.push_back(v_org);
				}
			}

			onext(temptri, temptri2);
			if( temptri2.tri != this->dummytri && elemattribute(temptri2,0) == 0 )
			{
				innerotris.push_back(temptri2);
				setelemattribute( temptri2, 0, 1 );
			}

			lprevself(temptri);
		}

	}
	
	for( triangle* visitedtri : visitedtris )
	{ // reset visited triangle markers
		temptri.tri = visitedtri;
		setelemattribute( temptri, 0, 0 );
	}
	
	// reset visited vertex markers
	for(wertex* vit : verts)
	{
#ifdef SELF_CHECK
		if (vertexmark(vit) == 0)
			throw TriangleExc_Vert("vertexmark should have been marked", vit);
#endif // SELF_CHECK
		setvertexmark(vit,0);
	}

}

struct otri Mesh::preciseLocate_py(	const Point2d& searchpoint, 
									const struct otri& searchtri, 
									bool backtrack,
									bool stopatsubsegment)
{
	
	struct otri temptri = searchtri;
	int stopatsubseg = stopatsubsegment ? 1 : 0;

	if( preciselocate( searchpoint, &temptri, stopatsubseg ) == OUTSIDE )
	{
		// if searchpoint is outside the mesh, preciselocate backtracks and returns a boundary triangle.
		// here instead we return dummytri "outside" tri to make it clear 
		// there is no returned enum to pass down the information.
		if( !backtrack )
			return otri( this->dummytri );
	}

	return temptri;
}

enum locateresult Mesh::preciselocate(	const Point2d& searchpoint,
										struct otri *searchtri,
										int stopatsubsegment )
{
	osub checkedge;
	vertex forg, fdest, fapex;
	REAL orgorient, destorient;
	int moveleft;
  
	if( searchtri == nullptr )
		throw TriangleExc( "preciselocate: searchtri is NULL");
	  
	/* Check if a good starting triangle has already been provided by the caller.     */
	if( searchtri->tri == nullptr || searchtri->tri == this->dummytri ) 
	{
		/* find any triangle to start from. */
		searchtri->tri = this->triangles[0];

#ifdef SELF_CHECK
		if(searchtri->tri == nullptr )
			throw TriangleExc("preciselocate : could not find good starting tri");
#endif
	}
	
	// algorithm below requires searchpoint to be left of the edge given by searchtri;
	// so we rotate searchtri until that is the case
	for( uint i=0; i<3; i++ )
	{
		forg = searchtri->org();
		fdest = searchtri->dest();
		if( isdirect(forg->coords,fdest->coords,searchpoint) )
			break;

		lnextself(*searchtri);
	}
  	
	fapex = searchtri->apex();

	while (true) 
	{
    
		/* Check whether the apex is the point we seek. */
		if ((fapex->coords[0] == searchpoint[0]) && (fapex->coords[1] == searchpoint[1])) {
			lprevself(*searchtri);
			return ONVERTEX;
		}
		/* Does the point lie on the other side of the line defined by the */
		/*   triangle edge opposite the triangle's destination?            */
		destorient = counterclockwise(forg->coords, fapex->coords, searchpoint);

		/* Does the point lie on the other side of the line defined by the */
		/*   triangle edge opposite the triangle's origin?                 */
		orgorient = counterclockwise(fapex->coords, fdest->coords, searchpoint);

		if (destorient > 0.0)
		{
			if (orgorient > 0.0) 
			{
				/* Move left if the inner product of (fapex - searchpoint) and  */
				/*   (fdest - forg) is positive.  This is equivalent to drawing */
				/*   a line perpendicular to the line (forg, fdest) and passing */
				/*   through `fapex', and determining which side of this line   */
				/*   `searchpoint' falls on.                                    */
				moveleft = (fapex->coords[0] - searchpoint[0]) * (fdest->coords[0] - forg->coords[0]) +
						   (fapex->coords[1] - searchpoint[1]) * (fdest->coords[1] - forg->coords[1]) > 0.0;
			} else {
				moveleft = 1;
			}
		}else 
		{
			if (orgorient > 0.0) {
			moveleft = 0;
			} else {
			/* The point we seek must be on the boundary of or inside this */
			/*   triangle.                                                 */
			if (destorient == 0.0) {
				lprevself(*searchtri);
				return ONEDGE;
			}
			if (orgorient == 0.0) {
				lnextself(*searchtri);
				return ONEDGE;
			}
			return INTRIANGLE;
			}
		}

		/* Move to another triangle. */
		if (moveleft) {
			lprevself(*searchtri);
			fdest = fapex;
		} else {
			lnextself(*searchtri);
			forg = fapex;
		}

		if (this->checksegments && stopatsubsegment) {
			/* Check for walking through a subsegment. */
			tspivot(*searchtri, checkedge);
			if (checkedge.ss != this->dummysub) {
				return BEYONDSUBSEG;
			}
		}

		/* Check for walking right out of the triangulation. */
		if (right_tri(*searchtri) == this->dummytri) {
			return OUTSIDE;
		}

		symself(*searchtri);

		fapex = searchtri->apex();
	}
}

/*****************************************************************************/
/*                                                                           */
/*  locate()   Find a triangle or edge containing a given point.             */
/*                                                                           */
/*  Searching begins from one of:  the input `searchtri', a recently         */
/*  encountered triangle `recenttri', or from a triangle chosen from a       */
/*  random sample.  The choice is made by determining which triangle's       */
/*  origin is closest to the point we are searching for.  Normally,          */
/*  `searchtri' should be a handle on the convex hull of the triangulation.  */
/*                                                                           */
/*  Details on the random sampling method can be found in the Mucke, Saias,  */
/*  and Zhu paper cited in the header of this code.                          */
/*                                                                           */
/*  On completion, `searchtri' is a triangle that contains `searchpoint'.    */
/*                                                                           */
/*  Returns ONVERTEX if the point lies on an existing vertex.  `searchtri'   */
/*  is a handle whose origin is the existing vertex.                         */
/*                                                                           */
/*  Returns ONEDGE if the point lies on a mesh edge.  `searchtri' is a       */
/*  handle whose primary edge is the edge on which the point lies.           */
/*                                                                           */
/*  Returns INTRIANGLE if the point lies strictly within a triangle.         */
/*  `searchtri' is a handle on the triangle that contains the point.         */
/*                                                                           */
/*  Returns OUTSIDE if the point lies outside the mesh.  `searchtri' is a    */
/*  handle whose primary edge the point is to the right of.  This might      */
/*  occur when the circumcenter of a triangle falls just slightly outside    */
/*  the mesh due to floating-point roundoff error.  It also occurs when      */
/*  seeking a hole or region point that a foolish user has placed outside    */
/*  the mesh.                                                                */
/*                                                                           */
/*  WARNING:  This routine is designed for convex triangulations, and will   */
/*  not generally work after the holes and concavities have been carved.     */
/*                                                                           */
/*****************************************************************************/

struct otri Mesh::Locate(const wertex& searchpoint)
{
	struct otri temptri;
	locate( searchpoint.coords, &temptri);

	return temptri;
}
enum locateresult Mesh::locate( const Point2d& searchpoint, 
								struct otri *searchtri)
{
  struct otri sampletri;
  vertex torg, tdest;
  REAL searchdist, dist, ahead;

  /* Record the distance from the suggested starting triangle to the */
  /*   point we seek.                                                */
  torg = searchtri->org();
  searchdist = (searchpoint[0] - torg->coords[0]) * (searchpoint[0] - torg->coords[0]) +
               (searchpoint[1] - torg->coords[1]) * (searchpoint[1] - torg->coords[1]);

  //Boundary triangle has origin torg;
  

  /* If a recently encountered triangle has been recorded and has not been */
  /*   deallocated, test it as a good starting point.                      */
  if (this->recenttri.tri) {
    if (!deadtri(this->recenttri.tri)) {
      torg = this->recenttri.org();
      if ((torg->coords[0] == searchpoint[0]) && (torg->coords[1] == searchpoint[1])) {
        *searchtri = this->recenttri;
        return ONVERTEX;
      }
      dist = (searchpoint[0] - torg->coords[0]) * (searchpoint[0] - torg->coords[0]) +
             (searchpoint[1] - torg->coords[1]) * (searchpoint[1] - torg->coords[1]);
      if (dist < searchdist) 
	  {
        *searchtri = this->recenttri;
        searchdist = dist;
        //Choosing recent triangle with origin torg  
      }
    }
  }

  /* Where are we? */
  torg = searchtri->org();
  tdest = searchtri->dest();
  /* Check the starting triangle's vertices. */
  if ((torg->coords[0] == searchpoint[0]) && (torg->coords[1] == searchpoint[1])) {
    return ONVERTEX;
  }
  if ((tdest->coords[0] == searchpoint[0]) && (tdest->coords[1] == searchpoint[1])) {
    lnextself(*searchtri);
    return ONVERTEX;
  }
  /* Orient `searchtri' to fit the preconditions of calling preciselocate(). */
  ahead = counterclockwise(torg->coords, tdest->coords, searchpoint);
  if (ahead < 0.0) {
    /* Turn around so that `searchpoint' is to the left of the */
    /*   edge specified by `searchtri'.                        */
	if( right_tri(*searchtri) == this->dummytri ){
		return OUTSIDE;
	}
	else{
		symself(*searchtri);
	}
  } else if (ahead == 0.0) {
    /* Check if `searchpoint' is between `torg' and `tdest'. */
    if (((torg->coords[0] < searchpoint[0]) == (searchpoint[0] < tdest->coords[0])) &&
        ((torg->coords[1] < searchpoint[1]) == (searchpoint[1] < tdest->coords[1]))) {
      return ONEDGE;
    }
  }
  return preciselocate(searchpoint, searchtri, 0);
}

/**                                                                         **/
/**                                                                         **/
/********* Point location routines end here                          *********/

/********* Mesh transformation routines begin here                   *********/
/**                                                                         **/
/**                                                                         **/

/*****************************************************************************/
/*                                                                           */
/*  insertsubseg()   Create a new subsegment and insert it between two       */
/*                   triangles.                                              */
/*                                                                           */
/*  The new subsegment is inserted at the edge described by the handle       */
/*  `tri'.  Its vertices are properly initialized.  The marker `subsegmark'  */
/*  is applied to the subsegment and, if appropriate, its vertices.          */
/*                                                                           */
/*****************************************************************************/

subseg* Mesh::insertsubseg( struct otri *tri,
                  int subsegmark)
{
	struct otri oppotri;
	struct osub newsubseg;
  
	vertex triorg = tri->org();
	vertex tridest= tri->dest();
	/* Mark vertices if possible. */
	if (vertexmark(triorg) == 0) {
		setvertexmark(triorg, subsegmark);
	}
	if (vertexmark(tridest) == 0) {
		setvertexmark(tridest, subsegmark);
	}
	/* Check if there's already a subsegment here. */
	tspivot(*tri, newsubseg);
	if (newsubseg.ss == this->dummysub) 
	{
		/* Make new subsegment and initialize its vertices. */
		makesubseg(&newsubseg);
		setsorg(newsubseg, tridest);
		setsdest(newsubseg, triorg);
		setsegorg(newsubseg, tridest);
		setsegdest(newsubseg, triorg);

		/* Bond new subsegment to the two triangles it is sandwiched between. */
		/*   Note that the facing triangle `oppotri' might be equal to        */
		/*   `dummytri' (outer space), but the new subsegment is bonded to it */
		/*   all the same.                                                    */
		tsbond(*tri, newsubseg);
		sym(*tri, oppotri);
		ssymself(newsubseg);
		tsbond(oppotri, newsubseg);
		setmark(newsubseg, subsegmark);

		// Inserting new

		// Populate next / prev segments pointers on new sub segment for both orientations
		newsubseg.SetNeighborSegs();
		ssymself(newsubseg);
		newsubseg.SetNeighborSegs();
		// precompute unit vec
		newsubseg.ss->setUnitVec();

	} else {
		if (mark(newsubseg) == 0) {
			setmark(newsubseg, subsegmark);
		}
	}

	if( this->searchGraph )
	{
		this->searchDualDirty = true;
	}

	return newsubseg.ss;
}

/*****************************************************************************/
/*                                                                           */
/*  Terminology                                                              */
/*                                                                           */
/*  A "local transformation" replaces a small set of triangles with another  */
/*  set of triangles.  This may or may not involve inserting or deleting a   */
/*  vertex.                                                                  */
/*                                                                           */
/*  The term "casing" is used to describe the set of triangles that are      */
/*  attached to the triangles being transformed, but are not transformed     */
/*  themselves.  Think of the casing as a fixed hollow structure inside      */
/*  which all the action happens.  A "casing" is only defined relative to    */
/*  a single transformation; each occurrence of a transformation will        */
/*  involve a different casing.                                              */
/*                                                                           */
/*****************************************************************************/

/*****************************************************************************/
/*                                                                           */
/*  flip()   Transform two triangles to two different triangles by flipping  */
/*           an edge counterclockwise within a quadrilateral.                */
/*                                                                           */
/*  Imagine the original triangles, abc and bad, oriented so that the        */
/*  shared edge ab lies in a horizontal plane, with the vertex b on the left */
/*  and the vertex a on the right.  The vertex c lies below the edge, and    */
/*  the vertex d lies above the edge.  The `flipedge' handle holds the edge  */
/*  ab of triangle abc, and is directed left, from vertex a to vertex b.     */
/*                                                                           */
/*  The triangles abc and bad are deleted and replaced by the triangles cdb  */
/*  and dca.  The triangles that represent abc and bad are NOT deallocated;  */
/*  they are reused for dca and cdb, respectively.  Hence, any handles that  */
/*  may have held the original triangles are still valid, although not       */
/*  directed as they were before.                                            */
/*                                                                           */
/*  Upon completion of this routine, the `flipedge' handle holds the edge    */
/*  dc of triangle dca, and is directed down, from vertex d to vertex c.     */
/*  (Hence, the two triangles have rotated counterclockwise.)                */
/*                                                                           */
/*  WARNING:  This transformation is geometrically valid only if the         */
/*  quadrilateral adbc is convex.  Furthermore, this transformation is       */
/*  valid only if there is not a subsegment between the triangles abc and    */
/*  bad.  This routine does not check either of these preconditions, and     */
/*  it is the responsibility of the calling routine to ensure that they are  */
/*  met.  If they are not, the streets shall be filled with wailing and      */
/*  gnashing of teeth.                                                       */
/*                                                                           */
/*****************************************************************************/

void Mesh::flip(struct otri *flipedge)
{
	struct otri botleft, botright;
	struct otri topleft, topright;
	struct otri top;
	struct otri botlcasing, botrcasing;
	struct otri toplcasing, toprcasing;
	struct osub botlsubseg, botrsubseg;
	struct osub toplsubseg, toprsubseg;
  
	/* Identify the vertices of the quadrilateral. */
	vertex rightvertex= flipedge->org();
	vertex leftvertex = flipedge->dest();
	vertex botvertex  = flipedge->apex();
	sym( *flipedge, top);

#ifdef SELF_CHECK
	if (top.tri == this->dummytri)
		throw TriangleExc_Tri( "Attempt to flip on hull.", *flipedge);

	if (this->checksegments){
		tspivot(*flipedge, toplsubseg);
		if (toplsubseg.ss != this->dummysub)
			throw TriangleExc_Tri( "Attempt to flip on seg.", *flipedge);
	}
#endif /* SELF_CHECK */
	vertex farvertex = top.apex();

	/* Identify the casing of the quadrilateral. */
	lprev(top, topleft);
	lnext(top, topright);
	lnext(*flipedge, botleft);
	lprev(*flipedge, botright);
	sym(topleft, toplcasing);
	sym(topright, toprcasing);
	sym(botleft, botlcasing);
	sym(botright, botrcasing);

	/* Rotate the quadrilateral one-quarter turn counterclockwise. */
	bond(topleft,  botlcasing);
	bond(botleft,  botrcasing);
	bond(botright, toprcasing);
	bond(topright, toplcasing);

	if (this->checksegments) {
		/* Check for subsegments and rebond them to the quadrilateral. */
		tspivot(topleft,  toplsubseg);
		tspivot(botleft,  botlsubseg);
		tspivot(botright, botrsubseg);
		tspivot(topright, toprsubseg);

		if (toplsubseg.ss == this->dummysub)
			tsdissolve(topright);
		else
			tsbond(topright, toplsubseg);

		if (botlsubseg.ss == this->dummysub)
			tsdissolve(topleft);
		else
			tsbond(topleft, botlsubseg);
			
		if (botrsubseg.ss == this->dummysub)
			tsdissolve(botleft);
		else
			tsbond(botleft, botrsubseg);

		if (toprsubseg.ss == this->dummysub)
			tsdissolve(botright);
		else
			tsbond(botright, toprsubseg);
	}
	
	/* New vertex assignments for the rotated quadrilateral. */
	flipedge->setorg(farvertex);
	flipedge->setdest(botvertex);
	flipedge->setapex(rightvertex);
	top.setorg(botvertex);
	top.setdest(farvertex);
	top.setapex(leftvertex);

	setvertex2tri(farvertex,*flipedge);
	setvertex2tri_orient(rightvertex,*flipedge,minus1mod3[flipedge->orient]);

	setvertex2tri(botvertex,top);
	setvertex2tri_orient(leftvertex,top,minus1mod3[top.orient]);
}


int Mesh::repairDelaunay(otri& starttri){
	/* Circles around vert, checks Delaunay property of triangles and flip edges if needed.

	   Given  a triangle with org starttri.org, perform two types of Delaunay property checks:
	    - one against the previous neighboring triangle as we rotate around org
		- one against the triangle opposite to org.
	*/

	bool clockwisepass = true;
	 
	otri countingtri = starttri;
	vertex orgvert = starttri.org(); // vertex we circle around.
	
	int numFlips = 0;
	vertex temporg, tempdes, tempapx;
	otri oppotri;
	osub tempsub;

	// keep track of previously flipped triangle to avoid looping infinitely in pathological cases
	// typically with low floating precision (noexact=true flag)
	otri prevFlip; 

	do{

		// Stop if we have found dummytri on both sides or if we have circled around.
		if( countingtri.tri == this->dummytri )
		{
			if( clockwisepass ){
				clockwisepass = false;
				countingtri = starttri;
			}else{
				break;
			}
		}
		
		assert( countingtri.org() == orgvert );

		dprev(countingtri,oppotri);
		tspivot(oppotri,tempsub);

		if( tempsub.ss == this->dummysub && oppotri.tri != this->dummytri && oppotri.tri != prevFlip.tri )
		{// prevFlip is used to keep track of previously flipped tri so we don't check it again.
			temporg = oppotri.org();
			tempdes = oppotri.dest();
			tempapx = oppotri.apex();

	  		/* Check Delaunay in-circle condition with respect to respect on the opposite side of orgvert. */
			if( isdirect(  orgvert->coords, tempdes->coords, tempapx->coords) && 
				isdirect(  temporg->coords,	orgvert->coords, tempapx->coords) && 
				nonregular(temporg->coords, tempdes->coords, tempapx->coords, orgvert->coords) > 0. ){
				  
				// flip
				flip(&oppotri);
				numFlips++;
				// prevnewtriangle has been affected by the flip.
				// make sure it still points to the right thing
				bool flippedStarttri = starttri.tri == countingtri.tri;
				
				prevFlip = countingtri;

				if(clockwisepass){
					countingtri = oppotri;
				} else { // flipping took us back so make sure we keep going forward
					oprev(oppotri,countingtri);
				}
				if( flippedStarttri )
					starttri = countingtri;
				
				assert( countingtri.org() == orgvert );
				continue; // after a flip, skip iterating so we test again countingtri which has just been modified
			}
		}

		if( countingtri.tri!=prevFlip.tri && countingtri.tri!=starttri.tri )
		{
			temporg = countingtri.org();
			tempdes = countingtri.dest();
			tempapx = countingtri.apex();

			otri prevtri;
			bool hasflipped = false;
			if(clockwisepass)
			{
				onext(countingtri, prevtri);
				tspivot(prevtri, tempsub);

				if( tempsub.ss == this->dummysub )
				{
					vertex othervert = prevtri.apex();

					if( isdirect(  orgvert->coords,tempdes->coords,othervert->coords) && 
						nonregular(temporg->coords,tempdes->coords,tempapx->coords,othervert->coords) > 0.0 )
					{
						flip(&prevtri);
						prevFlip = countingtri;
						numFlips++;
						lprev(prevtri,countingtri);
						hasflipped = true;
					}
				}
			}
			else
			{
				tspivot(countingtri, tempsub);

				if( tempsub.ss == this->dummysub )
				{
					oprev(countingtri, prevtri);
					vertex othervert = prevtri.dest();

					if( isdirect(  orgvert->coords,othervert->coords,tempapx->coords) && 
						nonregular(temporg->coords,  tempdes->coords,tempapx->coords,othervert->coords) > 0.0 )
					{
						flip(&countingtri);
						prevFlip = countingtri;
						numFlips++;
						lprevself(countingtri);
						hasflipped = true;
					}
				}
			}

			if(hasflipped)
			{
				if( prevtri.tri == starttri.tri)
				{ // starttri was affected by the flip, make sure we adjust it.
					starttri = countingtri;
				}
				assert( starttri.org() == orgvert);
				continue;// after a flip, skip iterating so we test again countingtri which has just been modified
			}
		
		}
		
		if( clockwisepass ){
			oprevself(countingtri);
		}else{
			onextself(countingtri);
		}

	//Stop when we have circled back to the starting tri.
	}while( countingtri.tri != starttri.tri );

	return numFlips;
}

/*****************************************************************************/
/*                                                                           */
/*  insertvertex()   Insert a vertex into a Delaunay triangulation,          */
/*                   performing flips as necessary to maintain the Delaunay  */
/*                   property.                                               */
/*                                                                           */
/*  The point `insertvertex' is located.  If `searchtri.tri' is not nullptr,    */
/*  the search for the containing triangle begins from `searchtri'.  If      */
/*  `searchtri.tri' is nullptr, a full point location procedure is called.      */
/*  If `insertvertex' is found inside a triangle, the triangle is split into */
/*  three; if `insertvertex' lies on an edge, the edge is split in two,      */
/*  thereby splitting the two adjacent triangles into four.  Edge flips are  */
/*  used to restore the Delaunay property.  If `insertvertex' lies on an     */
/*  existing vertex, no action is taken, and the value DUPLICATEVERTEX is    */
/*  returned.  On return, `searchtri' is set to a handle whose origin is the */
/*  existing vertex.                                                         */
/*                                                                           */
/*  Normally, the parameter `splitseg' is set to nullptr, implying that no      */
/*  subsegment should be split.  In this case, if `insertvertex' is found to */
/*  lie on a segment, no action is taken, and the value VIOLATINGVERTEX is   */
/*  returned.  On return, `searchtri' is set to a handle whose primary edge  */
/*  is the violated subsegment.                                              */
/*                                                                           */
/*  If the calling routine wishes to split a subsegment by inserting a       */
/*  vertex in it, the parameter `splitseg' should be that subsegment.  In    */
/*  this case, `searchtri' MUST be the triangle handle reached by pivoting   */
/*  from that subsegment; no point location is done.                         */
/*                                                                           */
/*  If a duplicate vertex or violated segment does not prevent the vertex    */
/*  from being inserted, the return value will be ENCROACHINGVERTEX if the   */
/*  vertex encroaches upon a subsegment (and checking is enabled), or        */
/*  SUCCESSFULVERTEX otherwise.  In either case, `searchtri' is set to a     */
/*  handle whose origin is the newly inserted vertex.                        */
/*                                                                           */
/*  insertvertex() does not use flip() for reasons of speed; some            */
/*  information can be reused from edge flip to edge flip, like the          */
/*  locations of subsegments.                                                */
/*                                                                           */
/*****************************************************************************/

enum insertvertexresult Mesh::insertvertex(	wertex* newvertex,
											struct otri *searchtri,
											struct osub *splitseg)
{
  struct otri horiz;
  struct otri top;
  struct otri botleft, botright;
  struct otri topleft, topright;
  struct otri newbotleft, newbotright;
  struct otri newtopright;
  struct otri botlcasing, botrcasing;
  struct otri toplcasing, toprcasing;
  struct otri testtri;
  struct osub botlsubseg, botrsubseg;
  struct osub toplsubseg, toprsubseg;
  struct osub brokensubseg;
  struct osub checksubseg;
  struct osub rightsubseg;
  struct osub newsubseg;
  
  vertex first;
  vertex leftvertex, rightvertex, botvertex, topvertex, farvertex;
  vertex segmentorg, segmentdest;

  enum insertvertexresult success;
  enum locateresult intersect;
  int doflip;
  int mirrorflag;
  
  if (splitseg == nullptr) {
    /* Find the location of the vertex to be inserted.  */
	/* Search for a triangle containing `newvertex'. */
	if( searchtri )
		horiz = *searchtri;

	intersect = preciselocate(newvertex->coords, &horiz, 0);

  } else {
    /* The calling routine provides the subsegment in which */
    /*   the vertex is inserted.                             */
    horiz = *searchtri;
    intersect = ONEDGE;
  }

  if (intersect == ONVERTEX) {
    /* There's already a vertex there.  Return in `searchtri' a triangle */
    /*   whose origin is the existing vertex.                            */
    *searchtri = horiz;
    this->recenttri = horiz;
    return DUPLICATEVERTEX;
  }
  else if (intersect == OUTSIDE) {

	/*
     * The vertex falls outside the convex hull
	 */
	  otri newtriangle;
	  otri temptri;
	  osub tempsub;
	  otri prevnewtriangle(this->dummytri);
	  vertex temporg, tempdest;
	  
	  int createdtris = 0;

	  temptri = horiz;

	  bool checkingleft = true;
	  

	  while( true )
	  {						
		  // Check both sides of horiz along the convex hull for extra triangles to create.
		  if( checkingleft )
		  {
			  while( right_tri(temptri) != this->dummytri){
				oprevself(temptri);
			  }
		  }
		  else
		  {
			  while(left_tri(temptri) != this->dummytri){
				  onextself(temptri);
			  }
			  lprevself(temptri);
		  }

		  temporg = temptri.org();
		  tempdest = temptri.dest();

		  if(prodvect(tempdest->coords, temporg->coords, newvertex->coords) > 0)
		  {
				/* temptri is seen from newvertex so we need to add a triangle */
				maketriangle(&newtriangle);

				newtriangle.setorg(tempdest);
				newtriangle.setdest(temporg);
				newtriangle.setapex(newvertex);
				bond(temptri,newtriangle);

				// Check segment on the common side between existing and new triangle
				tspivot(temptri, tempsub);
				if( tempsub.ss != this->dummysub )
				{
					ssymself(tempsub);
					tsbond(newtriangle,tempsub);
				}

				if( createdtris == 0 )
				{   
					setvertex2tri_orient(newvertex,newtriangle,2);
					*searchtri = newtriangle; // point searchtri to a valid triangle
					this->recenttri = horiz;  // point recentri to a valid triangle
				}

				createdtris++;

				/* Bond to existing triangles to the right or left existing triangle */ 
				/* and adjust next step starting position */

				if( checkingleft )
				{
					lnextself(newtriangle);
					bond(newtriangle, prevnewtriangle);

					lnext(newtriangle, prevnewtriangle);
					lnextself(temptri);
#ifdef SELF_CHECK
					assert( prevnewtriangle.org() == newvertex );
#endif
						
				}else
				{
#ifdef SELF_CHECK
					assert( prevnewtriangle.dest() == newvertex );
#endif
					lprevself(newtriangle);
					bond(newtriangle, prevnewtriangle);
					lprev(newtriangle, prevnewtriangle);
				}
			  
		  }
		  else
		  {
			  if( !checkingleft ){
				  break;
			  }else
			  {
				  /* Go through other side of the convex hull, start walking to the left of horiz */
				  checkingleft = false;
				  //prevnewtriangle.tri->otris[prevnewtriangle.orient].tri = this->dummytri; // Bond to dummytri needed or implicit?
				  temptri = horiz;
				  oprev(horiz,prevnewtriangle);
			  }
		  }
	  }

	  /* Do flips on triangles added in previous step to enforce Delaunay property. */
	  	  
	  temptri = newvertex->otri_;
	  assert(temptri.org()==newvertex);
	  repairDelaunay(temptri);

	  return SUCCESSFULVERTEX;
  }
  else if (intersect == ONEDGE)
  {

	/*
     * The vertex falls on an edge or boundary.
	 */

    if (this->checksegments && (splitseg == nullptr)) {
      /* Check whether the vertex falls on a subsegment. */
      tspivot(horiz, brokensubseg);
      if (brokensubseg.ss != this->dummysub) {
        /* The vertex falls on a subsegment, and hence will not be inserted. */
        
        /* Return a handle whose primary edge contains the vertex, */
        /*   which has not been inserted.                          */
        *searchtri = horiz;
        this->recenttri = horiz;
        return VIOLATINGVERTEX;
      }
    }

    /* Insert the vertex on an edge, dividing one triangle into two (if */
    /*   the edge lies on a boundary) or two triangles into four.       */
    lprev(horiz, botright);
    sym(botright, botrcasing);
    sym(horiz, topright);
    /* Is there a second triangle?  (Or does this edge lie on a boundary?) */
    mirrorflag = topright.tri != this->dummytri;
    if (mirrorflag) {
      lnextself(topright);
      sym(topright, toprcasing);
      maketriangle(&newtopright);
    } else {
      /* Splitting a boundary edge increases the number of boundary edges. */
      //this->hullsize++;
    }
    maketriangle(&newbotright);

    /* Set the vertices of changed and new triangles. */
    rightvertex = horiz.org();
    leftvertex  = horiz.dest();
    botvertex   = horiz.apex();
    newbotright.setorg(botvertex);
	setvertex2tri(botvertex,newbotright);
    newbotright.setdest(rightvertex);
	setvertex2tri_orient(rightvertex,newbotright,plus1mod3[newbotright.orient]);
    newbotright.setapex(newvertex);

    horiz.setorg(newvertex);
	setvertex2tri(newvertex, horiz);

    if (mirrorflag) {
      topvertex = topright.dest();
      newtopright.setorg(rightvertex);
	  setvertex2tri(rightvertex, newtopright);

	   newtopright.setdest(topvertex);
	  setvertex2tri_orient(topvertex, newtopright,plus1mod3[newtopright.orient]);

      newtopright.setapex(newvertex);
      topright.setorg(newvertex);

#ifdef SELF_CHECK

	  if (mirrorflag) {
		  if (counterclockwise(leftvertex->coords, rightvertex->coords, topvertex->coords) < 0.0) {
			  throw TriangleExc("Clockwise triangle prior to edge vertex insertion (top).\n");
		  }
		  if (counterclockwise(rightvertex->coords, topvertex->coords, newvertex->coords) < 0.0) {
			  throw TriangleExc("Clockwise triangle after edge vertex insertion (top right).\n");
		  }
		  if (counterclockwise(topvertex->coords, leftvertex->coords, newvertex->coords) < 0.0) {
			  throw TriangleExc("Clockwise triangle after edge vertex insertion (top left).\n");
		  }
	  }
#endif //SELF_CHECK
    }

    /* There may be subsegments that need to be bonded */
    /*   to the new triangle(s).                       */
    if (this->checksegments) {
      tspivot(botright, botrsubseg);
      if (botrsubseg.ss != this->dummysub) {
        tsdissolve(botright);
        tsbond(newbotright, botrsubseg);
      }
      if (mirrorflag) {
        tspivot(topright, toprsubseg);
        if (toprsubseg.ss != this->dummysub) {
          tsdissolve(topright);
          tsbond(newtopright, toprsubseg);
        }
      }
    }

    /* Bond the new triangle(s) to the surrounding triangles. */
    bond(newbotright, botrcasing);
    lprevself(newbotright);
    bond(newbotright, botright);
    lprevself(newbotright);
    if (mirrorflag) {
      bond(newtopright, toprcasing);
      lnextself(newtopright);
      bond(newtopright, topright);
      lnextself(newtopright);
      bond(newtopright, newbotright);
    }

    if (splitseg) {
      /* Split the subsegment into two. */
      setsdest(*splitseg, newvertex);
      segorg(*splitseg, segmentorg);
      segdest(*splitseg, segmentdest);
      ssymself(*splitseg);
      spivot(*splitseg, rightsubseg);
      insertsubseg(&newbotright, mark(*splitseg));
      tspivot(newbotright, newsubseg);
      setsegorg(newsubseg, segmentorg);
      setsegdest(newsubseg, segmentdest);
      sbond(*splitseg, newsubseg);
      ssymself(newsubseg);
      sbond(newsubseg, rightsubseg);
      ssymself(*splitseg);
      /* Transfer the subsegment's boundary marker to the vertex */
      /*   if required.                                          */
      if (vertexmark(newvertex) == 0) {
        setvertexmark(newvertex, mark(*splitseg));
      }
    }

#ifdef SELF_CHECK
	if (counterclockwise(rightvertex->coords, leftvertex->coords, botvertex->coords) < 0.0) {
	  throw TriangleExc("Clockwise triangle prior to edge vertex insertion (bottom).\n");
    }
    if (counterclockwise(leftvertex->coords, botvertex->coords, newvertex->coords) < 0.0) {
      throw TriangleExc("Clockwise triangle after edge vertex insertion (bottom left).\n");
    }
    if (counterclockwise(botvertex->coords, rightvertex->coords, newvertex->coords) < 0.0) {
      throw TriangleExc("Clockwise triangle after edge vertex insertion (bottom right).\n");
    }
#endif /* SELF_CHECK */
    
    /* Position `horiz' on the first edge to check for */
    /*   the Delaunay property.                        */
    lnextself(horiz);

  } else {

	/*
    * Insert the vertex in a triangle, splitting it into three.	
	*/

    lnext(horiz, botleft);
    lprev(horiz, botright);
    sym(botleft, botlcasing);
    sym(botright, botrcasing);
    maketriangle(&newbotleft);
    maketriangle(&newbotright);

    /* Set the vertices of changed and new triangles. */
    rightvertex = horiz.org();
    leftvertex = horiz.dest();
    botvertex = horiz.apex();
    newbotleft.setorg(leftvertex);
	setvertex2tri(leftvertex, newbotleft);

    newbotleft.setdest(botvertex);
	setvertex2tri_orient(botvertex,newbotleft,plus1mod3[newbotleft.orient]);

    newbotleft.setapex(newvertex);
	setvertex2tri_orient(newvertex,newbotleft,minus1mod3[newbotleft.orient]);

    newbotright.setorg(botvertex);
    newbotright.setdest(rightvertex);
	setvertex2tri_orient(rightvertex, newbotright,plus1mod3[newbotright.orient]);

    newbotright.setapex(newvertex);
    horiz.setapex(newvertex);

    /* There may be subsegments that need to be bonded */
    /*   to the new triangles.                         */
    if (this->checksegments) {
      tspivot(botleft, botlsubseg);
      if (botlsubseg.ss != this->dummysub) {
        tsdissolve(botleft);
        tsbond(newbotleft, botlsubseg);
      }
      tspivot(botright, botrsubseg);
      if (botrsubseg.ss != this->dummysub) {
        tsdissolve(botright);
        tsbond(newbotright, botrsubseg);
      }
    }

    /* Bond the new triangles to the surrounding triangles. */
    bond(newbotleft, botlcasing);
    bond(newbotright, botrcasing);
    lnextself(newbotleft);
    lprevself(newbotright);
    bond(newbotleft, newbotright);
    lnextself(newbotleft);
    bond(botleft, newbotleft);
    lprevself(newbotright);
    bond(botright, newbotright);

#ifdef SELF_CHECK
    if (counterclockwise(rightvertex->coords, leftvertex->coords, botvertex->coords) < 0.0) {
      throw TriangleExc("Clockwise triangle prior to vertex insertion.");
    }
    if (counterclockwise(rightvertex->coords, leftvertex->coords, newvertex->coords) < 0.0) {
		std::ostringstream os;
		os << "  Clockwise triangle after vertex insertion (top).\n" << *rightvertex << *leftvertex << *botvertex << *newvertex ;
		throw TriangleExc( os.str().c_str() );
      
    }
    if (counterclockwise(leftvertex->coords, botvertex->coords, newvertex->coords) < 0.0) {
      throw TriangleExc("Clockwise triangle after vertex insertion (left).");
    }
    if (counterclockwise(botvertex->coords, rightvertex->coords, newvertex->coords) < 0.0) {
      throw TriangleExc("Clockwise triangle after vertex insertion (right).");
    }
#endif /* SELF_CHECK */
    if (b.verbose > 2) {
      printf("  Updating top ");
      //printtriangle(&horiz);
      printf("  Creating left ");
      //printtriangle(&newbotleft);
      printf("  Creating right ");
      //printtriangle(&newbotright);
    }
  }

  /* The insertion is successful by default, unless an encroached */
  /*   subsegment is found.                                       */
  success = SUCCESSFULVERTEX;
  /* Circle around the newly inserted vertex, checking each edge opposite */
  /*   it for the Delaunay property.  Non-Delaunay edges are flipped.     */
  /*   `horiz' is always the edge being checked.  `first' marks where to  */
  /*   stop circling.                                                     */
  first = horiz.org();
  rightvertex = first;
  leftvertex = horiz.dest();
  /* Circle until finished. */
  while (1) {
    /* By default, the edge will be flipped. */
    doflip = 1;

    if (this->checksegments) {
      /* Check for a subsegment, which cannot be flipped. */
      tspivot(horiz, checksubseg);
      if (checksubseg.ss != this->dummysub) {
        /* The edge is a subsegment and cannot be flipped. */
        doflip = 0;
      }
    }

    if (doflip) {
      /* Check if the edge is a boundary edge. */
      sym(horiz, top);
      if (top.tri == this->dummytri) {
        /* The edge is a boundary edge and cannot be flipped. */
        doflip = 0;
      } else {
        /* Find the vertex on the other side of the edge. */
        farvertex = top.apex();
        /* In the incremental Delaunay triangulation algorithm, any of      */
        /*   `leftvertex', `rightvertex', and `farvertex' could be vertices */
        /*   of the triangular bounding box.  These vertices must be        */
        /*   treated as if they are infinitely distant, even though their   */
        /*   "coordinates" are not.                                         */
        if ((leftvertex == &this->infvertex1) || (leftvertex == &this->infvertex2) ||
            (leftvertex == &this->infvertex3)) {
          /* `leftvertex' is infinitely distant.  Check the convexity of  */
          /*   the boundary of the triangulation.  'farvertex' might be   */
          /*   infinite as well, but trust me, this same condition should */
          /*   be applied.                                                */
          doflip = counterclockwise(newvertex->coords, rightvertex->coords, farvertex->coords)
                   > 0.0;
        } else if ((rightvertex == &this->infvertex1) ||
                   (rightvertex == &this->infvertex2) ||
                   (rightvertex == &this->infvertex3)) {
          /* `rightvertex' is infinitely distant.  Check the convexity of */
          /*   the boundary of the triangulation.  'farvertex' might be   */
          /*   infinite as well, but trust me, this same condition should */
          /*   be applied.                                                */
          doflip = counterclockwise(farvertex->coords, leftvertex->coords, newvertex->coords)
                   > 0.0;
        } else if ((farvertex == &this->infvertex1) ||
                   (farvertex == &this->infvertex2) ||
                   (farvertex == &this->infvertex3)) {
          /* `farvertex' is infinitely distant and cannot be inside */
          /*   the circumcircle of the triangle `horiz'.            */
          doflip = 0;
        } else {
          /* Test whether the edge is locally Delaunay. */
          doflip = nonregular(leftvertex->coords, newvertex->coords, rightvertex->coords,
                            farvertex->coords) > 0.0;
        }
        if (doflip) {
          /* We made it!  Flip the edge `horiz' by rotating its containing */
          /*   quadrilateral (the two triangles adjacent to `horiz').      */
          /* Identify the casing of the quadrilateral. */
          lprev(top, topleft);
          sym(topleft, toplcasing);
          lnext(top, topright);
          sym(topright, toprcasing);
          lnext(horiz, botleft);
          sym(botleft, botlcasing);
          lprev(horiz, botright);
          sym(botright, botrcasing);
          /* Rotate the quadrilateral one-quarter turn counterclockwise. */
          bond(topleft, botlcasing);
          bond(botleft, botrcasing);
          bond(botright, toprcasing);
          bond(topright, toplcasing);
          if (this->checksegments) {
            /* Check for subsegments and rebond them to the quadrilateral. */
            tspivot(topleft, toplsubseg);
            tspivot(botleft, botlsubseg);
            tspivot(botright, botrsubseg);
            tspivot(topright, toprsubseg);
            if (toplsubseg.ss == this->dummysub) {
              tsdissolve(topright);
            } else {
              tsbond(topright, toplsubseg);
            }
            if (botlsubseg.ss == this->dummysub) {
              tsdissolve(topleft);
            } else {
              tsbond(topleft, botlsubseg);
            }
            if (botrsubseg.ss == this->dummysub) {
              tsdissolve(botleft);
            } else {
              tsbond(botleft, botrsubseg);
            }
            if (toprsubseg.ss == this->dummysub) {
              tsdissolve(botright);
            } else {
              tsbond(botright, toprsubseg);
            }
          }
          /* New vertex assignments for the rotated quadrilateral. */
          horiz.setorg(farvertex);
          horiz.setdest(newvertex);
          horiz.setapex(rightvertex);
          top.setorg(newvertex);
          top.setdest(farvertex);
          top.setapex(leftvertex);
		  
		  setvertex2tri(farvertex,horiz);
		  setvertex2tri(newvertex,top);
		  setvertex2tri_orient(rightvertex,horiz,minus1mod3[horiz.orient]);
		  setvertex2tri_orient(leftvertex,top,minus1mod3[top.orient]);

#ifdef SELF_CHECK
          if (newvertex) {
            if (counterclockwise(leftvertex->coords, newvertex->coords, rightvertex->coords) <
                0.0) {
              throw TriangleExc("Clockwise triangle prior to edge flip (bottom).\n");
            }
            /* The following test has been removed because constrainededge() */
            /*   sometimes generates inverted triangles that insertvertex()  */
            /*   removes.                                                    */
/*
            if (counterclockwise(rightvertex, farvertex, leftvertex) <
                0.0) {
              printf("Internal error in insertvertex():\n");
              printf("  Clockwise triangle prior to edge flip (top).\n");
            }
*/
            if (counterclockwise(farvertex->coords, leftvertex->coords, newvertex->coords) <
                0.0) {
              throw TriangleExc("Clockwise triangle after edge flip (left).");
            }
            if (counterclockwise(newvertex->coords, rightvertex->coords, farvertex->coords) <
                0.0) {
              throw TriangleExc("Clockwise triangle after edge flip (right).");
            }
          }
#endif /* SELF_CHECK */
          if (b.verbose > 2) {
            printf("  Edge flip results in left ");
            lnextself(topleft);
            //printtriangle(&topleft);
            printf("  and right ");
            //printtriangle(&horiz);
          }
          /* On the next iterations, consider the two edges that were  */
          /*   exposed (this is, are now visible to the newly inserted */
          /*   vertex) by the edge flip.                               */
          lprevself(horiz);
          leftvertex = farvertex;
        }
      }
    }
    if (!doflip) {
      /* The handle `horiz' is accepted as locally Delaunay. */

      /* Look for the next edge around the newly inserted vertex. */
      lnextself(horiz);
      sym(horiz, testtri);
      /* Check for finishing a complete revolution about the new vertex, or */
      /*   falling outside  of the triangulation.  The latter will happen   */
      /*   when a vertex is inserted at a boundary.                         */
      if ((leftvertex == first) || (testtri.tri == this->dummytri)) {
        /* We're done.  Return a triangle whose origin is the new vertex. */
        lnext(horiz, *searchtri);
        lnext(horiz, this->recenttri);
        return success;
      }
      /* Finish finding the next edge around the newly inserted vertex. */
      lnext(testtri, horiz);
      rightvertex = leftvertex;
      leftvertex = horiz.dest();
    }
  }
}

/*****************************************************************************/
/*                                                                           */
/*  triangulatepolygon()   Find the Delaunay triangulation of a polygon that */
/*                         has a certain "nice" shape.  This includes the    */
/*                         polygons that result from deletion of a vertex or */
/*                         insertion of a segment.                           */
/*                                                                           */
/*  This is a conceptually difficult routine.  The starting assumption is    */
/*  that we have a polygon with n sides.  n - 1 of these sides are currently */
/*  represented as edges in the mesh.  One side, called the "base", need not */
/*  be.                                                                      */
/*                                                                           */
/*  Inside the polygon is a structure I call a "fan", consisting of n - 1    */
/*  triangles that share a common origin.  For each of these triangles, the  */
/*  edge opposite the origin is one of the sides of the polygon.  The        */
/*  primary edge of each triangle is the edge directed from the origin to    */
/*  the destination; note that this is not the same edge that is a side of   */
/*  the polygon.  `firstedge' is the primary edge of the first triangle.     */
/*  From there, the triangles follow in counterclockwise order about the     */
/*  polygon, until `lastedge', the primary edge of the last triangle.        */
/*  `firstedge' and `lastedge' are probably connected to other triangles     */
/*  beyond the extremes of the fan, but their identity is not important, as  */
/*  long as the fan remains connected to them.                               */
/*                                                                           */
/*  Imagine the polygon oriented so that its base is at the bottom.  This    */
/*  puts `firstedge' on the far right, and `lastedge' on the far left.       */
/*  The right vertex of the base is the destination of `firstedge', and the  */
/*  left vertex of the base is the apex of `lastedge'.                       */
/*                                                                           */
/*  The challenge now is to find the right sequence of edge flips to         */
/*  transform the fan into a Delaunay triangulation of the polygon.  Each    */
/*  edge flip effectively removes one triangle from the fan, committing it   */
/*  to the polygon.  The resulting polygon has one fewer edge.  If `doflip'  */
/*  is set, the final flip will be performed, resulting in a fan of one      */
/*  (useless?) triangle.  If `doflip' is not set, the final flip is not      */
/*  performed, resulting in a fan of two triangles, and an unfinished        */
/*  triangular polygon that is not yet filled out with a single triangle.    */
/*  On completion of the routine, `lastedge' is the last remaining triangle, */
/*  or the leftmost of the last two.                                         */
/*                                                                           */
/*  Although the flips are performed in the order described above, the       */
/*  decisions about what flips to perform are made in precisely the reverse  */
/*  order.  The recursive triangulatepolygon() procedure makes a decision,   */
/*  uses up to two recursive calls to triangulate the "subproblems"          */
/*  (polygons with fewer edges), and then performs an edge flip.             */
/*                                                                           */
/*  The "decision" it makes is which vertex of the polygon should be         */
/*  connected to the base.  This decision is made by testing every possible  */
/*  vertex.  Once the best vertex is found, the two edges that connect this  */
/*  vertex to the base become the bases for two smaller polygons.  These     */
/*  are triangulated recursively.  Unfortunately, this approach can take     */
/*  O(n^2) time not only in the worst case, but in many common cases.  It's  */
/*  rarely a big deal for vertex deletion, where n is rarely larger than     */
/*  ten, but it could be a big deal for segment insertion, especially if     */
/*  there's a lot of long segments that each cut many triangles.  I ought to */
/*  code a faster algorithm some day.                                        */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

void Mesh::triangulatepolygon(
                        struct otri *firstedge, struct otri *lastedge,
                        int edgecount, int doflip)
{
  struct otri testtri;
  struct otri besttri;
  struct otri tempedge;
  vertex leftbasevertex, rightbasevertex;
  vertex testvertex;
  vertex bestvertex;
  int bestnumber;
  int i;

  /* Identify the base vertices. */
  leftbasevertex = lastedge->apex();
  rightbasevertex = firstedge->dest();
  if (b.verbose > 2) {
    printf("  Triangulating interior polygon at edge\n");
    printf("    (%.12g, %.12g) (%.12g, %.12g)\n", leftbasevertex->coords[0],
           leftbasevertex->coords[1], rightbasevertex->coords[0], rightbasevertex->coords[1]);
  }
  /* Find the best vertex to connect the base to. */
  onext(*firstedge, besttri);
  bestvertex = besttri.dest();
  testtri = besttri;
  bestnumber = 1;
  for (i = 2; i <= edgecount - 2; i++) {
    onextself(testtri);
    testvertex = testtri.dest();
    /* Is this a better vertex? */
    if (nonregular(leftbasevertex->coords, rightbasevertex->coords, bestvertex->coords,
                 testvertex->coords) > 0.0) {
      besttri = testtri;
      bestvertex = testvertex;
      bestnumber = i;
    }
  }
  if (b.verbose > 2) {
    printf("    Connecting edge to (%.12g, %.12g)\n", bestvertex->coords[0],
           bestvertex->coords[1]);
  }
  if (bestnumber > 1) {
    /* Recursively triangulate the smaller polygon on the right. */
    oprev(besttri, tempedge);
    triangulatepolygon(firstedge, &tempedge, bestnumber + 1, 1);
  }
  if (bestnumber < edgecount - 2) {
    /* Recursively triangulate the smaller polygon on the left. */
    sym(besttri, tempedge);
    triangulatepolygon(&besttri, lastedge, edgecount - bestnumber, 1);
    /* Find `besttri' again; it may have been lost to edge flips. */
    sym(tempedge, besttri);
  }
  if (doflip) {
    /* Do one final edge flip. */
    flip(&besttri);
  }
  /* Return the base triangle. */
  *lastedge = besttri;
}

/*****************************************************************************/
/*                                                                           */
/*  deletevertex()   Delete a vertex from a Delaunay triangulation, ensuring */
/*                   that the triangulation remains Delaunay.                */
/*                                                                           */
/*  The origin of `deltri' is deleted.  The union of the triangles adjacent  */
/*  to this vertex is a polygon, for which the Delaunay triangulation is     */
/*  found.  Two triangles are removed from the mesh.                         */
/*                                                                           */
/*  GG: At the end deltri is set to a valid remaining triangle or dummytri.  */
/*                                                                           */
/*****************************************************************************/

void Mesh::deleteVertex(const wertex& delvertex,bool resetDual)
{
	if( delvertex.index < 0 )
		return;

	if( resetDual && searchGraph && segmentend( &delvertex ) )
	{
		searchDual->deleteVertex( *delvertex.dualVert );
		searchDual->resetConvexAreas();
	}

	struct otri deltri = delvertex.getTri();
	deletevertex( &deltri, true );
	
}


void Mesh::deletevertex( struct otri * deltri, bool dealloc, std::vector<wertex*>* segVerts )
{
  // Return information about one remaining triangle after deletion
  // to help with location of a moving vertex 
  struct otri countingtri;
  struct otri firstedge, lastedge;
  struct otri deltriright;
  struct otri lefttri, righttri;
  struct otri leftcasing, rightcasing;
  struct osub leftsubseg, rightsubseg;
  vertex delvertex;
  vertex neworg;
  
  delvertex = deltri->org();
  if (b.verbose > 1) {
    printf("  Deleting (%.12g, %.12g).\n", delvertex->coords[0], delvertex->coords[1]);
  }
  
  struct otri temptri;

  int degree = 1;
  int convexdegree = 0;
  countingtri = *deltri;

  struct otri prevtri = *deltri;
  bool clockwisepass = true; // start off with a clock-wise pass
  
  struct otri firstconvextri;

  struct otri rightmosttri;
  bool nomoreconvexstretch = false;
  osub tempsub;
  osub tempsub2;

  while ( true ) {
	// Stop if we have circled round.
	// Don't stop if we reach deltri again because we found a boundary and have switched circling direction.
	  tsapex( countingtri, tempsub );
	  if( tempsub.ss != this->dummysub )
	  {
		if( segVerts ){
			segVerts->push_back(countingtri.apex());
		}
		deletesegment(tempsub);
	  }

	  if( clockwisepass )
	  {
		  if( right_tri(countingtri) == this->dummytri )
		  {
			clockwisepass = false;

			tspivot( countingtri, tempsub );
			if( tempsub.ss != this->dummysub )
			{
				if( segVerts ){
					segVerts->push_back(countingtri.dest());
				}
				deletesegment(tempsub);
			}

			rightmosttri = countingtri;
			degree = 2;
			prevtri = countingtri;
		  }
		  else
		  {
			  oprevself(countingtri);
			  
			  if( countingtri == *deltri )
			  {
				  // we have looped back to initial tri
				  break;
			  }
			  degree++;
		  }
	  }
	  else
	  {
		  /* Deleting a Vertex from the convex Hull */

			bool reachedTheEnd = left_tri(countingtri) == this->dummytri;

			prevtri = countingtri;

			if( !reachedTheEnd )
			{
				onextself(countingtri);
				degree++;
			}

			if( nomoreconvexstretch )
			{
				// Neighbors form a localy concave boundary : 
				// delete prevtri triangle, no need to retriangulate.

				// reset tri that dest and apex of deltri point to.
				struct otri oppositetri;

				lnext(prevtri, oppositetri);

				tspivot(oppositetri, tempsub);
				if( tempsub.ss != this->dummysub )
					stdissolve(tempsub);

				symself(oppositetri); // only now does it really become 'opposite' tri

				if( oppositetri.tri == this->dummytri )
				{
					// There are no more triangles for segment to point to
					deletesegment(tempsub);
					if( reachedTheEnd )
						throw TriangleExc("TODO find another valid tri to assign to *deltri out parameter");
				}
				else
				{
					dissolve(oppositetri);

	  				// Reset vertex2tri mapping
					setvertex2tri_org(oppositetri);
					lnextself(oppositetri);
					setvertex2tri_org(oppositetri);
					
					if( reachedTheEnd )
						*deltri = oppositetri;
				}

				if( !reachedTheEnd )
					dissolve(countingtri);

				triangledealloc(prevtri.tri);

			}
			else
			{
				bool dealwithprevious = false;
				assert( countingtri.org() == delvertex);

				if( reachedTheEnd )
				{
					dealwithprevious = convexdegree > 0;
				}
				else
				{
					bool isdirect = prodvect(countingtri.dest()->coords,prevtri.dest()->coords,countingtri.apex()->coords) >= 0;
					
					if( isdirect )
					{
						dealwithprevious = convexdegree > 0;
					}
					else
					{
						if( convexdegree == 0 )
						{
							firstconvextri = prevtri;
							convexdegree = 2;
						}
						convexdegree++;
					}
				}

				if( dealwithprevious )
				{
					// Neighbors form a localy convex boundary : 
					// re-triangulate that area.
					assert( convexdegree >= 3 );

					if( convexdegree > 3 )
					{	
						triangulatepolygon(&firstconvextri, &prevtri, convexdegree, 1);
					}
					else
					{
						flip(&prevtri);	
					}
					lprevself(prevtri);

					assert(degree >= convexdegree);

					if( (reachedTheEnd && degree == convexdegree) || ( !reachedTheEnd && degree-1 == convexdegree ) )
					{
						// Start of the pass, rightmosttri has been changed by the flip.
						rightmosttri = prevtri;
						// all the vertices connected to del vertex formed one convex boundary
						// that we have just retriangulated - we know there are no more convexity now.	
					}

					countingtri = rightmosttri;
					degree = 2;
					convexdegree = 0;

					assert( prevtri.org() == delvertex );
					assert( rightmosttri.org() == delvertex );
					assert( right_tri(rightmosttri) == this->dummytri );

					//sym(prevtri,*deltri);
					//dissolve(*deltri);
					//setallvertex2tri(*deltri);
					continue;
				}

			}

			if( reachedTheEnd )
			{
				if( nomoreconvexstretch )
				{
					break;
				}
				else{
					nomoreconvexstretch = true;// no convexstretch is found - let's start removing triangles forming a concave boundary.
					// Go back to right most tri and do another pass.
					// until no convexstretch is found
					degree = 2;
					countingtri = rightmosttri;
				}
			}
	  }

  }

#ifdef SELF_CHECK

  /* Count the degree of the vertex being deleted. */
  int edgecount = delvertex->getDegree();

  if (clockwisepass && abs(edgecount) != degree ) {
	  throw TriangleExc("Internal degree different from getDegree().");
  }
  if (abs(edgecount) < 2) {
	  throw TriangleExc("Internal error in deletevertex():\n  Vertex has degree < 2.");
  }

#endif

  if( clockwisepass )
  {
	  // Inner vertex

	  if (degree > 3) {
		/* Triangulate the polygon defined by the union of all triangles */
		/*   adjacent to the vertex being deleted.  Check the quality of */
		/*   the resulting triangles.                                    */
		onext(*deltri, firstedge);
		oprev(*deltri, lastedge);
		triangulatepolygon(&firstedge, &lastedge, degree, 0);
	  }
	  /* Splice out two triangles. */
	  lprev(*deltri, deltriright);
	  dnext(*deltri, lefttri);
	  sym(lefttri, leftcasing);
	  oprev(deltriright, righttri);
	  sym(righttri, rightcasing);
	  bond(*deltri, leftcasing);
	  bond(deltriright, rightcasing);
	  tspivot(lefttri, leftsubseg);

	  if (leftsubseg.ss != this->dummysub) {
		tsbond(*deltri, leftsubseg);
	  }
	  tspivot(righttri, rightsubseg);
	  if (rightsubseg.ss != this->dummysub) {
		tsbond(deltriright, rightsubseg);
	  }

	  /* Set the new origin of `deltri' */
	  neworg = lefttri.org();
	  deltri->setorg(neworg);
	  setallvertex2tri(*deltri);

	  /* Delete the two spliced-out triangles. */
	  triangledealloc(lefttri.tri);
	  triangledealloc(righttri.tri);

  }

  //checkmesh();

  if( dealloc )
	vertexdealloc(delvertex);

}

void Mesh::deletesegment(osub& seg)
{
	if( seg.ss == nullptr || seg.ss == this->dummysub || seg.ss->index < 0 )
		return;

	otri temptri;
	stpivot(seg,temptri);
	tsdissolve(temptri);

	if( segnext(seg).ss != this->dummysub )
		segnext(seg).SetNeighborSegs();

	if( segprev(seg).ss != this->dummysub )
		segprev(seg).SetNeighborSegs();

	ssymself(seg);
	stpivot(seg,temptri);
	tsdissolve(temptri);

	if( segnext(seg).ss != this->dummysub )
		segnext(seg).SetNeighborSegs();

	if( segprev(seg).ss != this->dummysub )
		segprev(seg).SetNeighborSegs();

	osub tempsub;
	spivot(seg,tempsub);
	sdissolve(tempsub);

	snext(seg,tempsub);
	sdissolve(tempsub);

	subsegdealloc(seg.ss);

	if( this->searchGraph )
		this->searchDualDirty = true;
}

/**                                                                         **/
/**                                                                         **/
/********* Mesh transformation routines end here                     *********/

/********* Divide-and-conquer Delaunay triangulation begins here     *********/
/**                                                                         **/
/**                                                                         **/

/*****************************************************************************/
/*                                                                           */
/*  The divide-and-conquer bounding box                                      */
/*                                                                           */
/*  I originally implemented the divide-and-conquer and incremental Delaunay */
/*  triangulations using the edge-based data structure presented by Guibas   */
/*  and Stolfi.  Switching to a triangle-based data structure doubled the    */
/*  speed.  However, I had to think of a few extra tricks to maintain the    */
/*  elegance of the original algorithms.                                     */
/*                                                                           */
/*  The "bounding box" used by my variant of the divide-and-conquer          */
/*  algorithm uses one triangle for each edge of the convex hull of the      */
/*  triangulation.  These bounding triangles all share a common apical       */
/*  vertex, which is represented by nullptr and which represents nothing.       */
/*  The bounding triangles are linked in a circular fan about this nullptr      */
/*  vertex, and the edges on the convex hull of the triangulation appear     */
/*  opposite the nullptr vertex.  You might find it easiest to imagine that     */
/*  the nullptr vertex is a point in 3D space behind the center of the          */
/*  triangulation, and that the bounding triangles form a sort of cone.      */
/*                                                                           */
/*  This bounding box makes it easy to represent degenerate cases.  For      */
/*  instance, the triangulation of two vertices is a single edge.  This edge */
/*  is represented by two bounding box triangles, one on each "side" of the  */
/*  edge.  These triangles are also linked together in a fan about the nullptr  */
/*  vertex.                                                                  */
/*                                                                           */
/*  The bounding box also makes it easy to traverse the convex hull, as the  */
/*  divide-and-conquer algorithm needs to do.                                */
/*                                                                           */
/*****************************************************************************/

/*****************************************************************************/
/*                                                                           */
/*  vertexsort()   Sort an array of vertices by x-coordinate, using the      */
/*                 y-coordinate as a secondary key.                          */
/*                                                                           */
/*  Uses quicksort.  Randomized O(n log n) time.  No, I did not make any of  */
/*  the usual quicksort mistakes.                                            */
/*                                                                           */
/*****************************************************************************/

void vertexsort(wertex **sortarray, size_t arraysize)
{
  int left, right;
  int pivot;
  REAL pivotx, pivoty;
  vertex temp;

  if (arraysize == 2) {
    /* Recursive base case. */
    if ((sortarray[0]->coords[0] > sortarray[1]->coords[0]) ||
        ((sortarray[0]->coords[0] == sortarray[1]->coords[0]) &&
         (sortarray[0]->coords[1] > sortarray[1]->coords[1]))) {
      temp = sortarray[1];
      sortarray[1] = sortarray[0];
      sortarray[0] = temp;
    }
    return;
  }
  /* Choose a random pivot to split the array. */
  pivot = (int) randomnation((uint) arraysize);
  pivotx = sortarray[pivot]->coords[0];
  pivoty = sortarray[pivot]->coords[1];
  /* Split the array. */
  left = -1;
  right = (int) arraysize;
  while (left < right) {
    /* Search for a vertex whose x-coordinate is too large for the left. */
    do {
      left++;
    } while ((left <= right) && ((sortarray[left]->coords[0] < pivotx) ||
                                 ((sortarray[left]->coords[0] == pivotx) &&
                                  (sortarray[left]->coords[1] < pivoty))));
    /* Search for a vertex whose x-coordinate is too small for the right. */
    do {
      right--;
    } while ((left <= right) && ((sortarray[right]->coords[0] > pivotx) ||
                                 ((sortarray[right]->coords[0] == pivotx) &&
                                  (sortarray[right]->coords[1] > pivoty))));
    if (left < right) {
      /* Swap the left and right vertices. */
      temp = sortarray[left];
      sortarray[left] = sortarray[right];
      sortarray[right] = temp;
    }
  }
  if (left > 1) {
    /* Recursively sort the left subset. */
    vertexsort(sortarray, left);
  }
  if (right < (int)arraysize - 2) {
    /* Recursively sort the right subset. */
    vertexsort(&sortarray[right + 1], arraysize - right - 1);
  }
}

/*****************************************************************************/
/*                                                                           */
/*  vertexmedian()   An order statistic algorithm, almost.  Shuffles an      */
/*                   array of vertices so that the first `median' vertices   */
/*                   occur lexicographically before the remaining vertices.  */
/*                                                                           */
/*  Uses the x-coordinate as the primary key if axis == 0; the y-coordinate  */
/*  if axis == 1.  Very similar to the vertexsort() procedure, but runs in   */
/*  randomized linear time.                                                  */
/*                                                                           */
/*****************************************************************************/

void vertexmedian(wertex **sortarray, int arraysize, int median, int axis)
{
  int left, right;
  int pivot;
  REAL pivot1, pivot2;
  vertex temp;

  if (arraysize == 2) {
    /* Recursive base case. */
    if ((sortarray[0]->coords[axis] > sortarray[1]->coords[axis]) ||
        ((sortarray[0]->coords[axis] == sortarray[1]->coords[axis]) &&
         (sortarray[0]->coords[1 - axis] > sortarray[1]->coords[1 - axis]))) {
      temp = sortarray[1];
      sortarray[1] = sortarray[0];
      sortarray[0] = temp;
    }
    return;
  }
  /* Choose a random pivot to split the array. */
  pivot = (int) randomnation((uint) arraysize);
  pivot1 = sortarray[pivot]->coords[axis];
  pivot2 = sortarray[pivot]->coords[1 - axis];
  /* Split the array. */
  left = -1;
  right = arraysize;
  while (left < right) {
    /* Search for a vertex whose x-coordinate is too large for the left. */
    do {
      left++;
    } while ((left <= right) && ((sortarray[left]->coords[axis] < pivot1) ||
                                 ((sortarray[left]->coords[axis] == pivot1) &&
                                  (sortarray[left]->coords[1 - axis] < pivot2))));
    /* Search for a vertex whose x-coordinate is too small for the right. */
    do {
      right--;
    } while ((left <= right) && ((sortarray[right]->coords[axis] > pivot1) ||
                                 ((sortarray[right]->coords[axis] == pivot1) &&
                                  (sortarray[right]->coords[1 - axis] > pivot2))));
    if (left < right) {
      /* Swap the left and right vertices. */
      temp = sortarray[left];
      sortarray[left] = sortarray[right];
      sortarray[right] = temp;
    }
  }
  /* Unlike in vertexsort(), at most one of the following */
  /*   conditionals is true.                             */
  if (left > median) {
    /* Recursively shuffle the left subset. */
    vertexmedian(sortarray, left, median, axis);
  }
  if (right < median - 1) {
    /* Recursively shuffle the right subset. */
    vertexmedian(&sortarray[right + 1], arraysize - right - 1,
                 median - right - 1, axis);
  }
}

/*****************************************************************************/
/*                                                                           */
/*  alternateaxes()   Sorts the vertices as appropriate for the divide-and-  */
/*                    conquer algorithm with alternating cuts.               */
/*                                                                           */
/*  Partitions by x-coordinate if axis == 0; by y-coordinate if axis == 1.   */
/*  For the base case, subsets containing only two or three vertices are     */
/*  always sorted by x-coordinate.                                           */
/*                                                                           */
/*****************************************************************************/

void alternateaxes(wertex **sortarray, int arraysize, int axis)
{
  int divider;

  divider = arraysize >> 1;
  if (arraysize <= 3) {
    /* Recursive base case:  subsets of two or three vertices will be    */
    /*   handled specially, and should always be sorted by x-coordinate. */
    axis = 0;
  }
  /* Partition with a horizontal or vertical cut. */
  vertexmedian(sortarray, arraysize, divider, axis);
  /* Recursively partition the subsets with a cross cut. */
  if (arraysize - divider >= 2) {
    if (divider >= 2) {
      alternateaxes(sortarray, divider, 1 - axis);
    }
    alternateaxes(&sortarray[divider], arraysize - divider, 1 - axis);
  }
}

/*****************************************************************************/
/*                                                                           */
/*  mergehulls()   Merge two adjacent Delaunay triangulations into a         */
/*                 single Delaunay triangulation.                            */
/*                                                                           */
/*  This is similar to the algorithm given by Guibas and Stolfi, but uses    */
/*  a triangle-based, rather than edge-based, data structure.                */
/*                                                                           */
/*  The algorithm walks up the gap between the two triangulations, knitting  */
/*  them together.  As they are merged, some of their bounding triangles     */
/*  are converted into real triangles of the triangulation.  The procedure   */
/*  pulls each hull's bounding triangles apart, then knits them together     */
/*  like the teeth of two gears.  The Delaunay property determines, at each  */
/*  step, whether the next "tooth" is a bounding triangle of the left hull   */
/*  or the right.  When a bounding triangle becomes real, its apex is        */
/*  changed from nullptr to a real vertex.                                      */
/*                                                                           */
/*  Only two new triangles need to be allocated.  These become new bounding  */
/*  triangles at the top and bottom of the seam.  They are used to connect   */
/*  the remaining bounding triangles (those that have not been converted     */
/*  into real triangles) into a single fan.                                  */
/*                                                                           */
/*  On entry, `farleft' and `innerleft' are bounding triangles of the left   */
/*  triangulation.  The origin of `farleft' is the leftmost vertex, and      */
/*  the destination of `innerleft' is the rightmost vertex of the            */
/*  triangulation.  Similarly, `innerright' and `farright' are bounding      */
/*  triangles of the right triangulation.  The origin of `innerright' and    */
/*  destination of `farright' are the leftmost and rightmost vertices.       */
/*                                                                           */
/*  On completion, the origin of `farleft' is the leftmost vertex of the     */
/*  merged triangulation, and the destination of `farright' is the rightmost */
/*  vertex.                                                                  */
/*                                                                           */
/*****************************************************************************/

void Mesh::mergehulls( struct otri *farleft,
                struct otri *innerleft, struct otri *innerright,
                struct otri *farright, int axis)
{
  struct otri leftcand, rightcand;
  struct otri baseedge;
  struct otri nextedge;
  struct otri sidecasing, topcasing, outercasing;
  struct otri checkedge;
  vertex innerleftdest;
  vertex innerrightorg;
  vertex innerleftapex, innerrightapex;
  vertex farleftpt, farrightpt;
  vertex farleftapex, farrightapex;
  vertex lowerleft, lowerright;
  vertex upperleft, upperright;
  vertex nextapex;
  vertex checkvertex;
  int changemade;
  int badedge;
  int leftfinished, rightfinished;

  innerleftdest = innerleft->dest();
  innerleftapex = innerleft->apex();
  innerrightorg = innerright->org();
  innerrightapex = innerright->apex();
  /* Special treatment for horizontal cuts. */
  if (b.dwyer && (axis == 1)) {
    farleftpt = farleft->org();
    farleftapex = farleft->apex();
    farrightpt = farright->dest();
    farrightapex = farright->apex();
    /* The pointers to the extremal vertices are shifted to point to the */
    /*   topmost and bottommost vertex of each hull, rather than the     */
    /*   leftmost and rightmost vertices.                                */
    while (farleftapex->coords[1] < farleftpt->coords[1]) {
      lnextself(*farleft);
      symself(*farleft);
      farleftpt = farleftapex;
      farleftapex = farleft->apex();
    }
    sym(*innerleft, checkedge);
    checkvertex = checkedge.apex();
    while (checkvertex->coords[1] > innerleftdest->coords[1]) {
      lnext(checkedge, *innerleft);
      innerleftapex = innerleftdest;
      innerleftdest = checkvertex;
      sym(*innerleft, checkedge);
      checkvertex = checkedge.apex();
    }
    while (innerrightapex->coords[1] < innerrightorg->coords[1]) {
      lnextself(*innerright);
      symself(*innerright);
      innerrightorg = innerrightapex;
      innerrightapex = innerright->apex();
    }
    sym(*farright, checkedge);
    checkvertex = checkedge.apex();
    while (checkvertex->coords[1] > farrightpt->coords[1]) {
      lnext(checkedge, *farright);
      farrightapex = farrightpt;
      farrightpt = checkvertex;
      sym(*farright, checkedge);
      checkvertex = checkedge.apex();
    }
  }
  /* Find a line tangent to and below both hulls. */
  do {
    changemade = 0;
    /* Make innerleftdest the "bottommost" vertex of the left hull. */
    if (counterclockwise(innerleftdest->coords, innerleftapex->coords, innerrightorg->coords) >
        0.0) {
      lprevself(*innerleft);
      symself(*innerleft);
      innerleftdest = innerleftapex;
      innerleftapex = innerleft->apex();
      changemade = 1;
    }
    /* Make innerrightorg the "bottommost" vertex of the right hull. */
    if (counterclockwise(innerrightapex->coords, innerrightorg->coords, innerleftdest->coords) >
        0.0) {
      lnextself(*innerright);
      symself(*innerright);
      innerrightorg = innerrightapex;
      innerrightapex = innerright->apex();
      changemade = 1;
    }
  } while (changemade);
  /* Find the two candidates to be the next "gear tooth." */
  sym(*innerleft, leftcand);
  sym(*innerright, rightcand);
  /* Create the bottom new bounding triangle. */
  maketriangle(&baseedge);
  /* Connect it to the bounding boxes of the left and right triangulations. */
  bond(baseedge, *innerleft);
  lnextself(baseedge);
  bond(baseedge, *innerright);
  lnextself(baseedge);
  baseedge.setorg(innerrightorg);
  baseedge.setdest(innerleftdest);
  /* Apex is intentionally left nullptr. */
  if (b.verbose > 2) {
    printf("  Creating base bounding ");
    //printtriangle(&baseedge);
  }
  /* Fix the extreme triangles if necessary. */
  farleftpt = farleft->org();
  if (innerleftdest == farleftpt) {
    lnext(baseedge, *farleft);
  }
  farrightpt = farright->dest();
  if (innerrightorg == farrightpt) {
    lprev(baseedge, *farright);
  }
  /* The vertices of the current knitting edge. */
  lowerleft = innerleftdest;
  lowerright = innerrightorg;
  /* The candidate vertices for knitting. */
  upperleft = leftcand.apex();
  upperright = rightcand.apex();
  /* Walk up the gap between the two triangulations, knitting them together. */
  while (1) {
    /* Have we reached the top?  (This isn't quite the right question,       */
    /*   because even though the left triangulation might seem finished now, */
    /*   moving up on the right triangulation might reveal a new vertex of   */
    /*   the left triangulation.  And vice-versa.)                           */
    leftfinished = counterclockwise(upperleft->coords, lowerleft->coords, lowerright->coords) <=
                   0.0;
    rightfinished = counterclockwise(upperright->coords, lowerleft->coords, lowerright->coords)
                 <= 0.0;
    if (leftfinished && rightfinished) {
      /* Create the top new bounding triangle. */
      maketriangle(&nextedge);
      nextedge.setorg(lowerleft);
      nextedge.setdest(lowerright);
      /* Apex is intentionally left nullptr. */
      /* Connect it to the bounding boxes of the two triangulations. */
      bond(nextedge, baseedge);
      lnextself(nextedge);
      bond(nextedge, rightcand);
      lnextself(nextedge);
      bond(nextedge, leftcand);
      if (b.verbose > 2) {
        printf("  Creating top bounding ");
        //printtriangle(&nextedge);
      }
      /* Special treatment for horizontal cuts. */
      if (b.dwyer && (axis == 1)) {
        farleftpt = farleft->org();
        farleftapex = farleft->apex();
        farrightpt = farright->dest();
        farrightapex = farright->apex();
        sym(*farleft, checkedge);
        checkvertex = checkedge.apex();
        /* The pointers to the extremal vertices are restored to the  */
        /*   leftmost and rightmost vertices (rather than topmost and */
        /*   bottommost).                                             */
        while (checkvertex->coords[0] < farleftpt->coords[0]) {
          lprev(checkedge, *farleft);
          farleftapex = farleftpt;
          farleftpt = checkvertex;
          sym(*farleft, checkedge);
          checkvertex = checkedge.apex();
        }
        while (farrightapex->coords[0] > farrightpt->coords[0]) {
          lprevself(*farright);
          symself(*farright);
          farrightpt = farrightapex;
          farrightapex = farright->apex();
        }
      }
      return;
    }
    /* Consider eliminating edges from the left triangulation. */
    if (!leftfinished) {
      /* What vertex would be exposed if an edge were deleted? */
      lprev(leftcand, nextedge);
      symself(nextedge);
      nextapex = nextedge.apex();
      /* If nextapex is nullptr, then no vertex would be exposed; the */
      /*   triangulation would have been eaten right through.      */
      if (nextapex) {
        /* Check whether the edge is Delaunay. */
        badedge = nonregular(lowerleft->coords, lowerright->coords, upperleft->coords, nextapex->coords) >
                  0.0;
        while (badedge) {
          /* Eliminate the edge with an edge flip.  As a result, the    */
          /*   left triangulation will have one more boundary triangle. */
          lnextself(nextedge);
          sym(nextedge, topcasing);
          lnextself(nextedge);
          sym(nextedge, sidecasing);
          bond(nextedge, topcasing);
          bond(leftcand, sidecasing);
          lnextself(leftcand);
          sym(leftcand, outercasing);
          lprevself(nextedge);
          bond(nextedge, outercasing);
          /* Correct the vertices to reflect the edge flip. */
          leftcand.setorg(lowerleft);
          leftcand.setdest(nullptr);
          leftcand.setapex(nextapex);
          nextedge.setorg(nullptr);
          nextedge.setdest(upperleft);
          nextedge.setapex(nextapex);
          /* Consider the newly exposed vertex. */
          upperleft = nextapex;
          /* What vertex would be exposed if another edge were deleted? */
          nextedge = sidecasing;
          nextapex = nextedge.apex();
          if (nextapex) {
            /* Check whether the edge is Delaunay. */
            badedge = nonregular(lowerleft->coords, lowerright->coords, upperleft->coords,
                               nextapex->coords) > 0.0;
          } else {
            /* Avoid eating right through the triangulation. */
            badedge = 0;
          }
        }
      }
    }
    /* Consider eliminating edges from the right triangulation. */
    if (!rightfinished) {
      /* What vertex would be exposed if an edge were deleted? */
      lnext(rightcand, nextedge);
      symself(nextedge);
      nextapex = nextedge.apex();
      /* If nextapex is nullptr, then no vertex would be exposed; the */
      /*   triangulation would have been eaten right through.      */
      if (nextapex) {
        /* Check whether the edge is Delaunay. */
        badedge = nonregular(lowerleft->coords, lowerright->coords, upperright->coords, nextapex->coords) >
                  0.0;
        while (badedge) {
          /* Eliminate the edge with an edge flip.  As a result, the     */
          /*   right triangulation will have one more boundary triangle. */
          lprevself(nextedge);
          sym(nextedge, topcasing);
          lprevself(nextedge);
          sym(nextedge, sidecasing);
          bond(nextedge, topcasing);
          bond(rightcand, sidecasing);
          lprevself(rightcand);
          sym(rightcand, outercasing);
          lnextself(nextedge);
          bond(nextedge, outercasing);
          /* Correct the vertices to reflect the edge flip. */
          rightcand.setorg(nullptr);
          rightcand.setdest(lowerright);
          rightcand.setapex(nextapex);
          nextedge.setorg(upperright);
          nextedge.setdest(nullptr);
          nextedge.setapex(nextapex);
          /* Consider the newly exposed vertex. */
          upperright = nextapex;
          /* What vertex would be exposed if another edge were deleted? */
          nextedge = sidecasing;
          nextapex = nextedge.apex();
          if (nextapex) {
            /* Check whether the edge is Delaunay. */
            badedge = nonregular(lowerleft->coords, lowerright->coords, upperright->coords,
                               nextapex->coords) > 0.0;
          } else {
            /* Avoid eating right through the triangulation. */
            badedge = 0;
          }
        }
      }
    }
    if (leftfinished || (!rightfinished &&
           (nonregular(upperleft->coords, lowerleft->coords, lowerright->coords, upperright->coords) >
            0.0))) {
      /* Knit the triangulations, adding an edge from `lowerleft' */
      /*   to `upperright'.                                       */
      bond(baseedge, rightcand);
      lprev(rightcand, baseedge);
      baseedge.setdest(lowerleft);
      lowerright = upperright;
      sym(baseedge, rightcand);
      upperright = rightcand.apex();
    } else {
      /* Knit the triangulations, adding an edge from `upperleft' */
      /*   to `lowerright'.                                       */
      bond(baseedge, leftcand);
      lnext(leftcand, baseedge);
      baseedge.setorg(lowerright);
      lowerleft = upperleft;
      sym(baseedge, leftcand);
      upperleft = leftcand.apex();
    }
    if (b.verbose > 2) {
      printf("  Connecting ");
      //printtriangle(&baseedge);
    }
  }
}

/*****************************************************************************/
/*                                                                           */
/*  divconqrecurse()   Recursively form a Delaunay triangulation by the      */
/*                     divide-and-conquer method.                            */
/*                                                                           */
/*  Recursively breaks down the problem into smaller pieces, which are       */
/*  knitted together by mergehulls().  The base cases (problems of two or    */
/*  three vertices) are handled specially here.                              */
/*                                                                           */
/*  On completion, `farleft' and `farright' are bounding triangles such that */
/*  the origin of `farleft' is the leftmost vertex (breaking ties by         */
/*  choosing the highest leftmost vertex), and the destination of            */
/*  `farright' is the rightmost vertex (breaking ties by choosing the        */
/*  lowest rightmost vertex).                                                */
/*                                                                           */
/*****************************************************************************/

void Mesh::divconqrecurse( wertex **sortarray,
                    int num_vertices, 
					int axis,
                    struct otri *farleft, 
					struct otri *farright)
{
  struct otri midtri, tri1, tri2, tri3;
  struct otri innerleft, innerright;
  REAL area;
  int divider;

  if (b.verbose > 2) {
    printf("  Triangulating %d vertices.\n", num_vertices);
  }
  if (num_vertices == 2) {
    /* The triangulation of two vertices is an edge.  An edge is */
    /*   represented by two bounding triangles.                  */
    maketriangle(farleft);
    farleft->setorg(sortarray[0]);
    farleft->setdest(sortarray[1]);
    /* The apex is intentionally left nullptr. */
    maketriangle(farright);
    farright->setorg(sortarray[1]);
    farright->setdest(sortarray[0]);
    /* The apex is intentionally left nullptr. */
    bond(*farleft, *farright);
    lprevself(*farleft);
    lnextself(*farright);
    bond(*farleft, *farright);
    lprevself(*farleft);
    lnextself(*farright);
    bond(*farleft, *farright);
    if (b.verbose > 2) {
      //printf("  Creating ");
      //printtriangle(farleft);
      //printf("  Creating ");
      //printtriangle(farright);
    }
    /* Ensure that the origin of `farleft' is sortarray[0]. */
    lprev(*farright, *farleft);
    return;
  } else if (num_vertices == 3) {
    /* The triangulation of three vertices is either a triangle (with */
    /*   three bounding triangles) or two edges (with four bounding   */
    /*   triangles).  In either case, four triangles are created.     */
    maketriangle(&midtri);
    maketriangle(&tri1);
    maketriangle(&tri2);
    maketriangle(&tri3);
    area = counterclockwise(sortarray[0]->coords, sortarray[1]->coords, sortarray[2]->coords);
    if (area == 0.0) {
      /* Three collinear vertices; the triangulation is two edges. */
      midtri.setorg(sortarray[0]);
      midtri.setdest(sortarray[1]);
      tri1.setorg(sortarray[1]);
      tri1.setdest(sortarray[0]);
      tri2.setorg(sortarray[2]);
      tri2.setdest(sortarray[1]);
      tri3.setorg(sortarray[1]);
      tri3.setdest(sortarray[2]);
      /* All apices are intentionally left nullptr. */
      bond(midtri, tri1);
      bond(tri2, tri3);
      lnextself(midtri);
      lprevself(tri1);
      lnextself(tri2);
      lprevself(tri3);
      bond(midtri, tri3);
      bond(tri1, tri2);
      lnextself(midtri);
      lprevself(tri1);
      lnextself(tri2);
      lprevself(tri3);
      bond(midtri, tri1);
      bond(tri2, tri3);
      /* Ensure that the origin of `farleft' is sortarray[0]. */
      *farleft = tri1;
      /* Ensure that the destination of `farright' is sortarray[2]. */
      *farright = tri2;
    } else {
      /* The three vertices are not collinear; the triangulation is one */
      /*   triangle, namely `midtri'.                                   */
      midtri.setorg(sortarray[0]);
      tri1.setdest(sortarray[0]);
      tri3.setorg(sortarray[0]);
      /* Apices of tri1, tri2, and tri3 are left nullptr. */
      if (area > 0.0) {
        /* The vertices are in counterclockwise order. */
        midtri.setdest(sortarray[1]);
        tri1.setorg(sortarray[1]);
        tri2.setdest(sortarray[1]);
        midtri.setapex(sortarray[2]);
        tri2.setorg(sortarray[2]);
        tri3.setdest(sortarray[2]);
      } else {
        /* The vertices are in clockwise order. */
        midtri.setdest(sortarray[2]);
        tri1.setorg(sortarray[2]);
        tri2.setdest(sortarray[2]);
        midtri.setapex(sortarray[1]);
        tri2.setorg(sortarray[1]);
        tri3.setdest(sortarray[1]);
      }
      /* The topology does not depend on how the vertices are ordered. */
      bond(midtri, tri1);
      lnextself(midtri);
      bond(midtri, tri2);
      lnextself(midtri);
      bond(midtri, tri3);
      lprevself(tri1);
      lnextself(tri2);
      bond(tri1, tri2);
      lprevself(tri1);
      lprevself(tri3);
      bond(tri1, tri3);
      lnextself(tri2);
      lprevself(tri3);
      bond(tri2, tri3);
      /* Ensure that the origin of `farleft' is sortarray[0]. */
      *farleft = tri1;
      /* Ensure that the destination of `farright' is sortarray[2]. */
      if (area > 0.0) {
        *farright = tri2;
      } else {
        lnext(*farleft, *farright);
      }
    }
    if (b.verbose > 2) {
      printf("  Creating ");
      //printtriangle(&midtri);
      printf("  Creating ");
      //printtriangle(&tri1);
      printf("  Creating ");
      //printtriangle(&tri2);
      printf("  Creating ");
      //printtriangle(&tri3);
    }
    return;
  } else {
    /* Split the vertices in half. */
    divider = num_vertices >> 1;
    /* Recursively triangulate each half. */
    divconqrecurse(sortarray, divider, 1 - axis, farleft, &innerleft);
    divconqrecurse(&sortarray[divider], num_vertices - divider, 1 - axis,
                   &innerright, farright);
    if (b.verbose > 1) {
      printf("  Joining triangulations with %d and %d vertices.\n", divider,
		  num_vertices - divider);
    }
    /* Merge the two triangulations into one. */
    mergehulls(farleft, &innerleft, &innerright, farright, axis);
  }
}

long Mesh::removeghosts(struct otri *startghost)
{
	struct otri searchedge;
	struct otri dissolveedge;
	struct otri deadtriangle;
	vertex markorg;
	long hullsize;

	/* Find an edge on the convex hull to start point location from. */
	lprev(*startghost, searchedge);
	symself(searchedge);
	this->dummytri->otris[0] = searchedge;
	/* Remove the bounding box and count the convex hull edges. */
	dissolveedge = *startghost;
	hullsize = 0;

	do
	{
		hullsize++;
		lnext(dissolveedge, deadtriangle);
		lprevself(dissolveedge);
		symself(dissolveedge);
		/* If no PSLG is involved, set the boundary markers of all the vertices */
		/*   on the convex hull.  If a PSLG is used, this step is done later.   */
		if (!b.poly)
		{
			/* Watch out for the case where all the input vertices are collinear. */
			if (dissolveedge.tri != this->dummytri)
			{
				markorg = dissolveedge.org();
				if (vertexmark(markorg) == 0)
				{
					setvertexmark(markorg, 1);
				}
			}
		}

		/* Remove a bounding triangle from a convex hull triangle. */
		dissolve(dissolveedge);
		/* Find the next bounding triangle. */
		sym(deadtriangle, dissolveedge);
		/* Delete the bounding triangle. */
		triangledealloc(deadtriangle.tri);

	} while (dissolveedge != *startghost);

	return hullsize;
}

/*****************************************************************************/
/*                                                                           */
/*  divconqdelaunay()   Form a Delaunay triangulation by the divide-and-     */
/*                      conquer method.                                      */
/*                                                                           */
/*  Sorts the vertices, calls a recursive procedure to triangulate them, and */
/*  removes the bounding box, setting boundary markers as appropriate.       */
/*                                                                           */
/*****************************************************************************/

long Mesh::divconqdelaunay()
{
	wertex **sortarray = &this->vertices.front();
	struct otri hullleft, hullright;
	int divider;
	uint i, j;

	/* Allocate an array of pointers to vertices for sorting. */
  
	/* Sort the vertices. */
	vertexsort(sortarray, this->vertices.size());
	/* Discard duplicate vertices, which can really mess up the algorithm. */
	i = 0;
	for (j = 1; j < this->vertices.size(); j++)
	{
		if (sortarray[i]->coords == sortarray[j]->coords)
		{	
			throw TriangleExc_Vert("duplicate vertex found", sortarray[j]);
		}else
		{
			i++;
			sortarray[i] = sortarray[j];
		}
	}
	i++;

	if (b.dwyer)
	{
		/* Re-sort the array of vertices to accommodate alternating cuts. */
		divider = i >> 1;
		if (i - divider >= 2) {
			if (divider >= 2) {
			alternateaxes(sortarray, divider, 1);
			}
			alternateaxes(&sortarray[divider], i - divider, 1);
		}
	}

	/* Form the Delaunay triangulation. */
	divconqrecurse(sortarray, i, 0, &hullleft, &hullright);

	// Reassign vertices index which has been affected by the sorting of this->vertices.
	for( uint idx=0; idx<vertices.size(); idx++ )
		vertices[idx]->index = idx;

	return removeghosts(&hullleft);
}

/**                                                                         **/
/**                                                                         **/
/********* Divide-and-conquer Delaunay triangulation ends here       *********/

/********* Incremental Delaunay triangulation begins here            *********/
/**                                                                         **/
/**                                                                         **/

/*****************************************************************************/
/*                                                                           */
/*  boundingbox()   Form an "infinite" bounding triangle to insert vertices  */
/*                  into.                                                    */
/*                                                                           */
/*  The vertices at "infinity" are assigned finite coordinates, which are    */
/*  used by the point location routines, but (mostly) ignored by the         */
/*  Delaunay edge flip routines.                                             */
/*                                                                           */
/*****************************************************************************/


void Mesh::boundingbox()
{
  struct otri inftri;          /* Handle for the triangular bounding box. */
  REAL width;

  if (b.verbose) {
    printf("  Creating triangular bounding box.\n");
  }
  /* Find the width (or height, whichever is larger) of the triangulation. */
  width = this->xmax - this->xmin;
  if (this->ymax - this->ymin > width) {
    width = this->ymax - this->ymin;
  }
  if (width == 0.0) {
    width = 1.0;
  }
  /* Create the vertices of the bounding box. */
  this->infvertex1.coords[0] = this->xmin - 50.0 * width;
  this->infvertex1.coords[1] = this->ymin - 40.0 * width;
  this->infvertex2.coords[0] = this->xmax + 50.0 * width;
  this->infvertex2.coords[1] = this->ymin - 40.0 * width;
  this->infvertex3.coords[0] = 0.5 * (this->xmin + this->xmax);
  this->infvertex3.coords[1] = this->ymax + 60.0 * width;

  /* Create the bounding box. */
  maketriangle(&inftri);
  inftri.setorg(&this->infvertex1);
  inftri.setdest(&this->infvertex2);
  inftri.setapex(&this->infvertex3);
  
  
  /* Link dummytri to the bounding box so we can always find an */
  /*   edge to begin searching (point location) from.           */
  this->dummytri->otris[0] = inftri;
  
}

/*****************************************************************************/
/*                                                                           */
/*  removebox()   Remove the "infinite" bounding triangle, setting boundary  */
/*                markers as appropriate.                                    */
/*                                                                           */
/*  The triangular bounding box has three boundary triangles (one for each   */
/*  side of the bounding box), and a bunch of triangles fanning out from     */
/*  the three bounding box vertices (one triangle for each edge of the       */
/*  convex hull of the inner mesh).  This routine removes these triangles.   */
/*                                                                           */
/*  Returns the number of edges on the convex hull of the triangulation.     */
/*                                                                           */
/*****************************************************************************/

long Mesh::removebox()
{
  struct otri deadtriangle;
  struct otri searchedge;
  struct otri checkedge;
  struct otri nextedge, finaledge, dissolveedge;
  vertex markorg;
  long hullsize;

  if (b.verbose) {
    printf("  Removing triangular bounding box.\n");
  }
  /* Find a boundary triangle. */
  nextedge.tri = this->dummytri;
  nextedge.orient = 0;
  symself(nextedge);
  /* Mark a place to stop. */
  lprev(nextedge, finaledge);
  lnextself(nextedge);
  symself(nextedge);
  /* Find a triangle (on the boundary of the vertex set) that isn't */
  /*   a bounding box triangle.                                     */
  lprev(nextedge, searchedge);
  symself(searchedge);
  /* Check whether nextedge is another boundary triangle */
  /*   adjacent to the first one.                        */
  lnext(nextedge, checkedge);
  symself(checkedge);
  if (checkedge.tri == this->dummytri) {
    /* Go on to the next triangle.  There are only three boundary   */
    /*   triangles, and this next triangle cannot be the third one, */
    /*   so it's safe to stop here.                                 */
    lprevself(searchedge);
    symself(searchedge);
  }
  /* Find a new boundary edge to search from, as the current search */
  /*   edge lies on a bounding box triangle and will be deleted.    */
  this->dummytri->otris[0] = searchedge;
  hullsize = -2l;
  while ( nextedge != finaledge ) {
    hullsize++;
    lprev(nextedge, dissolveedge);
    symself(dissolveedge);
    /* If not using a PSLG, the vertices should be marked now. */
    /*   (If using a PSLG, markhull() will do the job.)        */
    if (!b.poly) {
      /* Be careful!  One must check for the case where all the input     */
      /*   vertices are collinear, and thus all the triangles are part of */
      /*   the bounding box.  Otherwise, the setvertexmark() call below   */
      /*   will cause a bad pointer reference.                            */
      if (dissolveedge.tri != this->dummytri) {
        markorg = dissolveedge.org();
        if (vertexmark(markorg) == 0) {
          setvertexmark(markorg, 1);
        }
      }
    }
    /* Disconnect the bounding box triangle from the mesh triangle. */
    dissolve(dissolveedge);
    lnext(nextedge, deadtriangle);
    sym(deadtriangle, nextedge);
    /* Get rid of the bounding box triangle. */
    /* Do we need to turn the corner? */
    if (nextedge.tri == this->dummytri) {
      /* Turn the corner. */
      nextedge = dissolveedge;
    }
  }

  return hullsize;
}

/*****************************************************************************/
/*                                                                           */
/*  incrementaldelaunay()   Form a Delaunay triangulation by incrementally   */
/*                          inserting vertices.                              */
/*                                                                           */
/*  Returns the number of edges on the convex hull of the triangulation.     */
/*                                                                           */
/*****************************************************************************/

long Mesh::incrementaldelaunay()
{
  struct otri starttri;

  /* Create a triangular bounding box. */
  boundingbox();
  if (b.verbose) {
    printf("  Incrementally inserting vertices.\n");
  }
  
  for (vertex vertexloop : vertices)
  {
    starttri.tri = this->dummytri;
    if (insertvertex(vertexloop, &starttri, nullptr)
        == DUPLICATEVERTEX) {
      if (!b.quiet) {
        printf(
"Warning:  A duplicate vertex at (%.12g, %.12g) appeared and was ignored.\n",
               vertexloop->coords[0], vertexloop->coords[1]);
      }
      setvertextype(vertexloop, wertex::UNDEADVERTEX);
      this->undeads++;
    }
  }
  /* Remove the bounding box. */
  return removebox();
}

/**                                                                         **/
/**                                                                         **/
/********* Incremental Delaunay triangulation ends here              *********/


/********* General mesh construction routines begin here             *********/
/**                                                                         **/
/**                                                                         **/

/*****************************************************************************/
/*                                                                           */
/*  delaunay()   Form a Delaunay triangulation.                              */
/*                                                                           */
/*****************************************************************************/

long Mesh::delaunay(enum DelaunayAlgo algo)
{
	long hulledges;

	if (this->vertices.size() < 3)
	    throw TriangleExc("Input must have at least three input vertices.");

	while( !triangles.empty() )
	{
		triangledealloc(triangles.back());
	}
	
	std::vector<std::pair<wertex*, wertex*>> vertToSeg;
	
	const size_t numSegs = subsegs.size();
	vertToSeg.reserve(numSegs);

	while( !subsegs.empty() )
	{
		osub subtemp(subsegs.front());
		wertex * porg = svorg(subtemp);
		wertex* pdest = svdest(subtemp);
		vertToSeg.emplace_back(porg, pdest);

		subsegdealloc(subtemp.ss);
	}

	switch( algo )
	{
	case Incremental:
		hulledges = incrementaldelaunay();
		break;
	case DivAndConquer:
	default:
		hulledges = divconqdelaunay();
		break;
	}

	// Map each vertex to a triangle
	makevertexmap();

	// Re-insert segments
	for(uint i=0; i<numSegs; i++)
		insertsegment(vertToSeg[i].first,vertToSeg[i].second);

	if (this->triangles.size() == 0) 
	{
		/* The input vertices were all collinear, so there are no triangles. */
		return 0l;
	} else {
		return hulledges;
	}
}
/**                                                                         **/
/**                                                                         **/
/********* General mesh construction routines end here               *********/

/********* Segment insertion begins here                             *********/
/**                                                                         **/
/**                                                                         **/

/*****************************************************************************/
/*                                                                           */
/*  finddirection()   Find the first triangle on the path from one point     */
/*                    to another.                                            */
/*                                                                           */
/*  Finds the triangle that intersects a line segment drawn from the         */
/*  origin of `searchtri' to the point `searchpoint', and returns the result */
/*  in `searchtri'.  The origin of `searchtri' does not change, even though  */
/*  the triangle returned may differ from the one passed in.  This routine   */
/*  is used to find the direction to move in to get from one point to        */
/*  another.                                                                 */
/*                                                                           */
/*  The return value notes whether the destination or apex of the found      */
/*  triangle is collinear with the two points in question.                   */
/*                                                                           */
/*****************************************************************************/

enum finddirectionresult Mesh::finddirection(
                                       struct otri *searchtri,
                                       vertex searchpoint)
{
  struct otri checktri;
  REAL leftccw, rightccw;
  int leftflag, rightflag;

  vertex startvertex = searchtri->org();
  vertex rightvertex = searchtri->dest();
  vertex leftvertex  = searchtri->apex();
  /* Is `searchpoint' to the left? */
  leftccw = counterclockwise(searchpoint->coords, startvertex->coords, leftvertex->coords);
  leftflag = leftccw > 0.0;
  /* Is `searchpoint' to the right? */
  rightccw = counterclockwise(startvertex->coords, searchpoint->coords, rightvertex->coords);
  rightflag = rightccw > 0.0;
  if (leftflag && rightflag) {
    /* `searchtri' faces directly away from `searchpoint'.  We could go left */
    /*   or right.  Ask whether it's a triangle or a boundary on the left.   */
    onext(*searchtri, checktri);
    if (checktri.tri == this->dummytri) {
      leftflag = 0;
    } else {
      rightflag = 0;
    }
  }
  while (leftflag) {
    /* Turn left until satisfied. */
    onextself(*searchtri);
    if (searchtri->tri == this->dummytri) {
      throw TriangleExc_Vert2("Unable to find a triangle leading from/to", startvertex, searchpoint);
    }
    leftvertex = searchtri->apex();
    rightccw = leftccw;
    leftccw = counterclockwise(searchpoint->coords, startvertex->coords, leftvertex->coords);
    leftflag = leftccw > 0.0;
  }
  while (rightflag) {
    /* Turn right until satisfied. */
    oprevself(*searchtri);
    if (searchtri->tri == this->dummytri) {
        throw TriangleExc_Vert2("Unable to find a triangle leading from/to", startvertex, searchpoint);
    }
    rightvertex = searchtri->dest();
    leftccw = rightccw;
    rightccw = counterclockwise(startvertex->coords, searchpoint->coords, rightvertex->coords);
    rightflag = rightccw > 0.0;
  }
  if (leftccw == 0.0) {
    return LEFTCOLLINEAR;
  } else if (rightccw == 0.0) {
    return RIGHTCOLLINEAR;
  } else {
    return WITHIN;
  }
}

/*****************************************************************************/
/*                                                                           */
/*  segmentintersection()   Find the intersection of an existing segment     */
/*                          and a segment that is being inserted.  Insert    */
/*                          a vertex at the intersection, splitting an       */
/*                          existing subsegment.                             */
/*                                                                           */
/*  The segment being inserted connects the apex of splittri to endpoint2.   */
/*  splitsubseg is the subsegment being split, and MUST adjoin splittri.     */
/*  Hence, endpoints of the subsegment being split are the origin and        */
/*  destination of splittri.                                                 */
/*                                                                           */
/*  On completion, splittri is a handle having the newly inserted            */
/*  intersection point as its origin, and endpoint1 as its destination.      */
/*                                                                           */
/*****************************************************************************/

void Mesh::segmentintersection(
                         struct otri *splittri, struct osub *splitsubseg,
                         vertex endpoint2)
{
  struct osub opposubseg;
  vertex endpoint1;
  vertex torg, tdest;
  vertex leftvertex, rightvertex;
  vertex newvertex;
  enum insertvertexresult success;
  REAL ex, ey;
  REAL tx, ty;
  //REAL etx, ety;
  REAL denom;
  
  /* Find the other three segment endpoints. */
  endpoint1 = splittri->apex();
  torg		= splittri->org();
  tdest		= splittri->dest();
  /* Segment intersection formulae; see the Antonio reference. */
  tx = tdest->coords[0] - torg->coords[0];
  ty = tdest->coords[1] - torg->coords[1];
  ex = endpoint2->coords[0] - endpoint1->coords[0];
  ey = endpoint2->coords[1] - endpoint1->coords[1];
  //etx = torg->coords[0] - endpoint2->coords[0];
  //ety = torg->coords[1] - endpoint2->coords[1];
  denom = ty * ex - tx * ey;
  if (denom == 0.0) {
    throw TriangleExc("Attempt to find intersection of parallel segments.");
  }
  //REAL split = (ey * etx - ex * ety) / denom;
  /* Create the new vertex. */
  makevertex(newvertex);
  
  setvertexmark(newvertex, mark(*splitsubseg));
  setvertextype(newvertex, wertex::INPUTVERTEX);
  if (b.verbose > 1) {
    printf(
  "  Splitting subsegment (%.12g, %.12g) (%.12g, %.12g) at (%.12g, %.12g).\n",
           torg->coords[0], torg->coords[1], tdest->coords[0], tdest->coords[1], newvertex->coords[0], newvertex->coords[1]);
  }
  /* Insert the intersection vertex.  This should always succeed. */
  success = insertvertex(newvertex, splittri, splitsubseg);
  if (success != SUCCESSFULVERTEX) {
    throw TriangleExc("Failure to split a segment");
  }
  /* Record a triangle whose origin is the new vertex. */
  setvertex2tri(newvertex, *splittri);
  if (this->steinerleft > 0) {
    this->steinerleft--;
  }

  /* Divide the segment into two, and correct the segment endpoints. */
  ssymself(*splitsubseg);
  spivot(*splitsubseg, opposubseg);
  sdissolve(*splitsubseg);
  sdissolve(opposubseg);
  do {
    setsegorg(*splitsubseg, newvertex);
    snextself(*splitsubseg);
  } while (splitsubseg->ss != this->dummysub);
  do {
    setsegorg(opposubseg, newvertex);
    snextself(opposubseg);
  } while (opposubseg.ss != this->dummysub);

  /* Inserting the vertex may have caused edge flips.  We wish to rediscover */
  /*   the edge connecting endpoint1 to the new intersection vertex.         */
  finddirection(splittri, endpoint1);
  rightvertex = splittri->dest();
  leftvertex  = splittri->apex();
  if ((leftvertex->coords[0] == endpoint1->coords[0]) && (leftvertex->coords[1] == endpoint1->coords[1])) {
    onextself(*splittri);
  } else if (rightvertex->coords != endpoint1->coords) {
    throw TriangleExc("Topological inconsistency after splitting a segment");

  }
  /* `splittri' should have destination endpoint1. */
}

/*****************************************************************************/
/*                                                                           */
/*  scoutsegment()   Scout the first triangle on the path from one endpoint  */
/*                   to another, and check for completion (reaching the      */
/*                   second endpoint), a collinear vertex, or the            */
/*                   intersection of two segments.                           */
/*                                                                           */
/*  Returns one if the entire segment is successfully inserted, and zero if  */
/*  the job must be finished by conformingedge() or constrainededge().       */
/*                                                                           */
/*  If the first triangle on the path has the second endpoint as its         */
/*  destination or apex, a subsegment is inserted and the job is done.       */
/*                                                                           */
/*  If the first triangle on the path has a destination or apex that lies on */
/*  the segment, a subsegment is inserted connecting the first endpoint to   */
/*  the collinear vertex, and the search is continued from the collinear     */
/*  vertex.                                                                  */
/*                                                                           */
/*  If the first triangle on the path has a subsegment opposite its origin,  */
/*  then there is a segment that intersects the segment being inserted.      */
/*  Their intersection vertex is inserted, splitting the subsegment.         */
/*                                                                           */
/*****************************************************************************/

int Mesh::scoutsegment( struct otri *searchtri,
						vertex endpoint2, int newmark)
{
  struct otri crosstri;
  struct osub crosssubseg;
  enum finddirectionresult collinear;

  collinear = finddirection(searchtri, endpoint2);
  vertex rightvertex = searchtri->dest();
  vertex leftvertex = searchtri->apex();


  if ( vect_eq(leftvertex->coords, endpoint2->coords) || 
	   vect_eq(rightvertex->coords,endpoint2->coords) ) 
  {
    /* The segment is already an edge in the mesh. */

    if (vect_eq(leftvertex->coords, endpoint2->coords)) {
      lprevself(*searchtri);
    }
    /* Insert a subsegment, if there isn't already one there. */
    insertsubseg(searchtri, newmark);
	return 1;

  } else if (collinear == LEFTCOLLINEAR) {
    /* We've collided with a vertex between the segment's endpoints. */
    /* Make the collinear vertex be the triangle's origin. */
    lprevself(*searchtri);
    insertsubseg(searchtri, newmark);
    /* Insert the remainder of the segment. */
    return scoutsegment(searchtri, endpoint2, newmark);

  } else if (collinear == RIGHTCOLLINEAR) {
    /* We've collided with a vertex between the segment's endpoints. */
    insertsubseg(searchtri, newmark);
    /* Make the collinear vertex be the triangle's origin. */
    lnextself(*searchtri);
    /* Insert the remainder of the segment. */
    return scoutsegment(searchtri, endpoint2, newmark);

  } else {
	/* General Case : the segment crosses the opposite side of searchtri */

    lnext(*searchtri, crosstri);
    tspivot(crosstri, crosssubseg);
    /* Check for a crossing segment. */
    if (crosssubseg.ss == this->dummysub) {
      return 0;
    } else {
      /* Insert a vertex at the intersection. */
      segmentintersection(&crosstri, &crosssubseg, endpoint2);
      *searchtri = crosstri;
      insertsubseg(searchtri, newmark);
      /* Insert the remainder of the segment. */
      return scoutsegment(searchtri, endpoint2, newmark);

    }

  }

}

/*****************************************************************************/
/*                                                                           */
/*  conformingedge()   Force a segment into a conforming Delaunay            */
/*                     triangulation by inserting a vertex at its midpoint,  */
/*                     and recursively forcing in the two half-segments if   */
/*                     necessary.                                            */
/*                                                                           */
/*  Generates a sequence of subsegments connecting `endpoint1' to            */
/*  `endpoint2'.  `newmark' is the boundary marker of the segment, assigned  */
/*  to each new splitting vertex and subsegment.                             */
/*                                                                           */
/*  Note that conformingedge() does not always maintain the conforming       */
/*  Delaunay property.  Once inserted, segments are locked into place;       */
/*  vertices inserted later (to force other segments in) may render these    */
/*  fixed segments non-Delaunay.  The conforming Delaunay property will be   */
/*  restored by enforcequality() by splitting encroached subsegments.        */
/*                                                                           */
/*****************************************************************************/


void Mesh::conformingedge(
                    vertex endpoint1, vertex endpoint2, int newmark)
{
  struct otri searchtri1, searchtri2;
  struct osub brokensubseg;
  vertex newvertex;
  vertex midvertex1, midvertex2;
  enum insertvertexresult success;

  if (b.verbose > 2) {
    printf("Forcing segment into triangulation by recursive splitting:\n");
    printf("  (%.12g, %.12g) (%.12g, %.12g)\n", endpoint1->coords[0], endpoint1->coords[1],
           endpoint2->coords[0], endpoint2->coords[1]);
  }
  /* Create a new vertex to insert in the middle of the segment. */
  makevertex(newvertex);
  setvertexmark(newvertex, newmark);
  setvertextype(newvertex, wertex::SEGMENTVERTEX);
  /* No known triangle to search from. */
  searchtri1.tri = this->dummytri;
  /* Attempt to insert the new vertex. */
  success = insertvertex(newvertex, &searchtri1, nullptr);
  if (success == DUPLICATEVERTEX) {
    if (b.verbose > 2) {
      printf("  Segment intersects existing vertex (%.12g, %.12g).\n",
             newvertex->coords[0], newvertex->coords[1]);
    }
    /* Use the vertex that's already there. */
    vertexdealloc(newvertex);
    newvertex = searchtri1.org();
  } else {
    if (success == VIOLATINGVERTEX) {
      if (b.verbose > 2) {
        printf("  Two segments intersect at (%.12g, %.12g).\n",
               newvertex->coords[0], newvertex->coords[1]);
      }
      /* By fluke, we've landed right on another segment.  Split it. */
      tspivot(searchtri1, brokensubseg);
      success = insertvertex(newvertex, &searchtri1, &brokensubseg);
      if (success != SUCCESSFULVERTEX) {
        throw TriangleExc("Failure to split a segment");
      }
    }
    /* The vertex has been inserted successfully. */
    if (this->steinerleft > 0) {
      this->steinerleft--;
    }
  }
  searchtri2 = searchtri1;
  /* `searchtri1' and `searchtri2' are fastened at their origins to         */
  /*   `newvertex', and will be directed toward `endpoint1' and `endpoint2' */
  /*   respectively.  First, we must get `searchtri2' out of the way so it  */
  /*   won't be invalidated during the insertion of the first half of the   */
  /*   segment.                                                             */
  finddirection(&searchtri2, endpoint2);
  if (!scoutsegment(&searchtri1, endpoint1, newmark)) {
    /* The origin of searchtri1 may have changed if a collision with an */
    /*   intervening vertex on the segment occurred.                    */
    midvertex1 = searchtri1.org();
    conformingedge(midvertex1, endpoint1, newmark);
  }
  if (!scoutsegment(&searchtri2, endpoint2, newmark)) {
    /* The origin of searchtri2 may have changed if a collision with an */
    /*   intervening vertex on the segment occurred.                    */
    midvertex2 = searchtri2.org();
    conformingedge(midvertex2, endpoint2, newmark);
  }
}


/*****************************************************************************/
/*                                                                           */
/*  delaunayfixup()   Enforce the Delaunay condition at an edge, fanning out */
/*                    recursively from an existing vertex.  Pay special      */
/*                    attention to stacking inverted triangles.              */
/*                                                                           */
/*  This is a support routine for inserting segments into a constrained      */
/*  Delaunay triangulation.                                                  */
/*                                                                           */
/*  The origin of fixuptri is treated as if it has just been inserted, and   */
/*  the local Delaunay condition needs to be enforced.  It is only enforced  */
/*  in one sector, however, that being the angular range defined by          */
/*  fixuptri.                                                                */
/*                                                                           */
/*  This routine also needs to make decisions regarding the "stacking" of    */
/*  triangles.  (Read the description of constrainededge() below before      */
/*  reading on here, so you understand the algorithm.)  If the position of   */
/*  the new vertex (the origin of fixuptri) indicates that the vertex before */
/*  it on the polygon is a reflex vertex, then "stack" the triangle by       */
/*  doing nothing.  (fixuptri is an inverted triangle, which is how stacked  */
/*  triangles are identified.)                                               */
/*                                                                           */
/*  Otherwise, check whether the vertex before that was a reflex vertex.     */
/*  If so, perform an edge flip, thereby eliminating an inverted triangle    */
/*  (popping it off the stack).  The edge flip may result in the creation    */
/*  of a new inverted triangle, depending on whether or not the new vertex   */
/*  is visible to the vertex three edges behind on the polygon.              */
/*                                                                           */
/*  If neither of the two vertices behind the new vertex are reflex          */
/*  vertices, fixuptri and fartri, the triangle opposite it, are not         */
/*  inverted; hence, ensure that the edge between them is locally Delaunay.  */
/*                                                                           */
/*  `leftside' indicates whether or not fixuptri is to the left of the       */
/*  segment being inserted.  (Imagine that the segment is pointing up from   */
/*  endpoint1 to endpoint2.)                                                 */
/*                                                                           */
/*****************************************************************************/

void Mesh::delaunayfixup(
                   struct otri *fixuptri, int leftside)
{
  struct otri neartri;
  struct otri fartri;
  struct osub faredge;
  vertex nearvertex, leftvertex, rightvertex, farvertex;
  
  lnext(*fixuptri, neartri);
  sym(neartri, fartri);
  /* Check if the edge opposite the origin of fixuptri can be flipped. */
  if (fartri.tri == this->dummytri) {
    return;
  }
  tspivot(neartri, faredge);
  if (faredge.ss != this->dummysub) {
    return;
  }
  /* Find all the relevant vertices. */
  nearvertex  = neartri.apex();
  leftvertex  = neartri.org();
  rightvertex = neartri.dest();
  farvertex   = fartri.apex();
  /* Check whether the previous polygon vertex is a reflex vertex. */
  if (leftside) {
    if (counterclockwise(nearvertex->coords, leftvertex->coords, farvertex->coords) <= 0.0) {
      /* leftvertex is a reflex vertex too.  Nothing can */
      /*   be done until a convex section is found.      */
      return;
    }
  } else {
    if (counterclockwise(farvertex->coords, rightvertex->coords, nearvertex->coords) <= 0.0) {
      /* rightvertex is a reflex vertex too.  Nothing can */
      /*   be done until a convex section is found.       */
      return;
    }
  }
  if (counterclockwise(rightvertex->coords, leftvertex->coords, farvertex->coords) > 0.0) {
    /* fartri is not an inverted triangle, and farvertex is not a reflex */
    /*   vertex.  As there are no reflex vertices, fixuptri isn't an     */
    /*   inverted triangle, either.  Hence, test the edge between the    */
    /*   triangles to ensure it is locally Delaunay.                     */
    if (nonregular(leftvertex->coords, farvertex->coords, rightvertex->coords, nearvertex->coords) <=
        0.0) {
      return;
    }
    /* Not locally Delaunay; go on to an edge flip. */
  }        /* else fartri is inverted; remove it from the stack by flipping. */
  flip(&neartri);
  lprevself(*fixuptri);    /* Restore the origin of fixuptri after the flip. */
  /* Recursively process the two triangles that result from the flip. */
  delaunayfixup(fixuptri, leftside);
  delaunayfixup(&fartri, leftside);
}

/*****************************************************************************/
/*                                                                           */
/*  constrainededge()   Force a segment into a constrained Delaunay          */
/*                      triangulation by deleting the triangles it           */
/*                      intersects, and triangulating the polygons that      */
/*                      form on each side of it.                             */
/*                                                                           */
/*  Generates a single subsegment connecting `endpoint1' to `endpoint2'.     */
/*  The triangle `starttri' has `endpoint1' as its origin.  `newmark' is the */
/*  boundary marker of the segment.                                          */
/*                                                                           */
/*  To insert a segment, every triangle whose interior intersects the        */
/*  segment is deleted.  The union of these deleted triangles is a polygon   */
/*  (which is not necessarily monotone, but is close enough), which is       */
/*  divided into two polygons by the new segment.  This routine's task is    */
/*  to generate the Delaunay triangulation of these two polygons.            */
/*                                                                           */
/*  You might think of this routine's behavior as a two-step process.  The   */
/*  first step is to walk from endpoint1 to endpoint2, flipping each edge    */
/*  encountered.  This step creates a fan of edges connected to endpoint1,   */
/*  including the desired edge to endpoint2.  The second step enforces the   */
/*  Delaunay condition on each side of the segment in an incremental manner: */
/*  proceeding along the polygon from endpoint1 to endpoint2 (this is done   */
/*  independently on each side of the segment), each vertex is "enforced"    */
/*  as if it had just been inserted, but affecting only the previous         */
/*  vertices.  The result is the same as if the vertices had been inserted   */
/*  in the order they appear on the polygon, so the result is Delaunay.      */
/*                                                                           */
/*  In truth, constrainededge() interleaves these two steps.  The procedure  */
/*  walks from endpoint1 to endpoint2, and each time an edge is encountered  */
/*  and flipped, the newly exposed vertex (at the far end of the flipped     */
/*  edge) is "enforced" upon the previously flipped edges, usually affecting */
/*  only one side of the polygon (depending upon which side of the segment   */
/*  the vertex falls on).                                                    */
/*                                                                           */
/*  The algorithm is complicated by the need to handle polygons that are not */
/*  convex.  Although the polygon is not necessarily monotone, it can be     */
/*  triangulated in a manner similar to the stack-based algorithms for       */
/*  monotone polygons.  For each reflex vertex (local concavity) of the      */
/*  polygon, there will be an inverted triangle formed by one of the edge    */
/*  flips.  (An inverted triangle is one with negative area - that is, its   */
/*  vertices are arranged in clockwise order - and is best thought of as a   */
/*  wrinkle in the fabric of the mesh.)  Each inverted triangle can be       */
/*  thought of as a reflex vertex pushed on the stack, waiting to be fixed   */
/*  later.                                                                   */
/*                                                                           */
/*  A reflex vertex is popped from the stack when a vertex is inserted that  */
/*  is visible to the reflex vertex.  (However, if the vertex behind the     */
/*  reflex vertex is not visible to the reflex vertex, a new inverted        */
/*  triangle will take its place on the stack.)  These details are handled   */
/*  by the delaunayfixup() routine above.                                    */
/*                                                                           */
/*****************************************************************************/

void Mesh::constrainededge(
                     struct otri *starttri, vertex endpoint2, int newmark)
{
  struct otri fixuptri, fixuptri2;
  struct osub crosssubseg;
  vertex endpoint1;
  vertex farvertex;
  REAL area;
  int collision;
  int done;
  
  endpoint1 = starttri->org();
  lnext(*starttri, fixuptri);
  flip(&fixuptri);
  /* `collision' indicates whether we have found a vertex directly */
  /*   between endpoint1 and endpoint2.                            */
  collision = 0;
  done = 0;
  do {
    farvertex = fixuptri.org();
    /* `farvertex' is the extreme point of the polygon we are "digging" */
    /*   to get from endpoint1 to endpoint2.                           */
    if (farvertex->coords == endpoint2->coords) {
      oprev(fixuptri, fixuptri2);
      /* Enforce the Delaunay condition around endpoint2. */
      delaunayfixup(&fixuptri, 0);
      delaunayfixup(&fixuptri2, 1);
      done = 1;
    } else {
      /* Check whether farvertex is to the left or right of the segment */
      /*   being inserted, to decide which edge of fixuptri to dig      */
      /*   through next.                                                */
      area = counterclockwise(endpoint1->coords, endpoint2->coords, farvertex->coords);
      if (area == 0.0) {
        /* We've collided with a vertex between endpoint1 and endpoint2. */
        collision = 1;
        oprev(fixuptri, fixuptri2);
        /* Enforce the Delaunay condition around farvertex. */
        delaunayfixup(&fixuptri, 0);
        delaunayfixup(&fixuptri2, 1);
        done = 1;
      } else {
        if (area > 0.0) {        /* farvertex is to the left of the segment. */
          oprev(fixuptri, fixuptri2);
          /* Enforce the Delaunay condition around farvertex, on the */
          /*   left side of the segment only.                        */
          delaunayfixup(&fixuptri2, 1);
          /* Flip the edge that crosses the segment.  After the edge is */
          /*   flipped, one of its endpoints is the fan vertex, and the */
          /*   destination of fixuptri is the fan vertex.               */
          lprevself(fixuptri);
        } else {                /* farvertex is to the right of the segment. */
          delaunayfixup(&fixuptri, 0);
          /* Flip the edge that crosses the segment.  After the edge is */
          /*   flipped, one of its endpoints is the fan vertex, and the */
          /*   destination of fixuptri is the fan vertex.               */
          oprevself(fixuptri);
        }
        /* Check for two intersecting segments. */
        tspivot(fixuptri, crosssubseg);
        if (crosssubseg.ss == this->dummysub) {
          flip(&fixuptri);    /* May create inverted triangle at left. */
        } else {
          /* We've collided with a segment between endpoint1 and endpoint2. */
          collision = 1;
          /* Insert a vertex at the intersection. */
          segmentintersection(&fixuptri, &crosssubseg, endpoint2);
          done = 1;
        }
      }
    }
  } while (!done);
  /* Insert a subsegment to make the segment permanent. */
  insertsubseg(&fixuptri, newmark);
  /* If there was a collision with an interceding vertex, install another */
  /*   segment connecting that vertex with endpoint2.                     */
  if (collision) {
    /* Insert the remainder of the segment. */
    if (!scoutsegment(&fixuptri, endpoint2, newmark)) {
      constrainededge(&fixuptri, endpoint2, newmark);
    }
  }
}

/*****************************************************************************/
/*                                                                           */
/*  insertsegment()   Insert a PSLG segment into a triangulation.            */
/*                                                                           */
/*****************************************************************************/

void Mesh::insertsegment(	vertex endpoint1, 
							vertex endpoint2, 
							int newmark )
{
	struct otri searchtri1, searchtri2;
	vertex checkvertex;
  
	/* Find a triangle whose origin is the segment's first endpoint. */
	checkvertex = nullptr;
	searchtri1 = vertex2tri(endpoint1);
	if (searchtri1.tri) {
		checkvertex = searchtri1.org();
	}

	if (checkvertex != endpoint1) {
		/* Find a boundary triangle to search from. */
		searchtri1.tri = this->dummytri;
		searchtri1.orient = 0;
		symself(searchtri1);
		/* Search for the segment's first endpoint by point location. */
		if (locate(endpoint1->coords, &searchtri1) != ONVERTEX)
		{
			throw TriangleExc_Vert2( " Unable to locate PSLG vertex for insertion between two vertex", endpoint1, endpoint2);
		}
	}

	/* Remember this triangle to improve subsequent point location. */
	this->recenttri = searchtri1;
	/* Scout the beginnings of a path from the first endpoint */
	/*   toward the second.                                   */

	if ( !scoutsegment(&searchtri1, endpoint2, newmark) )
	{
		/* The segment was not easily inserted. */
    
		/* The first endpoint may have changed if a collision with an intervening */
		/*   vertex on the segment occurred.                                      */
		endpoint1 = searchtri1.org();

		/* Find a triangle whose origin is the segment's second endpoint. */
		checkvertex = nullptr;
		searchtri2 = vertex2tri(endpoint2);
		if (searchtri2.tri)
		{
			checkvertex = searchtri2.org();
		}
		if (checkvertex != endpoint2)
		{
			/* Find a boundary triangle to search from. */
			searchtri2.tri = this->dummytri;
			searchtri2.orient = 0;
			symself(searchtri2);
			/* Search for the segment's second endpoint by point location. */
			if (locate(endpoint2->coords, &searchtri2) != ONVERTEX) 
			{
				throw TriangleExc_Vert("Unable to locate PSLG vertex", endpoint2);
			}
		}

		/* Remember this triangle to improve subsequent point location. */
		this->recenttri = searchtri2;
		/* Scout the beginnings of a path from the second endpoint */
		/*   toward the first.                                     */
		
		if (!scoutsegment(&searchtri2, endpoint1, newmark)) 
		{
			/* The segment was not easily inserted. */

			/* The second endpoint may have changed if a collision with an intervening */
			/*   vertex on the segment occurred.                                       */
			endpoint2 = searchtri2.org();

			if (b.splitseg) {
				/* Insert vertices to force the segment into the triangulation. */
				conformingedge(endpoint1, endpoint2, newmark);
			} else {
				/* Insert the segment directly into the triangulation. */
				constrainededge(&searchtri1, endpoint2, newmark);
			}
		}
	}
	
}

/*****************************************************************************/
/*                                                                           */
/*  markhull()   Cover the convex hull of a triangulation with subsegments.  */
/*                                                                           */
/*****************************************************************************/


void Mesh::markhull()
{
  struct otri hulltri;
  struct otri nexttri;
  struct otri starttri;
  
  /* Find a triangle handle on the hull. */
  hulltri.tri = this->dummytri;
  hulltri.orient = 0;
  symself(hulltri);
  /* Remember where we started so we know when to stop. */
  starttri = hulltri;
  /* Go once counterclockwise around the convex hull. */
  do {
    /* Create a subsegment if there isn't already one here. */
    insertsubseg(&hulltri, 1);
    /* To find the next hull edge, go clockwise around the next vertex. */
    lnextself(hulltri);
    oprev(hulltri, nexttri);
    while (nexttri.tri != this->dummytri) {
      hulltri = nexttri;
      oprev(hulltri, nexttri);
    }
  } while ( hulltri != starttri );
}


/**                                                                         **/
/**                                                                         **/
/********* Segment insertion ends here                               *********/


/********* File I/O routines begin here                              *********/
/**                                                                         **/
/**                                                                         **/

/*****************************************************************************/
/*                                                                           */
/*  Add Vertices									.                         */
/*                                                                           */
/*****************************************************************************/

std::vector<wertex*> Mesh::addVertices_py(	const std::vector<Point2d>& pointlist,
										const std::vector<REAL>& pointattriblist,
										const std::vector<REAL>& pointmarkerlist)
{
	const size_t numberofpoints = pointlist.size();
	const size_t numberofpointattribs = pointattriblist.size() / numberofpoints;
	//assert( numberofpointattribs == 1 );
	bool hasmarkers = pointmarkerlist.size()!=0;

	std::vector<int> markers(numberofpoints);
	std::vector<REAL> attribs(std::max(numberofpoints,pointattriblist.size()));

	int attribindex = 0;

	size_t i;
	/* Read the vertices. */
	for (i = 0; i < numberofpoints; i++) {

		/* If no markers are specified, they default to zero. */
		int pointmarker = 0;
		if (hasmarkers != 0 ) {
		  /* Read a vertex marker. */
		   markers[i] = (int)pointmarkerlist[i];
		}
		markers[i] = pointmarker;

		/* Read the vertex attributes. */
		if( numberofpointattribs )
		{
			for (size_t j = 0; j < numberofpointattribs; j++, attribindex++) {
			  attribs[attribindex] = pointattriblist[numberofpointattribs*i+j];
			}
		}
		else
		{
			attribs[attribindex] = 0;
		}
	}
	std::vector<wertex*> out;
	addvertices(&pointlist[0],numberofpoints,out,&attribs[0],&markers[0]);

	// perform triangulation
	delaunay();

	if( searchGraph )
	{
		resetDual();
	}
	
	return out;
}

void Mesh::addvertices(   Point2d const* ptList,
						  size_t numberofpoints,
						  std::vector<wertex*>& out,
						  const REAL* attribs,
						  const int* markers)
{
  out.resize(numberofpoints);

  /* Read the vertices. */
  for (size_t i = 0; i < numberofpoints; i++)
  {    
	/* If no markers are specified, they default to zero. */
	int pointmarker = 0;
	if ( markers ) {
      /* Read a vertex marker. */
      pointmarker = markers[i];
	}

	wertex * newvertex;
	makevertex(newvertex);
	
	out[i] = newvertex;
	Point2d const* pt = (ptList+i);
    /* Read the vertex coordinates. */
    newvertex->coords = *pt;

    /* Read the vertex attributes. */
	if ( attribs ) 
	{
		newvertex->setRadius(attribs[i]);
	}else
	{
		newvertex->setRadius(0);
	}
   
    setvertexmark(newvertex, pointmarker);
    setvertextype(newvertex, wertex::INPUTVERTEX);
	
	const REAL x = pt->x;
	const REAL y = pt->y;
    /* Determine the smallest and largest x and y coordinates. */
    if (this->vertices.size() == 1) {
      this->xmin = this->xmax = x;
      this->ymin = this->ymax = y;
    } else {
      this->xmin = (x < this->xmin) ? x : this->xmin;
      this->xmax = (x > this->xmax) ? x : this->xmax;
      this->ymin = (y < this->ymin) ? y : this->ymin;
      this->ymax = (y > this->ymax) ? y : this->ymax;
    }
  }
}

wertex* Mesh::insertVertex_py(	const Point2d& point,
								const std::vector<REAL>& attribvec,
								int marker,
								const wertex* const nearbyVertex )
{
	// TODO: check delaunay has been performed once so there preexists a triangulation
	
	size_t numAttribs = attribvec.size();
	assert( numAttribs == 1);// only one arg that should be the weight.
	
	std::vector<wertex*> vvect;
	addvertices(&point,1,vvect,&attribvec[0],&marker);
	
	struct otri searchtri;
	if( nearbyVertex ){
		searchtri = nearbyVertex->getTri();
	}
	else
	{
		searchtri.tri = this->dummytri;
		//searchtri = this->recenttri; //TODO check recenttri is not nullptr
	}
	enum insertvertexresult result = insertvertex(vvect[0],&searchtri);

	if( result != SUCCESSFULVERTEX ){
		std::ostringstream os;
		os << result << " insertVertex failed at " << point << ", r=" << vertexradius(vvect[0]);
		throw TriangleExc(os.str());
	}

	if( searchGraph && segmentend(vvect[0]) )
	{
		// Update searchGraph if the new vertex is a boundary endpoint.
		wertex* wertexDual = searchDual->insertVertex_py( point, attribvec, marker, nearbyVertex );
		vvect[0]->dualVert = wertexDual;
		wertexDual->dualVert = vvect[0];

		searchDual->resetConvexAreas();
	}

	return vvect[0];
}


/********* Geometric primitives begin here                           *********/
/**                                                                         **/
/**                                                                         **/

/* The adaptive exact arithmetic geometric predicates implemented herein are */
/*   described in detail in my paper, "Adaptive Precision Floating-Point     */
/*   Arithmetic and Fast Robust Geometric Predicates."  See the header for a */
/*   full citation.                                                          */

/* Which of the following two methods of finding the absolute values is      */
/*   fastest is compiler-dependent.  A few compilers can inline and optimize */
/*   the fabs() call; but most will incur the overhead of a function call,   */
/*   which is disastrously slow.  A faster way on IEEE machines might be to  */
/*   mask the appropriate bit, but that's difficult to do in C without       */
/*   forcing the value to be stored to memory (rather than be kept in the    */
/*   register to which the optimizer assigned it).                           */

#define Absolute(a)  ((a) >= 0.0 ? (a) : -(a))
/* #define Absolute(a)  fabs(a) */

/* Many of the operations are broken up into two pieces, a main part that    */
/*   performs an approximate operation, and a "tail" that computes the       */
/*   roundoff error of that operation.                                       */
/*                                                                           */
/* The operations Fast_Two_Sum(), Fast_Two_Diff(), Two_Sum(), Two_Diff(),    */
/*   Split(), and Two_Product() are all implemented as described in the      */
/*   reference.  Each of these macros requires certain variables to be       */
/*   defined in the calling routine.  The variables `bvirt', `c', `abig',    */
/*   `_i', `_j', `_k', `_l', `_m', and `_n' are declared `INEXACT' because   */
/*   they store the result of an operation that may incur roundoff error.    */
/*   The input parameter `x' (or the highest numbered `x_' parameter) must   */
/*   also be declared `INEXACT'.                                             */

#define Fast_Two_Sum_Tail(a, b, x, y) \
  bvirt = x - a; \
  y = b - bvirt

#define Fast_Two_Sum(a, b, x, y) \
  x = (REAL) (a + b); \
  Fast_Two_Sum_Tail(a, b, x, y)

#define Two_Sum_Tail(a, b, x, y) \
  bvirt = (REAL) (x - a); \
  avirt = x - bvirt; \
  bround = b - bvirt; \
  around = a - avirt; \
  y = around + bround

#define Two_Sum(a, b, x, y) \
  x = (REAL) (a + b); \
  Two_Sum_Tail(a, b, x, y)

#define Two_Diff_Tail(a, b, x, y) \
  bvirt = (REAL) (a - x); \
  avirt = x + bvirt; \
  bround = bvirt - b; \
  around = a - avirt; \
  y = around + bround

#define Two_Diff(a, b, x, y) \
  x = (REAL) (a - b); \
  Two_Diff_Tail(a, b, x, y)

#define Split(a, ahi, alo) \
  c = (REAL) (splitter * a); \
  abig = (REAL) (c - a); \
  ahi = c - abig; \
  alo = a - ahi

#define Two_Product_Tail(a, b, x, y) \
  Split(a, ahi, alo); \
  Split(b, bhi, blo); \
  err1 = x - (ahi * bhi); \
  err2 = err1 - (alo * bhi); \
  err3 = err2 - (ahi * blo); \
  y = (alo * blo) - err3

#define Two_Product(a, b, x, y) \
  x = (REAL) (a * b); \
  Two_Product_Tail(a, b, x, y)

/* Two_Product_Presplit() is Two_Product() where one of the inputs has       */
/*   already been split.  Avoids redundant splitting.                        */

#define Two_Product_Presplit(a, b, bhi, blo, x, y) \
  x = (REAL) (a * b); \
  Split(a, ahi, alo); \
  err1 = x - (ahi * bhi); \
  err2 = err1 - (alo * bhi); \
  err3 = err2 - (ahi * blo); \
  y = (alo * blo) - err3

/* Square() can be done more quickly than Two_Product().                     */

#define Square_Tail(a, x, y) \
  Split(a, ahi, alo); \
  err1 = x - (ahi * ahi); \
  err3 = err1 - ((ahi + ahi) * alo); \
  y = (alo * alo) - err3

#define Square(a, x, y) \
  x = (REAL) (a * a); \
  Square_Tail(a, x, y)

/* Macros for summing expansions of various fixed lengths.  These are all    */
/*   unrolled versions of Expansion_Sum().                                   */

#define Two_One_Sum(a1, a0, b, x2, x1, x0) \
  Two_Sum(a0, b , _i, x0); \
  Two_Sum(a1, _i, x2, x1)

#define Two_One_Diff(a1, a0, b, x2, x1, x0) \
  Two_Diff(a0, b , _i, x0); \
  Two_Sum( a1, _i, x2, x1)

#define Two_Two_Sum(a1, a0, b1, b0, x3, x2, x1, x0) \
  Two_One_Sum(a1, a0, b0, _j, _0, x0); \
  Two_One_Sum(_j, _0, b1, x3, x2, x1)

#define Two_Two_Diff(a1, a0, b1, b0, x3, x2, x1, x0) \
  Two_One_Diff(a1, a0, b0, _j, _0, x0); \
  Two_One_Diff(_j, _0, b1, x3, x2, x1)

/* Macro for multiplying a two-component expansion by a single component.    */

#define Two_One_Product(a1, a0, b, x3, x2, x1, x0) \
  Split(b, bhi, blo); \
  Two_Product_Presplit(a0, b, bhi, blo, _i, x0); \
  Two_Product_Presplit(a1, b, bhi, blo, _j, _0); \
  Two_Sum(_i, _0, _k, x1); \
  Fast_Two_Sum(_j, _k, x3, x2)


/*****************************************************************************/
/*                                                                           */
/*  fast_expansion_sum_zeroelim()   Sum two expansions, eliminating zero     */
/*                                  components from the output expansion.    */
/*                                                                           */
/*  Sets h = e + f.  See my Robust Predicates paper for details.             */
/*                                                                           */
/*  If round-to-even is used (as with IEEE 754), maintains the strongly      */
/*  nonoverlapping property.  (That is, if e is strongly nonoverlapping, h   */
/*  will be also.)  Does NOT maintain the nonoverlapping or nonadjacent      */
/*  properties.                                                              */
/*                                                                           */
/*****************************************************************************/

int fast_expansion_sum_zeroelim(int elen, REAL *e, int flen, REAL *f, REAL *h)
{
  REAL Q;
  INEXACT REAL Qnew;
  INEXACT REAL hh;
  INEXACT REAL bvirt;
  REAL avirt, bround, around;
  int eindex, findex, hindex;
  REAL enow, fnow;

  enow = e[0];
  fnow = f[0];
  eindex = findex = 0;
  if ((fnow > enow) == (fnow > -enow)) {
    Q = enow;
    enow = e[++eindex];
  } else {
    Q = fnow;
    fnow = f[++findex];
  }
  hindex = 0;
  if ((eindex < elen) && (findex < flen)) {
    if ((fnow > enow) == (fnow > -enow)) {
      Fast_Two_Sum(enow, Q, Qnew, hh);
      enow = e[++eindex];
    } else {
      Fast_Two_Sum(fnow, Q, Qnew, hh);
      fnow = f[++findex];
    }
    Q = Qnew;
    if (hh != 0.0) {
      h[hindex++] = hh;
    }
    while ((eindex < elen) && (findex < flen)) {
      if ((fnow > enow) == (fnow > -enow)) {
        Two_Sum(Q, enow, Qnew, hh);
        enow = e[++eindex];
      } else {
        Two_Sum(Q, fnow, Qnew, hh);
        fnow = f[++findex];
      }
      Q = Qnew;
      if (hh != 0.0) {
        h[hindex++] = hh;
      }
    }
  }
  while (eindex < elen) {
    Two_Sum(Q, enow, Qnew, hh);
    enow = e[++eindex];
    Q = Qnew;
    if (hh != 0.0) {
      h[hindex++] = hh;
    }
  }
  while (findex < flen) {
    Two_Sum(Q, fnow, Qnew, hh);
    fnow = f[++findex];
    Q = Qnew;
    if (hh != 0.0) {
      h[hindex++] = hh;
    }
  }
  if ((Q != 0.0) || (hindex == 0)) {
    h[hindex++] = Q;
  }
  return hindex;
}

/*****************************************************************************/
/*                                                                           */
/*  scale_expansion_zeroelim()   Multiply an expansion by a scalar,          */
/*                               eliminating zero components from the        */
/*                               output expansion.                           */
/*                                                                           */
/*  Sets h = be.  See my Robust Predicates paper for details.                */
/*                                                                           */
/*  Maintains the nonoverlapping property.  If round-to-even is used (as     */
/*  with IEEE 754), maintains the strongly nonoverlapping and nonadjacent    */
/*  properties as well.  (That is, if e has one of these properties, so      */
/*  will h.)                                                                 */
/*                                                                           */
/*****************************************************************************/

int scale_expansion_zeroelim(int elen, REAL *e, REAL b, REAL *h)
{
  INEXACT REAL Q, sum;
  REAL hh;
  INEXACT REAL product1;
  REAL product0;
  int eindex, hindex;
  REAL enow;
  INEXACT REAL bvirt;
  REAL avirt, bround, around;
  INEXACT REAL c;
  INEXACT REAL abig;
  REAL ahi, alo, bhi, blo;
  REAL err1, err2, err3;

  Split(b, bhi, blo);
  Two_Product_Presplit(e[0], b, bhi, blo, Q, hh);
  hindex = 0;
  if (hh != 0) {
    h[hindex++] = hh;
  }
  for (eindex = 1; eindex < elen; eindex++) {
    enow = e[eindex];
    Two_Product_Presplit(enow, b, bhi, blo, product1, product0);
    Two_Sum(Q, product0, sum, hh);
    if (hh != 0) {
      h[hindex++] = hh;
    }
    Fast_Two_Sum(product1, sum, Q, hh);
    if (hh != 0) {
      h[hindex++] = hh;
    }
  }
  if ((Q != 0.0) || (hindex == 0)) {
    h[hindex++] = Q;
  }
  return hindex;
}

/*****************************************************************************/
/*                                                                           */
/*  estimate()   Produce a one-word estimate of an expansion's value.        */
/*                                                                           */
/*  See my Robust Predicates paper for details.                              */
/*                                                                           */
/*****************************************************************************/

REAL estimate(int elen, REAL *e)
{
  REAL Q;
  int eindex;

  Q = e[0];
  for (eindex = 1; eindex < elen; eindex++) {
    Q += e[eindex];
  }
  return Q;
}

/*****************************************************************************/
/*                                                                           */
/*  counterclockwise()   Return a positive value if the points pa, pb, and   */
/*                       pc occur in counterclockwise order; a negative      */
/*                       value if they occur in clockwise order; and zero    */
/*                       if they are collinear.  The result is also a rough  */
/*                       approximation of twice the signed area of the       */
/*                       triangle defined by the three points.               */
/*                                                                           */
/*  Uses exact arithmetic if necessary to ensure a correct answer.  The      */
/*  result returned is the determinant of a matrix.  This determinant is     */
/*  computed adaptively, in the sense that exact arithmetic is used only to  */
/*  the degree it is needed to ensure that the returned value has the        */
/*  correct sign.  Hence, this function is usually quite fast, but will run  */
/*  more slowly when the input points are collinear or nearly so.            */
/*                                                                           */
/*  See my Robust Predicates paper for details.                              */
/*                                                                           */
/*****************************************************************************/

REAL counterclockwiseadapt(const REAL pa[2], const REAL pb[2], const REAL pc[2], REAL detsum)
{
  INEXACT REAL acx, acy, bcx, bcy;
  REAL acxtail, acytail, bcxtail, bcytail;
  INEXACT REAL detleft, detright;
  REAL detlefttail, detrighttail;
  REAL det, errbound;
  REAL B[4], C1[8], C2[12], D[16];
  INEXACT REAL B3;
  int C1length, C2length, Dlength;
  REAL u[4];
  INEXACT REAL u3;
  INEXACT REAL s1, t1;
  REAL s0, t0;

  INEXACT REAL bvirt;
  REAL avirt, bround, around;
  INEXACT REAL c;
  INEXACT REAL abig;
  REAL ahi, alo, bhi, blo;
  REAL err1, err2, err3;
  INEXACT REAL _i, _j;
  REAL _0;

  acx = (REAL) (pa[0] - pc[0]);
  bcx = (REAL) (pb[0] - pc[0]);
  acy = (REAL) (pa[1] - pc[1]);
  bcy = (REAL) (pb[1] - pc[1]);

  Two_Product(acx, bcy, detleft, detlefttail);
  Two_Product(acy, bcx, detright, detrighttail);

  Two_Two_Diff(detleft, detlefttail, detright, detrighttail,
               B3, B[2], B[1], B[0]);
  B[3] = B3;

  det = estimate(4, B);
  errbound = ccwerrboundB * detsum;
  if ((det >= errbound) || (-det >= errbound)) {
    return det;
  }

  Two_Diff_Tail(pa[0], pc[0], acx, acxtail);
  Two_Diff_Tail(pb[0], pc[0], bcx, bcxtail);
  Two_Diff_Tail(pa[1], pc[1], acy, acytail);
  Two_Diff_Tail(pb[1], pc[1], bcy, bcytail);

  if ((acxtail == 0.0) && (acytail == 0.0)
      && (bcxtail == 0.0) && (bcytail == 0.0)) {
    return det;
  }

  errbound = ccwerrboundC * detsum + resulterrbound * Absolute(det);
  det += (acx * bcytail + bcy * acxtail)
       - (acy * bcxtail + bcx * acytail);
  if ((det >= errbound) || (-det >= errbound)) {
    return det;
  }

  Two_Product(acxtail, bcy, s1, s0);
  Two_Product(acytail, bcx, t1, t0);
  Two_Two_Diff(s1, s0, t1, t0, u3, u[2], u[1], u[0]);
  u[3] = u3;
  C1length = fast_expansion_sum_zeroelim(4, B, 4, u, C1);

  Two_Product(acx, bcytail, s1, s0);
  Two_Product(acy, bcxtail, t1, t0);
  Two_Two_Diff(s1, s0, t1, t0, u3, u[2], u[1], u[0]);
  u[3] = u3;
  C2length = fast_expansion_sum_zeroelim(C1length, C1, 4, u, C2);

  Two_Product(acxtail, bcytail, s1, s0);
  Two_Product(acytail, bcxtail, t1, t0);
  Two_Two_Diff(s1, s0, t1, t0, u3, u[2], u[1], u[0]);
  u[3] = u3;
  Dlength = fast_expansion_sum_zeroelim(C2length, C2, 4, u, D);

  return(D[Dlength - 1]);
}

REAL Mesh::counterclockwise(const Point2d& pa, const Point2d& pb, const Point2d& pc)
{
  REAL detleft  = (pa[0] - pc[0]) * (pb[1] - pc[1]);
  REAL detright = (pa[1] - pc[1]) * (pb[0] - pc[0]);
  REAL det = detleft - detright;

  if (b.noexact) {
    return det;
  }

  REAL detsum, errbound;

  if (detleft > 0.0) {
    if (detright <= 0.0) {
      return det;
    } else {
      detsum = detleft + detright;
    }
  } else if (detleft < 0.0) {
    if (detright >= 0.0) {
      return det;
    } else {
      detsum = -detleft - detright;
    }
  } else {
    return det;
  }

  errbound = ccwerrboundA * detsum;
  if ((det >= errbound) || (-det >= errbound)) {
    return det;
  }

  return counterclockwiseadapt(&pa.x, &pb.x, &pc.x, detsum);
}

/*****************************************************************************/
/*                                                                           */
/*  incircle()   Return a positive value if the point pd lies inside the     */
/*               circle passing through pa, pb, and pc; a negative value if  */
/*               it lies outside; and zero if the four points are cocircular.*/
/*               The points pa, pb, and pc must be in counterclockwise       */
/*               order, or the sign of the result will be reversed.          */
/*                                                                           */
/*  Uses exact arithmetic if necessary to ensure a correct answer.  The      */
/*  result returned is the determinant of a matrix.  This determinant is     */
/*  computed adaptively, in the sense that exact arithmetic is used only to  */
/*  the degree it is needed to ensure that the returned value has the        */
/*  correct sign.  Hence, this function is usually quite fast, but will run  */
/*  more slowly when the input points are cocircular or nearly so.           */
/*                                                                           */
/*  See my Robust Predicates paper for details.                              */
/*                                                                           */
/*****************************************************************************/

REAL incircleadapt(const REAL* pa, const REAL* pb, const REAL* pc, const REAL* pd, REAL permanent)
{
  INEXACT REAL adx, bdx, cdx, ady, bdy, cdy;
  REAL det, errbound;

  INEXACT REAL bdxcdy1, cdxbdy1, cdxady1, adxcdy1, adxbdy1, bdxady1;
  REAL bdxcdy0, cdxbdy0, cdxady0, adxcdy0, adxbdy0, bdxady0;
  REAL bc[4], ca[4], ab[4];
  INEXACT REAL bc3, ca3, ab3;
  REAL axbc[8], axxbc[16], aybc[8], ayybc[16], adet[32];
  int axbclen, axxbclen, aybclen, ayybclen, alen;
  REAL bxca[8], bxxca[16], byca[8], byyca[16], bdet[32];
  int bxcalen, bxxcalen, bycalen, byycalen, blen;
  REAL cxab[8], cxxab[16], cyab[8], cyyab[16], cdet[32];
  int cxablen, cxxablen, cyablen, cyyablen, clen;
  REAL abdet[64];
  int ablen;
  REAL fin1[1152], fin2[1152];
  REAL *finnow, *finother, *finswap;
  int finlength;

  REAL adxtail, bdxtail, cdxtail, adytail, bdytail, cdytail;
  INEXACT REAL adxadx1, adyady1, bdxbdx1, bdybdy1, cdxcdx1, cdycdy1;
  REAL adxadx0, adyady0, bdxbdx0, bdybdy0, cdxcdx0, cdycdy0;
  REAL aa[4], bb[4], cc[4];
  INEXACT REAL aa3, bb3, cc3;
  INEXACT REAL ti1, tj1;
  REAL ti0, tj0;
  REAL u[4], v[4];
  INEXACT REAL u3, v3;
  REAL temp8[8], temp16a[16], temp16b[16], temp16c[16];
  REAL temp32a[32], temp32b[32], temp48[48], temp64[64];
  int temp8len, temp16alen, temp16blen, temp16clen;
  int temp32alen, temp32blen, temp48len, temp64len;
  REAL axtbb[8], axtcc[8], aytbb[8], aytcc[8];
  int axtbblen, axtcclen, aytbblen, aytcclen;
  REAL bxtaa[8], bxtcc[8], bytaa[8], bytcc[8];
  int bxtaalen, bxtcclen, bytaalen, bytcclen;
  REAL cxtaa[8], cxtbb[8], cytaa[8], cytbb[8];
  int cxtaalen, cxtbblen, cytaalen, cytbblen;
  REAL axtbc[8], aytbc[8], bxtca[8], bytca[8], cxtab[8], cytab[8];
  int axtbclen, aytbclen, bxtcalen, bytcalen, cxtablen, cytablen;
  REAL axtbct[16], aytbct[16], bxtcat[16], bytcat[16], cxtabt[16], cytabt[16];
  int axtbctlen, aytbctlen, bxtcatlen, bytcatlen, cxtabtlen, cytabtlen;
  REAL axtbctt[8], aytbctt[8], bxtcatt[8];
  REAL bytcatt[8], cxtabtt[8], cytabtt[8];
  int axtbcttlen, aytbcttlen, bxtcattlen, bytcattlen, cxtabttlen, cytabttlen;
  REAL abt[8], bct[8], cat[8];
  int abtlen, bctlen, catlen;
  REAL abtt[4], bctt[4], catt[4];
  int abttlen, bcttlen, cattlen;
  INEXACT REAL abtt3, bctt3, catt3;
  REAL negate;

  INEXACT REAL bvirt;
  REAL avirt, bround, around;
  INEXACT REAL c;
  INEXACT REAL abig;
  REAL ahi, alo, bhi, blo;
  REAL err1, err2, err3;
  INEXACT REAL _i, _j;
  REAL _0;

  adx = (REAL) (pa[0] - pd[0]);
  bdx = (REAL) (pb[0] - pd[0]);
  cdx = (REAL) (pc[0] - pd[0]);
  ady = (REAL) (pa[1] - pd[1]);
  bdy = (REAL) (pb[1] - pd[1]);
  cdy = (REAL) (pc[1] - pd[1]);

  Two_Product(bdx, cdy, bdxcdy1, bdxcdy0);
  Two_Product(cdx, bdy, cdxbdy1, cdxbdy0);
  Two_Two_Diff(bdxcdy1, bdxcdy0, cdxbdy1, cdxbdy0, bc3, bc[2], bc[1], bc[0]);
  bc[3] = bc3;
  axbclen = scale_expansion_zeroelim(4, bc, adx, axbc);
  axxbclen = scale_expansion_zeroelim(axbclen, axbc, adx, axxbc);
  aybclen = scale_expansion_zeroelim(4, bc, ady, aybc);
  ayybclen = scale_expansion_zeroelim(aybclen, aybc, ady, ayybc);
  alen = fast_expansion_sum_zeroelim(axxbclen, axxbc, ayybclen, ayybc, adet);

  Two_Product(cdx, ady, cdxady1, cdxady0);
  Two_Product(adx, cdy, adxcdy1, adxcdy0);
  Two_Two_Diff(cdxady1, cdxady0, adxcdy1, adxcdy0, ca3, ca[2], ca[1], ca[0]);
  ca[3] = ca3;
  bxcalen = scale_expansion_zeroelim(4, ca, bdx, bxca);
  bxxcalen = scale_expansion_zeroelim(bxcalen, bxca, bdx, bxxca);
  bycalen = scale_expansion_zeroelim(4, ca, bdy, byca);
  byycalen = scale_expansion_zeroelim(bycalen, byca, bdy, byyca);
  blen = fast_expansion_sum_zeroelim(bxxcalen, bxxca, byycalen, byyca, bdet);

  Two_Product(adx, bdy, adxbdy1, adxbdy0);
  Two_Product(bdx, ady, bdxady1, bdxady0);
  Two_Two_Diff(adxbdy1, adxbdy0, bdxady1, bdxady0, ab3, ab[2], ab[1], ab[0]);
  ab[3] = ab3;
  cxablen = scale_expansion_zeroelim(4, ab, cdx, cxab);
  cxxablen = scale_expansion_zeroelim(cxablen, cxab, cdx, cxxab);
  cyablen = scale_expansion_zeroelim(4, ab, cdy, cyab);
  cyyablen = scale_expansion_zeroelim(cyablen, cyab, cdy, cyyab);
  clen = fast_expansion_sum_zeroelim(cxxablen, cxxab, cyyablen, cyyab, cdet);

  ablen = fast_expansion_sum_zeroelim(alen, adet, blen, bdet, abdet);
  finlength = fast_expansion_sum_zeroelim(ablen, abdet, clen, cdet, fin1);

  det = estimate(finlength, fin1);
  errbound = iccerrboundB * permanent;
  if ((det >= errbound) || (-det >= errbound)) {
    return det;
  }

  Two_Diff_Tail(pa[0], pd[0], adx, adxtail);
  Two_Diff_Tail(pa[1], pd[1], ady, adytail);
  Two_Diff_Tail(pb[0], pd[0], bdx, bdxtail);
  Two_Diff_Tail(pb[1], pd[1], bdy, bdytail);
  Two_Diff_Tail(pc[0], pd[0], cdx, cdxtail);
  Two_Diff_Tail(pc[1], pd[1], cdy, cdytail);
  if ((adxtail == 0.0) && (bdxtail == 0.0) && (cdxtail == 0.0)
      && (adytail == 0.0) && (bdytail == 0.0) && (cdytail == 0.0)) {
    return det;
  }

  errbound = iccerrboundC * permanent + resulterrbound * Absolute(det);
  det += ((adx * adx + ady * ady) * ((bdx * cdytail + cdy * bdxtail)
                                     - (bdy * cdxtail + cdx * bdytail))
          + 2.0 * (adx * adxtail + ady * adytail) * (bdx * cdy - bdy * cdx))
       + ((bdx * bdx + bdy * bdy) * ((cdx * adytail + ady * cdxtail)
                                     - (cdy * adxtail + adx * cdytail))
          + 2.0 * (bdx * bdxtail + bdy * bdytail) * (cdx * ady - cdy * adx))
       + ((cdx * cdx + cdy * cdy) * ((adx * bdytail + bdy * adxtail)
                                     - (ady * bdxtail + bdx * adytail))
          + 2.0 * (cdx * cdxtail + cdy * cdytail) * (adx * bdy - ady * bdx));
  if ((det >= errbound) || (-det >= errbound)) {
    return det;
  }

  finnow = fin1;
  finother = fin2;

  if ((bdxtail != 0.0) || (bdytail != 0.0)
      || (cdxtail != 0.0) || (cdytail != 0.0)) {
    Square(adx, adxadx1, adxadx0);
    Square(ady, adyady1, adyady0);
    Two_Two_Sum(adxadx1, adxadx0, adyady1, adyady0, aa3, aa[2], aa[1], aa[0]);
    aa[3] = aa3;
  }
  if ((cdxtail != 0.0) || (cdytail != 0.0)
      || (adxtail != 0.0) || (adytail != 0.0)) {
    Square(bdx, bdxbdx1, bdxbdx0);
    Square(bdy, bdybdy1, bdybdy0);
    Two_Two_Sum(bdxbdx1, bdxbdx0, bdybdy1, bdybdy0, bb3, bb[2], bb[1], bb[0]);
    bb[3] = bb3;
  }
  if ((adxtail != 0.0) || (adytail != 0.0)
      || (bdxtail != 0.0) || (bdytail != 0.0)) {
    Square(cdx, cdxcdx1, cdxcdx0);
    Square(cdy, cdycdy1, cdycdy0);
    Two_Two_Sum(cdxcdx1, cdxcdx0, cdycdy1, cdycdy0, cc3, cc[2], cc[1], cc[0]);
    cc[3] = cc3;
  }

  if (adxtail != 0.0) {
    axtbclen = scale_expansion_zeroelim(4, bc, adxtail, axtbc);
    temp16alen = scale_expansion_zeroelim(axtbclen, axtbc, 2.0 * adx,
                                          temp16a);

    axtcclen = scale_expansion_zeroelim(4, cc, adxtail, axtcc);
    temp16blen = scale_expansion_zeroelim(axtcclen, axtcc, bdy, temp16b);

    axtbblen = scale_expansion_zeroelim(4, bb, adxtail, axtbb);
    temp16clen = scale_expansion_zeroelim(axtbblen, axtbb, -cdy, temp16c);

    temp32alen = fast_expansion_sum_zeroelim(temp16alen, temp16a,
                                            temp16blen, temp16b, temp32a);
    temp48len = fast_expansion_sum_zeroelim(temp16clen, temp16c,
                                            temp32alen, temp32a, temp48);
    finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp48len,
                                            temp48, finother);
    finswap = finnow; finnow = finother; finother = finswap;
  }
  if (adytail != 0.0) {
    aytbclen = scale_expansion_zeroelim(4, bc, adytail, aytbc);
    temp16alen = scale_expansion_zeroelim(aytbclen, aytbc, 2.0 * ady,
                                          temp16a);

    aytbblen = scale_expansion_zeroelim(4, bb, adytail, aytbb);
    temp16blen = scale_expansion_zeroelim(aytbblen, aytbb, cdx, temp16b);

    aytcclen = scale_expansion_zeroelim(4, cc, adytail, aytcc);
    temp16clen = scale_expansion_zeroelim(aytcclen, aytcc, -bdx, temp16c);

    temp32alen = fast_expansion_sum_zeroelim(temp16alen, temp16a,
                                            temp16blen, temp16b, temp32a);
    temp48len = fast_expansion_sum_zeroelim(temp16clen, temp16c,
                                            temp32alen, temp32a, temp48);
    finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp48len,
                                            temp48, finother);
    finswap = finnow; finnow = finother; finother = finswap;
  }
  if (bdxtail != 0.0) {
    bxtcalen = scale_expansion_zeroelim(4, ca, bdxtail, bxtca);
    temp16alen = scale_expansion_zeroelim(bxtcalen, bxtca, 2.0 * bdx,
                                          temp16a);

    bxtaalen = scale_expansion_zeroelim(4, aa, bdxtail, bxtaa);
    temp16blen = scale_expansion_zeroelim(bxtaalen, bxtaa, cdy, temp16b);

    bxtcclen = scale_expansion_zeroelim(4, cc, bdxtail, bxtcc);
    temp16clen = scale_expansion_zeroelim(bxtcclen, bxtcc, -ady, temp16c);

    temp32alen = fast_expansion_sum_zeroelim(temp16alen, temp16a,
                                            temp16blen, temp16b, temp32a);
    temp48len = fast_expansion_sum_zeroelim(temp16clen, temp16c,
                                            temp32alen, temp32a, temp48);
    finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp48len,
                                            temp48, finother);
    finswap = finnow; finnow = finother; finother = finswap;
  }
  if (bdytail != 0.0) {
    bytcalen = scale_expansion_zeroelim(4, ca, bdytail, bytca);
    temp16alen = scale_expansion_zeroelim(bytcalen, bytca, 2.0 * bdy,
                                          temp16a);

    bytcclen = scale_expansion_zeroelim(4, cc, bdytail, bytcc);
    temp16blen = scale_expansion_zeroelim(bytcclen, bytcc, adx, temp16b);

    bytaalen = scale_expansion_zeroelim(4, aa, bdytail, bytaa);
    temp16clen = scale_expansion_zeroelim(bytaalen, bytaa, -cdx, temp16c);

    temp32alen = fast_expansion_sum_zeroelim(temp16alen, temp16a,
                                            temp16blen, temp16b, temp32a);
    temp48len = fast_expansion_sum_zeroelim(temp16clen, temp16c,
                                            temp32alen, temp32a, temp48);
    finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp48len,
                                            temp48, finother);
    finswap = finnow; finnow = finother; finother = finswap;
  }
  if (cdxtail != 0.0) {
    cxtablen = scale_expansion_zeroelim(4, ab, cdxtail, cxtab);
    temp16alen = scale_expansion_zeroelim(cxtablen, cxtab, 2.0 * cdx,
                                          temp16a);

    cxtbblen = scale_expansion_zeroelim(4, bb, cdxtail, cxtbb);
    temp16blen = scale_expansion_zeroelim(cxtbblen, cxtbb, ady, temp16b);

    cxtaalen = scale_expansion_zeroelim(4, aa, cdxtail, cxtaa);
    temp16clen = scale_expansion_zeroelim(cxtaalen, cxtaa, -bdy, temp16c);

    temp32alen = fast_expansion_sum_zeroelim(temp16alen, temp16a,
                                            temp16blen, temp16b, temp32a);
    temp48len = fast_expansion_sum_zeroelim(temp16clen, temp16c,
                                            temp32alen, temp32a, temp48);
    finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp48len,
                                            temp48, finother);
    finswap = finnow; finnow = finother; finother = finswap;
  }
  if (cdytail != 0.0) {
    cytablen = scale_expansion_zeroelim(4, ab, cdytail, cytab);
    temp16alen = scale_expansion_zeroelim(cytablen, cytab, 2.0 * cdy,
                                          temp16a);

    cytaalen = scale_expansion_zeroelim(4, aa, cdytail, cytaa);
    temp16blen = scale_expansion_zeroelim(cytaalen, cytaa, bdx, temp16b);

    cytbblen = scale_expansion_zeroelim(4, bb, cdytail, cytbb);
    temp16clen = scale_expansion_zeroelim(cytbblen, cytbb, -adx, temp16c);

    temp32alen = fast_expansion_sum_zeroelim(temp16alen, temp16a,
                                            temp16blen, temp16b, temp32a);
    temp48len = fast_expansion_sum_zeroelim(temp16clen, temp16c,
                                            temp32alen, temp32a, temp48);
    finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp48len,
                                            temp48, finother);
    finswap = finnow; finnow = finother; finother = finswap;
  }

  if ((adxtail != 0.0) || (adytail != 0.0)) {
    if ((bdxtail != 0.0) || (bdytail != 0.0)
        || (cdxtail != 0.0) || (cdytail != 0.0)) {
      Two_Product(bdxtail, cdy, ti1, ti0);
      Two_Product(bdx, cdytail, tj1, tj0);
      Two_Two_Sum(ti1, ti0, tj1, tj0, u3, u[2], u[1], u[0]);
      u[3] = u3;
      negate = -bdy;
      Two_Product(cdxtail, negate, ti1, ti0);
      negate = -bdytail;
      Two_Product(cdx, negate, tj1, tj0);
      Two_Two_Sum(ti1, ti0, tj1, tj0, v3, v[2], v[1], v[0]);
      v[3] = v3;
      bctlen = fast_expansion_sum_zeroelim(4, u, 4, v, bct);

      Two_Product(bdxtail, cdytail, ti1, ti0);
      Two_Product(cdxtail, bdytail, tj1, tj0);
      Two_Two_Diff(ti1, ti0, tj1, tj0, bctt3, bctt[2], bctt[1], bctt[0]);
      bctt[3] = bctt3;
      bcttlen = 4;
    } else {
      bct[0] = 0.0;
      bctlen = 1;
      bctt[0] = 0.0;
      bcttlen = 1;
    }

    if (adxtail != 0.0) {
      temp16alen = scale_expansion_zeroelim(axtbclen, axtbc, adxtail, temp16a);
      axtbctlen = scale_expansion_zeroelim(bctlen, bct, adxtail, axtbct);
      temp32alen = scale_expansion_zeroelim(axtbctlen, axtbct, 2.0 * adx,
                                            temp32a);
      temp48len = fast_expansion_sum_zeroelim(temp16alen, temp16a,
                                              temp32alen, temp32a, temp48);
      finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp48len,
                                              temp48, finother);
      finswap = finnow; finnow = finother; finother = finswap;
      if (bdytail != 0.0) {
        temp8len = scale_expansion_zeroelim(4, cc, adxtail, temp8);
        temp16alen = scale_expansion_zeroelim(temp8len, temp8, bdytail,
                                              temp16a);
        finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp16alen,
                                                temp16a, finother);
        finswap = finnow; finnow = finother; finother = finswap;
      }
      if (cdytail != 0.0) {
        temp8len = scale_expansion_zeroelim(4, bb, -adxtail, temp8);
        temp16alen = scale_expansion_zeroelim(temp8len, temp8, cdytail,
                                              temp16a);
        finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp16alen,
                                                temp16a, finother);
        finswap = finnow; finnow = finother; finother = finswap;
      }

      temp32alen = scale_expansion_zeroelim(axtbctlen, axtbct, adxtail,
                                            temp32a);
      axtbcttlen = scale_expansion_zeroelim(bcttlen, bctt, adxtail, axtbctt);
      temp16alen = scale_expansion_zeroelim(axtbcttlen, axtbctt, 2.0 * adx,
                                            temp16a);
      temp16blen = scale_expansion_zeroelim(axtbcttlen, axtbctt, adxtail,
                                            temp16b);
      temp32blen = fast_expansion_sum_zeroelim(temp16alen, temp16a,
                                              temp16blen, temp16b, temp32b);
      temp64len = fast_expansion_sum_zeroelim(temp32alen, temp32a,
                                              temp32blen, temp32b, temp64);
      finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp64len,
                                              temp64, finother);
      finswap = finnow; finnow = finother; finother = finswap;
    }
    if (adytail != 0.0) {
      temp16alen = scale_expansion_zeroelim(aytbclen, aytbc, adytail, temp16a);
      aytbctlen = scale_expansion_zeroelim(bctlen, bct, adytail, aytbct);
      temp32alen = scale_expansion_zeroelim(aytbctlen, aytbct, 2.0 * ady,
                                            temp32a);
      temp48len = fast_expansion_sum_zeroelim(temp16alen, temp16a,
                                              temp32alen, temp32a, temp48);
      finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp48len,
                                              temp48, finother);
      finswap = finnow; finnow = finother; finother = finswap;


      temp32alen = scale_expansion_zeroelim(aytbctlen, aytbct, adytail,
                                            temp32a);
      aytbcttlen = scale_expansion_zeroelim(bcttlen, bctt, adytail, aytbctt);
      temp16alen = scale_expansion_zeroelim(aytbcttlen, aytbctt, 2.0 * ady,
                                            temp16a);
      temp16blen = scale_expansion_zeroelim(aytbcttlen, aytbctt, adytail,
                                            temp16b);
      temp32blen = fast_expansion_sum_zeroelim(temp16alen, temp16a,
                                              temp16blen, temp16b, temp32b);
      temp64len = fast_expansion_sum_zeroelim(temp32alen, temp32a,
                                              temp32blen, temp32b, temp64);
      finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp64len,
                                              temp64, finother);
      finswap = finnow; finnow = finother; finother = finswap;
    }
  }
  if ((bdxtail != 0.0) || (bdytail != 0.0)) {
    if ((cdxtail != 0.0) || (cdytail != 0.0)
        || (adxtail != 0.0) || (adytail != 0.0)) {
      Two_Product(cdxtail, ady, ti1, ti0);
      Two_Product(cdx, adytail, tj1, tj0);
      Two_Two_Sum(ti1, ti0, tj1, tj0, u3, u[2], u[1], u[0]);
      u[3] = u3;
      negate = -cdy;
      Two_Product(adxtail, negate, ti1, ti0);
      negate = -cdytail;
      Two_Product(adx, negate, tj1, tj0);
      Two_Two_Sum(ti1, ti0, tj1, tj0, v3, v[2], v[1], v[0]);
      v[3] = v3;
      catlen = fast_expansion_sum_zeroelim(4, u, 4, v, cat);

      Two_Product(cdxtail, adytail, ti1, ti0);
      Two_Product(adxtail, cdytail, tj1, tj0);
      Two_Two_Diff(ti1, ti0, tj1, tj0, catt3, catt[2], catt[1], catt[0]);
      catt[3] = catt3;
      cattlen = 4;
    } else {
      cat[0] = 0.0;
      catlen = 1;
      catt[0] = 0.0;
      cattlen = 1;
    }

    if (bdxtail != 0.0) {
      temp16alen = scale_expansion_zeroelim(bxtcalen, bxtca, bdxtail, temp16a);
      bxtcatlen = scale_expansion_zeroelim(catlen, cat, bdxtail, bxtcat);
      temp32alen = scale_expansion_zeroelim(bxtcatlen, bxtcat, 2.0 * bdx,
                                            temp32a);
      temp48len = fast_expansion_sum_zeroelim(temp16alen, temp16a,
                                              temp32alen, temp32a, temp48);
      finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp48len,
                                              temp48, finother);
      finswap = finnow; finnow = finother; finother = finswap;
      if (cdytail != 0.0) {
        temp8len = scale_expansion_zeroelim(4, aa, bdxtail, temp8);
        temp16alen = scale_expansion_zeroelim(temp8len, temp8, cdytail,
                                              temp16a);
        finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp16alen,
                                                temp16a, finother);
        finswap = finnow; finnow = finother; finother = finswap;
      }
      if (adytail != 0.0) {
        temp8len = scale_expansion_zeroelim(4, cc, -bdxtail, temp8);
        temp16alen = scale_expansion_zeroelim(temp8len, temp8, adytail,
                                              temp16a);
        finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp16alen,
                                                temp16a, finother);
        finswap = finnow; finnow = finother; finother = finswap;
      }

      temp32alen = scale_expansion_zeroelim(bxtcatlen, bxtcat, bdxtail,
                                            temp32a);
      bxtcattlen = scale_expansion_zeroelim(cattlen, catt, bdxtail, bxtcatt);
      temp16alen = scale_expansion_zeroelim(bxtcattlen, bxtcatt, 2.0 * bdx,
                                            temp16a);
      temp16blen = scale_expansion_zeroelim(bxtcattlen, bxtcatt, bdxtail,
                                            temp16b);
      temp32blen = fast_expansion_sum_zeroelim(temp16alen, temp16a,
                                              temp16blen, temp16b, temp32b);
      temp64len = fast_expansion_sum_zeroelim(temp32alen, temp32a,
                                              temp32blen, temp32b, temp64);
      finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp64len,
                                              temp64, finother);
      finswap = finnow; finnow = finother; finother = finswap;
    }
    if (bdytail != 0.0) {
      temp16alen = scale_expansion_zeroelim(bytcalen, bytca, bdytail, temp16a);
      bytcatlen = scale_expansion_zeroelim(catlen, cat, bdytail, bytcat);
      temp32alen = scale_expansion_zeroelim(bytcatlen, bytcat, 2.0 * bdy,
                                            temp32a);
      temp48len = fast_expansion_sum_zeroelim(temp16alen, temp16a,
                                              temp32alen, temp32a, temp48);
      finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp48len,
                                              temp48, finother);
      finswap = finnow; finnow = finother; finother = finswap;


      temp32alen = scale_expansion_zeroelim(bytcatlen, bytcat, bdytail,
                                            temp32a);
      bytcattlen = scale_expansion_zeroelim(cattlen, catt, bdytail, bytcatt);
      temp16alen = scale_expansion_zeroelim(bytcattlen, bytcatt, 2.0 * bdy,
                                            temp16a);
      temp16blen = scale_expansion_zeroelim(bytcattlen, bytcatt, bdytail,
                                            temp16b);
      temp32blen = fast_expansion_sum_zeroelim(temp16alen, temp16a,
                                              temp16blen, temp16b, temp32b);
      temp64len = fast_expansion_sum_zeroelim(temp32alen, temp32a,
                                              temp32blen, temp32b, temp64);
      finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp64len,
                                              temp64, finother);
      finswap = finnow; finnow = finother; finother = finswap;
    }
  }
  if ((cdxtail != 0.0) || (cdytail != 0.0)) {
    if ((adxtail != 0.0) || (adytail != 0.0)
        || (bdxtail != 0.0) || (bdytail != 0.0)) {
      Two_Product(adxtail, bdy, ti1, ti0);
      Two_Product(adx, bdytail, tj1, tj0);
      Two_Two_Sum(ti1, ti0, tj1, tj0, u3, u[2], u[1], u[0]);
      u[3] = u3;
      negate = -ady;
      Two_Product(bdxtail, negate, ti1, ti0);
      negate = -adytail;
      Two_Product(bdx, negate, tj1, tj0);
      Two_Two_Sum(ti1, ti0, tj1, tj0, v3, v[2], v[1], v[0]);
      v[3] = v3;
      abtlen = fast_expansion_sum_zeroelim(4, u, 4, v, abt);

      Two_Product(adxtail, bdytail, ti1, ti0);
      Two_Product(bdxtail, adytail, tj1, tj0);
      Two_Two_Diff(ti1, ti0, tj1, tj0, abtt3, abtt[2], abtt[1], abtt[0]);
      abtt[3] = abtt3;
      abttlen = 4;
    } else {
      abt[0] = 0.0;
      abtlen = 1;
      abtt[0] = 0.0;
      abttlen = 1;
    }

    if (cdxtail != 0.0) {
      temp16alen = scale_expansion_zeroelim(cxtablen, cxtab, cdxtail, temp16a);
      cxtabtlen = scale_expansion_zeroelim(abtlen, abt, cdxtail, cxtabt);
      temp32alen = scale_expansion_zeroelim(cxtabtlen, cxtabt, 2.0 * cdx,
                                            temp32a);
      temp48len = fast_expansion_sum_zeroelim(temp16alen, temp16a,
                                              temp32alen, temp32a, temp48);
      finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp48len,
                                              temp48, finother);
      finswap = finnow; finnow = finother; finother = finswap;
      if (adytail != 0.0) {
        temp8len = scale_expansion_zeroelim(4, bb, cdxtail, temp8);
        temp16alen = scale_expansion_zeroelim(temp8len, temp8, adytail,
                                              temp16a);
        finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp16alen,
                                                temp16a, finother);
        finswap = finnow; finnow = finother; finother = finswap;
      }
      if (bdytail != 0.0) {
        temp8len = scale_expansion_zeroelim(4, aa, -cdxtail, temp8);
        temp16alen = scale_expansion_zeroelim(temp8len, temp8, bdytail,
                                              temp16a);
        finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp16alen,
                                                temp16a, finother);
        finswap = finnow; finnow = finother; finother = finswap;
      }

      temp32alen = scale_expansion_zeroelim(cxtabtlen, cxtabt, cdxtail,
                                            temp32a);
      cxtabttlen = scale_expansion_zeroelim(abttlen, abtt, cdxtail, cxtabtt);
      temp16alen = scale_expansion_zeroelim(cxtabttlen, cxtabtt, 2.0 * cdx,
                                            temp16a);
      temp16blen = scale_expansion_zeroelim(cxtabttlen, cxtabtt, cdxtail,
                                            temp16b);
      temp32blen = fast_expansion_sum_zeroelim(temp16alen, temp16a,
                                              temp16blen, temp16b, temp32b);
      temp64len = fast_expansion_sum_zeroelim(temp32alen, temp32a,
                                              temp32blen, temp32b, temp64);
      finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp64len,
                                              temp64, finother);
      finswap = finnow; finnow = finother; finother = finswap;
    }
    if (cdytail != 0.0) {
      temp16alen = scale_expansion_zeroelim(cytablen, cytab, cdytail, temp16a);
      cytabtlen = scale_expansion_zeroelim(abtlen, abt, cdytail, cytabt);
      temp32alen = scale_expansion_zeroelim(cytabtlen, cytabt, 2.0 * cdy,
                                            temp32a);
      temp48len = fast_expansion_sum_zeroelim(temp16alen, temp16a,
                                              temp32alen, temp32a, temp48);
      finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp48len,
                                              temp48, finother);
      finswap = finnow; finnow = finother; finother = finswap;


      temp32alen = scale_expansion_zeroelim(cytabtlen, cytabt, cdytail,
                                            temp32a);
      cytabttlen = scale_expansion_zeroelim(abttlen, abtt, cdytail, cytabtt);
      temp16alen = scale_expansion_zeroelim(cytabttlen, cytabtt, 2.0 * cdy,
                                            temp16a);
      temp16blen = scale_expansion_zeroelim(cytabttlen, cytabtt, cdytail,
                                            temp16b);
      temp32blen = fast_expansion_sum_zeroelim(temp16alen, temp16a,
                                              temp16blen, temp16b, temp32b);
      temp64len = fast_expansion_sum_zeroelim(temp32alen, temp32a,
                                              temp32blen, temp32b, temp64);
      finlength = fast_expansion_sum_zeroelim(finlength, finnow, temp64len,
                                              temp64, finother);
      finswap = finnow; finnow = finother; finother = finswap;
    }
  }

  return finnow[finlength - 1];
}

REAL Mesh::incircle(const Point2d& pa, const Point2d& pb, const Point2d& pc, const Point2d& pd)
{
  REAL adx, bdx, cdx, ady, bdy, cdy;
  REAL bdxcdy, cdxbdy, cdxady, adxcdy, adxbdy, bdxady;
  REAL alift, blift, clift;
  REAL det;
  REAL permanent, errbound;

  adx = pa[0] - pd[0];
  bdx = pb[0] - pd[0];
  cdx = pc[0] - pd[0];
  ady = pa[1] - pd[1];
  bdy = pb[1] - pd[1];
  cdy = pc[1] - pd[1];

  bdxcdy = bdx * cdy;
  cdxbdy = cdx * bdy;
  alift = adx * adx + ady * ady;

  cdxady = cdx * ady;
  adxcdy = adx * cdy;
  blift = bdx * bdx + bdy * bdy;

  adxbdy = adx * bdy;
  bdxady = bdx * ady;
  clift = cdx * cdx + cdy * cdy;

  det = alift * (bdxcdy - cdxbdy)
      + blift * (cdxady - adxcdy)
      + clift * (adxbdy - bdxady);

  if (b.noexact) {
    return det;
  }

  permanent = (Absolute(bdxcdy) + Absolute(cdxbdy)) * alift
            + (Absolute(cdxady) + Absolute(adxcdy)) * blift
            + (Absolute(adxbdy) + Absolute(bdxady)) * clift;
  errbound = iccerrboundA * permanent;
  if ((det > errbound) || (-det > errbound)) {
    return det;
  }

  return incircleadapt(&pa.x, &pb.x, &pc.x, &pd.x, permanent);
}

/*****************************************************************************/
/*                                                                           */
/*  orient3d()   Return a positive value if the point pd lies below the      */
/*               plane passing through pa, pb, and pc; "below" is defined so */
/*               that pa, pb, and pc appear in counterclockwise order when   */
/*               viewed from above the plane.  Returns a negative value if   */
/*               pd lies above the plane.  Returns zero if the points are    */
/*               coplanar.  The result is also a rough approximation of six  */
/*               times the signed volume of the tetrahedron defined by the   */
/*               four points.                                                */
/*                                                                           */
/*  Uses exact arithmetic if necessary to ensure a correct answer.  The      */
/*  result returned is the determinant of a matrix.  This determinant is     */
/*  computed adaptively, in the sense that exact arithmetic is used only to  */
/*  the degree it is needed to ensure that the returned value has the        */
/*  correct sign.  Hence, this function is usually quite fast, but will run  */
/*  more slowly when the input points are coplanar or nearly so.             */
/*                                                                           */
/*  See my Robust Predicates paper for details.                              */
/*                                                                           */
/*****************************************************************************/

REAL orient3dadapt(REAL const* pa, REAL const* pb, REAL const* pc, REAL const* pd,
                   REAL aheight, REAL bheight, REAL cheight, REAL dheight,
                   REAL permanent)
{
  INEXACT REAL adx, bdx, cdx, ady, bdy, cdy, adheight, bdheight, cdheight;
  REAL det, errbound;

  INEXACT REAL bdxcdy1, cdxbdy1, cdxady1, adxcdy1, adxbdy1, bdxady1;
  REAL bdxcdy0, cdxbdy0, cdxady0, adxcdy0, adxbdy0, bdxady0;
  REAL bc[4], ca[4], ab[4];
  INEXACT REAL bc3, ca3, ab3;
  REAL adet[8], bdet[8], cdet[8];
  int alen, blen, clen;
  REAL abdet[16];
  int ablen;
  REAL *finnow, *finother, *finswap;
  REAL fin1[192], fin2[192];
  int finlength;

  REAL adxtail, bdxtail, cdxtail;
  REAL adytail, bdytail, cdytail;
  REAL adheighttail, bdheighttail, cdheighttail;
  INEXACT REAL at_blarge, at_clarge;
  INEXACT REAL bt_clarge, bt_alarge;
  INEXACT REAL ct_alarge, ct_blarge;
  REAL at_b[4], at_c[4], bt_c[4], bt_a[4], ct_a[4], ct_b[4];
  int at_blen, at_clen, bt_clen, bt_alen, ct_alen, ct_blen;
  INEXACT REAL bdxt_cdy1, cdxt_bdy1, cdxt_ady1;
  INEXACT REAL adxt_cdy1, adxt_bdy1, bdxt_ady1;
  REAL bdxt_cdy0, cdxt_bdy0, cdxt_ady0;
  REAL adxt_cdy0, adxt_bdy0, bdxt_ady0;
  INEXACT REAL bdyt_cdx1, cdyt_bdx1, cdyt_adx1;
  INEXACT REAL adyt_cdx1, adyt_bdx1, bdyt_adx1;
  REAL bdyt_cdx0, cdyt_bdx0, cdyt_adx0;
  REAL adyt_cdx0, adyt_bdx0, bdyt_adx0;
  REAL bct[8], cat[8], abt[8];
  int bctlen, catlen, abtlen;
  INEXACT REAL bdxt_cdyt1, cdxt_bdyt1, cdxt_adyt1;
  INEXACT REAL adxt_cdyt1, adxt_bdyt1, bdxt_adyt1;
  REAL bdxt_cdyt0, cdxt_bdyt0, cdxt_adyt0;
  REAL adxt_cdyt0, adxt_bdyt0, bdxt_adyt0;
  REAL u[4], v[12], w[16];
  INEXACT REAL u3;
  int vlength, wlength;
  REAL negate;

  INEXACT REAL bvirt;
  REAL avirt, bround, around;
  INEXACT REAL c;
  INEXACT REAL abig;
  REAL ahi, alo, bhi, blo;
  REAL err1, err2, err3;
  INEXACT REAL _i, _j, _k;
  REAL _0;

  adx = (REAL) (pa[0] - pd[0]);
  bdx = (REAL) (pb[0] - pd[0]);
  cdx = (REAL) (pc[0] - pd[0]);
  ady = (REAL) (pa[1] - pd[1]);
  bdy = (REAL) (pb[1] - pd[1]);
  cdy = (REAL) (pc[1] - pd[1]);
  adheight = (REAL) (aheight - dheight);
  bdheight = (REAL) (bheight - dheight);
  cdheight = (REAL) (cheight - dheight);

  Two_Product(bdx, cdy, bdxcdy1, bdxcdy0);
  Two_Product(cdx, bdy, cdxbdy1, cdxbdy0);
  Two_Two_Diff(bdxcdy1, bdxcdy0, cdxbdy1, cdxbdy0, bc3, bc[2], bc[1], bc[0]);
  bc[3] = bc3;
  alen = scale_expansion_zeroelim(4, bc, adheight, adet);

  Two_Product(cdx, ady, cdxady1, cdxady0);
  Two_Product(adx, cdy, adxcdy1, adxcdy0);
  Two_Two_Diff(cdxady1, cdxady0, adxcdy1, adxcdy0, ca3, ca[2], ca[1], ca[0]);
  ca[3] = ca3;
  blen = scale_expansion_zeroelim(4, ca, bdheight, bdet);

  Two_Product(adx, bdy, adxbdy1, adxbdy0);
  Two_Product(bdx, ady, bdxady1, bdxady0);
  Two_Two_Diff(adxbdy1, adxbdy0, bdxady1, bdxady0, ab3, ab[2], ab[1], ab[0]);
  ab[3] = ab3;
  clen = scale_expansion_zeroelim(4, ab, cdheight, cdet);

  ablen = fast_expansion_sum_zeroelim(alen, adet, blen, bdet, abdet);
  finlength = fast_expansion_sum_zeroelim(ablen, abdet, clen, cdet, fin1);

  det = estimate(finlength, fin1);
  errbound = o3derrboundB * permanent;
  if ((det >= errbound) || (-det >= errbound)) {
    return det;
  }

  Two_Diff_Tail(pa[0], pd[0], adx, adxtail);
  Two_Diff_Tail(pb[0], pd[0], bdx, bdxtail);
  Two_Diff_Tail(pc[0], pd[0], cdx, cdxtail);
  Two_Diff_Tail(pa[1], pd[1], ady, adytail);
  Two_Diff_Tail(pb[1], pd[1], bdy, bdytail);
  Two_Diff_Tail(pc[1], pd[1], cdy, cdytail);
  Two_Diff_Tail(aheight, dheight, adheight, adheighttail);
  Two_Diff_Tail(bheight, dheight, bdheight, bdheighttail);
  Two_Diff_Tail(cheight, dheight, cdheight, cdheighttail);

  if ((adxtail == 0.0) && (bdxtail == 0.0) && (cdxtail == 0.0) &&
      (adytail == 0.0) && (bdytail == 0.0) && (cdytail == 0.0) &&
      (adheighttail == 0.0) &&
      (bdheighttail == 0.0) &&
      (cdheighttail == 0.0)) {
    return det;
  }

  errbound = o3derrboundC * permanent + resulterrbound * Absolute(det);
  det += (adheight * ((bdx * cdytail + cdy * bdxtail) -
                      (bdy * cdxtail + cdx * bdytail)) +
          adheighttail * (bdx * cdy - bdy * cdx)) +
         (bdheight * ((cdx * adytail + ady * cdxtail) -
                      (cdy * adxtail + adx * cdytail)) +
          bdheighttail * (cdx * ady - cdy * adx)) +
         (cdheight * ((adx * bdytail + bdy * adxtail) -
                      (ady * bdxtail + bdx * adytail)) +
          cdheighttail * (adx * bdy - ady * bdx));
  if ((det >= errbound) || (-det >= errbound)) {
    return det;
  }

  finnow = fin1;
  finother = fin2;

  if (adxtail == 0.0) {
    if (adytail == 0.0) {
      at_b[0] = 0.0;
      at_blen = 1;
      at_c[0] = 0.0;
      at_clen = 1;
    } else {
      negate = -adytail;
      Two_Product(negate, bdx, at_blarge, at_b[0]);
      at_b[1] = at_blarge;
      at_blen = 2;
      Two_Product(adytail, cdx, at_clarge, at_c[0]);
      at_c[1] = at_clarge;
      at_clen = 2;
    }
  } else {
    if (adytail == 0.0) {
      Two_Product(adxtail, bdy, at_blarge, at_b[0]);
      at_b[1] = at_blarge;
      at_blen = 2;
      negate = -adxtail;
      Two_Product(negate, cdy, at_clarge, at_c[0]);
      at_c[1] = at_clarge;
      at_clen = 2;
    } else {
      Two_Product(adxtail, bdy, adxt_bdy1, adxt_bdy0);
      Two_Product(adytail, bdx, adyt_bdx1, adyt_bdx0);
      Two_Two_Diff(adxt_bdy1, adxt_bdy0, adyt_bdx1, adyt_bdx0,
                   at_blarge, at_b[2], at_b[1], at_b[0]);
      at_b[3] = at_blarge;
      at_blen = 4;
      Two_Product(adytail, cdx, adyt_cdx1, adyt_cdx0);
      Two_Product(adxtail, cdy, adxt_cdy1, adxt_cdy0);
      Two_Two_Diff(adyt_cdx1, adyt_cdx0, adxt_cdy1, adxt_cdy0,
                   at_clarge, at_c[2], at_c[1], at_c[0]);
      at_c[3] = at_clarge;
      at_clen = 4;
    }
  }
  if (bdxtail == 0.0) {
    if (bdytail == 0.0) {
      bt_c[0] = 0.0;
      bt_clen = 1;
      bt_a[0] = 0.0;
      bt_alen = 1;
    } else {
      negate = -bdytail;
      Two_Product(negate, cdx, bt_clarge, bt_c[0]);
      bt_c[1] = bt_clarge;
      bt_clen = 2;
      Two_Product(bdytail, adx, bt_alarge, bt_a[0]);
      bt_a[1] = bt_alarge;
      bt_alen = 2;
    }
  } else {
    if (bdytail == 0.0) {
      Two_Product(bdxtail, cdy, bt_clarge, bt_c[0]);
      bt_c[1] = bt_clarge;
      bt_clen = 2;
      negate = -bdxtail;
      Two_Product(negate, ady, bt_alarge, bt_a[0]);
      bt_a[1] = bt_alarge;
      bt_alen = 2;
    } else {
      Two_Product(bdxtail, cdy, bdxt_cdy1, bdxt_cdy0);
      Two_Product(bdytail, cdx, bdyt_cdx1, bdyt_cdx0);
      Two_Two_Diff(bdxt_cdy1, bdxt_cdy0, bdyt_cdx1, bdyt_cdx0,
                   bt_clarge, bt_c[2], bt_c[1], bt_c[0]);
      bt_c[3] = bt_clarge;
      bt_clen = 4;
      Two_Product(bdytail, adx, bdyt_adx1, bdyt_adx0);
      Two_Product(bdxtail, ady, bdxt_ady1, bdxt_ady0);
      Two_Two_Diff(bdyt_adx1, bdyt_adx0, bdxt_ady1, bdxt_ady0,
                  bt_alarge, bt_a[2], bt_a[1], bt_a[0]);
      bt_a[3] = bt_alarge;
      bt_alen = 4;
    }
  }
  if (cdxtail == 0.0) {
    if (cdytail == 0.0) {
      ct_a[0] = 0.0;
      ct_alen = 1;
      ct_b[0] = 0.0;
      ct_blen = 1;
    } else {
      negate = -cdytail;
      Two_Product(negate, adx, ct_alarge, ct_a[0]);
      ct_a[1] = ct_alarge;
      ct_alen = 2;
      Two_Product(cdytail, bdx, ct_blarge, ct_b[0]);
      ct_b[1] = ct_blarge;
      ct_blen = 2;
    }
  } else {
    if (cdytail == 0.0) {
      Two_Product(cdxtail, ady, ct_alarge, ct_a[0]);
      ct_a[1] = ct_alarge;
      ct_alen = 2;
      negate = -cdxtail;
      Two_Product(negate, bdy, ct_blarge, ct_b[0]);
      ct_b[1] = ct_blarge;
      ct_blen = 2;
    } else {
      Two_Product(cdxtail, ady, cdxt_ady1, cdxt_ady0);
      Two_Product(cdytail, adx, cdyt_adx1, cdyt_adx0);
      Two_Two_Diff(cdxt_ady1, cdxt_ady0, cdyt_adx1, cdyt_adx0,
                   ct_alarge, ct_a[2], ct_a[1], ct_a[0]);
      ct_a[3] = ct_alarge;
      ct_alen = 4;
      Two_Product(cdytail, bdx, cdyt_bdx1, cdyt_bdx0);
      Two_Product(cdxtail, bdy, cdxt_bdy1, cdxt_bdy0);
      Two_Two_Diff(cdyt_bdx1, cdyt_bdx0, cdxt_bdy1, cdxt_bdy0,
                   ct_blarge, ct_b[2], ct_b[1], ct_b[0]);
      ct_b[3] = ct_blarge;
      ct_blen = 4;
    }
  }

  bctlen = fast_expansion_sum_zeroelim(bt_clen, bt_c, ct_blen, ct_b, bct);
  wlength = scale_expansion_zeroelim(bctlen, bct, adheight, w);
  finlength = fast_expansion_sum_zeroelim(finlength, finnow, wlength, w,
                                          finother);
  finswap = finnow; finnow = finother; finother = finswap;

  catlen = fast_expansion_sum_zeroelim(ct_alen, ct_a, at_clen, at_c, cat);
  wlength = scale_expansion_zeroelim(catlen, cat, bdheight, w);
  finlength = fast_expansion_sum_zeroelim(finlength, finnow, wlength, w,
                                          finother);
  finswap = finnow; finnow = finother; finother = finswap;

  abtlen = fast_expansion_sum_zeroelim(at_blen, at_b, bt_alen, bt_a, abt);
  wlength = scale_expansion_zeroelim(abtlen, abt, cdheight, w);
  finlength = fast_expansion_sum_zeroelim(finlength, finnow, wlength, w,
                                          finother);
  finswap = finnow; finnow = finother; finother = finswap;

  if (adheighttail != 0.0) {
    vlength = scale_expansion_zeroelim(4, bc, adheighttail, v);
    finlength = fast_expansion_sum_zeroelim(finlength, finnow, vlength, v,
                                            finother);
    finswap = finnow; finnow = finother; finother = finswap;
  }
  if (bdheighttail != 0.0) {
    vlength = scale_expansion_zeroelim(4, ca, bdheighttail, v);
    finlength = fast_expansion_sum_zeroelim(finlength, finnow, vlength, v,
                                            finother);
    finswap = finnow; finnow = finother; finother = finswap;
  }
  if (cdheighttail != 0.0) {
    vlength = scale_expansion_zeroelim(4, ab, cdheighttail, v);
    finlength = fast_expansion_sum_zeroelim(finlength, finnow, vlength, v,
                                            finother);
    finswap = finnow; finnow = finother; finother = finswap;
  }

  if (adxtail != 0.0) {
    if (bdytail != 0.0) {
      Two_Product(adxtail, bdytail, adxt_bdyt1, adxt_bdyt0);
      Two_One_Product(adxt_bdyt1, adxt_bdyt0, cdheight, u3, u[2], u[1], u[0]);
      u[3] = u3;
      finlength = fast_expansion_sum_zeroelim(finlength, finnow, 4, u,
                                              finother);
      finswap = finnow; finnow = finother; finother = finswap;
      if (cdheighttail != 0.0) {
        Two_One_Product(adxt_bdyt1, adxt_bdyt0, cdheighttail,
                        u3, u[2], u[1], u[0]);
        u[3] = u3;
        finlength = fast_expansion_sum_zeroelim(finlength, finnow, 4, u,
                                                finother);
        finswap = finnow; finnow = finother; finother = finswap;
      }
    }
    if (cdytail != 0.0) {
      negate = -adxtail;
      Two_Product(negate, cdytail, adxt_cdyt1, adxt_cdyt0);
      Two_One_Product(adxt_cdyt1, adxt_cdyt0, bdheight, u3, u[2], u[1], u[0]);
      u[3] = u3;
      finlength = fast_expansion_sum_zeroelim(finlength, finnow, 4, u,
                                              finother);
      finswap = finnow; finnow = finother; finother = finswap;
      if (bdheighttail != 0.0) {
        Two_One_Product(adxt_cdyt1, adxt_cdyt0, bdheighttail,
                        u3, u[2], u[1], u[0]);
        u[3] = u3;
        finlength = fast_expansion_sum_zeroelim(finlength, finnow, 4, u,
                                                finother);
        finswap = finnow; finnow = finother; finother = finswap;
      }
    }
  }
  if (bdxtail != 0.0) {
    if (cdytail != 0.0) {
      Two_Product(bdxtail, cdytail, bdxt_cdyt1, bdxt_cdyt0);
      Two_One_Product(bdxt_cdyt1, bdxt_cdyt0, adheight, u3, u[2], u[1], u[0]);
      u[3] = u3;
      finlength = fast_expansion_sum_zeroelim(finlength, finnow, 4, u,
                                              finother);
      finswap = finnow; finnow = finother; finother = finswap;
      if (adheighttail != 0.0) {
        Two_One_Product(bdxt_cdyt1, bdxt_cdyt0, adheighttail,
                        u3, u[2], u[1], u[0]);
        u[3] = u3;
        finlength = fast_expansion_sum_zeroelim(finlength, finnow, 4, u,
                                                finother);
        finswap = finnow; finnow = finother; finother = finswap;
      }
    }
    if (adytail != 0.0) {
      negate = -bdxtail;
      Two_Product(negate, adytail, bdxt_adyt1, bdxt_adyt0);
      Two_One_Product(bdxt_adyt1, bdxt_adyt0, cdheight, u3, u[2], u[1], u[0]);
      u[3] = u3;
      finlength = fast_expansion_sum_zeroelim(finlength, finnow, 4, u,
                                              finother);
      finswap = finnow; finnow = finother; finother = finswap;
      if (cdheighttail != 0.0) {
        Two_One_Product(bdxt_adyt1, bdxt_adyt0, cdheighttail,
                        u3, u[2], u[1], u[0]);
        u[3] = u3;
        finlength = fast_expansion_sum_zeroelim(finlength, finnow, 4, u,
                                                finother);
        finswap = finnow; finnow = finother; finother = finswap;
      }
    }
  }
  if (cdxtail != 0.0) {
    if (adytail != 0.0) {
      Two_Product(cdxtail, adytail, cdxt_adyt1, cdxt_adyt0);
      Two_One_Product(cdxt_adyt1, cdxt_adyt0, bdheight, u3, u[2], u[1], u[0]);
      u[3] = u3;
      finlength = fast_expansion_sum_zeroelim(finlength, finnow, 4, u,
                                              finother);
      finswap = finnow; finnow = finother; finother = finswap;
      if (bdheighttail != 0.0) {
        Two_One_Product(cdxt_adyt1, cdxt_adyt0, bdheighttail,
                        u3, u[2], u[1], u[0]);
        u[3] = u3;
        finlength = fast_expansion_sum_zeroelim(finlength, finnow, 4, u,
                                                finother);
        finswap = finnow; finnow = finother; finother = finswap;
      }
    }
    if (bdytail != 0.0) {
      negate = -cdxtail;
      Two_Product(negate, bdytail, cdxt_bdyt1, cdxt_bdyt0);
      Two_One_Product(cdxt_bdyt1, cdxt_bdyt0, adheight, u3, u[2], u[1], u[0]);
      u[3] = u3;
      finlength = fast_expansion_sum_zeroelim(finlength, finnow, 4, u,
                                              finother);
      finswap = finnow; finnow = finother; finother = finswap;
      if (adheighttail != 0.0) {
        Two_One_Product(cdxt_bdyt1, cdxt_bdyt0, adheighttail,
                        u3, u[2], u[1], u[0]);
        u[3] = u3;
        finlength = fast_expansion_sum_zeroelim(finlength, finnow, 4, u,
                                                finother);
        finswap = finnow; finnow = finother; finother = finswap;
      }
    }
  }

  if (adheighttail != 0.0) {
    wlength = scale_expansion_zeroelim(bctlen, bct, adheighttail, w);
    finlength = fast_expansion_sum_zeroelim(finlength, finnow, wlength, w,
                                            finother);
    finswap = finnow; finnow = finother; finother = finswap;
  }
  if (bdheighttail != 0.0) {
    wlength = scale_expansion_zeroelim(catlen, cat, bdheighttail, w);
    finlength = fast_expansion_sum_zeroelim(finlength, finnow, wlength, w,
                                            finother);
    finswap = finnow; finnow = finother; finother = finswap;
  }
  if (cdheighttail != 0.0) {
    wlength = scale_expansion_zeroelim(abtlen, abt, cdheighttail, w);
    finlength = fast_expansion_sum_zeroelim(finlength, finnow, wlength, w,
                                            finother);
    finswap = finnow; finnow = finother; finother = finswap;
  }

  return finnow[finlength - 1];
}

REAL Mesh::orient3d( const Point2d& pa, const Point2d& pb, const Point2d& pc, const Point2d& pd,
              REAL aheight, REAL bheight, REAL cheight, REAL dheight)
{
  REAL adx, bdx, cdx, ady, bdy, cdy, adheight, bdheight, cdheight;
  REAL bdxcdy, cdxbdy, cdxady, adxcdy, adxbdy, bdxady;
  REAL det;
  REAL permanent, errbound;

  adx = pa[0] - pd[0];
  bdx = pb[0] - pd[0];
  cdx = pc[0] - pd[0];
  ady = pa[1] - pd[1];
  bdy = pb[1] - pd[1];
  cdy = pc[1] - pd[1];
  adheight = aheight - dheight;
  bdheight = bheight - dheight;
  cdheight = cheight - dheight;

  bdxcdy = bdx * cdy;
  cdxbdy = cdx * bdy;

  cdxady = cdx * ady;
  adxcdy = adx * cdy;

  adxbdy = adx * bdy;
  bdxady = bdx * ady;

  det = adheight * (bdxcdy - cdxbdy) 
      + bdheight * (cdxady - adxcdy)
      + cdheight * (adxbdy - bdxady);

  if (b.noexact) {
    return det;
  }

  permanent = (Absolute(bdxcdy) + Absolute(cdxbdy)) * Absolute(adheight)
            + (Absolute(cdxady) + Absolute(adxcdy)) * Absolute(bdheight)
            + (Absolute(adxbdy) + Absolute(bdxady)) * Absolute(cdheight);
  errbound = o3derrboundA * permanent;
  if ((det > errbound) || (-det > errbound)) {
    return det;
  }

  return orient3dadapt(&pa.x, &pb.x, &pc.x, &pd.x, aheight, bheight, cheight, dheight,
                       permanent);
}

/*****************************************************************************/
/*                                                                           */
/*  nonregular()   Return a positive value if the point pd is incompatible   */
/*                 with the circle or plane passing through pa, pb, and pc   */
/*                 (meaning that pd is inside the circle or below the        */
/*                 plane); a negative value if it is compatible; and zero if */
/*                 the four points are cocircular/coplanar.  The points pa,  */
/*                 pb, and pc must be in counterclockwise order, or the sign */
/*                 of the result will be reversed.                           */
/*                                                                           */
/*  If the -w switch is used, the points are lifted onto the parabolic       */
/*  lifting map, then they are dropped according to their weights, then the  */
/*  3D orientation test is applied.  If the -W switch is used, the points'   */
/*  heights are already provided, so the 3D orientation test is applied      */
/*  directly.  If neither switch is used, the incircle test is applied.      */
/*                                                                           */
/*****************************************************************************/

REAL Mesh::nonregular(const Point2d& pa, const Point2d& pb, const Point2d& pc, const Point2d& pd)
{
	if (b.weighted == 0) 
	{
		return incircle(pa, pb, pc, pd);
	} 
	else if (b.weighted == 1) 
	{
		return orient3d(pa, pb, pc, pd,
					pa[0] * pa[0] + pa[1] * pa[1] - pa[2],
					pb[0] * pb[0] + pb[1] * pb[1] - pb[2],
					pc[0] * pc[0] + pc[1] * pc[1] - pc[2],
					pd[0] * pd[0] + pd[1] * pd[1] - pd[2]);
	} else 
	{
		return orient3d(pa, pb, pc, pd, pa[2], pb[2], pc[2], pd[2]);
	}
}

/*****************************************************************************/
/*                                                                           */
/*  findcircumcenter()   Find the circumcenter of a triangle.                */
/*                                                                           */
/*  The result is returned both in terms of x-y coordinates and xi-eta       */
/*  (barycentric) coordinates.  The xi-eta coordinate system is defined in   */
/*  terms of the triangle:  the origin of the triangle is the origin of the  */
/*  coordinate system; the destination of the triangle is one unit along the */
/*  xi axis; and the apex of the triangle is one unit along the eta axis.    */
/*  This procedure also returns the square of the length of the triangle's   */
/*  shortest edge.                                                           */
/*                                                                           */
/*****************************************************************************/

void Mesh::findcircumcenter(
                      const Point2d& torg, const Point2d& tdest, const Point2d& tapex,
                      Point2d& circumcenter, REAL *xi, REAL *eta, int offcenter)
{
  REAL xdo, ydo, xao, yao;
  REAL dodist, aodist, dadist;
  REAL denominator;
  REAL dx, dy, dxoff, dyoff;

  /* Compute the circumcenter of the triangle. */
  xdo = tdest[0] - torg[0];
  ydo = tdest[1] - torg[1];
  xao = tapex[0] - torg[0];
  yao = tapex[1] - torg[1];
  dodist = xdo * xdo + ydo * ydo;
  aodist = xao * xao + yao * yao;
  dadist = (tdest[0] - tapex[0]) * (tdest[0] - tapex[0]) +
           (tdest[1] - tapex[1]) * (tdest[1] - tapex[1]);
  if (b.noexact) {
    denominator = 0.5 / (xdo * yao - xao * ydo);
  } else {
    /* Use the counterclockwise() routine to ensure a positive (and */
    /*   reasonably accurate) result, avoiding any possibility of   */
    /*   division by zero.                                          */
    denominator = 0.5 / counterclockwise(tdest, tapex, torg);
    
  }
  dx = (yao * dodist - ydo * aodist) * denominator;
  dy = (xdo * aodist - xao * dodist) * denominator;

  /* Find the (squared) length of the triangle's shortest edge.  This   */
  /*   serves as a conservative estimate of the insertion radius of the */
  /*   circumcenter's parent.  The estimate is used to ensure that      */
  /*   the algorithm terminates even if very small angles appear in     */
  /*   the input PSLG.                                                  */
  if ((dodist < aodist) && (dodist < dadist)) {
    if (offcenter && (b.offconstant > 0.0)) {
      /* Find the position of the off-center, as described by Alper Ungor. */
      dxoff = 0.5 * xdo - b.offconstant * ydo;
      dyoff = 0.5 * ydo + b.offconstant * xdo;
      /* If the off-center is closer to the origin than the */
      /*   circumcenter, use the off-center instead.        */
      if (dxoff * dxoff + dyoff * dyoff < dx * dx + dy * dy) {
        dx = dxoff;
        dy = dyoff;
      }
    }
  } else if (aodist < dadist) {
    if (offcenter && (b.offconstant > 0.0)) {
      dxoff = 0.5 * xao + b.offconstant * yao;
      dyoff = 0.5 * yao - b.offconstant * xao;
      /* If the off-center is closer to the origin than the */
      /*   circumcenter, use the off-center instead.        */
      if (dxoff * dxoff + dyoff * dyoff < dx * dx + dy * dy) {
        dx = dxoff;
        dy = dyoff;
      }
    }
  } else {
    if (offcenter && (b.offconstant > 0.0)) {
      dxoff = 0.5 * (tapex[0] - tdest[0]) -
              b.offconstant * (tapex[1] - tdest[1]);
      dyoff = 0.5 * (tapex[1] - tdest[1]) +
              b.offconstant * (tapex[0] - tdest[0]);
      /* If the off-center is closer to the destination than the */
      /*   circumcenter, use the off-center instead.             */
      if (dxoff * dxoff + dyoff * dyoff <
          (dx - xdo) * (dx - xdo) + (dy - ydo) * (dy - ydo)) {
        dx = xdo + dxoff;
        dy = ydo + dyoff;
      }
    }
  }

  circumcenter[0] = torg[0] + dx;
  circumcenter[1] = torg[1] + dy;

  /* To interpolate vertex attributes for the new vertex inserted at */
  /*   the circumcenter, define a coordinate system with a xi-axis,  */
  /*   directed from the triangle's origin to its destination, and   */
  /*   an eta-axis, directed from its origin to its apex.            */
  /*   Calculate the xi and eta coordinates of the circumcenter.     */
  *xi = (yao * dx - xao * dy) * (2.0 * denominator);
  *eta = (xdo * dy - ydo * dx) * (2.0 * denominator);
}

/**                                                                         **/
/**                                                                         **/
/********* Geometric primitives end here                             *********/

void Mesh::boundaryVertices(std::vector<wertex*>& verts )const
{
	for( auto vert : vertices)
	{
		if( segmentend(vert) )
		{
			verts.push_back(vert);
		}
	}
}

