#include "triexception.h"
#include <stdexcept>
#include <sstream>

// File separator
#if defined(_CNF_WIN32)
#define __FILE_SEP__ '\\'
#else
#define __FILE_SEP__ '/'
#endif


TriangleException::TriangleException(
	std::string methodName, 
	char const* fname, 
	int lineNo, 
	std::string message)
	:
	std::runtime_error(message),
	methodName(methodName),
	fileName(fname),
	lineNo(lineNo)
{
}

TriangleException::TriangleException(
	std::string methodName, char const* fname, int lineno, std::string message,
	otri tri, otri tri2)
	:
	TriangleException(methodName, fname, lineno, message)
{	
	otris.push_back(tri);
	otris.push_back(tri2);
}

TriangleException::TriangleException(
	std::string methodName, char const* fname, int lineno, std::string message, 
	otri tri, wertex const* vert) 
	:
	TriangleException(methodName, fname, lineno, message)
{	
	otris.push_back(tri); 
	if( vert != nullptr )
		verts.push_back(const_cast<wertex*>(vert));
}

TriangleException::TriangleException(
	std::string methodName, char const* fname, int lineno, std::string message, 
		osub sub, otri tri) 
	:
	TriangleException(methodName, fname, lineno, message)
{
	osubs.push_back(sub); 
	otris.push_back(tri);
}

TriangleException::TriangleException(
	std::string methodName, char const* fname, int lineno, std::string message,
	osub sub, osub sub2) 
	:
	TriangleException(methodName, fname, lineno, message)
{	
	osubs.push_back(sub); 
	osubs.push_back(sub2);
}

TriangleException::TriangleException(
	std::string methodName, char const* fname, int lineno, std::string message, 
	osub sub, wertex const* vert) 
	:
	TriangleException(methodName, fname, lineno, message)
{
	osubs.push_back(sub); 
	if( vert != nullptr )
		verts.push_back(const_cast<wertex*>(vert));
}

TriangleException::TriangleException(
	std::string methodName, char const* fname, int lineno, std::string message, 
	wertex const* vert, wertex const* vert2) 
	:
	TriangleException(methodName, fname, lineno, message)
{	
	verts.push_back(const_cast<wertex*>(vert));
	if( vert2 != nullptr )
		verts.push_back(const_cast<wertex*>(vert2));
}

TriangleException::TriangleException(
	std::string methodName, char const* fname, int lineno, std::string message, 
	ConvexArea const* convArea, ConvexArea const* convArea2) 
	:
	TriangleException(methodName, fname, lineno, message)
{	
	convexAreas.push_back(const_cast<ConvexArea*>(convArea));
	if( convArea2 != nullptr )
		convexAreas.push_back(const_cast<ConvexArea*>(convArea2));
}

TriangleException::TriangleException(
	std::string methodName, char const* fname, int lineno, std::string message, 
	Perimeter const* convArea, Perimeter const* convArea2) 
	:
	TriangleException(methodName, fname, lineno, message)
{	
	perims.push_back(const_cast<Perimeter*>(convArea));
	if( convArea2 != nullptr )
		perims.push_back(const_cast<Perimeter*>(convArea2));
}

std::string TriangleException::getLocation()const
{
	std::ostringstream loc;

	std::string fileN = fileName;

	auto separatorIndex = fileN.rfind(__FILE_SEP__);
	if(separatorIndex > 0)
		fileN.erase(0,separatorIndex);

	loc << "[" << methodName << ":" << fileN << ":" << lineNo << "]";

	return loc.str();
}

std::string TriangleException::getMessage() const
{
	return getLocation() + " " + this->what();
}

std::ostream&  operator<<(std::ostream& out, const TriangleException& arg)
{
	out << arg.getMessage();

	for(auto const& elem : arg.otris)
		out << " " << elem;

	for (auto const& elem : arg.verts)
		out << " " << *elem;

	for (auto const& elem : arg.osubs)
		out << " " << elem;

	for (auto const& elem : arg.convexAreas)
		out << " " << *elem;

	for (auto const& elem : arg.perims)
		out << " " << *elem;

    return out;
}
