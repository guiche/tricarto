#include "point2d.h"
#include <stdio.h>
#include "triexception.h"
#include <cmath>

REAL Point2d::dist(const Point2d& v) const{ REAL xx = x - v.x; REAL yy = y - v.y; return sqrt(xx*xx + yy*yy); }
REAL Point2d::nor()const{ return sqrt(x*x + y*y); }

std::ostream&  operator<<(std::ostream& out, const Point2d& p)
{
	char message[50];
	
	if(p > 1.0001)
		sprintf(message,"%.1f, %.1f",p.x, p.y);
	else
		sprintf(message,"%.2f, %.2f",p.x, p.y);
		
	out << message;
	return out;
}