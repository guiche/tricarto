'''
Created on 27 Nov 2012

@author: J913965
'''
from .tryangle import Reseau, sommet, toPointList, Vec, RealVec

def ReseauCadre(nom='',exact=False,largeur=100,hauteur=100,marge=10):
    
    graphe = Reseau(nom=nom, exact=exact)
    
    # Sommets ajoutes pour initialiser le graphe avec un triangle de sorte que les autres puissent etre ajoutes
    # incrementalement
        
    coords = [ (marge,2*marge), (largeur-marge,2*marge), (largeur-marge,hauteur-marge), (marge,hauteur-marge)]

    Bornes = graphe.AddVertices(toPointList(coords))
    for Borne in Bornes:
        graphe.VertexToWertex[Borne] = sommet.SommetBorne(Borne, reseau=graphe)

    sommet.Connecte(Bornes,cloture=True)
    
    graphe.CheckMesh()
    
    return graphe

def ReseauTriangle(nom='',exact=False,largeur=100,hauteur=100,marge=10):
    
    graphe = Reseau(nom=nom, exact=exact)
    
    # Sommets ajoutes pour initialiser le graphe avec un triangle de sorte que les autres puissent etre ajoutes
    # incrementalement
    
    coords = toPointList([(marge,2*marge), (largeur-marge,2*marge), (marge,hauteur-marge)])

    print('graphe', graphe)
    Bornes = graphe.AddVertices(coords, RealVec(), RealVec())

    for Borne in Bornes:
        graphe.VertexToWertex[Borne] = sommet.SommetBorne(Borne, reseau=graphe)

    sommet.Connecte(Bornes,cloture=True)
    
    graphe.CheckMesh()
    
    return graphe 


    
def RandomCoord(Num=5, bound=10):
    import random
    return [ bound * random.random() for _ in range(2 * Num) ]


def SimpleMesh(Nodes=[0, 0, 2, 0, 0, 2, 2, 2, 1, 1], Attribs=10, Exact=True):
    
    m = Reseau(exact=Exact)

    if not isinstance(Attribs, (tuple, list)):
        Attribs = [Attribs] * len(Nodes)
       
    # print 'AddVertices',
    m.AddVertices(Nodes, Attribs, [])
    m.DoDelaunay()  

    Vertices = m.Vertices
    print(len(Vertices), 'vertex')

    print(len(m.Triangles), 'triangles')
    
    m.CheckMesh()
    m.CheckDelaunay()
    
    return m
     
def FanMesh():
    return SimpleMesh(Nodes=[200, 0, 0, 0, 100, 100, 300, 300, 0, 200, 400, 400])

def isDirect(p1, p2, p3):
    return ((p2[0] - p1[0]) * (p3[1] - p1[1]) - (p2[1] - p1[1]) * (p3[0] - p1[0])) >= 0

def intri(tri, xy):
    ''' 
        Whether point x,y is inside this->tri
        import tritest; m = tritest.SimpleMesh(); tritest.intri( m.Triangles[3], 1.,1.5)
    '''
    
    for _ in 0, 1, 2:
        vxorg = tri.org;
        vxdest = tri.dest;
        # if we are on the outside of any of the counterclockwise oriented edges, 
        # we are outside of the triangle
        if  (xy - vxorg.coords) * (vxdest.coords - vxorg.coords) < 0:
            return False;
        tri = tri.lnext
    
    return True
