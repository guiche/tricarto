#pragma once

#include "triangle.h"

struct behavior {

/* Switches for the triangulator.                                            */
/*   poly: -p switch.  refine: -r switch.                                    */
/*   quality: -q switch.                                                     */
/*     minangle: minimum angle bound, specified after -q switch.             */
/*     goodangle: cosine squared of minangle.                                */
/*     offconstant: constant used to place off-center Steiner points.        */
/*   vararea: -a switch without number.                                      */
/*   fixedarea: -a switch with number.                                       */
/*     maxarea: maximum area bound, specified after -a switch.               */
/*   usertest: -u switch.                                                    */
/*   regionattrib: -A switch.  convex: -c switch.                            */
/*   weighted: 1 for -w switch, 2 for -W switch.  jettison: -j switch        */
/*   firstnumber: inverse of -z switch.  All items are numbered starting     */
/*     from `firstnumber'.                                                   */
/*   edgesout: -e switch.  voronoi: -v switch.                               */
/*   neighbors: -n switch.  geomview: -g switch.                             */
/*   nobound: -B switch.  nopolywritten: -P switch.                          */
/*   nonodewritten: -N switch.  noelewritten: -E switch.                     */
/*   noiterationnum: -I switch.  noholes: -O switch.                         */
/*   noexact: -X switch.                                                     */
/*   order: element order, specified after -o switch.                        */
/*   nobisect: count of how often -Y switch is selected.                     */
/*   steiner: maximum number of Steiner points, specified after -S switch.   */
/*   incremental: -i switch.						                         */
/*   dwyer: inverse of -l switch.                                            */
/*   splitseg: -s switch.                                                    */
/*   conformdel: -D switch.  docheck: -C switch.                             */
/*   quiet: -Q switch.  verbose: count of how often -V switch is selected.   */
/*   usesegments: -p, -r, -q, or -c switch; determines whether segments are  */
/*     used at all.                                                          */
/*                                                                           */
/* Read the instructions to find out the meaning of these switches.          */

  int poly, refine, quality, vararea, fixedarea, usertest;
  int regionattrib, convex, weighted, jettison;
  int firstnumber;
  int edgesout, voronoi, neighbors, geomview;
  int nobound, nopolywritten, nonodewritten, noelewritten, noiterationnum;
  int noholes, noexact, conformdel;
  int incremental, dwyer;
  int splitseg;
  int docheck;
  int quiet, verbose;
  int usesegments;
  int order;
  int nobisect;
  int steiner;
  REAL minangle, goodangle, offconstant;
  REAL maxarea;

};                                              /* End of `struct behavior'. */

struct ConvexArea;
struct Passage;
struct Perimeter;
class Mesh;


typedef shared_ptr<Perimeter> PerimeterPtr;
typedef shared_ptr<ConvexArea> ConvexAreaPtr;
typedef shared_ptr<Mesh> MeshPtr;

/* Mesh data structure. */
class Mesh {
	Mesh(Mesh const&) = delete;
	Mesh & operator=(Mesh const&) = delete;

public:
	
	Mesh(bool exact=true, bool searchGraph=true, char const* flags="pwczAevnQX");//X : no exact
	~Mesh();

	void ResetDummy();

	int getWeighted(){return b.weighted;};
	void setWeighted(int arg){b.weighted=arg;};

	void initVertices( int n );
	void initTriangles( int n );
	void initSegments( int n );

	void resetNeighborSegs();

	triangle* dummytri;
	/* Pointer to the omnipresent subsegment.  Referenced by any triangle or     */
	/*   subsegment that isn't really connected to a subsegment at that          */
	/*   location.                                                               */
	subseg* dummysub;
	

	enum DelaunayAlgo {DivAndConquer, Incremental};


	// Creates vertex but doesn't add them to the triangulation
	std::vector<wertex*> addVertices_py(	
		const std::vector<Point2d>& pointlist,
		const std::vector<REAL>& pointattriblist = {},
		const std::vector<REAL>& pointmarkerlist = {});

	void addvertices(	Point2d const* ptList,
						size_t numberofpoints,
						std::vector<wertex*>& out,
						const REAL* attribs = nullptr,
						const int* markers = nullptr );

/* Incremental insertion / deletion of vertices - triggers an update of the triangulation */
/* also affects searchMesh if applicable												  */
private:
	
	enum insertvertexresult insertvertex(	wertex* newvertex,
											struct otri *searchtri = nullptr, //TODO: should be const?
											struct osub *splitseg = nullptr);

	void deletevertex(struct otri * deltri, bool dealloc=true, std::vector<wertex*>* connectedVerts = nullptr);

public:	

	wertex* insertVertex_py(const Point2d& point,
							const std::vector<REAL>& attribs,
							int marker=0,
							const wertex* const nearbyVertex=nullptr );

	void deleteVertex(const wertex& delvertex,bool resetDual=true);

	/* triangulation routines */

	long delaunay(enum DelaunayAlgo algo=DivAndConquer);
	long incrementaldelaunay();
	long divconqdelaunay();
	int repairDelaunay(otri& starttri);

	/* Validation routines */
	void checkmesh();
	void checkdelaunay();
	int HullSize(bool checkConvexity=false);

	/* Triangulation substructures and memory management */
	void swapVertices(wertex* vert1, wertex* vert2);
	std::vector<wertex*>&	GetVertices();
	std::vector<triangle*>& GetTriangles();
	std::vector<subseg*>&	GetSegments();

	otri getRecentTri()const;
	void setRecentTri(const struct otri& arg);

	void maketriangle(struct otri *newotri);
	void triangledealloc(triangle *dyingtriangle);

	void makevertex( wertex* &newvertex );
	void vertexdealloc( wertex *dying );

	void makesubseg(struct osub *newsubseg);
	void subsegdealloc(subseg *dying);


	REAL counterclockwise(const Point2d& pa, const Point2d& pb, const Point2d& pc);

	REAL incircle(const Point2d& pa, const Point2d& pb, const Point2d& pc, const Point2d& pd);

	REAL orient3d(const Point2d& pa, const Point2d& pb, const Point2d& pc, const Point2d& pd,
				  REAL aheight, REAL bheight, REAL cheight, REAL dheight);

	REAL nonregular(const Point2d& pa, const Point2d& pb, const Point2d& pc, const Point2d& pd);

	void findcircumcenter(const Point2d& torg, const Point2d& tdest, const Point2d& tapex,
						  Point2d& circumcenter, REAL *xi, REAL *eta, int offcenter);

	void makevertexmap();
	void checkvertexmap();

	// Triangles intersected by a linear path given by points coords
	std::vector<otri> intersectTris_py(	const std::vector<Point2d>& coords, 
										bool loop=true, 
										bool hullstrict=false, 
										const otri& searchtri=nullptr);

	void intersectTris( std::list<otri>& out,	const std::vector<Point2d>& coords, 
												bool loop=true, 
												bool hullstrict=false, 
												const otri& searchtri=nullptr );

	enum PathFindAlgo {MinDistToEnd, MinDist};

	std::vector<Passage> pathFind(const Point2d& startpoint,
									const Point2d& endpoint,
									enum PathFindAlgo algo,
									REAL& distance,
									REAL& searchRatio, // number of convex areas visited either zero or closer to 1 is better.
									const REAL& distMax =0,
									const REAL& widthMax=0,
									bool rectify = true);

	std::vector<Passage> pathFindTri(const Point2d& start, // Path starting point
											const Point2d& end, // Path objective
											REAL& distance,
											REAL& searchRatio,
											otri searchtri = otri(),
											enum PathFindAlgo algo = MinDist,
											const REAL& distMax = 0,
											const REAL& widthMax = 0,
											bool rectify = true);

	std::vector<Passage> pathFind_py(	const Point2d& startpoint, 
										const Point2d& endpoint,
										enum PathFindAlgo algo=MinDist,
										const REAL& distMax=0,
										const REAL& widthMax=0,
										bool rectify = true);

	subseg* firstSegObstacle(const Point2d& p1, 
							const Point2d& p2,
							const otri& starttri = nullptr);

	/* Location */
	// Vertices found inside a polygon given by points coordsList
	std::vector<wertex*> verticesInPolygon( const std::vector<Point2d>& coordsList, bool includeAll=false, const otri& searchtri=nullptr );

	std::vector<wertex*> verticesInCircle( const REAL centre [], const REAL& radius, const otri& searchtri=nullptr );

	void vertAndTrisInPolygon( const std::vector<Point2d>& coordsList,
								bool includeAll,
								std::vector<wertex*>& verts,
								std::vector<triangle*>&	tris,
								const otri& searchtri=nullptr );

	std::vector<triangle*> trianglesInPolygon( const std::vector<Point2d>& coordsList, 
												bool includeAll=false, 
												const otri& searchtri=nullptr );

	otri preciseLocate_py(const Point2d& point, const otri& searchtri=nullptr, 
							bool backtrack=true, bool stopatsubsegment=false);
	
	enum locateresult preciselocate(const Point2d& searchPoint, struct otri *searchtri, int stopatsubsegment=0);

	struct otri Locate(const wertex& searchpoint);
	enum locateresult locate(const Point2d& searchpoint, struct otri *searchtri);

	subseg* insertsubseg( struct otri *tri,
					  int subsegmark);

	void flip(struct otri *flipedge);

	void triangulatepolygon(struct otri *firstedge, struct otri *lastedge,
							int edgecount, int doflip);

	void mergehulls( struct otri *farleft,
					struct otri *innerleft, struct otri *innerright,
					struct otri *farright, int axis);

	void divconqrecurse( wertex **sortarray,
						int num_vertices, int axis,
						struct otri *farleft, struct otri *farright);

	long removeghosts( struct otri *startghost);

	void boundingbox();

	long removebox();

	enum finddirectionresult finddirection(struct otri *searchtri,
										   vertex searchpoint);

	void segmentintersection(struct otri *splittri, struct osub *splitsubseg,
							 vertex endpoint2);

	int scoutsegment( struct otri *searchtri,
					 vertex endpoint2, int newmark);

	void conformingedge(vertex endpoint1, vertex endpoint2, int newmark);

	void delaunayfixup(struct otri *fixuptri, int leftside);

	void constrainededge(struct otri *starttri, vertex endpoint2, int newmark=0);

	void insertsegment(vertex endpoint1, vertex endpoint2, int newmark=0);

	void deletesegment(osub& seg);

	void markhull();

	
	void parsecommandline(int argc, const char **argv);

	struct behavior b;

/* Variables used to allocate memory for triangles, subsegments, vertices   */

	std::vector<triangle *> triangles;
	std::vector<triangle *> deadtriangles;

	std::vector<subseg *> subsegs;
	std::vector<subseg *> deadsubsegs;

	std::vector<wertex *> vertices;
	std::vector<wertex *> deadvertices;
  
	wertex*		GetVert(int index)const;
	triangle*	GetTri(int index)const;
	subseg*		GetSeg(int index)const;

/* Other variables. */

	REAL xmin, xmax, ymin, ymax;                            /* x and y bounds. */
  
	int createdvertices;							 /*increasing only - total number of vertices created*/
	int inelements;                              /* Number of input triangles. */
	int insegments;                               /* Number of input segments. */
	//int holes;                                       /* Number of input holes. */
	//int regions;                                   /* Number of input regions. */
	int undeads;    /* Number of input vertices that don't appear in the mesh. */
	long edges;                                     /* Number of output edges. */
	
	//long hullsize;                          /* Number of edges in convex hull. */
	int steinerleft;                 /* Number of Steiner points not yet used. */
  
	int highorderindex;  /* Index to find extra nodes for high-order elements. */
	int elemattribindex;            /* Index to find attributes of a triangle. */
	int areaboundindex;             /* Index to find area bound of a triangle. */
	int checksegments;         /* Are there segments in the triangulation yet? */
	long samples;              /* Number of random samples for point location. */

  
	/* Triangular bounding box vertices.                                         */

	wertex infvertex1, infvertex2, infvertex3;

	/* Pointer to a recently visited triangle.  Improves point location if       */
	/*   proximate vertices are inserted sequentially.                           */

	struct otri recenttri;

	void boundaryVertices(std::vector<wertex*>& verts)const;

	/* Search Graph */
	
	void deleteDual();
	
	void addDualVertices();
	
	void resetDual();
	
	void resetConvexAreas();
	void validateSearchGraph();

	PerimeterPtr locateConvexArea(const Point2d& pos, otri& searchtri);

	bool searchGraph;
	bool searchDualDirty;
	Mesh * searchDual;

	std::vector<PerimeterPtr> convexAreasByPerim;
	std::vector<int> connexAreaIndex; // vec[i] = j where i is the convexAreas index and j the connexAreas index : i >= j
	std::vector<std::vector<PerimeterPtr> > connexAreas;
	void connectConvexAreas();

	std::vector<PerimeterPtr> triToConvexArea;
	
	ConvexAreaPtr convexAreaForTri(triangle* const tri, std::vector<ConvexAreaPtr>& triToConvexA, 
													    std::vector<ConvexAreaPtr>& convexAreas);
	
	std::vector<PerimeterPtr> getConnexArea( PerimeterPtr perim )const;

	void validatePerimeters()const;
	
	void deleteConvexAreas();

	friend struct wertex;
	friend struct Perimeter;

};    /* End of `Mesh'. */


//Whether two convex areas defined by Perimeter ptr are connex.
#define isconnexDual( perimPtr1, perimPtr2 ) ( searchDual->connexAreaIndex[(perimPtr1)->index] == searchDual->connexAreaIndex[(perimPtr2)->index] )
#define isconnex( perimPtr1, perimPtr2 ) ( connexAreaIndex[(perimPtr1)->index] == connexAreaIndex[(perimPtr2)->index] )

#define connectPerimSides( perimPrev, perimNext )\
	(perimPrev)->setNext((perimNext));			\
	(perimNext)->prev = (perimPrev)


struct Perimeter // segment de perimetre
{	

	Perimeter(int index, const Point2d& point, cvertex cvert=nullptr):index(index),coords(point), vert(cvert){}
	
	int index; // Index de zone convex : commun a tous les perimeters delimitant une meme zone
	
	Point2d coords;
	Point2d univec; // vecteur unitaire de coords vers next->coords (redundant with coords/next->coords but precomputed for performance)
	REAL width=-1;  // perimeter length (redundant with coords/next->coords but precomputed for performance)
	cvertex vert;

	//otri tri;
	PerimeterPtr next; // segment de perimetre suivant dans le sens direct (anti-horaire)
	PerimeterPtr prev; // segment de perimetre precedant dans le sens indirect (horaire)
	PerimeterPtr neighbor; // nullptr unless there is a passage to another perimeter accross the segment.
	osub oseg; // segment coinciding with perimeter segment. nullptr unless port is nullptr.

	PerimeterPtr nextPort; // next segment-free perimeter. Can be nullptr
	PerimeterPtr prevPort; // prev segment-free perimeter. Can be nullptr.

	//inline int int_ptr_py()const{ return (int)this; };

	osub get_oseg();
	void setNext(const PerimeterPtr& perim);
	
	void setNextPrevPort(); // causes all perims in area pointed to by this to set their nextPort/prevPort members.
	
	void validate(bool recursive=false);
	
	inline const Point2d& getNextCoords()const{ return next->coords; }
	inline const Point2d& getCoords(bool next_coord = false)const{ return next_coord ? next->coords : coords; };
	inline const Point2d& getUnivec()const{ return univec; };

	bool contains( const Point2d& point, bool segOnly=false )const; // whether area pointed to by this contains point
		
	bool isConvex()const; // whether area pointed to by this is convex.
	PerimeterPtr findIntersect(Point2d start, Point2d end)const; // finds the perimeter side crossed by start->end vector, assuming start is inside the convex area.

	inline bool operator==(const Perimeter& o)const{ return this == &o; }
	inline bool operator!=(const Perimeter& o)const{ return this != &o; }

	friend std::ostream&  operator<<(std::ostream& out, const Perimeter& arg);
	
	/* Iterator related methods */
	typedef iter_bidir<Perimeter,Perimeter*> iterator;
	typedef iter_bidir<const Perimeter, const Perimeter*> const_iterator;
	typedef size_t size_type;
	typedef ptrdiff_t difference_type;
	typedef Perimeter value_type;
	typedef Perimeter* pointer;
	typedef Perimeter& reference;

	pointer getNext()const{return next.get();};
	pointer getPrev()const{return prev.get();};

	iterator begin() {return iterator(this, true, true); };
	iterator end() {return iterator(this, true, false); };
	iterator rbegin() {return iterator(this, false, true); };
	iterator rend() {return iterator(this, false, false); };
	/* End of Iterator related methods */

};


struct Passage // way point with 
{
	//Passage() {}
	Passage(const Point2d& coors, REAL sign=0, cvertex cvert=nullptr) : coords(coors), sign(sign), vert(cvert){}

	Point2d coords; // way point coordinates
	REAL sign = 0; // signed width
	// PerimeterPtr pass; // gate constraining the passage, a side of a convex area.
	cvertex vert = nullptr;

	const Point2d& getCoords()const{ return coords; }
	cvertex getVert()const{ return vert; }

	inline bool operator==(const Passage& other)const{ return coords == other.coords && sign == other.sign; }
	inline bool operator!=(const Passage& o)const{ return !(*this == o); }

	friend std::ostream&  operator<<(std::ostream& out, const Passage& arg);

};

struct ConvexArea
{	// polygone convexe delimite par des segments de perimetre
	ConvexArea( int index ):index(index){}

	struct triplet
	{
		triplet(const otri& tri,const osub& seg):tri(tri),oseg(seg){};

		otri tri; // triangle included in the Convex area with org->dest on the boundary.
		osub oseg;
		ConvexAreaPtr neighbor;

		inline bool operator==(const ConvexArea::triplet& other){ return tri == other.tri && oseg == other.oseg && neighbor == other.neighbor; }
		inline bool operator!=(const ConvexArea::triplet& o){ return !(*this == o); }

		friend std::ostream&  operator<<(std::ostream& out, const ConvexArea::triplet& arg);

	};

	int index; // identifiant unique de zone convexe
	bool visited = false;

	std::vector<triplet> boundary; // frontiere de la zone convexe parcourue dans le sens horaire (?).

	void validate();

	bool contains( const Point2d& point, bool segOnly=false )const; // true if point is inside ConvexArea; Segonly : true if point is on the inside of all segment sides.
	PerimeterPtr toPerimeter(std::vector<std::pair<int,PerimeterPtr> >& perimMap) const;

	inline bool operator==(const ConvexArea& o)const{ return this == &o; }
	inline bool operator!=(const ConvexArea& o)const{ return this != &o; }

	friend std::ostream&  operator<<(std::ostream& out, const ConvexArea& arg); // print formatting
};

struct PathStep;
typedef shared_ptr<PathStep> PathStepPtr;

struct PathStep
{
	Point2d point;			// way point for that step
	PerimeterPtr toArea;	// Area that step arrives into
	PerimeterPtr fromArea;	// boundary from previous area that one crosses to go to toArea
	REAL distance;			// distance depuis le point d'origine du chemin + distance a vol d'oiseau vers endpoint.
	REAL distSoFar;		// distance depuis l'origine du chemin jusqu'� this->point
	REAL distToEnd;		// distance depuis le point d'origine du chemin
	PathStepPtr prevStep; // pas precedent


	PathStep(	const Point2d& pt, 
				PerimeterPtr toArea,
				PerimeterPtr fromArea,
				REAL distance, 
				REAL distSoFar,
				REAL distToEnd,
				PathStepPtr prevStep=nullptr)
				:point(pt), toArea(toArea), fromArea(fromArea), distance(distance), distSoFar(distSoFar), distToEnd(distToEnd), prevStep(prevStep)
	{}

};

struct PathStepTri;
typedef shared_ptr<PathStepTri> PathStepTriPtr;


template<typename T>
struct PathStepIterator : public std::iterator<std::forward_iterator_tag, T>
{

	PathStepIterator(T * ptr = nullptr) : currentPathStep_(ptr){}
	PathStepIterator& operator++() { 
		if (currentPathStep_) { 
			currentPathStep_ = currentPathStep_->prevStep.get();
		}
		return *this;
	}
	T& operator*() { return *currentPathStep_; }
	inline bool operator!=(const PathStepIterator& v)const { return currentPathStep_ != v.currentPathStep_; }

protected:
	
	T* currentPathStep_;
};

struct PathStepTri
{
	Point2d point;		// way point for that step
	otri toArea;		// Area that step arrives into
	REAL distance;		// distance depuis le point d'origine du chemin + distance a vol d'oiseau vers endpoint.
	REAL distSoFar;		// distance depuis l'origine du chemin jusqu'� this->point
	REAL distToEnd;		// distance depuis le point d'origine du chemin
	PathStepTriPtr prevStep; // pas precedent
	REAL gauge;			// min step gauge

	PathStepTri(const Point2d& pt,
				otri toArea,
				REAL distance,
				REAL distSoFar,
				REAL distToEnd,
				PathStepTriPtr prevStep=nullptr)
				:point(pt), toArea(toArea), distance(distance), distSoFar(distSoFar), distToEnd(distToEnd), prevStep(prevStep), gauge(0)
	{}

	typedef PathStepIterator<PathStepTri> iterator;
	typedef PathStepIterator<const PathStepTri> const_iterator;

	iterator begin() { return iterator(this); }
	iterator end() { return iterator(); }

};
