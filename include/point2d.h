#pragma once

#include "trigeneral.h"
#include <iostream>


struct Point2d
{
	// Capacity.
	inline size_t size() const { return 2; }

	REAL x = 0.;
	REAL y = 0.;

	explicit Point2d() = default;
	Point2d(const REAL x0, const REAL y0): x(x0), y(y0) {}

	inline REAL &operator [](unsigned int i){ return *(&x+i); }
	inline REAL operator [](unsigned int i)const{ return *(&x + i); }
	
	inline void operator ()(const REAL x0, const REAL y0) { x = x0; y = y0; }

	inline bool operator!=(const Point2d &v)const{ return (x != v.x || y != v.y); }
	inline bool operator==(const Point2d &v)const{ return (x == v.x && y == v.y); }
	inline bool operator!()const { return x == 0 && y == 0; }
	inline operator bool() const { return x != 0 && y != 0; }

	Point2d copy()const{return Point2d(x,y);}
	Point2d& copier(const Point2d& u){x=u.x; y=u.y; return *this;}

	typedef REAL* iterator;
	typedef const REAL* const_iterator;
	typedef size_t size_type;
	typedef REAL value_type;
	typedef std::ptrdiff_t difference_type;
	typedef REAL * pointer;
	typedef REAL & reference;
	iterator begin() { return &x; }
	iterator end() { return &y+1; }
	
	inline REAL operator *(const Point2d &v) const{ return x*v.x + y*v.y; }

	inline REAL operator ^(const Point2d &v) const{ return x*v.y - y*v.x; }


	inline Point2d& operator +=(const Point2d &v){ x += v.x; y += v.y; return *this; }

	inline Point2d& operator -=(const Point2d &v){ x -= v.x; y -= v.y; return *this; }

	//! uniform scaling
	template<typename V>
	inline Point2d& operator *=(const V num){ x *= num; y *= num; return *this; }
	
	//! uniform scaling
	template<typename V>
	inline Point2d& operator /=(const V num){ REAL inv_num = 1. / num; x *= inv_num; y *= inv_num; return *this; }
	

	inline REAL nor2()const{return x*x+y*y;}
	REAL nor()const;
	inline Point2d set_nor(REAL val){ REAL fact = val / nor(); return (*this) *= fact; }

	inline Point2d uni()const{ return Point2d(*this) /= nor(); }
	inline Point2d& iuni(){ return (*this) /= nor(); }

	inline Point2d nml()const{ return Point2d(-y, x); }
	inline Point2d& inml(){ REAL z = x; x = -y; y = z; return *this; }
	
	inline Point2d anml()const{ return Point2d(y, -x); }
	inline Point2d& ianml(){ REAL z = x; x = y; y = -z; return *this; }
	
	inline Point2d uni_nml()const{ return Point2d(-y, x).iuni(); }
	inline Point2d& iuni_nml(){ return inml() /= nor(); }
	
	//operator REAL *(){return &x;}

	// (b-a)*this
	inline REAL prodscal_(const Point2d& a, const Point2d& b)const{ return (b[0] - a[0])*x + (b[1] - a[1])*y; }
	// (b-a)*this.nml
	inline REAL prodscalN(const Point2d& a, const Point2d& b)const{ return x*(b[1] - a[1]) - y*(b[0] - a[0]); }

	inline bool isdirect_(const Point2d& a, const Point2d& b)const{return prodscalN(a,b) >= 0; }
	inline bool isdirect_(const Point2d& v)const{return ((*this) ^ v) >= 0; }

	REAL dist(const Point2d& v)  const;
	inline REAL distSq(const Point2d& v)const{ REAL xx = x - v.x; REAL yy = y - v.y; return xx*xx + yy*yy; }
	
	inline REAL tan_abs(const Point2d& v)const{ return fabs((*this ^ v) / (*this * v)); }
	inline bool tan_abs_min(const Point2d& v, REAL const tan)const{ return fabs(*this ^ v) < tan * (*this * v); }

	inline bool angle_inf(const Point2d& v, const Point2d& v1, const Point2d& v2){ return fabs(*this ^ v) * (v1 * v2) <= fabs(v1 ^ v2) * (*this * v); }
	
	Point2d& irot(const REAL a)
	{
		REAL cosa = cosinus(a);
	    REAL sina = sinus(a);
		REAL z = x;
		x = x*cosa - y*sina;
		y = y*cosa + z*sina;
		return *this;
	}
	const Point2d rot(const REAL a)const{Point2d temp(*this); return temp.irot(a);}

	Point2d& irot_v(const Point2d& u /*u: (cosa, sina)*/)
	{
		REAL z = x;
		x = x*u.x - y*u.y;
		y = y*u.x + z*u.y;
		return *this;
	}

	Point2d& iarot_v(const Point2d& u /*u: (cosa, sina)*/)
	{
		REAL z = x;
		x = x*u.x + y*u.y;
		y = y*u.x - z*u.y;
		return *this;
	}

	const Point2d rot_v(const Point2d& u)const{Point2d temp(*this); return temp.irot_v(u);}

	Point2d& irot_vc(const Point2d& u /*u: (cosa, sina)*/, const Point2d& c) {*this -= c; return irot_v(u)+=c;}
	Point2d& iarot_vc(const Point2d& u /*u: (cosa, sina)*/, const Point2d& c){*this -= c; return iarot_v(u)+=c;}

	const Point2d rot_vc(const Point2d& u, const Point2d& p)const{Point2d temp(*this); return temp.irot_vc(u,p);}

	Point2d& iadd_mul(const Point2d& v, const REAL scal){x += scal*v.x; y += scal*v.y; return *this;}
	Point2d& ass_mul(const Point2d& v, const REAL scal){x = scal*v.x; y = scal*v.y; return *this;}

	friend std::ostream&  operator<<(std::ostream& out, const Point2d& p);

	void __setitem__(unsigned int index, REAL value);
	REAL& __getitem__(unsigned int index);
};

template<typename T>
bool operator>=(const Point2d& lhs, const T scal){ return lhs.x*lhs.x+lhs.y*lhs.y >= scal*scal;}
template<typename T>
bool operator>(const Point2d& lhs, const T scal){ return lhs.x*lhs.x+lhs.y*lhs.y > scal*scal;}
template<typename T>
bool operator<(const Point2d& lhs, const T scal){ return lhs.x*lhs.x+lhs.y*lhs.y < scal*scal;}
template<typename T>
bool operator<=(const Point2d& lhs, const T scal){ return lhs.x*lhs.x+lhs.y*lhs.y <= scal*scal;}


template<typename T>
bool operator >=(const T num, const Point2d& rhs) { return rhs<num; }
template<typename T>
bool operator <=(const T num, const Point2d& rhs) { return rhs>num; }
template<typename T>
bool operator >(const T num, const Point2d& rhs) { return rhs<=num; }
template<typename T>
bool operator <(const T num, const Point2d& rhs) { return rhs>=num; }

/* We can use the same trick that operator= use 
	and create the temporary right in the function signature(by taking one argument by copy).
	If the type is movable, the move constructor will get called, otherwise it'll be the copy constructor, 
	but since you need the temporary anyway, no loss of performance.
*/
inline Point2d operator -(Point2d u) { return u *= -1; }

inline Point2d operator +(Point2d u, const Point2d &v)   { return u += v;}
inline Point2d operator +(const Point2d& u, Point2d &&v) { return v += u;}
inline Point2d operator -(Point2d u, const Point2d &v)   { return u -= v; }
inline Point2d operator -(const Point2d& u, Point2d &&v) { v -= u; return -v; }

// uniform scaling with implicit move semantics
template<typename T>
inline Point2d operator *(Point2d p, const T num) { return p *= num; }

// uniform scaling with implicit move semantics
template<typename T>
inline Point2d operator /(Point2d p, const T num) { return p /= num; }

template<typename T>
inline Point2d operator *(const T num, Point2d rhs) { return rhs*num; }
