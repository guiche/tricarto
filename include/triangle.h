#pragma once

#include "trigeneral.h"
#include "point2d.h"
#include <iterator>
#include <stdio.h>
#include <vector>
#include <list>
#include "super_enum.h"

/* Labels that signify the result of direction finding.  The result          */
/*   indicates that a segment connecting the two query points falls within   */
/*   the direction triangle, along the left edge of the direction triangle,  */
/*   or along the right edge of the direction triangle.                      */

enum finddirectionresult {WITHIN, LEFTCOLLINEAR, RIGHTCOLLINEAR};

// below declaration moved to preprocessor declaration in project properties.
//#define _CRT_SECURE_NO_WARNINGS // GG added - VC++ drsprintf compiler warning removal

/*****************************************************************************/
/*                                                                           */
/*  The basic mesh data structures                                           */
/*                                                                           */
/*  There are three:  vertices, triangles, and subsegments (abbreviated      */
/*  `subseg').  These three data structures, linked by pointers, comprise    */
/*  the mesh.  A vertex simply represents a mesh vertex and its properties.  */
/*  A triangle is a triangle.  A subsegment is a special data structure used */
/*  to represent an impenetrable edge of the mesh (perhaps on the outer      */
/*  boundary, on the boundary of a hole, or part of an internal boundary     */
/*  separating two triangulated regions).  Subsegments represent boundaries, */
/*  defined by the user, that triangles may not lie across.                  */
/*                                                                           */
/*  A triangle consists of a list of three vertices, a list of three         */
/*  adjoining triangles, a list of three adjoining subsegments (when         */
/*  segments exist), an arbitrary number of optional user-defined            */
/*  floating-point attributes, and an optional area constraint.  The latter  */
/*  is an upper bound on the permissible area of each triangle in a region,  */
/*  used for mesh refinement.                                                */
/*                                                                           */
/*  For a triangle on a boundary of the mesh, some or all of the neighboring */
/*  triangles may not be present.  For a triangle in the interior of the     */
/*  mesh, often no neighboring subsegments are present.  Such absent         */
/*  triangles and subsegments are never represented by nullptr pointers; they   */
/*  are represented by two special records:  `dummytri', the triangle that   */
/*  fills "outer space", and `dummysub', the omnipresent subsegment.         */
/*  `dummytri' and `dummysub' are used for several reasons; for instance,    */
/*  they can be dereferenced and their contents examined without violating   */
/*  protected memory.                                                        */
/*                                                                           */
/*  However, it is important to understand that a triangle includes other    */
/*  information as well.  The pointers to adjoining vertices, triangles, and */
/*  subsegments are ordered in a way that indicates their geometric relation */
/*  to each other.  Furthermore, each of these pointers contains orientation */
/*  information.  Each pointer to an adjoining triangle indicates which face */
/*  of that triangle is contacted.  Similarly, each pointer to an adjoining  */
/*  subsegment indicates which side of that subsegment is contacted, and how */
/*  the subsegment is oriented relative to the triangle.                     */
/*                                                                           */
/*  The data structure representing a subsegment may be thought to be        */
/*  abutting the edge of one or two triangle data structures:  either        */
/*  sandwiched between two triangles, or resting against one triangle on an  */
/*  exterior boundary or hole boundary.                                      */
/*                                                                           */
/*  A subsegment consists of a list of four vertices--the vertices of the    */
/*  subsegment, and the vertices of the segment it is a part of--a list of   */
/*  two adjoining subsegments, and a list of two adjoining triangles.  One   */
/*  of the two adjoining triangles may not be present (though there should   */
/*  always be one), and neighboring subsegments might not be present.        */
/*  Subsegments also store a user-defined integer "boundary marker".         */
/*  Typically, this integer is used to indicate what boundary conditions are */
/*  to be applied at that location in a finite element simulation.           */
/*                                                                           */
/*  Like triangles, subsegments maintain information about the relative      */
/*  orientation of neighboring objects.                                      */
/*                                                                           */
/*  Vertices are relatively simple.  A vertex is a list of floating-point    */
/*  numbers, starting with the x, and y coordinates, followed by an          */
/*  arbitrary number of optional user-defined floating-point attributes,     */
/*  followed by an integer boundary marker.  During the segment insertion    */
/*  phase, there is also a pointer from each vertex to a triangle that may   */
/*  contain it.  Each pointer is not always correct, but when one is, it     */
/*  speeds up segment insertion.  These pointers are assigned values once    */
/*  at the beginning of the segment insertion phase, and are not used or     */
/*  updated except during this phase.  Edge flipping during segment          */
/*  insertion will render some of them incorrect.  Hence, don't rely upon    */
/*  them for anything.                                                       */
/*                                                                           */
/*  Other than the exception mentioned above, vertices have no information   */
/*  about what triangles, subfacets, or subsegments they are linked to.      */
/*                                                                           */
/*****************************************************************************/

/*****************************************************************************/
/*                                                                           */
/*  Handles                                                                  */
/*                                                                           */
/*  The oriented triangle (`otri') and oriented subsegment (`osub') data     */
/*  structures defined below do not themselves store any part of the mesh.   */
/*  The mesh itself is made of `triangle's, `subseg's, and `vertex's.        */
/*                                                                           */
/*  Oriented triangles and oriented subsegments will usually be referred to  */
/*  as "handles."  A handle is essentially a pointer into the mesh; it       */
/*  allows you to "hold" one particular part of the mesh.  Handles are used  */
/*  to specify the regions in which one is traversing and modifying the mesh.*/
/*  A single `triangle' may be held by many handles, or none at all.  (The   */
/*  latter case is not a memory leak, because the triangle is still          */
/*  connected to other triangles in the mesh.)                               */
/*                                                                           */
/*  An `otri' is a handle that holds a triangle.  It holds a specific edge   */
/*  of the triangle.  An `osub' is a handle that holds a subsegment.  It     */
/*  holds either the left or right side of the subsegment.                   */
/*                                                                           */
/*  Navigation about the mesh is accomplished through a set of mesh          */
/*  manipulation primitives, further below.  Many of these primitives take   */
/*  a handle and produce a new handle that holds the mesh near the first     */
/*  handle.  Other primitives take two handles and glue the corresponding    */
/*  parts of the mesh together.  The orientation of the handles is           */
/*  important.  For instance, when two triangles are glued together by the   */
/*  bond() primitive, they are glued at the edges on which the handles lie.  */
/*                                                                           */
/*  Because vertices have no information about which triangles they are      */
/*  attached to, I commonly represent a vertex by use of a handle whose      */
/*  origin is the vertex.  A single handle can simultaneously represent a    */
/*  triangle, an edge, and a vertex.                                         */
/*                                                                           */
/*****************************************************************************/

template<typename T, typename T_ptr>
struct iter_bidir
{	// circular bidirectional iterator
	typedef std::bidirectional_iterator_tag iterator_category;
	typedef size_t size_type;
	typedef std::ptrdiff_t difference_type;
	typedef T value_type;
	typedef T_ptr pointer;
	typedef T & reference;
	
	
	iter_bidir<T,T_ptr>(T_ptr start, bool isdirect=true, bool isstart=true):
			_start(start),
			_counter(start),
			_is_direct(isdirect),
			_is_start(isstart){}

	reference operator*() { return *_counter; }

	iter_bidir<T,T_ptr> & operator++(){ _is_start=false; _counter = _is_direct ? _counter->getNext() : _counter->getPrev(); return *this; }
	iter_bidir<T,T_ptr> & operator--(){ _is_start=false; _counter = _is_direct ? _counter->getPrev() : _counter->getNext(); return *this; }
	iter_bidir<T,T_ptr> operator++(int){ iter_bidir<T,T_ptr> out=*this; ++(*this); return out; }
	iter_bidir<T,T_ptr> operator--(int){ iter_bidir<T,T_ptr> out=*this; --(*this); return out; }

	inline bool operator==(const iter_bidir &v)const{return _is_start == v._is_start && _counter == _start;}
	inline bool operator!=(const iter_bidir &v)const{return !(*this==v);}

	inline bool at_end(){ return !_is_start && _counter == _start; }

	T_ptr _start;	 // starting point
	T_ptr _counter;  // current point
	bool _is_direct; // whether it iterates in anti-clockwise direction.
	bool _is_start;  // keeps track of whether it's the first time we see an item or when we have done the full loop.
};


struct wertex;
struct osub;
struct subseg;
struct triangle;
class Mesh;

/* An oriented triangle:  includes a pointer to a triangle and orientation.  */
/*   The orientation denotes an edge of the triangle.  Hence, there are      */
/*   three possible orientations.  By convention, each edge always points    */
/*   counterclockwise about the corresponding triangle.                     */


/** 
* positions of a point relative to a triangle
*/
SUPER_ENUM_DEFINE(ETriSidePos, 
	opORG, // beyond side opposite org vertex
	opDST, // beyond side opposite dest vertex
	opAPX, // beyond side opposite apex vertex
	inTRI); // inside triangle

struct otri_iter;
struct otri_iter_unidir;

struct otri {

	triangle *tri = nullptr;
	uint orient = 0; /* Ranges from 0 to 2. */

	otri(triangle* tri = nullptr, uint orient = 0) : tri(tri), orient(orient){}
  
	inline bool operator==(const otri& o)const{ return o.tri == tri && o.orient == orient; }
	inline bool operator!=(const otri& o)const{ return o.tri != tri || o.orient != orient; }
	
	bool operator <(otri const& o) const { return tri < o.tri && orient < o.orient; }

	// iterators : 
	// alternate : left/right or right/left
	// direct stop at boundary
	// direct continue accross boundary
	// direct but flips direction if boundary found
	
	typedef otri_iter iterator;
	typedef const otri_iter const_iterator;
	typedef size_t size_type;
	typedef std::ptrdiff_t difference_type;
	typedef otri value_type;
	typedef otri pointer;
	typedef otri reference;
	
	iterator begin()const;
	iterator end()const;
	iterator rbegin()const;
	iterator rend()const;

	
	otri_iter_unidir begin_unidir()const;
	otri_iter_unidir end_unidir()const;
	otri_iter_unidir rbegin_unidir()const;
	otri_iter_unidir rend_unidir()const;
	
	wertex* org()const;
	wertex* dest()const;
	wertex* apex()const;

	void setorg(wertex* vert);
	void setdest(wertex* vert);
	void setapex(wertex* vert);

	Point2d& coords(uint i)const; // 0 for org, 1 for dest, 2 for apex
	wertex* getVert(uint index)const; // 0 for org, 1 for dest, 2 for apex

	const osub& org_s()const; // dest->apex seg
	const osub& dest_s()const;// apex->org seg
	const osub& apex_s()const;// org->dest seg

	std::vector<wertex*> vertices()const;
  
	otri right_tri_()const;
	otri left_tri_()const;

	otri lprev_() const;
	otri lnext_() const;
	void lprevself_();
	void lnextself_();
	otri oprev_() const;
	otri onext_() const;
	otri dnext_() const;
	otri dprev_() const;
	otri sym_() const;
	otri rnext_() const;
	otri rprev_() const;

	osub osegnext()const;
	osub osegprev()const;
	osub dsegnext()const;
	osub dsegprev()const;
	osub asegnext()const;
	osub asegprev()const;
  
	void bond_(const struct otri& other);
	void flip();
	void Delete();

	friend std::ostream&  operator<<(std::ostream& out, const otri& arg);
	otri & operator++();
	otri  operator++(int){ otri out=*this; ++(*this); return out;  }//postfix
	otri & operator--();
	otri operator--(int){ otri out=*this; --(*this); return out;  }//postfix

	bool contains(const Point2d&, bool strict=false)const;
	ETriSidePos sideposition(const Point2d&, bool strict=false)const;

	bool isDirect(const Point2d& point)const;
	bool isDummy()const;
	bool isNull()const{return !tri;}

	void setorient( int );
	int getorient()const{return orient;}

	ETriSidePos orientTri( const Point2d& point1, const Point2d& point2, bool inplace=true, bool moveToNext=true );
  
};

struct otri_iter : public iter_bidir<otri,otri>
{
	typedef otri reference;
	// Iterator around the triangles sharing the same vertex origin.

	// Goes back to starting point and flips iteration direction if a boundary is found
	otri_iter(const otri& start, bool isdirect=true, bool isstart=true, bool stopatseg=false):
			iter_bidir(start,isdirect,isstart),
			_stop_at_seg(stopatseg),
			_found_edge(false){}

	const otri& operator*() { return _counter; }

	otri_iter & increment(bool forward, bool uni_dir);
	otri_iter & operator++(){ increment(true, false); return *this; }
	otri_iter & operator--(){ increment(false, false); return *this; }
	otri_iter operator++(int){ otri_iter out=*this; ++(*this); return out; }
	otri_iter operator--(int){ otri_iter out=*this; --(*this); return out; }

	bool _stop_at_seg; // whether to stop at a segment
	bool _found_edge; // whether an edge has been found already, causing to reverse direction
};


struct otri_iter_unidir : public otri_iter
{
	// Iterator around the triangles sharing the same vertex origin.

	// Always iterates in the same direction around a vertex.
	// If a boundary is found, go back in the other direction until it finds the other boundary
	// then keeps iterating from there, as if it had jumped over the void.

	otri_iter_unidir(const otri& start, bool isdirect=true, bool isstart=true):otri_iter(start,isdirect,isstart){}
	
	otri_iter_unidir & operator++(){ increment(true, true); return *this; }
	otri_iter_unidir & operator--(){ increment(false, true); return *this; }
	otri_iter_unidir operator++(int){ otri_iter_unidir out=*this; ++(*this); return out; }
	otri_iter_unidir operator--(int){ otri_iter_unidir out=*this; --(*this); return out; }

};


/* An oriented subsegment:  includes a pointer to a subsegment and an        */
/*   orientation.  The orientation denotes a side of the edge.  Hence, there */
/*   are two possible orientations.  By convention, the edge is always       */
/*   directed so that the "side" denoted is the right side of the edge.      */

struct osub {

  subseg *ss;
  int ssorient;                                       /* Ranges from 0 to 1. */

  osub(subseg* ss=nullptr, int orient=0):ss(ss),ssorient(orient){;};

  inline size_t int_ptr_py()const{ return (size_t)ss; };

  inline bool operator==(const osub& o)const{ return o.ss == ss && o.ssorient == ssorient; }
  inline bool operator!=(const osub& o)const{ return o.ss != ss || o.ssorient != ssorient; }

  friend std::ostream&  operator<<(std::ostream& out, const osub& arg);

  wertex* getVert(int index)const;
  
  void Delete(bool resetDual=true);

  osub ssym_py()const;
  osub spivot_py()const;
  osub snext_py()const;
  otri stpivot_py()const;

  REAL getMass_py()const;
  void setMass_py(REAL arg);
  REAL getElas_py()const;
  void setElas_py(REAL arg);
  REAL getFrot_py()const;
  void setFrot_py(REAL arg);
  REAL getGlis_py()const;
  void setGlis_py(REAL arg);

  Point2d univec_py()const;

  wertex* sorg_py()const;
  wertex* sdest_py()const;

  osub segnext_py()const;
  osub segprev_py()const;

  osub dsegnext_py()const;
  osub dsegprev_py()const;

  bool isDummy()const;
  bool isNull()const{return !ss ;};

  void SetNeighborSegs();
  
};

struct InfoCollision
{
	Point2d coords; // x,y coords of the furthest new position we can move to
	bool blocking;
	std::vector<wertex*> obstacles; // TODO: make it wertex const* after corresponding update in boost python code.
	std::vector<REAL> obstProjXs;

	REAL segAvanceX;
	osub oseg;
	
	REAL sideTolFrac;
	REAL side_block_dist;
	wertex const* side_block;
	
	InfoCollision(){Reset();}

	void add_obstacle(wertex const& vert, const REAL& projX);

	void Reset(const REAL& sidetolFrac=0.)
	{
		coords(0,0);
		blocking = false;
		obstacles.clear();
		obstProjXs.clear();

		oseg.ss = nullptr;
		segAvanceX = -1;
		
		sideTolFrac = sidetolFrac;
		side_block_dist = 0;
		side_block = nullptr;

	};

};


/* The vertex data structure.  Each vertex is actually an array of REALs.    */
/*   The number of REALs is unknown until runtime.  An integer boundary      */
/*   marker, and sometimes a pointer to a triangle, is appended after the    */
/*   REALs.                                                                  */

struct wertex {

	int index; // wertex index in mesh.vertices array - can change
	
	int id; // wertex number for user friendly display - constant throughout its life

	REAL radius;

	Point2d coords;
	REAL mass;
	//REAL coords [3]; // = { x, y, radius*radius }
					 // nonregular() routine expects radius*radius in the 3rd coordinate.

	Point2d vel;// velocity
	REAL w;		// angular speed a (rad/s)
	REAL elas;  // elasticite
	REAL frot;  // frottement

	//int boundary;
	int mark;
	otri otri_;

	/* The vertex types.   A DEADVERTEX has been deleted entirely.  An           */
	/*   UNDEADVERTEX is not part of the mesh, but is written to the output      */
	/*   .node file and affects the node indexing in the other output files.     */
	enum Type {INPUTVERTEX=0, SEGMENTVERTEX=1, FREEVERTEX=2, DEADVERTEX=-32768, UNDEADVERTEX=-32767, NULLVERTEX=-32766};

	Type type;
	
	void setTri(triangle* tri, int orient);
	
	inline bool operator==(const wertex& other)const{ return this == &other; }
	inline bool operator!=(const wertex& other)const{ return this != &other; }

	void Delete(bool resetDual=true);
	
	REAL weightRatio() const{ return sqrt(mass) / radius; }
	void setWeightRatio(const REAL& ratio){ mass=ratio*radius; mass*=mass; }

	const otri& getTri()const;

	int getMarker()const;
	int vertID()const { return id; }
	
	enum wertex::Type getType()const;

	inline REAL getRadius()const{return radius;};
	inline void setRadius(const REAL& arg){radius = arg; mass=arg*arg;}

	void Connect( wertex& other );

	friend std::ostream& operator<<(std::ostream& out, const wertex& arg);
	
	enum insertvertexresult move( const Point2d& move, bool relative=true, bool checkCollisions=false, bool enforceDelaunay=true );

	InfoCollision& firstObstacle( const Point2d& newPos, 
									REAL const& frontRadius,
									REAL const& sideRadius=0,
									bool relative=true,	 // whether newPos is a relative move or an absolute position
									bool segOnly =true,	 // detect obstacle among segments only
									bool detectFar=true, // return first obstacle seen, even if it's beyond moveDist
									bool checkResult=false, 
									const REAL& sideTolFrac=0,
									const REAL& frontTolFrac=0 )const;

	InfoCollision& firstObstacleLoop(int NCalls, 
									const Point2d& newPos, 
									REAL const& frontRadius,
									REAL const& sideRadius=0,
									bool relative=true, 
									bool segOnly =true,
									bool detectFar=true,
									bool checkResult=false, 
									const REAL& sideTolFrac=0,
									const REAL& frontTolFrac=0 )const;

	REAL distToSeg(const subseg& checkseg)const;

	bool detectSegContact(const osub& checkseg,
		bool squareSection,
		REAL const& frontRadius,
		REAL const& sideRadius,
		const Point2d& moveUnit,
		REAL& moveDist,
		bool checkValid = false)const;

	void detectVertexContact( wertex const& v_check,	
							bool squareSection,
							const REAL& projX, 
							const REAL& projY,
							REAL const& frontRadius,
							REAL const& sideRadius,
							const Point2d& moveUnit,
							REAL& moveDist,
							const REAL& frontTolFrac)const;

	void getNeighbors(std::vector<wertex*>& neighbors, int degree=0, bool seeThroughWall=false);
	std::vector<wertex*> getNeighbors_py(int degree=0,bool seeThroughWall=false);

	typedef otri::iterator iterator;
	typedef otri::const_iterator const_iterator;
	typedef size_t size_type;
	typedef std::ptrdiff_t difference_type;
	typedef otri::value_type value_type;
	typedef otri::pointer pointer;
	typedef otri::reference reference;
	
	iterator begin() { return iterator(otri_,true, true); }
	iterator end()   { return iterator(otri_,true, false); }
	iterator rbegin(){ return iterator(otri_,false,true); }
	iterator rend()  { return iterator(otri_,false,false); }

	otri triSecant(const Point2d& pos);
	std::vector<osub> segments()const; // segments ending on this vertex
	int getDegree(bool checkCollisions=false)const;
	bool isConvexAndInterior(const Point2d& newpos)const;

	void checkCollide( const Point2d& newPos, bool fullCheck=true )const;

	wertex* getDual()const{ return dualVert; }

	friend class Mesh;

protected:

	wertex(Mesh& mesh) : mesh(mesh)
	{
		reset();
	}
	
	void reset();

	wertex* dualVert = nullptr;

	Mesh& mesh;

	//std::vector<std::pair<wertex*, subseg*> > starNeighbors;

	mutable InfoCollision __infoCollision;

};

typedef wertex* vertex;
typedef wertex const* cvertex;

/* The triangle data structure.  Each triangle contains three pointers to    */
/*   adjoining triangles, plus three pointers to vertices, plus three        */
/*   pointers to subsegments (declared below; these pointers are usually     */
/*   `dummysub').  It may or may not also contain user-defined attributes    */
/*   and/or a floating-point "area constraint."  It may also contain extra   */
/*   pointers for nodes, when the user asks for high-order elements.         */
/*   Because the size and structure of a `triangle' is not decided until     */
/*   runtime, I haven't simply declared the type `triangle' as a struct.     */

struct triangle {

	void Delete();
	void Reset();

	int index;

	otri	 otris[3];
	wertex * verts[3];
	osub	 osubs[3];
	//bool hullSides[3];
	REAL	attrs [3];

	const otri& getTri(int index)const;
	const osub& getSeg(int index)const;
	wertex* getVert(int index)const;

	void setTri(int index, triangle* tri, int orient);
	void setSeg(int index, subseg* seg, int orient);
	void setVert(int index, wertex* vert);
	
	Mesh& mesh;
	Mesh& getMesh() { return mesh; }

	inline bool operator==(const triangle& other)const{ return this == &other; }
	inline bool operator!=(const triangle& other)const{ return this != &other; }

	friend std::ostream&  operator<<(std::ostream& out, const triangle& arg);

	friend class Mesh;

protected:
	triangle(Mesh& mesh);

	triangle() = delete;
};

/* The subsegment data structure.	   Each subsegment contains two pointers */
/*   to adjoining subsegments, plus four pointers to vertices, plus two      */
/*   pointers to adjoining triangles, plus one boundary marker, plus one     */
/*   segment number.                                                         */

/* in the case more than three or more vertices are colinear and belong to one segment on the mesh,
 each subsegment still keeps track of the longest segment org and dest, in addition to its own org and dest, hence 4 vertex pointers,
 and two adjoining subsegments pointers. 
 */

struct subseg {
	

	int index;

	osub	osubs[2]; // adjoining segments
	wertex *verts[4]; // verts[0], verts[1] : segments ends. 
	otri	otris[2]; // adjoining triangles

	REAL mass; // 0 : masse infinie, seg immobile.

	REAL elas; // elasticite
	REAL frot; // frottement
	REAL glis; // glissement 'circulatoire'

	Point2d unitVec;// unit vector from verts[0] to verts[1]

	osub    seg_next[2];
	osub    seg_prev[2];

	int mark;
	int visited;

	enum SegType {	WALL,		// blocks sight
					SEETHROUGH, // can see through
					GLASSWALL	// invisible obstacle : blocks movement but not sight.
				 };//, DEADEND_RIGHT, DEADEND_LEFT};

	Mesh& mesh;
	Mesh& getMesh() { return mesh; }

	SegType type;

	subseg()=delete;

	void Delete(bool resetDual=true);

	const otri& getTri(int index)const;
	const osub& getColinearSeg(int index)const;
	
	wertex* getVert(int index)const;
	osub getNextSeg(int index)const;
	osub getPrevSeg(int index)const;

	void setTri(int index, triangle* tri, int orient);
	void setColinearSeg(int index, subseg* seg, int orient);

	void setNextSeg(int index, subseg* seg, int orient);
	void setPrevSeg(int index, subseg* seg, int orient);

	void setVert(int index, wertex* vert);

	REAL distToPoint(const Point2d& coords, bool distToLine=false)const;
	void setUnitVec();
	void calcUnitVec(Point2d&)const;
	
	bool isDummy()const;

	inline bool operator==(const subseg& other)const{ return this == &other; }
	inline bool operator!=(const subseg& other)const{ return this != &other; }

	friend std::ostream&  operator<<(std::ostream& out, const subseg& arg);

	friend class Mesh;

protected:

	subseg* dualSeg = nullptr;

	subseg(Mesh& mesh);

};
