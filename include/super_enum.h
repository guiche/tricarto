#pragma once

#include <vector>
#include <string>
#include <sstream>
#include <stdexcept>
#include "string_utils.h"

/*
	A template to declare enum classes that can be converted to and from string value.
	It also provides programmatic access to the list of enum values.
	It provides a work around for a limitation of c++11 : allow Enum to be used as a key in STL hash-based containers.
	
	usage:
	
	SUPER_ENUM_DEFINE(EnumName, VAL1, VAL2, VAL3);

	and then:
	EnumName val = EnumName::VAL1;
*/

#define SUPER_ENUM_DEFINE(EnumName,...)\
	\
	enum class EnumName : int;\
	\
	class EnumName##_enum\
	{\
		EnumName##_enum(){}; /* prevents instanciation */ \
	public:\
		\
		static const std::vector<std::string>& getValuesStr()\
		{\
			static std::vector<std::string> values;\
			if (values.empty()){\
				std::string valuesStr = #__VA_ARGS__; \
				std::stringstream ss(valuesStr);\
				std::string item;\
				while (std::getline(ss, item, ',')) {\
					string_utils::trim(item);\
					if (!item.empty())\
						values.push_back(item);\
				}\
			}\
			return values;\
		};\
		static const std::vector<EnumName>& getValues()\
		{\
			static std::vector<EnumName> values;\
			if (values.empty()) {\
				for (size_t i = 0; i < getValuesStr().size(); i++)\
					values.push_back(static_cast<EnumName>(i));\
			}\
			return values;\
		};\
		\
		static const std::string& toString(EnumName arg){\
			return getValuesStr()[static_cast<unsigned>(arg)];\
		}\
		static EnumName fromString(std::string const& val)\
		{\
			if(string_utils::isInteger(val))\
			{\
				int valInt = atoi(val.c_str());\
				if (valInt >= 0 && valInt < (int) EnumName##_enum::getValues().size())\
				{/* integer value in the right range */\
					return static_cast<EnumName>(valInt);\
				}\
			}\
			/* Attempt at converting from string value */\
			for (unsigned int i = 0; i < EnumName##_enum::getValuesStr().size(); i++)\
			{/* Compare with and without fully qualified name*/\
				if (val == EnumName##_enum::getValuesStr()[i] || val == (#EnumName + EnumName##_enum::getValuesStr()[i]))\
				{\
					return static_cast<EnumName>(i);\
				}\
			}\
			throw std::runtime_error("No matching enum value found for token: "+val);\
		}\
	};\
	inline std::istream& operator>> (std::istream& input, EnumName& arg)\
	{\
		std::string val;\
		input >> val;\
		arg = EnumName##_enum::fromString(val);\
		return input;\
	}\
	\
	inline std::ostream& operator<< (std::ostream& output, const EnumName& arg)\
	{\
		return output << EnumName##_enum::toString(arg); \
	}\
	\
	namespace std {\
		template <>\
		struct hash<::EnumName> {\
			size_t operator()(::EnumName type) const {\
				return std::hash<int>()(static_cast<int>(type));\
			}\
		};\
	}\
	\
	enum class EnumName : int { __VA_ARGS__ }
