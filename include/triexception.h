#pragma once

#include <stdexcept>
#include <stdio.h>
#include <string>

#include "triangle.h"
#include "mesh.h"

#define TriangleExc(message) TriangleException( __FUNCTION__, __FILE__, __LINE__, message )
#define TriangleExc_Tri(message, tri) TriangleException( __FUNCTION__, __FILE__, __LINE__, message, tri )
#define TriangleExc_Tri2(message, tri1, tri2) TriangleException( __FUNCTION__, __FILE__, __LINE__, message, tri1, tri2 )
#define TriangleExc_Tri_Vert(message, tri, vert) TriangleException( __FUNCTION__, __FILE__, __LINE__, message, tri, vert )
#define TriangleExc_Sub(message, sub) TriangleException( __FUNCTION__, __FILE__, __LINE__, message, sub )
#define TriangleExc_Sub_Tri(message, sub, tri) TriangleException( __FUNCTION__, __FILE__, __LINE__, message, sub, tri )
#define TriangleExc_Sub2(message, sub1, sub2) TriangleException( __FUNCTION__, __FILE__, __LINE__, message, sub1, sub2 )
#define TriangleExc_Sub_Vert(message, sub, vert) TriangleException( __FUNCTION__, __FILE__, __LINE__, message, sub, vert )
#define TriangleExc_Vert(message, vert) TriangleException( __FUNCTION__, __FILE__, __LINE__, message, vert)
#define TriangleExc_Vert2(message, vert1, vert2) TriangleException( __FUNCTION__, __FILE__, __LINE__, message, vert1, vert2)

#define TriangleExc_ConvexArea(message, convAreaPtr) TriangleException( __FUNCTION__, __FILE__, __LINE__, message, convAreaPtr)
#define TriangleExc_ConvexArea2(message, convAreaPtr, convAreaPtr2) TriangleException( __FUNCTION__, __FILE__, __LINE__, message, convAreaPtr, convAreaPtr2)
#define TriangleExc_Perimeter(message, PerimPtr) TriangleException( __FUNCTION__, __FILE__, __LINE__, message, PerimPtr)
#define TriangleExc_Perim2(message, PerimPtr1, PerimPtr2) TriangleException( __FUNCTION__, __FILE__, __LINE__, message, PerimPtr1, PerimPtr2)

class TriangleException : public std::runtime_error
{

	std::string const methodName;
	char const* const fileName;
	int const lineNo;
	
public:

	std::vector<otri> otris;
	std::vector<osub> osubs;
	// Can't make the following const* because I don't know how to expose those const* in boost::python
	std::vector<wertex *> verts;
	std::vector<ConvexArea *> convexAreas;
	std::vector<Perimeter *> perims;

	TriangleException(std::string methodName, char const* fname, int lineno, std::string message);

	TriangleException(std::string methodName, char const* fname, int lineno, std::string message, otri tri, wertex const* vert=nullptr);

	TriangleException(std::string methodName, char const* fname, int lineno, std::string message, otri tri, otri tri2);

	TriangleException(std::string methodName, char const* fname, int lineno, std::string message, osub sub, wertex const* vert=nullptr);

	TriangleException(std::string methodName, char const* fname, int lineno, std::string message, osub sub, otri tri);

	TriangleException(std::string methodName, char const* fname, int lineno, std::string message, osub sub, osub sub2);

	TriangleException(std::string methodName, char const* fname, int lineno, std::string message, wertex const* vert, wertex const* vert2=nullptr);

	TriangleException(std::string methodName, char const* fname, int lineno, std::string message, ConvexArea const* convArea, ConvexArea const* convArea2=nullptr);

	TriangleException(std::string methodName, char const* fname, int lineno, std::string message, Perimeter const* convArea, Perimeter const* convArea2=nullptr);

	friend std::ostream& operator<<(std::ostream& out, const TriangleException& arg);

	~TriangleException() throw(){}

	std::string getLocation() const;

	std::string getMessage() const;
};
