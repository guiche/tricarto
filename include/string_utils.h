#pragma once

#include <cstring>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>

#include <sstream>

#include "trigeneral.h"

namespace string_utils
{
	//std::to_string not defined on NDK android for c++11
	template < typename T > std::string to_string(const T& n)
	{
		std::ostringstream stm;
		stm << n;
		return stm.str();
	}

	void trim(std::string& str, const std::string& whitespace = " \t\n\r");


	/*
	Returns true if the string can be interpreted as an integer.

	acceptSign: whether signs are accepted.
	i.e. : isInteger("-1", false) will return false.
	*/
	bool isInteger(const std::string & s, bool acceptSign = false);


	template <typename T>
	struct StreamPrinter {

		static std::ostream& toStream(std::ostream& output, const T& param)
		{
			return output << param;
		}
	};

	template <typename T>
	struct StreamPrinter<T*> {

		static std::ostream& toStream(std::ostream& output, const T*& param)
		{
			return output << (param ? typeid(param).name() : "NULL");
		}
	};

	template <typename T>
	struct StreamPrinter<shared_ptr<T>> {

		static std::ostream& toStream(std::ostream& output, std::shared_ptr<T> const& param)
		{
			std::string paramName = param ? typeid(*param).name() : typeid(param.get()).name();
			if (paramName.find("class ") == 0)
				paramName = paramName.substr(6);

			size_t starPos = paramName.find("*");
			if (starPos != std::string::npos)
				paramName = paramName.substr(0, starPos);

			return output << paramName;
		}
	};

	template <>
	struct StreamPrinter<char const*> {

		static std::ostream& toStream(std::ostream& output, char const* const& arg)
		{
			return output << arg;
		}
	};

	struct StreamWriter
	{
		std::ostream & out;
		StreamWriter(std::ostream & out)
			: out(out) {}

		template<typename TF, typename ... TR>
		void write(TF const& f, TR const& ... rest) {

			StreamPrinter<TF>::toStream(out, f);
			out << " ";
			write(rest...);
		}

		template<typename TF>
		void write(TF const& f) {
			StreamPrinter<TF>::toStream(out, f);
		}
	};

}
