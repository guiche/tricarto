#pragma once

#include "trigeneral.h"
#include "point2d.h"

template<typename Container, typename Value>
inline bool hasElem(Container const& container, Value const& elem) {
	return container.find(elem) != container.end();
}

#define dist2(point1,point2) ( ( (point2)[0]-(point1)[0] ) * ( (point2)[0]-(point1)[0] ) + ( (point2)[1]-(point1)[1] ) * ( (point2)[1]-(point1)[1] ) )
#define dist2_v(vec) ( (vec)[0] * (vec)[0] + (vec)[1] * (vec)[1] )
#define norm_v(vec) sqrt( dist2_v( vec ) )
#define norm(point1, point2) sqrt( dist2( (point1), (point2) ) )

#define point_equal(point1,point2) ((point2)[0]==(point1)[0]) && ((point2)[1]==(point1)[1])

#define point_add(point1, point2) \
	(point1)[0] += (point2)[0];   \
	(point1)[1] += (point2)[1]

#define point_add_mul(point1, point2, scal) \
	(point1)[0] += (scal)*(point2)[0];   \
	(point1)[1] += (scal)*(point2)[1]

#define point_sub(point1, point2) \
	(point1)[0] -= (point2)[0];   \
	(point1)[1] -= (point2)[1]

#define point_mul(point1, scal) \
	(point1)[0] *= scal;   \
	(point1)[1] *= scal

#define point_copy(pointTo, pointFrom) \
	(pointTo)[0] = (pointFrom)[0];   \
	(pointTo)[1] = (pointFrom)[1]


/*****************************************************************************/
/*                                                                           */
/*  Mesh manipulation primitives.  Each triangle contains three pointers to  */
/*  other triangles, with orientations.  Each pointer points not to the      */
/*  first byte of a triangle, but to one of the first three bytes of a       */
/*  triangle.  It is necessary to extract both the triangle itself and the   */
/*  orientation.  To save memory, I keep both pieces of information in one   */
/*  pointer.  To make this possible, I assume that all triangles are aligned */
/*  to four-byte boundaries.  The decode() routine below decodes a pointer,  */
/*  extracting an orientation (in the range 0 to 2) and a pointer to the     */
/*  beginning of a triangle.  The encode() routine compresses a pointer to a */
/*  triangle and an orientation into a single pointer.  My assumptions that  */
/*  triangles are four-byte-aligned and that the `unsigned long' type is     */
/*  long enough to hold a pointer are two of the few kludges in this program.*/
/*                                                                           */
/*  Subsegments are manipulated similarly.  A pointer to a subsegment        */
/*  carries both an address and an orientation in the range 0 to 1.          */
/*                                                                           */
/*  The other primitives take an oriented triangle or oriented subsegment,   */
/*  and return an oriented triangle or oriented subsegment or vertex; or     */
/*  they change the connections in the data structure.                       */
/*                                                                           */
/*  Below, triangles and subsegments are denoted by their vertices.  The     */
/*  triangle abc has origin (org) a, destination (dest) b, and apex (apex)   */
/*  c.  These vertices occur in counterclockwise order about the triangle.   */
/*  The handle abc may simultaneously denote vertex a, edge ab, and triangle */
/*  abc.                                                                     */
/*                                                                           */
/*  Similarly, the subsegment ab has origin (sorg) a and destination (sdest) */
/*  b.  If ab is thought to be directed upward (with b directly above a),    */
/*  then the handle ab is thought to grasp the right side of ab, and may     */
/*  simultaneously denote vertex a and edge ab.                              */
/*                                                                           */
/*  An asterisk (*) denotes a vertex whose identity is unknown.              */
/*                                                                           */
/*  Given this notation, a partial list of mesh manipulation primitives      */
/*  follows.                                                                 */
/*                                                                           */
/*                                                                           */
/*  For triangles:                                                           */
/*                                                                           */
/*  sym:  Find the abutting triangle; same edge.                             */
/*  sym(abc) -> ba*                                                          */
/*                                                                           */
/*  lnext:  Find the next edge (counterclockwise) of a triangle.             */
/*  lnext(abc) -> bca                                                        */
/*                                                                           */
/*  lprev:  Find the previous edge (clockwise) of a triangle.                */
/*  lprev(abc) -> cab                                                        */
/*                                                                           */
/*  onext:  Find the next edge counterclockwise with the same origin.        */
/*  onext(abc) -> ac*                                                        */
/*                                                                           */
/*  oprev:  Find the next edge clockwise with the same origin.               */
/*  oprev(abc) -> a*b                                                        */
/*                                                                           */
/*  dnext:  Find the next edge counterclockwise with the same destination.   */
/*  dnext(abc) -> *ba                                                        */
/*                                                                           */
/*  dprev:  Find the next edge clockwise with the same destination.          */
/*  dprev(abc) -> cb*                                                        */
/*                                                                           */
/*  rnext:  Find the next edge (counterclockwise) of the adjacent triangle.  */
/*  rnext(abc) -> *a*                                                        */
/*                                                                           */
/*  rprev:  Find the previous edge (clockwise) of the adjacent triangle.     */
/*  rprev(abc) -> b**                                                        */
/*                                                                           */
/*  org:  Origin          dest:  Destination          apex:  Apex            */
/*  org(abc) -> a         dest(abc) -> b              apex(abc) -> c         */
/*                                                                           */
/*  bond:  Bond two triangles together at the resepective handles.           */
/*  bond(abc, bad)                                                           */
/*                                                                           */
/*                                                                           */
/*  For subsegments:                                                         */
/*                                                                           */
/*  ssym:  Reverse the orientation of a subsegment.                          */
/*  ssym(ab) -> ba                                                           */
/*                                                                           */
/*  spivot:  Find adjoining subsegment with the same origin.                 */
/*  spivot(ab) -> a*                                                         */
/*                                                                           */
/*  snext:  Find next subsegment in sequence.                                */
/*  snext(ab) -> b*                                                          */
/*                                                                           */
/*  sorg:  Origin                      sdest:  Destination                   */
/*  sorg(ab) -> a                      sdest(ab) -> b                        */
/*                                                                           */
/*  sbond:  Bond two subsegments together at the respective origins.         */
/*  sbond(ab, ac)                                                            */
/*                                                                           */
/*                                                                           */
/*  For interacting tetrahedra and subfacets:                                */
/*                                                                           */
/*  tspivot:  Find a subsegment abutting a triangle.                         */
/*  tspivot(abc) -> ba                                                       */
/*                                                                           */
/*  stpivot:  Find a triangle abutting a subsegment.                         */
/*  stpivot(ab) -> ba*                                                       */
/*                                                                           */
/*  tsbond:  Bond a triangle to a subsegment.                                */
/*  tsbond(abc, ba)                                                          */
/*                                                                           */
/*****************************************************************************/

/********* Mesh manipulation primitives begin here                   *********/
/**                                                                         **/
/**                                                                         **/

/* Fast lookup arrays to speed some of the mesh manipulation primitives.     */

static constexpr int plus1mod3  [3] = {1, 2, 0};
static constexpr int minus1mod3 [3] = {2, 0, 1};


float fast_sqrt( float number );

bool intersection(	const Point2d& pt1, const Point2d& pt2,
					const Point2d& ptA, const Point2d& ptB, 
					Point2d& intersection );


bool segintersecte(	const Point2d& pt1, const Point2d& pt2,
					const Point2d& ptA, const Point2d& ptB);

/********* Primitives for triangles                                  *********/
/*                                                                           */
/*                                                                           */

/* The following handle manipulation primitives are all described by Guibas  */
/*   and Stolfi.  However, Guibas and Stolfi use an edge-based data          */
/*   structure, whereas I use a triangle-based data structure.               */

/* sym() finds the abutting triangle, on the same edge.  Note that the edge  */
/*   direction is necessarily reversed, because the handle specified by an   */
/*   oriented triangle is directed counterclockwise around the triangle.     */

#define sym(otri1, otri2)                                                     \
  (otri2) = (otri1).tri->otris[(otri1).orient]

#define symself(otri)                                                         \
  (otri) = (otri).tri->otris[(otri).orient]


/* lnext() finds the next edge (counterclockwise) of a triangle.             */

#define lnext(otri1, otri2)                                                   \
  (otri2).tri = (otri1).tri;                                                  \
  (otri2).orient = plus1mod3[(otri1).orient]

#define lnextself(otri)                                                       \
  (otri).orient = plus1mod3[(otri).orient]

/* lprev() finds the previous edge (clockwise) of a triangle.                */

#define lprev(otri1, otri2)                                                   \
  (otri2).tri = (otri1).tri;                                                  \
  (otri2).orient = minus1mod3[(otri1).orient]

#define lprevself(otri)                                                       \
  (otri).orient = minus1mod3[(otri).orient]

/* onext() spins counterclockwise around the org vertex; that is, it finds the     */
/*   next edge with the same origin in the counterclockwise direction.  This */
/*   edge is part of a different triangle.                                   */

#define onext(otri1, otri2)                                                   \
  lprev(otri1, otri2);                                                        \
  symself(otri2)

#define onextself(otri)                                                       \
  lprevself(otri);                                                            \
  symself(otri)

/* pointers to the left, right and opposite triangle seen from otri org. */

#define left_tri(otri1)                                                   \
  (otri1).tri->otris[minus1mod3[(otri1).orient]].tri

#define right_tri(otri1)                                                   \
	(otri1).tri->otris[(otri1).orient].tri

#define oppo_tri(otri1)                                                     \
    (otri1).tri->otris[plus1mod3[(otri1).orient]].tri


/* oprev() spins clockwise around the org vertex; that is, it finds the next edge  */
/*   with the same origin in the clockwise direction.  This edge is part of  */
/*   a different triangle.                                                   */

#define oprev(otri1, otri2)                                                   \
  sym(otri1, otri2);                                                          \
  lnextself(otri2)

#define oprevself(otri)                                                       \
  symself(otri);                                                              \
  lnextself(otri)

/* dnext() spins counterclockwise around the dest vertex; that is, it finds the     */
/*   next edge with the same destination in the counterclockwise direction.  */
/*   This edge is part of a different triangle.                              */

#define dnext(otri1, otri2)                                                   \
  sym(otri1, otri2);                                                          \
  lprevself(otri2)

#define dnextself(otri)                                                       \
  symself(otri);                                                              \
  lprevself(otri)

/* dprev() spins clockwise around the dest vertex; that is, it finds the next edge  */
/*   with the same destination in the clockwise direction.  This edge is     */
/*   part of a different triangle.                                           */

#define dprev(otri1, otri2)                                                   \
  lnext(otri1, otri2);                                                        \
  symself(otri2)

#define dprevself(otri)                                                       \
  lnextself(otri);                                                            \
  symself(otri)

/* rnext() moves one edge counterclockwise about the adjacent triangle.      */
/*   (It's best understood by reading Guibas and Stolfi.  It involves        */
/*   changing triangles twice.)                                              */

#define rnext(otri1, otri2)                                                   \
  sym(otri1, otri2);                                                          \
  lnextself(otri2);                                                           \
  symself(otri2)

#define rnextself(otri)                                                       \
  symself(otri);                                                              \
  lnextself(otri);                                                            \
  symself(otri)

/* rprev() moves one edge clockwise about the adjacent triangle.             */
/*   (It's best understood by reading Guibas and Stolfi.  It involves        */
/*   changing triangles twice.)                                              */

#define rprev(otri1, otri2)                                                   \
  sym(otri1, otri2);                                                          \
  lprevself(otri2);                                                           \
  symself(otri2)

#define rprevself(otri)                                                       \
  symself(otri);                                                              \
  lprevself(otri);                                                            \
  symself(otri)

/* Bond two triangles together.                                              */

#define bond(otri1, otri2)                                                    \
  (otri1).tri->otris[(otri1).orient] = (otri2);                                \
  (otri2).tri->otris[(otri2).orient] = (otri1);

/* Dissolve a bond (from one side).  Note that the other triangle will still */
/*   think it's connected to this triangle.  Usually, however, the other     */
/*   triangle is being deleted entirely, or bonded to another triangle, so   */
/*   it doesn't matter.                                                      */

#define dissolve(otri)                                                        \
  (otri).tri->otris[(otri).orient] = this->dummytri

/* Check or set a triangle's attributes.                                     */

#define elemattribute(otri, attnum)                                           \
  (otri).tri->attrs[(attnum)]

#define setelemattribute(otri, attnum, value)                                 \
  (otri).tri->attrs[(attnum)] = value

/* Check or set a triangle's deallocation.  Its second pointer is set to     */
/*   nullptr to indicate that it is not allocated.  (Its first pointer is used  */
/*   for the stack of dead items.)  Its fourth pointer (its first vertex)    */
/*   is set to nullptr in case a `badtriang' structure points to it.            */

#define deadtri(tria)  ((tria)->otris[1].tri == nullptr)

/********* Primitives for subsegments                                *********/
/*                                                                           */
/*                                                                           */

#define osubequal(osub1, osub2)                                               \
  (((osub1).ss == (osub2).ss) &&                                            \
   ((osub1).ssorient == (osub2).ssorient))

/* ssym() toggles the orientation of a subsegment.                           */

#define ssym(osub1, osub2)                                                    \
  (osub2).ss = (osub1).ss;                                                    \
  (osub2).ssorient = 1 - (osub1).ssorient

#define ssymself(osub)                                                        \
  (osub).ssorient = 1 - (osub).ssorient

/* spivot() finds the other subsegment (from the same segment) that shares   */
/*   the same origin.                                                        */

#define spivot(osub1, osub2)                                                  \
  (osub2) = (osub1).ss->osubs[(osub1).ssorient]

#define spivotself(osub)                                                      \
  (osub) = (osub).ss->osubs[(osub).ssorient]
  
/* snext() finds the next subsegment (from the same segment) in sequence;    */
/*   one whose origin is the input subsegment's destination.                 */

#define snext(osub1, osub2)                                                   \
  (osub2) = (osub1).ss->osubs[1 - (osub1).ssorient]

#define snextself(osub)                                                       \
  (osub) = (osub).ss->osubs[1 - (osub).ssorient]

/* segnext() finds the next subsegment with the same origin when rotating counter-clockwise */

#define segnext(osub1)                                                \
  (osub1).ss->seg_next[(osub1).ssorient]

/* segprev() finds the next subsegment with the same origin when rotating clockwise */

#define segprev(osub1)                                                \
  (osub1).ss->seg_prev[(osub1).ssorient]

/* These primitives determine or set the origin or destination of a          */
/*   subsegment or the segment that includes it.                             */

#define svorg(osub)															  \
  (osub).ss->verts[(osub).ssorient]

#define sorg(osub, vertexptr)                                                 \
  vertexptr = svorg(osub)

#define svdest(osub)														  \
  (osub).ss->verts[1 - (osub).ssorient]

#define sdest(osub, vertexptr)                                                \
  vertexptr = svdest(osub)

#define setsorg(osub, vertexptr)                                              \
  (osub).ss->verts[(osub).ssorient] = vertexptr

#define setsdest(osub, vertexptr)                                             \
  (osub).ss->verts[1 - (osub).ssorient] = vertexptr

#define segorg(osub, vertexptr)                                               \
  vertexptr = (osub).ss->verts[2 + (osub).ssorient]

#define segdest(osub, vertexptr)                                              \
  vertexptr = (osub).ss->verts[3 - (osub).ssorient]

#define setsegorg(osub, vertexptr)                                            \
  (osub).ss->verts[2 + (osub).ssorient] = vertexptr

#define setsegdest(osub, vertexptr)                                           \
  (osub).ss->verts[3 - (osub).ssorient] = vertexptr

/* These primitives read or set a boundary marker.  Boundary markers are     */
/*   used to hold user-defined tags for setting boundary conditions in       */
/*   finite element solvers.                                                 */

#define mark(osub)  (osub).ss->mark

#define setmark(osub, value)                                                  \
  mark(osub) = value

/* Bond two subsegments together.                                            */

#define sbond(osub1, osub2)                                                   \
  (osub1).ss->osubs[(osub1).ssorient] = osub2;                              \
  (osub2).ss->osubs[(osub2).ssorient] = osub1

/* Dissolve a subsegment bond (from one side).  Note that the other          */
/*   subsegment will still think it's connected to this subsegment.          */

#define sdissolve(osub)                                                       \
  (osub).ss->osubs[(osub).ssorient].ss = this->dummysub

/* Copy a subsegment.                                                        */

#define subsegcopy(osub1, osub2)                                              \
  (osub2).ss = (osub1).ss;                                                    \
  (osub2).ssorient = (osub1).ssorient

/* Test for equality of subsegments.                                         */

#define subsegequal(osub1, osub2)                                             \
  (((osub1).ss == (osub2).ss) &&                                              \
   ((osub1).ssorient == (osub2).ssorient))

/********* Primitives for interacting triangles and subsegments      *********/
/*                                                                           */
/*                                                                           */

/* tspivot() finds a subsegment abutting a triangle.                         */

#define tspivot(otri, osub)                                                   \
  (osub)=(otri).apex_s()

#define tsorg(otri, osub)                                                   \
  (osub) = (otri).apex_s()

#define tsdest(otri, osub)                                                   \
  (osub) = (otri).org_s()

#define tsapex(otri, osub)                                                   \
  (osub) = (otri).dest_s()

/* stpivot() finds a triangle abutting a subsegment.						*/
#define stpivot(osub, otri)                                                   \
  (otri) = (osub).ss->otris[(osub).ssorient]

/* Bond a triangle to a subsegment.                                          */
#define tsbond(otri, osub)                                                    \
  (otri).tri->osubs[(otri).orient] = (osub);                   \
  (osub).ss->otris[(osub).ssorient] = (otri)

/* Dissolve a bond (from the triangle side).                                 */

#define tsdissolve(otri)                                                      \
  (otri).tri->osubs[(otri).orient].ss = this->dummysub

/* Dissolve a bond (from the subsegment side).                               */

#define stdissolve(osub)                                                      \
  (osub).ss->otris[(osub).ssorient].tri = this->dummytri

/********* Primitives for vertices                                   *********/
/*                                                                           */
/*                                                                           */

#define vertexmark(vx)  (vx)->mark

#define setvertexmark(vx, value)                                              \
  vertexmark(vx) = (value)

#define vertexradius(vx)  (vx)->radius

//Whether it's one end of a segment
#define segmentend(vx) (vertexradius(vx) <= 0)

#define vertextype(vx)  (vx)->type

#define setvertextype(vx, value)                                              \
  vertextype(vx) = (value)

#define vertex2tri(vx)  (vx)->otri_

#define setvertex2tri(vx, value)                                              \
  vertex2tri(vx) = (value)

#define setvertex2tri_orient(vx, value, orien)                             \
  vertex2tri(vx).tri = (value).tri;											\
  vertex2tri(vx).orient = (orien)

#define setvertex2tri_org(otri)                                              \
	(otri).org()->otri_ = (otri)
	
#define setallvertex2tri(otri)                                              \
	for( int _i=0; _i<3; _i++ ){											\
		setvertex2tri_org(otri);											\
		lnextself(otri);													\
	}

// vect_eq : checks two vectors have the same coordinates
#define vect_eq(coords1, coords2)										\
	(((coords1)[0] == (coords2)[0]) && ((coords1)[1] == (coords2)[1]))

#define prodvect(vxorg, vxdest, vxapex)										\
	(((vxdest)[0]-(vxorg)[0])*((vxapex)[1]-(vxorg)[1]) - ((vxapex)[0]-(vxorg)[0])*((vxdest)[1]-(vxorg)[1]))

#define prodvect_vp(vxorg, vxdest, vec)										\
	(((vxdest)[0]-(vxorg)[0])*((vec)[1]) - ((vec)[0])*((vxdest)[1]-(vxorg)[1]))

#define prodscal(vxorg, vx1, vx2)											\
	(((vx1)[0]-(vxorg)[0])*((vx2)[0]-(vxorg)[0]) + ((vx1)[1]-(vxorg)[1])*((vx2)[1]-(vxorg)[1]))

#define prodscal_v(vec1, vec2)											\
	( (vec1)[0]*(vec2)[0]+(vec1)[1]*(vec2)[1] )

#define prodvect_v(vec1,vec2)										\
	( (vec1)[0]*(vec2)[1]-(vec1)[1]*(vec2)[0] )

/* Produit scalaire d'un vecteur defini par ces deux points contre la normale directe de vec */
#define prodscalN_vp(vxorg, vxdest, vec)											\
	(((vxdest)[0]-(vxorg)[0])*(-(vec)[1]) + ((vxdest)[1]-(vxorg)[1])*((vec)[0]))

#define prodscal_vp(vxorg, vxdest, vec)											\
	(((vxdest)[0]-(vxorg)[0])*((vec)[0]) + ((vxdest)[1]-(vxorg)[1])*((vec)[1]))

#define norm2(vec)											\
	((vec)[0]*(vec)[0]+(vec)[1]*(vec)[1])

#define vecpluse( vec1, vec2 )		\
	(vec1)[0]+=(vec2)[0];			\
	(vec1)[1]+=(vec2)[1]

#define vecminuse( vec1, vec2 )		\
	(vec1)[0]-=(vec2)[0];			\
	(vec1)[1]-=(vec2)[1]O

#define vecmulte( vec1, scal )		\
	(vec1)[0]*=(scal);				\
	(vec1)[1]*=(scal)

// whether vxorg, vxdest, (x,y) triangle is direct (counter-clockwise)
#define isdirect(vxorg, vxdest, xy)											\
	(prodvect((vxorg), (vxdest), (xy)) >= 0)

#define isdirect_strict(vxorg, vxdest, xy)											\
	(prodvect((vxorg), (vxdest), (xy)) > 0)


/**                                                                         **/
/**                                                                         **/
/********* Mesh manipulation primitives end here                     *********/