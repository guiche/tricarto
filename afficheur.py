from .tryangle import otri, osub, TryangleException, \
    PathFindAlgo, triangle, subseg, Perimeter

from pygapp import app
from pygapp.partieutils import clic, SelecteAjoutRetrait, TOUCHES_NUM
from pygapp.utils import fanions
from .tryangle import Vec, PointList, SommetBorne
import traceback
from .tryangle import reseau
from .tryangle import sommet

try:
    import pygame
    from pygame import mouse as souris
except ImportError:
    # pygame not present - display will fail
    pygame = NotImplemented
    souris = NotImplemented

CouleurContourSelec = 0, 63, 0
CouleurNumerosVert  = 170, 200, 255


def Affiche(graphe):
    pygame.init()
    AfficheurDeGraphe(graphe=graphe).GrandBoucle()


class AfficheurDeGraphe(app.Application):
    
    Vec = Vec
        
    def __init__(self, ClasseUnite=sommet.Wertex, graphe=None, DimEcran=(400, 600)):
        
        app.Application.__init__(self, DimEcran)
        
        self.graphe      = graphe

        self.ClasseUnite = ClasseUnite

        self.SelectionTris  = set()
        self.SelectionSegs  = set()
        self.SelectionPerims= set()
        self.SelectionGroupes = {}
        self.SelectionGroupe = None
        self.ExactPrecision  = False
        
        self.AfficheCercleCirc = False
                        
        self.AfficheTriangles = True
        self.AfficheRecentTri = False
        
        self.AfficheMouvement   = 0  # False
        self.AfficheDeplacement = False
        self.ActiveDelaunay     = True
        
        self.ModeGravite    = False
        self.AfficheDual    = False
        self.AfficheConnexe = False
        self.VecGravite     = Vec(0, 500.)  
            
    def CoordsVues_ep(self, CoordsReeles, Entier=False):
        
        app.Application.CoordsVues_ep(self, CoordsReeles, Entier=False)
        
        # La classe Vec en cpp retransformerait des entiers en float e.g. 2 -> 2.0
        if Entier:
            return int(CoordsReeles[0]),int(CoordsReeles[1])
        return CoordsReeles 
    
    def _coords_triangle(self, tri, coordEntier=True):
        """ formattage des coordonnees des sommets d'un triangle sous forme de liste
            de coordonnes dans le referentiel d'affichage """
        
        if isinstance(tri, otri):
            Tri = tri.tri
        else:
            Tri = tri
            
        return [ self.CoordsVues(Tri.getVert(i).coords, Entier=coordEntier) for i in (0, 1, 2) ]
    
    def DessineConvexArea(self, perim, couleur=(250, 250, 0), numerote=True, ports=True):
            
        bary = Vec()
        
        numPerims = 0
        
        ArgsTraitFin   = dict(trait=1, lisse=True,  couleur=couleur)
        ArgsTraitEpais = dict(trait=6, lisse=False, couleur=couleur)
        
        for tempPerim in perim: 

            nextPerim = tempPerim.next
            
            numPerims += 1
                              
            if tempPerim.neighbor:
                KWArgs = ArgsTraitFin
            else:
                KWArgs = ArgsTraitEpais
                
            point1 = tempPerim.coords
            point2 = nextPerim.coords
                                                    
            self.dessineLigne(point1, point2, **KWArgs)

            if ports and numPerims == 1:
                # affichage du pointeur perim representant la zone convexe
                self.dessineLigne(point1, point2, couleur=(255,0,255), trait=ArgsTraitEpais['trait'])

            if ports and tempPerim.neighbor:
                # Dessine les passages a une zone mitoyenne.
                milieu = (point1 + point2) / 2.
                
                nml = -20 * tempPerim.univec.nml
            
                milieuPlusNml = milieu + nml
                
                self.dessineLigne(milieu, milieuPlusNml, couleur=couleur, trait=1)
                self.afficheParatexte(str(tempPerim.neighbor.index), milieuPlusNml, couleur=couleur, coordsReeles=True, centrage=nml)
                                                       
            bary += point1

        if numerote:
            # affiche le numero de la zone convexe
            bary /= numPerims
            self.afficheParatexte(str(perim.index), tuple(bary), couleur=couleur, coordsReeles=True)


    def afficheTriangles(self, Tris, couleurArretes=(200, 100, 100), couleurInterieur=None):
        """ couleurInterieur : colore l'interieur des triangles, sinon seulement les arretes. """
                
        for tri in Tris:
            # Surligne org and org->dest pour les triangles selectionnes
            intpointlist = self._coords_triangle(tri)
            
            if couleurInterieur:
                # colore l'interieur
                pygame.draw.polygon(self._Surf, couleurInterieur, intpointlist)
            
            if couleurArretes:
                # affiche les trois cotes
                pygame.draw.polygon(self._Surf, couleurArretes, intpointlist, 1)
            
            if couleurInterieur and hasattr(tri, 'org'):
                self.dessineCercle(tri.org.coords, min(10, int(tri.org.radius + 4)) / self.VueRatio, (255, 0, 100), trait=1)
                self.dessineLigne(tri.org.coords, tri.dest.coords, couleur=(255, 0, 100), trait=3)
            
            # Numerotation des triangles
            if self.AfficheNumeros: 
                self.afficheParatexte(str(tri.index), tri.barycentre, coordsReeles=True, couleur=couleurArretes or (200,100,100))
    
    def PeintUneToile(self, deltaT):
        
        xm, ym = self.CoordsReeles(self.DimEcran) # TODO verifier que les coords reeles des points a dessiner sont dans le domaine visible en coords reeles 

        if self.AfficheDual or self.AfficheConnexe:
            graphe = self.graphe.searchDual
        else:
            graphe = self.graphe
        
        # Affichage des Triangles
        if self.AfficheTriangles:
            
            self.afficheTriangles(self.SelectionTris,couleurArretes=None,couleurInterieur=(50,50,50))
            
            PerimVu = self.VueOrg, self.CoordsReeles(self.DimEcran)
            
            #TrianglesVus = graphe.Triangles
            TrianglesVus = self.SelectionneDansContour(PerimVu, triangles=True, ContourSeul=False, affiche=True)
            if not TrianglesVus:
                self.afficheTriangles(graphe.Triangles)
            
            if self.AfficheRecentTri:
                self.afficheTriangles([graphe.RecentTri],couleurArretes=None,couleurInterieur=(210, 240, 240))
                
        # Affichage des segments
        for seg in graphe.Segments:
            
            coords = [seg.getVert(i).coords for i in (0, 1)]
            self.dessineLigne(coords[0], coords[1], couleur=seg.couleur())
            
            # Numerotation des segments
            if self.AfficheNumeros:
                
                IsoBary = (coords[0] + coords[1]) / 2.
                self.afficheParatexte(str(seg.index), [int(IsoBary.x), int(IsoBary.y)], coordsReeles=True, couleur=seg.couleur())
                
                # univec = -seg.univec.nml * 30
                # self.dessineLigne(IsoBarycentre, IsoBarycentre+univec, couleur=couleurSeg)


        # Affichage des sommets
        affichables = self.graphe.AllVertices() if self.Afficher else self.Selection

        for vert in affichables:
            vert.Afficher()

        # Numerotation des sommets
        if self.AfficheNumeros or self.ModeInfra: 
            for Vert in graphe.Vertices:
                self.afficheParatexte(str(Vert), Vert.coords, coordsReeles=True, couleur=CouleurNumerosVert)
            
        if self.AfficheConnexe and self.graphe.searchDual and not self.AfficheTriangles:
            
            if self.SelectionPerims:
                convexAreas = self.SelectionPerims
                
            elif self.SelectionTris:
                
                try:
                    convexAreas = set( self.graphe.searchDual.TriToConvexArea[trian.index] for trian in self.SelectionTris )
                except:
                    traceback.print_exc()
                    convexAreas = self.graphe.searchDual.ConvexAreas
                    
            else:
                convexAreas = self.graphe.searchDual.ConvexAreas
                        
            convexAreasADessiner = convexAreas
            if len(self.SelectionTris) == 1:
                # ConvexAreasADessiner = self.graphe.searchDual.getConnexArea(ConvexAreas[0] )
                if False:
                    print('ConnexArea Index')
                    for index in self.graphe.searchDual.ConnexAreaIndex:
                        print(index)
                    print()
                    print(self.SelectionTris)
                    for convArea in convexAreas:
                        print(convArea.index)

            numeroteports = len(convexAreas) == 1
                
            for Index, perim in enumerate(convexAreasADessiner):
                if perim in convexAreas:
                    couleur = (250, 250, 0)                    
                else:
                    couleur = (100, 50, 250)
                
                self.DessineConvexArea(perim, numerote=True, couleur=couleur, ports=numeroteports)
                
                if numeroteports:
                    # dessine le cote que coupe la ligne joignant le barycentre au curseur souris
                    bary = perim.barycentre
                    porteFranchie = perim.findIntersect(bary, self.CoordsReeles(souris.get_pos()) )
                    #porteFranchie = perim.findClosestSide(self.CoordsReeles(souris.get_pos()) )
                    self.dessineLigne(porteFranchie.coords, porteFranchie.next.coords)
                    #self.dessineCercle(bary,3,trait=0,couleur=couleur)
                                             
        # Affichage des segments selectionnes
        for seg in self.SelectionSegs:
            if hasattr(seg, 'org'):
                p1, p2 = seg.org.coords, seg.dest.coords
                self.dessineCercle(p1, 5, trait=1)
            else:
                p1, p2 = [seg.getVert(i).coords for i in (0, 1)]
                
            self.dessineLigne(p1, p2, trait=2)

        
        # Affichage des sommets selectionnees
        if not self.AfficheMouvement:
            self.AfficheVertSelection(self.Selection)    
        
        Alpha = self.AfficheAlpha
        if self.AfficheMouvement > 1:
            Alpha = 150
            
        self.AfficheOmbre(Alpha)
                                
        posSouris = self.CoordsReeles(Vec(*souris.get_pos()))
        
        if self.AfficheDeplacement and len(self.Selection) == 1:
            # Affiche trajectoire jusqu'a la position de la souris
            Select = next(iter(self.Selection))
            
            if Select.rayon > 0:
                                
                try:
                    dist_obst, Voisins = Select.PremierObstacle(posSouris, checkResult=self.valide_Mouvement_)
                except TryangleException:
                    
                    self.valide_Mouvement_ = False
                    self.AfficheMouvement = True
                    Voisins = []
                
                if dist_obst is None:
                    dist_obst = posSouris - Select.pos 
                    
                # Affiche la plus grande avance sans collision
                self.dessineCercle(dist_obst + Select.pos, Select.rayon, self.couleur_meta)
                
                for Voisin in Voisins:
                    if isinstance(Voisin, osub):
                        self.dessineLigne(Voisin.org.coords, Voisin.dest.coords, couleur=self.couleur_meta, trait=4)
                    else:
                        self.dessineCercle(Voisin.pos, Voisin.rayon / 3)

                    self.dessineCercle(posSouris, 3, self.couleur_meta)
                
                # Affiche l'empreinte du deplacement
                
                pos = Select.pos
                Mouvement = posSouris - pos
                
                if Mouvement:
                    
                    rayon = Select.rayon
                        
                    MouvRayon = Mouvement.uni * rayon
                                        
                    pt1 = pos + MouvRayon.nml
                    pt2 = pt1 + Mouvement
                    pt4 = pt1 - MouvRayon.nml
                    pt3 = pt4 + Mouvement
                    pts = [ self.CoordsVues(pt) for pt in (pt1, pt2, pt3, pt4) ]

                    pygame.draw.aalines(self._Surf, self.couleur_meta, False, pts, 1)
                    
                    self.dessineLigne(pos, posSouris, couleur=self.couleur_meta)

        if self.AfficheCercleCirc:
            # Affiche Cercle Circonscrit
            for tri in self.SelectionTris:            
                xC, yC, radius = tri.CircumCircle()
                self.dessineCercle((xC, yC), radius, (255, 0, 100))

        if self.ModeInfra:

            for Index, Otri in enumerate(self.SelectionTris):
                # affichage en para texte de la selection de triangles
                self.afficheParatexte(str(Otri), (10, -40 - Index * 15))

            if self.SurvolSelection:
                # Affichage des Sommets et Segments candidats a la selection sous la position du curseur
                pos_souris = souris.get_pos()
                _FoundTri, FoundVert, FoundSeg = self.SelectAPos(pos_souris, SommetSeul=False)
                    
                if FoundVert:
                    self.dessineCercle(FoundVert.pos, 20 / self.VueRatio, couleur=(110, 255, 0), trait=1, coordsReeles=True)
    
                if FoundSeg:
                    self.dessineLigne(FoundSeg.org.coords, FoundSeg.dest.coords, trait=2)
    
                SelectionBornes = self.SelectionBornes()
                if len(SelectionBornes) == 1:
                    for Borne in SelectionBornes:
                        self.dessineLigne(self.CoordsVues(Borne.pos), pos_souris, trait=2, coordsReeles=False)
        
        else:
        
            if self.etatSouris.jalons and clic.droite_enfonce():
                
                if not self.Selection:
                    pointsDepart = [self.etatSouris.jalons[0]]
                else:
                    pointsDepart = self.Selection

                point2 = self.CoordsReeles(souris.get_pos())
                mods = pygame.key.get_mods()
 
                for pointDepart in pointsDepart:
                    
                    if hasattr(pointDepart, 'pos'):
                        point1 = pointDepart.pos
                    else:
                        point1 = pointDepart
                        
                    if mods & pygame.KMOD_SHIFT and False:
                        # Recherche du premier segment obstacle en ligne droite

                        sseg = self.graphe.firstSegObstacle(point1, point2)
                        if sseg:
                            self.dessineLigne(sseg.getVert(0).coords, sseg.getVert(1).coords, couleur=(0, 0, 255), trait=8, lisse=False, coordsReeles=True)
                        
                        self.dessineLigne(point1, point2, couleur=(0, 0, 255), coordsReeles=True)
        
                    elif not self.ModeGravite:
                        # Recherche de chemin.
                        
                        try:
                            
                            Algo = PathFindAlgo.MinDistToEnd if mods & pygame.KMOD_ALT else PathFindAlgo.MinDist
                            if hasattr(pointDepart, 'Chemin'):
                                chemin = pointDepart.Chemin(point2)
                            else:
                                chemin = self.graphe.Chemin(point1, point2, algo=Algo)
                            
                            # print graphe.Segments
                            
                            if len(chemin) > 1:
                                points = [point1] + [point for (point, _signe) in chemin]
                                self.dessineLigne(*points, couleur=(0, 150, 255), trait=2, coordsReeles=True)
                                
                        except:
                            traceback.print_exc()

        if self.ModeGravite:
            pos = self.DimEcran / 2
            self.dessineCercle(pos, 5, self.couleur_meta, trait=0, coordsReeles=False)
            vecGrav = Vec(self.VecGravite[0], -self.VecGravite[1])/5
            self.dessineLigne(pos, pos + vecGrav, couleur=self.couleur_meta, trait=10, coordsReeles=False)

        if self.Affiche:
            # Affiche d'informations suplementaire par unite.
            for etant in self.graphe.AllVertices():#self.Selection:
                etant.AfficherInfo(deltaT, DansSelection=etant in self.Selection)

    def SelectionBornes(self):
        return [ Vert for Vert in self.Selection if Vert.rayon <= 0 ]

    def AfficheVertSelection(self, Verts, trait=1):
        for Vert in Verts:
            self.dessineCercle(Vert.pos, max(trait, Vert.rayon / 2), trait=trait)

    def TraiteTriangleException(self, try_exc, selectionne=False):
        
        exc = try_exc.exc
        
        Verts = [_f for _f in (self.graphe.VertexToWertex.get(vert) for vert in exc.vertices) if _f]
        OTris = exc.triangles
        OSubs = exc.segments
        Perims = exc.perims
        
        if selectionne or self._validation & Validation.TriangleExc:
            
            self.Courir = False

            self.Selection = set(Verts)
            self.SelectionTris = set(OTris)
            
            if self.SelectionTris:
                self.AfficheTriangles = True
            # else:
            #    self.AfficheMouvement = False
                
            self.SelectionSegs = set(OSubs)

        print('[Tour %d]' % self.NumeroDuTour, type(try_exc).__name__, exc.location())
        print(exc.message)
        for Arg in Verts, OTris, OSubs, Perims:
            if Arg:
                print('\t', Arg)
        print()
        print()

        if exc.message.startswith('Overlapping Vertices') and False:

            self.AfficheMouvement = True
            self.Selection = set([ self.graphe.VertexToWertex[exc.vertices[0]] ])

            # newPos = [ float(arg) for arg in exc.message.split()[-1].split(',') ]
            #   print 'move to pos', newPos
            #    souris.set_pos(self.CoordsVues(newPos))
    
            self.Courir = False

        elif exc.message.startswith('Non-Delaunay'):
            
            self.AfficheCercleCirc = True
        
    def Recadre(self):
        """ Reinit la vue """
        Selections = set()
        Selections.update(self.Selection)
        if self.AfficheTriangles or self.AfficheConnexe:
            if self.AfficheConnexe and self.SelectionPerims:
                Selections.update(self.SelectionPerims)
            else:
                Selections.update(self.SelectionTris)

        Selections.update(self.SelectionSegs)

        if Selections:
            self.Cadrer(list(Selections))
        else:
            app.Application.Recadre(self)

    def Reinit(self):
        """ Reinit """
        app.Application.Reinit(self)
        
        self.AfficheMouvement = False
        self.SelectionGroupe = None
        self.SelectionGroupes = {}
                 
    def CheckMesh(self, CheckDelaunay=True, selectionne=False, verbose=False):

        try:
            
            self.graphe.CheckMesh()
            if CheckDelaunay:
                self.graphe.CheckDelaunay()
            self.AfficheCercleCirc = False
            
        except TryangleException as exc:
                        
            self.TraiteTriangleException(exc, selectionne=selectionne)
            
            self.Courir = False
            
            return False
        
        except:
            traceback.print_exc()
            
        else:
            if verbose:
                print('graphe valide')
            return True

        # Il y eut une exception
        self.Courir = False
        
        return False

    def Validation(self,**kwargs):
        try:
            if self.valide_CheckMesh_:
                kwargs.setdefault('selectionne',True)
                return self.CheckMesh(CheckDelaunay=False,**kwargs)
            else:
                return self.CheckMesh(**kwargs)
            
        except TryangleException as exc:
            
            self.TraiteTriangleException( exc )
            return True
        
    def VidangeSelect(self, Verts=True, Tris=True, Segs=True, Perims=True):
        """ Vide toutes les selections """
        if Verts:
            self.Selection.clear()
        if Tris:
            self.SelectionTris.clear()
        if Segs:
            self.SelectionSegs.clear()
        if Perims:
            self.SelectionPerims.clear()

    def MaJSelection(self, Elems, ajout=False):
        """ distribue les elements de Elems dans les bons ensembles de selection """
        
        if not ajout:
            self.VidangeSelect()
            
        for Elem in Elems:
            if isinstance(Elem, sommet.Wertex):
                self.Selection.add(Elem)
            elif isinstance(Elem, (osub, subseg)):
                self.SelectionSegs.add(Elem)
            elif isinstance(Elem, (otri, triangle)):
                self.SelectionTris.add(Elem)
            elif isinstance(Elem, Perimeter):
                self.SelectionPerims.add(Elem)
     
    def SelectAPos(self, posVue, SommetSeul=False, tolVert=15, tolSeg=15, **kwargs):
        ''' Selectionne un sommet a ces coordonnees '''
        graphe = self.graphe.searchDual if self.AfficheConnexe else self.graphe  
        FoundTri, FoundVert, FoundSeg = graphe.SelectAtPos(self.CoordsReeles(posVue),
                                                                 tolVert= tolVert / self.VueRatio,
                                                                 tolSeg = tolSeg / self.VueRatio,
                                                                 **kwargs)
        
        if SommetSeul:
            return FoundVert 
        else:
            return FoundTri, FoundVert, FoundSeg
    
    def SelecteElemAPos(self, posVue):
        LocTri, LocSom, LocSeg= self.SelectAPos(posVue)
        LocElem = LocSom or LocSeg or ( self.AfficheTriangles and LocTri )
        return LocElem
        
    def SelectionneGroupe(self, Groupe):
        self.SelectionGroupe = Groupe
        self.Selection = self.SelectionGroupe.Unites.copy()
                        
        print('formation %s selectionnee' % Groupe)
            
    def DeplaceSelection(self, dpos, verifCollisions=None):

        # Le deplacement peut causer une retriangulation.
        # Ne gardons pas de pointeurs triangles au cas ou.
        self.SelectionTris.clear()
        
        if verifCollisions is None:
            verifCollisions = len(self.Selection) == 1
            
        
        for Vert in self.Selection:
            # print 'Moving vertex', Vert, 'to', Vert.coords + dpos
            try:
                
                Vert.move(dpos, True, checkCollisions=verifCollisions, enforceDelaunay=self.ActiveDelaunay)
                
            except TryangleException as exc:
                
                self.TraiteTriangleException(exc)
                                
            # Save(self.graphe, nom='autosave')

    def EffaceSelections(self):
        ''' Efface les sommets, triangles et segments de la selection'''
        
        for Seg in self.SelectionSegs:
            Seg.Delete(False)

        self.SelectionSegs.clear()

        # Some triangles might become invalid with the deletion of sommets
        self.SelectionTris.clear()
        
        for Wert in self.Selection:                
            Wert.Delete()

        self.Selection.clear()
        
        self.graphe.searchDual.resetConvexAreas()
        self.CheckMesh(False)
        
    def EffacerSegment(self, OSeg, ResetDual=True):
        
        for autr in list( self.SelectionSegs ):
            if OSeg.ss == autr.ss:
                self.SelectionSegs.remove(autr)
                        
        OSeg.Delete(ResetDual)

    def EffacerSommet(self, Vert, ResetDual=True):

        #print 'Effacage du sommmet', Vert
        
        if isinstance( Vert, sommet.Wertex ):
            Wert = Vert
            Vert = Wert._vertex
            
        else:
            Wert = self.graphe.VertexToWertex.get(Vert)
            
            if not Wert:
                assert Vert.index == -1, "sommet devrait etre deja efface"
                return
        
        # Au cas ou le sommet faisait partie d'un triangle/Segment selectionne
        self.SelectionTris.clear()
        
        for oseg in Vert.segments():
            self.EffacerSegment( oseg, ResetDual )
        
        self.Selection.discard(Wert)
        Wert.Efface()
            
        #self.CheckMesh()
        
    def SelectionForEdit(self):
        if self.Selection:
            elems = self.Selection
        elif self.SelectionSegs:
            elems = self.SelectionSegs
        else:
            elems = [self] 
        
        return elems
    
    def SelectionneDansContour(self, contour, triangles=False, 
                                Boucle=True, hullstrict=True, 
                                ContourSeul=True, affiche=False, majSelection=False):
        ''' Selectionne des sommets ou des triangles a l'interieur d'un contour
        
            contour : [ x0,y0, x1,y1, x2,y2, ... ]
        '''
        if triangles is None:
            mods = pygame.key.get_mods()
            triangles = (mods & pygame.KMOD_ALT) and (mods & pygame.KMOD_SHIFT)

        NumPoints = len(contour)
             
        if NumPoints == 2:
            # Contour de selection est une boite
    
            # Ordonnons les sommets de la boite pour avoir une bonne orientation
            # pour pouvoir definir un interieur / exterieur du contour
            (x0, y0), (x1, y1) = self.BoiteJalons(contour[0], contour[-1])

            Coords = [Vec(x0,y0), Vec(x1,y0), Vec(x1,y1), Vec(x0,y1)]
            
        else:
            # Contour de selection arbitraire
            Coords = contour
        
        if not isinstance( Coords, PointList ):
            _coordsList = PointList()
            for pt in Coords:
                _coordsList.append(pt)
            Coords = _coordsList

        graphe = self.graphe.searchDual if self.AfficheConnexe else self.graphe  

        if triangles:
            # Selection des triangles intersectes
            # print 'IntersectTris on', NumPoints, 'points'
            if ContourSeul:
                Selection = set(graphe.IntersectTris(Coords, loop=Boucle, hullstrict=hullstrict))
            else:
                Selection = graphe.TrianglesInPolygon(Coords)
            
            if affiche:
                self.afficheTriangles(Selection)
                
            if majSelection:
                self.SelectionTris.update(Selection)

            return Selection
        
        else:
            # print 'Selection dans le Polygone ', NumPoints, 'points'
            Elems = set(self.graphe.SelectVertices(Coords, includeBornes=self.ModeInfra))
    
            # Soin special de selectionner ce qui se trouve sous le Premier et dernier clic
            for PosSpecial in Coords[0], Coords[-1]:
                
                _FoundTri, FoundVert, _FoundSeg = self.graphe.SelectAtPos(PosSpecial)
                
                if FoundVert:
                    Elems.add(FoundVert)
            
            mods = pygame.key.get_mods()

            if not mods & pygame.KMOD_ALT and any( getattr(Vert,'vie',0) > 0 for Vert in Elems ):
                # Si un des elements est en vie, ne pas selectionner les morts.
                Depouilles = []
                for Vert in Elems:
                    if getattr(Vert,'vie',0) <= 0:
                        Depouilles.append(Vert)
                Elems.difference_update(Depouilles)
                
            if affiche:
                self.AfficheVertSelection(Elems)
        
            if majSelection:

                if mods & pygame.KMOD_CTRL and self.Selection.issuperset(Elems):
                    # Si tous les nouveaux elements sont deja dans la selection,
                    # les en exclure
                    self.Selection.difference_update(Elems)
                else:
                    self.Selection.update(Elems)
                    
            return Elems


    def NavigationTriangles(self, event):
        ''' Navigation d'un triangle a l'autre avec les primitives de circulation '''
        key = event.key
        if key == pygame.K_f:
            # Changment de la triangulation par le basculement d'une arrete
            Tri = next(iter(self.SelectionTris))
            print('flipping', Tri, '->', Tri.flip())
            self.CheckMesh()
            return True   
        
        elif key in [pygame.K_n, pygame.K_p]:
            
            if key == pygame.K_p:
                prevNext = 'prev'
            else:
                prevNext = 'next'

            method = 'oseg' + prevNext
                    
            Otri = list(self.SelectionTris)[0]
            NewSeg = getattr(Otri, method)
            
            if not NewSeg.isDummy():
                self.SelectionSegs.clear()
                self.SelectionSegs.add(NewSeg)
                
            return True
                
        else:
            method = None
            
            if key in [pygame.K_o, pygame.K_d, pygame.K_l, pygame.K_r]:
            
                if event.mod & pygame.KMOD_LSHIFT:
                    prevNext = 'prev'
                else:
                    prevNext = 'next'

                method = event.str.lower() + prevNext
                
            else:
                
                if key == pygame.K_s:
                    method = 'sym'

                if key == pygame.K_t:
                    if event.mod & pygame.KMOD_LSHIFT:
                        method = 'right_tri'
                    else:
                        method = 'left_tri'

            if method:
                
                for OldTri in list(self.SelectionTris):
                    
                    NewTri = getattr(OldTri, method)
                    
                    if not NewTri.isDummy():
                        self.SelectionTris.remove(OldTri)
                        self.SelectionTris.add(NewTri)
                    else:
                        print('selected dummytri')
                        
                return True
            
    def NavigationSegments(self, event):
        ''' Navigation d'un segment a l'autre par les primitives de circulation '''
        
        method = None
        key = event.key
        
        if key == pygame.K_s:
            method = 'sym'
        else:
            if key == pygame.K_n:
                method = 'segnext'
            elif key == pygame.K_p:
                method = 'segprev'
        
            if method and event.mod & pygame.KMOD_LSHIFT:
                method = 'd' + method
            
        if method:
            
            for OldOsub in list(self.SelectionSegs):
                
                NewOsub = getattr(OldOsub, method)
                
                if not NewOsub.isDummy():
                    self.SelectionSegs.remove(OldOsub)
                    self.SelectionSegs.add(NewOsub)
                else:
                    print('selected dummyseg')
                    
            return True
            

    def SelectionNouvPos(self, pos, Colle=False):
        ''' Affiche (ou Copie/Colle) la selection a l'emplacement pos'''

        NovElems = app.Application.SelectionNouvPos(self, pos, Colle)
                
        if NovElems:
            # if any of the new items have been copied
            # from vertex associated so segments, copy the segments too.            
            for Seg in self.SelectionSegs:
                NovOrg = NovElems.get(Seg.org)
                NovDest = NovElems.get(Seg.dest)
                if NovOrg and NovDest:
                    NovOrg.connect(NovDest)
                        
    def DistBarySelection(self, pos):
        ''' Distance de pos au barycentre de la selection '''
        bary = barycentre(elem.pos for elem in self.Selection)
                    
        return pos - bary
        
    def OuvrirReseau(self, nomReseau):
        
        NouvRes = reseau.Ouvrir(nomReseau)
                    
        if NouvRes:
    
            self.graphe = NouvRes
            self.Reinit()
        
    def Gomme(self):
        
        x, y = self.CoordsReeles(souris.get_pos())
        t = self.TailleGomme / self.VueRatio
        Contour = Vec(x - t, y - t), Vec(x + t, y - t), Vec(x + t, y + t), Vec(x - t, y + t)
        
        Tris = [Otri.tri for Otri in self.SelectionneDansContour(Contour, triangles=True, hullstrict=False)]
        Verts = set()
        OSegs = set()
        Segs  = set()
                    
        for tri in Tris:
            
            for i in 0, 1, 2:
                vert = tri.getVert(i)
                
                if vert.coords - (x,y) < t:
                    if vert not in Verts:
                        Verts.add(vert)
                        
                oseg = tri.getSeg(i)
                if not oseg.isDummy() and oseg.ss not in Segs:
                    
                    if oseg.sym.stpivot.tri in Tris:
                        Segs.add(oseg.ss)
                        OSegs.add(oseg)
        
        for oseg in OSegs:
            
            if not self.AfficheTriangles:
                # Ote les points a l'extremites d'un segment s'ils ne sont pas lies a d'autres segments.
                for segend in oseg.org, oseg.dest:
                    if segend not in Verts and len( segend.segments() ) == 1:
                        Verts.add( segend )
            
            self.EffacerSegment(oseg, ResetDual=False)
            
        for Vert in Verts:
            self.EffacerSommet(Vert, ResetDual=False)

        #self.CheckMesh()
        
    
    Gommer = app.Application.Gommer
    @Gommer.setter
    def Gommer(self, val):
        if not val:
            self.graphe.searchDual.resetConvexAreas()

        return app.Application.Gommer(self, val)
        
    def TraiteEvenementAnte(self, event, deltaT):
        '''
        '''
        app.Application.TraiteEvenementAnte(self, event, deltaT)
        
        if event.type in (pygame.MOUSEBUTTONDOWN, pygame.MOUSEBUTTONUP) and \
            event.button in (clic.mol_haut, clic.mol_bas) and \
            not (clic.droite_enfonce() or clic.gauche_enfonce()):
            # Evenement de la molette souris
            
            if event.type == pygame.MOUSEBUTTONDOWN:
                
                mods = pygame.key.get_mods()
                if mods & pygame.KMOD_CTRL & pygame.KMOD_SHIFT:
    
                    # Retaille le rayon des sommets de la selection
                    increment = 1.1 if event.button == 4 else 1 / 1.1
                    
                    for Vert in self.Selection:
                        
                        Vert.rayon *= increment
                        if hasattr(Vert, 'masse_'):
                            Vert.masse_ *= increment * increment
                            
                elif not mods & pygame.KMOD_CTRL:
                    pass
                                        
        elif event.type == pygame.MOUSEBUTTONDOWN:
            # Clic souris (d'un bouton qui n'est pas la molette)
            
            if not (event.button == clic.gauche and clic.droite_enfonce()):
                # si le bouton droit n'est pas enfonce.
                
                self.ClicSourisPrec[event.button - 1] = event.pos
                
                if event.button == clic.milieu:  # Clic milieu
                    Elems = self.Selection if self.Selection else [self]
                    self.EditElems(Elems)
                    
                elif event.button == clic.gauche:
                    
                    mods = pygame.key.get_mods()
                    
                    if mods & pygame.KMOD_SHIFT and mods & pygame.KMOD_CTRL:
                        
                        # Copie/Colle la selection
                        self.SelectionNouvPos(pos=event.pos, Colle=True)
                            
                    else:
                        if not self.AfficheConnexe:
                            if not mods & pygame.KMOD_CTRL:
                                self.VidangeSelect()
                
                if event.button == clic.droit and self.Gommer:
                    
                    self.Gommer = False                  
    
                elif event.button in (clic.gauche, clic.droit):
                                        
                    BoiteSelection = True
                    
                    if self.ModeInfra:
                        
                        mods = pygame.key.get_mods()
                                             
                        _FoundTri, FoundVert, FoundSeg = self.SelectAPos(event.pos)
                    
                        if event.button == clic.droit:
                                                            
                            if not ( mods & pygame.KMOD_SHIFT ):
                                
                                Bornes = self.SelectionBornes()
                                    
                                if FoundVert:
                                                                
                                    if (not Bornes or len(Bornes) == 1 and Bornes[0] == FoundVert) and not FoundSeg:
                                        self.EffacerSommet(FoundVert)
                                        FoundVert = None
                                    
                                elif FoundSeg and not Bornes:
                                    self.EffacerSegment(FoundSeg)
                                     
                                else:
                                    # Insertion d'un sommet borne
                                    FoundVert = SommetBorne(self.CoordsReeles(event.pos), self.graphe)
                                    
                                if FoundVert and Bornes:
                                    # Construction de segments en connectant le sommet trouve 
                                    # aux sommets selectionnes
                                    for Wert in Bornes:
                                        if FoundVert != Wert:
                                            FoundVert.Connecte(Wert)
        
                                self.graphe.searchDual.resetConvexAreas()
                                                                                                
                        if FoundVert:    
                            SelecteAjoutRetrait(FoundVert, self.Selection, Empile=mods & pygame.KMOD_CTRL)
    
                        if FoundVert or FoundSeg:
                            BoiteSelection = False
                    
                    if BoiteSelection:
                        # Clic gauche ou droit
                        # Premier jalon d'une selection    
                        self.AddJalonSouris(event.pos, premier=True)
                
                
        elif event.type == pygame.MOUSEMOTION:
            
            mods = pygame.key.get_mods()
            
            if self.Selection and self.ModeInfra and \
                mods & pygame.KMOD_SHIFT and not mods & pygame.KMOD_CTRL and not mods & pygame.KMOD_ALT \
                and not any(event.buttons):
                pass
                            
            elif event.buttons[0]:  # bouton gauche enfonce
                pass
            elif not self.Selection and self.ModeGravite and event.buttons[2]:  # clic droit enfonce
                
                self.VecGravite = (-self.DimEcran / 2 + event.pos) # passage en referentiel direct
                self.VecGravite[0] *= 5
                self.VecGravite[1] *= -5

        elif event.type == pygame.KEYUP:
            
            touche = event.key
            if touche == pygame.K_s:
                if self.etatSouris.jalons:
                    # Selection dans le contour du premier jalon et du curseur
                    posSouris = self.CoordsReeles(souris.get_pos())
                    self.SelectionneDansContour(contour=[self.etatSouris.jalons[0], posSouris],
                                               triangles=None,
                                               majSelection=True)
                    self.AddJalonSouris(None) # vide les jalons

            if event.key == pygame.K_z:
                # Donne une impulsion de vitesse a la selection
                
                mods = pygame.key.get_mods()
                                        
                Radial = not mods & pygame.KMOD_CTRL
                Inverse = mods & pygame.KMOD_SHIFT
                
                self.ChocVitesse(Radial, Effectif=True, Inverse=Inverse)
            
        elif event.type == pygame.KEYDOWN:
            
            touche = event.key
            
            if self.ModeInfra:
                
                if self.SelectionTris and self.AfficheTriangles:                
                    # Navigation de triangle en triangle
                    if self.NavigationTriangles(event):
                        return
                
                if self.SelectionSegs:
                    # Navigation de segment en segment
                    if self.NavigationSegments(event):
                        return
    
            if touche == pygame.K_o:
                
                dialogue = event.mod & pygame.KMOD_SHIFT
                
                if event.mod & pygame.KMOD_ALT and False:
                    # Ouverture de graphe
                    self.OuvrirReseau(nomReseau='' if dialogue else self.graphe.nom)
                else:
                    pass                                    
          
            elif touche == pygame.K_TAB:
                if event.mod & pygame.KMOD_CTRL:
                    pass
                else:
                    # Affiche des informations sur l'element sous le curseur. 
                    self.SurvolSelection = not self.SurvolSelection
                
            elif touche == pygame.K_k:
                
                if self.AfficheConnexe and event.mod & pygame.KMOD_CTRL:
                    print('Reset Connex Areas')
                    self.graphe.searchDual.resetConvexAreas()
                else:
                    self.AfficheConnexe = not self.AfficheConnexe
                    self.SelectionTris.clear()
                    
            elif touche == pygame.K_g:
                
                if event.mod & pygame.KMOD_CTRL:
                    self.ModeGravite = not self.ModeGravite
                    print('ModeGravite', self.ModeGravite, self.VecGravite)

            elif touche == pygame.K_i:
                if event.mod & pygame.KMOD_CTRL:
                    pass
                else:
                    if not self.ModeInfra:
                        for Borne in self.SelectionBornes():
                            self.Selection.remove(Borne)

            elif touche == pygame.K_m:
                incr = -1 if event.mod & pygame.KMOD_SHIFT else 1
                self.AfficheMouvement = (self.AfficheMouvement + incr) % 4
                print('AfficheMouvement', self.AfficheMouvement)
                
            elif touche == pygame.K_j:
                self.AfficheDeplacement = not self.AfficheDeplacement
                print('AfficheDeplacement', self.AfficheDeplacement)
                        
            elif touche == pygame.K_s:
                
                if event.mod & pygame.KMOD_CTRL:                  
                    if event.mod & pygame.KMOD_ALT:
                        # Sauvegarde de graphe                      
                        self.graphe.Sauvegarde(dialogue=event.mod & pygame.KMOD_LSHIFT, nom=self.graphe.nom)
                elif event.mod & pygame.KMOD_LSHIFT:
                    pass
                elif event.mod & pygame.KMOD_ALT:
                    pass
                else:
                    # Selectionne tous les sommets
                    self.Selection.clear()
                    self.Selection.update(self.graphe.AllVertices(IncludeBounds=self.ModeInfra))
                    
                    print('Selection de %d' % len(self.Selection))
                    
            elif touche == pygame.K_t:
                self.AfficheTriangles = not self.AfficheTriangles
                if self.AfficheTriangles:
                    print(len(self.graphe.Triangles), 'Triangles')
                    
            elif touche == pygame.K_d:
                if event.mod & pygame.KMOD_CTRL:
                    self.ActiveDelaunay = not self.ActiveDelaunay
                    print('ActiveDelaunay', self.ActiveDelaunay)
                else:
                    print('Re-triangule')
                    self.SelectionTris.clear()
                    self.graphe.DoDelaunay()
                                        
            elif touche == pygame.K_c:
                if event.mod & pygame.KMOD_CTRL and event.mod & pygame.KMOD_ALT:
                    # Affichage des cercles circonscrits
                    self.AfficheCercleCirc = not self.AfficheCercleCirc
                        
            elif touche in TOUCHES_NUM:
                
                intKey = int(pygame.key.name(touche)[-1])
                
                if event.mod & pygame.KMOD_CTRL:
    
                    if self.Selection:
                        # fait de la selection un groupe
                        # Attribue une numero a ce groupe
                    
                        if intKey in self.SelectionGroupes:
                            # Disperse le groupe existant sous ce numero
                            self.SelectionGroupes[intKey].Disperse()
                            self.SelectionGroupe = None
                        
                        self.SelectionGroupe = self._ClasseGroupe_(nom='Groupe %d' % intKey)
                        self.SelectionGroupes[intKey] = self.SelectionGroupe
                        self.SelectionGroupe.Enrole(self.Selection)
                        print('creation de Formation %s, %d personnels' % (self.SelectionGroupe, len(self.SelectionGroupe.Unites)))    
                        
                else:
                    # Selection d'un groupe par son numero
                    if intKey in self.SelectionGroupes:
                        
                        groupe = self.SelectionGroupes[intKey]
                        self.SelectionneGroupe(groupe)
                        
                    else:
                        self.SelectionGroupe = None

    def TraiteEvenementMid(self, event):
    
        app.Application.TraiteEvenementMid(self, event)
        
        if event.type == pygame.KEYDOWN:
            
            touche = event.key
            from pygapp.partieutils import TOUCHES_NUM

            if touche == pygame.K_DELETE:
                for item in list(self.Selection):
                    item.Eliminer()


    def TraiteEvenementPost(self, event):
        ''' Traitement des commandes du joueur
        '''
        app.Application.TraiteEvenementPost(self, event)
        
        if event.type == pygame.MOUSEBUTTONUP:
            
            if not (event.button == clic.gauche and clic.droite_enfonce()):
                # a condition que le bouton droit ne soit pas enfonce quand clic 1 remonte.
                
                mods = pygame.key.get_mods()
                
                if event.button == clic.gauche:
                    
                    position_souris = Vec(*event.pos)
    
                    if self.AfficheConnexe:
                        
                        coords_souris = self.CoordsReeles(event.pos)
                        FoundTri = self.graphe.searchDual.PreciseLocate(coords_souris)
                                                
                        if not FoundTri.isDummy():
                            if FoundTri in self.SelectionTris:
                                self.SelectionTris.clear()
                            else:
                                if mods & pygame.KMOD_CTRL:
                                    self.SelectionTris.add(FoundTri)
                                else:
                                    self.SelectionTris = set([FoundTri])
                        
                    else:
                        
                        if self.ClicSourisPrec[0] == event.pos:
                            # Double-clic
                        
                            x, y = position_souris.x, position_souris.y
                            if x < 0 or y < 0 or x > self.DimEcran[0] or y > self.DimEcran[1]:
                                # ignorons les clics en dehors de la zone d'affichage
                                return
                            
                            mods = pygame.key.get_mods()
                            
                            CTRLPresse = mods & pygame.KMOD_CTRL
                            _MajPresse = mods & pygame.KMOD_SHIFT
                            AltPresse = mods & pygame.KMOD_ALT
                            
                            Inserez = False
        
                            # Recherche d'une unite a proximite du clic
                            
                            FoundTri, FoundVert, FoundSeg = self.SelectAPos(position_souris)
        
                            if self.ModeInfra:
                                if FoundSeg:
                                    # Segment trouve
                                    SelecteAjoutRetrait(FoundSeg, self.SelectionSegs, Empile=CTRLPresse)
                            
                            if FoundVert:
                                SelecteAjoutRetrait(FoundVert, self.Selection, Empile=CTRLPresse)
                                
                            else:
                                # Pas de sommet trouve sous le clic souris.
                                
                                if AltPresse:
                                    Inserez = True
        
                                # Selection de triangle
                                if FoundTri:
                                
                                    SelecteAjoutRetrait(FoundTri, self.SelectionTris, Empile=CTRLPresse)
    
                                    if not self.AfficheTriangles:
                                        # Si tous les sommets appartiennent au meme groupe, selection d'icelui
                                        somms = FoundTri.org, FoundTri.dest, FoundTri.apex
                                        
                                        Verts = [self.graphe.VertexToWertex[vert] for vert in somms]
                                        
                                        groupe = getattr(Verts[0], 'groupe')
                                        for Vert in Verts[1:]:
                                            if getattr(Vert, 'groupe') != groupe:
                                                groupe = None
                                                break
                                        if groupe:
                                            self.SelectionneGroupe(groupe)

                            # Insertion d'un nouveau sommet
                            if Inserez:
                                x, y = self.CoordsReeles(position_souris)
                                print('Insertion de sommet a : ', x, y)
                                taille = 10
                                
                                vert = self.ClasseUnite(pos=(x, y), Reseau=self.graphe, taille=taille)
                                
                                # Save(self.graphe,'autosave')
                                self.Selection.add(vert)
        
                                self.CheckMesh()
                                    
                        else:
                            # #
                            # # Ca n'est pas un double clic (la souris a bouge depuis le premier clic)
                            # #
                            
                            # Ajout d'un jalon
                            self.AddJalonSouris(Vec(*event.pos))
                               
                            if len(self.etatSouris.jalons) >= 2:
                                # Au moins deux points pour definir un contour de selection
                                
                                # Selection dans le contour des jalons
                                self.SelectionneDansContour(contour=self.etatSouris.jalons, 
                                                            triangles=None,
                                                            majSelection=True)
                                
                                
class Validation:

    Aucune      = 0
    CheckMesh   = 2
    TriangleExc = 4
    NonDelaunay = 8
    Mouvement   = 16

fanions.AjouteProprietes(AfficheurDeGraphe, '_validation', Validation,
                         'CheckMesh TriangleExc NonDelaunay Mouvement'.split(), prefixe='valide_')


def barycentre(coords):
    """ Barycentre d'un ensemble de points
    @param coords: iterable de points (x,y)
    """
    bary = Vec(0, 0)
    numpoints = 0
    for Coors in coords:
        bary += Coors
        numpoints += 1

    bary /= numpoints
    
    return bary

