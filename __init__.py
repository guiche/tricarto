import math
from .tryangle import Vec

def angleToVec(theta):
    """ converti un angle (radians) en un vecteur unitaire
    """
    return Vec(math.cos(theta), math.sin(theta))


def vecToAngle(vec):
    """ converti un vecteur en angle (radians)
    """
    return math.atan2(vec[1], vec[0])

