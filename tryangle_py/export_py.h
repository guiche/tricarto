#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/operators.h>

PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);


#define VECTOR_INDEXING_SUITE(module, T, Name)\
	/*class_<std::vector<T>>(module, #T "Vec")*/\
	class_<std::vector<T>>(module, Name)\
		.def(init<>())\
		.def("pop_back", &std::vector<T>::pop_back)\
		/* There are multiple versions of push_back(), etc. Select the right ones. */\
		/*.def("append", (void (std::vector<T>::*)(const T &)) &std::vector<T>::push_back)*/\
		.def("append", [](std::vector<T> &s, T const& v) { s.push_back(v); })\
		/*.def("back", (T &(std::vector<T>::*)()) &std::vector<T>::back)*/\
		.def("__getitem__", [](std::vector<T> &s, int index) { if(index >= static_cast<int>(s.size())) { throw index_error(); } if(index < 0){ if (static_cast<size_t>(-index) > s.size()) { throw index_error(); } return s[s.size()+index];} return s[index]; })\
		.def("__getitem__", [](const std::vector<T> &s, slice slice) -> std::vector<T>* {\
            size_t start, stop, step, slicelength;\
			if (!slice.compute(s.size(), &start, &stop, &step, &slicelength)){\
				throw error_already_set();}\
			auto *seq = new std::vector<T>();\
			for (size_t i = 0; i<slicelength; ++i) {\
				seq->emplace_back(s[start]); start += step;\
			}\
			return seq;\
			})\
		.def("__len__", [](const std::vector<T> &v) { return v.size(); })\
		.def("__iter__", [](std::vector<T> &v) { return make_iterator(v.begin(), v.end()); }, keep_alive<0, 1>())\
        .def("__contains__", [](const std::vector<T> &s, T const& v) { return std::find(s.begin(), s.end(), v) != s.end(); })\
		/*.def("__reversed__", [](const std::vector<T> &s) { return s.reversed(); })*/\
		.def("__str__", [](std::vector<T> const &s) { std::ostringstream os; os << "("; for(auto const& item: s){os << item << ", ";} ;os << ")"; return os.str(); })\
		.def(self == self)\
		.def(self != self)\
		.def("__setstate__", [](std::vector<T> &p, tuple t) {\
			new (&p) std::vector<T>();\
			for(auto const& val: t)\
				p.push_back(val.cast<T>());\
		})

using namespace pybind11;
