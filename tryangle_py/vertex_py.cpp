#include "export_py.h"
#include <vector>
#include "triangle.h"

void export_py_vertex(module & m)
{
	class_<wertex, wertex*> vertex(m, "Vertex");

	enum_<wertex::Type>(vertex, "VertexType")
		.value("INPUTVERTEX",	wertex::INPUTVERTEX)
		.value("SEGMENTVERTEX",	wertex::SEGMENTVERTEX)
		.value("FREEVERTEX",	wertex::FREEVERTEX)
		.value("DEADVERTEX",	wertex::DEADVERTEX)
		.value("UNDEADVERTEX",	wertex::UNDEADVERTEX)
	;

	vertex
		.def_readonly("index", &wertex::index)
		.def_readwrite("id", &wertex::id)
		
		.def_readwrite("w", &wertex::w)
				
		.def_readwrite("coords", &wertex::coords)
		.def_readwrite("vel",	&wertex::vel)
		.def_readwrite("tri",	&wertex::otri_)
		.def_property_readonly("type",	&wertex::getType)
		.def_property_readonly("marker", &wertex::getMarker)
		.def_property("radius", &wertex::getRadius, &wertex::setRadius)

		.def_property_readonly("dualVert", &wertex::getDual, return_value_policy::reference)

		.def_readwrite("mass",	&wertex::mass)
		.def_readwrite("elas",	&wertex::elas)
		.def_readwrite("frot",	&wertex::frot)
				
		.def_property("weightRatio", &wertex::weightRatio, &wertex::setWeightRatio)

		.def_property_readonly("degree", &wertex::getDegree)

		.def("setTri",	&wertex::setTri)
		.def("Delete",	&wertex::Delete)

		//.def("neighbor_tris_iter", iterator<wertex>())
		.def("neighbors",		&wertex::getNeighbors_py, arg("Degree"), arg("seeThroughAll"))
		.def("move",			&wertex::move, arg("move"), arg("relative"), arg("checkCollisions"), arg("enforceDelaunay"))
		.def("firstObstacle",	&wertex::firstObstacle, 
			arg("pos"), 
			arg("frontRadius"), 
			arg("sideRadius"), 
			arg("relative"), 
			arg("segonly"), 
			arg("detectfar"), 
			arg("checkResult"), 
			arg("sideTol"), 
			arg("frontTol"), return_value_policy::reference)
		
		.def("firstObstacleLoop",	&wertex::firstObstacleLoop, 
			arg("N"), 
			arg("pos"), 
			arg("frontRadius"), 
			arg("sideRadius"), 
			arg("relative"), 
			arg("segonly"),
			arg("detectfar"),
			arg("checkResult"), 
			arg("sideTol"), 
			arg("frontTol"),return_value_policy::reference)

		.def("triSecant",		&wertex::triSecant, arg("pos") )
		.def("checkCollide",	&wertex::checkCollide, arg("pos"), arg("fullCheck"))

		.def("Connect",		&wertex::Connect)
		.def("contactSeg",	&wertex::detectSegContact)
		.def("distToSeg",	&wertex::distToSeg)
		.def("segments",	&wertex::segments)

		.def("__hash__", [](wertex const& s) { return (size_t) &s; })
		.def(self == self)
		.def(self != self)
		.def("__str__", [](wertex const &s) { std::ostringstream os; os << s; return os.str(); })
		//.def("__repr__", )
	;
	
	
	VECTOR_INDEXING_SUITE(m, wertex*, "VertVec");
	
}
