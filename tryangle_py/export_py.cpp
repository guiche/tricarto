/* Defines the exported functions for the DLL application. */
#include "export_py.h"

#include "mesh.h"
#include "point2d.h"
#include "triexception.h"

#include <string>
#include <fstream>
#include <istream>
#include <iostream>

extern void export_py_point2d(module &);
extern void export_py_vertex(module &);

/*
std::ostream&  operator<<(std::ostream& out, const std::vector<REAL>& arg)
{
	unsigned int i;
	for( i=0; i<arg.size() ; i++)
		out << arg[i] << "\n";

    return out;
}
*/

int add(int i, int j) {
	return i + j;
}

PYBIND11_PLUGIN(tryangle_py)
{
	module m("tryangle_py", "tryangle docstring");

	m.def("add", &add);
	
	//export_exception(m);
	export_py_point2d(m);
	export_py_vertex(m);
	
	VECTOR_INDEXING_SUITE(m, REAL, "RealVec");

	class_<Passage>(m, "Passage")
		.def_property_readonly("vert", &Passage::getVert, return_value_policy::reference)
		.def_readonly("coords", &Passage::coords)
		.def_readonly("sign", &Passage::sign)
		.def(self == self)
		.def(self != self)
		;

	VECTOR_INDEXING_SUITE(m, Passage, "PassageVec");
		
	class_<Mesh, Mesh*> mesh_py(m, "Mesh", "Mesh's Docstring");
		
	class_<triangle, triangle*> triangle_py(m, "triangle");

	enum_<ETriSidePos>(triangle_py, "ETriSidePos")
		.value("opORG", ETriSidePos::opORG)
		.value("opDST", ETriSidePos::opDST)
		.value("opAPX", ETriSidePos::opAPX)
		.value("inTRI", ETriSidePos::inTRI)
		;


	triangle_py
		.def_readonly("index", &triangle::index)

		.def_property_readonly("mesh", &triangle::getMesh)

		.def("getTri", &triangle::getTri, return_value_policy::reference)
		.def("getSeg", &triangle::getSeg, return_value_policy::reference)
		.def("getVert", &triangle::getVert, return_value_policy::reference)

		.def("setTri", &triangle::setTri)
		.def("setSeg", &triangle::setSeg)
		.def("setVert", &triangle::setVert)
		.def("Delete", &triangle::Delete)

		.def("__hash__", [](triangle const& s) { return (size_t)&s; })
		.def(self == self)
		.def(self != self)
		.def("__str__", [](triangle const &s) { std::ostringstream os; os << s; return os.str(); })
		//.def("__repr__", )
		;

	class_<otri>(m, "otri")

		.def(init<>())
		.def(init<triangle*, unsigned int>(), arg("triangle"), arg("orient"), "__init__ doc")

		.def_readwrite("tri", &otri::tri, return_value_policy::reference)
		.def_readwrite("orient", &otri::orient)

		.def("__iter__", [](const otri &o) { return make_iterator(o.begin(), o.end()); },
			keep_alive<0, 1>() /* Essential: keep object alive while iterator exists */)

		.def_property("org", &otri::org, &otri::setorg, return_value_policy::reference)
		.def_property("dest", &otri::dest, &otri::setdest, return_value_policy::reference)
		.def_property("apex", &otri::apex, &otri::setapex, return_value_policy::reference)
		.def_property_readonly("org_s", &otri::org_s, return_value_policy::reference)
		.def_property_readonly("dest_s", &otri::dest_s, return_value_policy::reference)
		.def_property_readonly("apex_s", &otri::apex_s, return_value_policy::reference)

		.def_property_readonly("lprev", &otri::lprev_)
		.def_property_readonly("lnext", &otri::lnext_)
		.def_property_readonly("oprev", &otri::oprev_)
		.def_property_readonly("onext", &otri::onext_)
		.def_property_readonly("dprev", &otri::dprev_)
		.def_property_readonly("dnext", &otri::dnext_)
		.def_property_readonly("rprev", &otri::rprev_)
		.def_property_readonly("rnext", &otri::rnext_)

		.def_property_readonly("osegnext", &otri::osegnext)
		.def_property_readonly("osegprev", &otri::osegprev)
		.def_property_readonly("dsegnext", &otri::dsegnext)
		.def_property_readonly("dsegprev", &otri::dsegprev)
		.def_property_readonly("asegnext", &otri::asegnext)
		.def_property_readonly("asegprev", &otri::asegprev)

		.def_property_readonly("right_tri", &otri::right_tri_)
		.def_property_readonly("left_tri", &otri::left_tri_)
		.def_property_readonly("sym", &otri::sym_)

		.def("isNull", &otri::isNull)
		.def("isDummy", &otri::isDummy)
		.def("isDirect", &otri::isDirect)

		.def("getVert", &otri::getVert, return_value_policy::reference)
		.def("sideposition", &otri::sideposition, arg("point"), arg("strict"))
		.def("vertices", &otri::vertices)
		.def("contains", &otri::contains, arg("point"), arg("strict"))
		.def("bond", &otri::bond_)
		.def("Delete", &otri::Delete)
		.def("flip", &otri::flip)

		.def("__hash__", [](otri const& s) { return (size_t)&s.tri; })
		.def(self == self)
		.def(self != self)
		.def("__str__", [](otri const &s) { std::ostringstream os; os << s; return os.str(); })
		//.def("__repr__", )
		;


	class_<subseg, subseg*> subseg_py(m, "subseg");

	subseg_py
		.def_readwrite("index", &subseg::index)

		.def_property_readonly("mesh", &subseg::getMesh)
		.def_readwrite("type", &subseg::type)

		.def_readwrite("mass", &subseg::mass)
		.def_readwrite("elas", &subseg::elas)
		.def_readwrite("frot", &subseg::frot)
		.def_readwrite("glis", &subseg::glis)

		.def_readwrite("univec", &subseg::unitVec)
		.def("setUnitVec", &subseg::setUnitVec)

		.def("getTri", &subseg::getTri, return_value_policy::reference)
		.def("getColinearSeg", &subseg::getColinearSeg, return_value_policy::reference)
		.def("getVert", &subseg::getVert, return_value_policy::reference)

		.def("setTri", &subseg::setTri)
		.def("setColinearSeg", &subseg::setColinearSeg)
		.def("setVert", &subseg::setVert)

		.def("setNextSeg", &subseg::setNextSeg)
		.def("setPrevSeg", &subseg::setPrevSeg)
		.def("getPrevSeg", &subseg::getPrevSeg)
		.def("getNextSeg", &subseg::getNextSeg)

		.def("Delete", &subseg::Delete)

		.def("__hash__", [](subseg const& s){ return (size_t)&s; })
		.def(self == self)
		.def(self != self)
		.def("__str__", [](subseg const &s) { std::ostringstream os; os << s; return os.str(); })
		//.def("__repr__", )
		;

	enum_<subseg::SegType>(subseg_py, "SegType")
		.value("WALL", subseg::WALL)
		.value("SEETHROUGH", subseg::SEETHROUGH)
		.value("GLASSWALL", subseg::GLASSWALL)
		//.value("DEADEND_LEFT",	subseg::DEADEND_LEFT)
		//.value("DEADEND_RIGHT",	subseg::DEADEND_RIGHT)
		.export_values()
		;

	class_<osub>(m, "osub")

		.def(init<>(), "__init__ doc")
		.def(init<subseg*, unsigned int>(), arg("subseg"), arg("orient"), "__init__ doc")

		.def_readwrite("ss", &osub::ss)
		.def_readwrite("orient", &osub::ssorient)

		.def_property_readonly("org", &osub::sorg_py, return_value_policy::reference)
		.def_property_readonly("dest", &osub::sdest_py, return_value_policy::reference)

		.def_property_readonly("univec", &osub::univec_py)

		.def_property("mass", &osub::getMass_py, &osub::setMass_py)
		.def_property("elas", &osub::getElas_py, &osub::setElas_py)
		.def_property("frot", &osub::getFrot_py, &osub::setFrot_py)
		.def_property("glis", &osub::getGlis_py, &osub::setGlis_py)

		.def_property_readonly("sym", &osub::ssym_py)
		.def_property_readonly("segnext", &osub::segnext_py)
		.def_property_readonly("segprev", &osub::segprev_py)
		.def_property_readonly("dsegnext", &osub::dsegnext_py)
		.def_property_readonly("dsegprev", &osub::dsegprev_py)

		.def_property_readonly("stpivot", &osub::stpivot_py)

		.def("isNull", &osub::isNull)
		.def("isDummy", &osub::isDummy)
		.def("getVert", &osub::getVert, return_value_policy::reference)
		.def("Delete", &osub::Delete)
		
		.def("__hash__", [](osub const& s) { return (size_t)&s.ss; })
		.def(self == self)
		.def(self != self)
		.def("__str__", [](osub const &s) { std::ostringstream os; os << s; return os.str(); })
		//.def("__repr__", )
		;

	enum_<insertvertexresult>(mesh_py, "InsertVertexResult")
		.value("SUCCESSFULVERTEX", SUCCESSFULVERTEX)
		.value("ENCROACHINGVERTEX", ENCROACHINGVERTEX)
		.value("VIOLATINGVERTEX", VIOLATINGVERTEX)
		.value("DUPLICATEVERTEX", DUPLICATEVERTEX)
		.export_values()
		;

	enum_<Mesh::PathFindAlgo>(mesh_py, "PathFindAlgo")
		.value("MinDistToEnd", Mesh::MinDistToEnd)
		.value("MinDist", Mesh::MinDist)
		.export_values()
		;

	enum_<Mesh::DelaunayAlgo>(mesh_py, "DelaunayAlgo")
		.value("DivAndConquer", Mesh::DivAndConquer)
		.value("Incremental", Mesh::Incremental)
		.export_values()
		;

	mesh_py
		.def(init<bool, bool, char const*>(), arg("exact")=true, arg("searchGraph")=true, arg("flags") = "pwczAevnQX")

		.def("__setstate__", [](Mesh &p, dict t) {
			
			/* Invoke the in-place constructor. Note that this is needed even
			when the object just has a trivial default constructor */
			new (&p) Mesh();
		})

		.def_property("RecentTri",	&Mesh::getRecentTri, &Mesh::setRecentTri)
		.def_property("Weighted", &Mesh::getWeighted, &Mesh::setWeighted)

		//.def("GetMesh", &Mesh::GetMesh, return_value_policy<reference_existing_object>())
		//.staticmethod("GetMesh") // example of a static method exposition

		.def("AddVertices", &Mesh::addVertices_py, "AddVertices's Docstring", arg("Coords"), arg("Attribs")=std::vector<REAL>(), arg("Markers")= std::vector<REAL>())

		.def("InsertVertex", &Mesh::insertVertex_py, arg("pos"), arg("Attribs") = std::vector<REAL>(), arg("Marker") = 0, arg("NearbyVertex") = NULL, return_value_policy::reference)
		
		.def("DoDelaunay",		&Mesh::delaunay, arg("algo")=Mesh::DelaunayAlgo::DivAndConquer)
		.def("CheckDelaunay",	&Mesh::checkdelaunay)
		.def("CheckMesh",		&Mesh::checkmesh)


		.def_readonly("Vertices",	&Mesh::vertices, return_value_policy::reference)
		.def_readonly("Triangles",	&Mesh::triangles, return_value_policy::reference)
		.def_readonly("Segments",	&Mesh::subsegs, return_value_policy::reference)

		.def("getVert",	&Mesh::GetVert, return_value_policy::reference)
		.def("getTri",	&Mesh::GetTri, return_value_policy::reference)
		.def("getSeg",	&Mesh::GetSeg, return_value_policy::reference)


		.def("ResetVertices",	&Mesh::initVertices)
		.def("ResetSegments",	&Mesh::initSegments)
		.def("ResetTriangles",	&Mesh::initTriangles)
		.def("ResetNeighborSegs",	&Mesh::resetNeighborSegs)
		
		.def("DeleteVertex",	&Mesh::deleteVertex)
		.def("DeleteSegment",	&Mesh::deletesegment)
				
		.def("ConstrainedEdge",	&Mesh::constrainededge)
		.def("InCircle",	&Mesh::incircle)
		.def("HullSize",	&Mesh::HullSize)
		
		.def("PreciseLocate",	&Mesh::preciseLocate_py, arg("pos"), arg("SearchTri")=otri(), arg("BackTrack")=true, arg("StopAtSubsegment")=false)

		.def("firstSegObstacle",	&Mesh::firstSegObstacle, return_value_policy::reference)

		.def("IntersectTris",		&Mesh::intersectTris_py, arg("coordslist"), arg("loop")=true, arg("hullstrict")=false, arg("searchTri")= otri())
		.def("VerticesInPolygon",	&Mesh::verticesInPolygon, arg("coordslist"), arg("includeAll")=false, arg("searchTri")= otri())
		.def("TrianglesInPolygon",	&Mesh::trianglesInPolygon, arg("coordslist"), arg("includeAll")=false, arg("searchTri")= otri())

		.def("PathFind",	&Mesh::pathFind_py, arg("start"), arg("end"), arg("algo")=Mesh::PathFindAlgo::MinDist, arg("maxDist")=0, arg("maxWidth")=0, arg("rectify")=true)
		.def_readonly("searchDual",	&Mesh::searchDual)
		.def("ResetDual",			&Mesh::resetDual)
		.def("resetConvexAreas",		&Mesh::resetConvexAreas)
		.def_readonly("TriToConvexArea",&Mesh::triToConvexArea)
		.def_readonly("ConvexAreas",	&Mesh::convexAreasByPerim)
		.def("locateConvArea",		    &Mesh::locateConvexArea)
		.def("getConnexArea",			&Mesh::getConnexArea)
		.def_readonly("ConnexAreaIndex",&Mesh::connexAreaIndex)
		.def("__str__", [](Mesh const &s) { std::ostringstream os; os << "Mesh[" << s.vertices.size() << "]"; return os.str(); })
	;
	
	class_<Perimeter, PerimeterPtr>(m, "Perimeter")
		
		.def_readonly("coords", &Perimeter::coords)
		.def_readonly("univec", &Perimeter::univec)
		.def_readonly("width", &Perimeter::width)
		.def_property_readonly("nextCoords", &Perimeter::getNextCoords, return_value_policy::reference)
				
		.def("__iter__", [](Perimeter &s) { return make_iterator(s.begin(), s.end()); },
				keep_alive<0, 1>() /* Essential: keep object alive while iterator exists */)

		.def_readonly("index",		&Perimeter::index)
		.def_readonly("prev",		&Perimeter::prev)
		.def_readonly("next",		&Perimeter::next)
		.def_readonly("neighbor",	&Perimeter::neighbor)
		.def_property_readonly("oseg",		&Perimeter::get_oseg)

		.def_readonly("nextPort",		&Perimeter::nextPort)
		.def_readonly("prevPort",		&Perimeter::prevPort)

		.def("contains", &Perimeter::contains, arg("point"), arg("segOnly") )
		.def("findIntersect", &Perimeter::findIntersect, arg("start"), arg("end"))

		//.def("__hash__", &Perimeter::int_ptr_py)
		.def(self == self)
		.def(self != self)
		.def("__str__", [](Perimeter const &s) { std::ostringstream os; os << s; return os.str(); })
		//.def("__repr__", )
	;

	class_<InfoCollision>(m, "InfoCollision")
		.def_readonly("Coords",	&InfoCollision::coords)
		.def_readonly("Oseg",	&InfoCollision::oseg)
		.def_readonly("blocking",	&InfoCollision::blocking)
		.def_readonly("segAvanceX",	&InfoCollision::segAvanceX)
		.def_readonly("Obstacles",	&InfoCollision::obstacles)
		.def_readonly("ObstProjXs",	&InfoCollision::obstProjXs)
		//.def("__str__", [](InfoCollision const &s) { std::ostringstream os; os << s; return os.str(); })
    ;

	VECTOR_INDEXING_SUITE(m, triangle*, "TriVec");
	VECTOR_INDEXING_SUITE(m, otri, "OtriVec");
	VECTOR_INDEXING_SUITE(m, osub, "OsubVec");
	VECTOR_INDEXING_SUITE(m, subseg*, "SubSegVec");
	VECTOR_INDEXING_SUITE(m, PerimeterPtr, "PerimeterVec");

	return m.ptr();
}
