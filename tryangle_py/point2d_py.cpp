#include "point2d.h"
#include "triexception.h"
#include "export_py.h"

/*
REAL dist(const tuple& v)const{REAL xx = x-getX(v); REAL yy = y-getY(y); return sqrt(xx*xx + yy*yy);}
REAL distSq(const tuple& v)const{REAL xx = x-getX(x); REAL yy = y-getY(y); return xx*xx + yy*yy;}
REAL dist(const list& v)const{REAL xx = x-getX(v); REAL yy = y-getY(y); return sqrt(xx*xx + yy*yy);}
REAL distSq(const list& v)const{REAL xx = x-getX(x); REAL yy = y-getY(y); return xx*xx + yy*yy;}
*/
REAL operator *(const list &v, const Point2d &p) { return p.x*v[0].cast<REAL>() + p.y*v[1].cast<REAL>(); }
REAL operator *(const tuple &v, const Point2d &p) { return p.x*v[0].cast<REAL>() + p.y*v[1].cast<REAL>(); }
REAL operator *(const Point2d &p, const list &v) { return p.x*v[0].cast<REAL>() + p.y*v[1].cast<REAL>(); }
REAL operator *(const Point2d &p, const tuple &v) { return p.x*v[0].cast<REAL>() + p.y*v[1].cast<REAL>(); }


REAL operator ^(const Point2d &p, const list &v) { return p.x*v[1].cast<REAL>() - p.y*v[0].cast<REAL>(); }
REAL operator ^(const Point2d &p, const tuple &v) { return p.x*v[1].cast<REAL>() - p.y*v[0].cast<REAL>(); }
REAL operator ^(const list &v, const Point2d &p) { return p.y*v[0].cast<REAL>() - p.x*v[1].cast<REAL>(); }
REAL operator ^(const tuple &v, const Point2d &p) { return p.y*v[0].cast<REAL>() - p.x*v[1].cast<REAL>(); }


template<typename V>
Point2d operator +(const V &v, const Point2d &p) { return Point2d(p.x + v[0].cast<REAL>(), p.y + v[1].cast<REAL>()); }

template<typename V>
Point2d operator +(const Point2d &p, const V &v) { return Point2d(p.x + v[0].cast<REAL>(), p.y + v[1].cast<REAL>()); }

template<typename V>
Point2d operator -(const V &v, const Point2d &p) { return Point2d(v[0].cast<REAL>() - p.x, v[1].cast<REAL>() - p.y); }

template<typename V>
Point2d operator -(const Point2d &p, const V &v)  { return Point2d(p.x - v[0].cast<REAL>(), p.y - v[1].cast<REAL>()); }

//! addition
template<typename V>
Point2d& operator +=(Point2d& u, const V &v){ u.x += v[0].cast<REAL>(); u.y += v[1].cast<REAL>(); return u; }
//! subtraction
template<typename V>
Point2d& operator -=(Point2d& u, const V &v){ u.x -= v[0].cast<REAL>(); u.y -= v[1].cast<REAL>(); return u; }


void export_py_point2d(module & m)
{

	class_<Point2d>(m, "Point2d")
		.def(init<REAL,REAL>(), arg("x"), arg("y"), "__init__ doc")
		.def( init<>() )
		//.def( init<tuple>(), arg("vect") )
		//.def( init<list>(), arg("vect") )
		
		.def_readwrite("x", &Point2d::x)
		.def_readwrite("y", &Point2d::y)
		.def("__len__",		&Point2d::size)
		.def("__call__", &Point2d::operator(), return_value_policy::reference)
		
		.def("__getitem__", [](Point2d &s, unsigned int index) { if (index >= s.size()) { throw index_error(); } return s[index]; })
		.def("__setitem__", [](Point2d &s, unsigned int index, REAL val){ if (index >= s.size()) { throw index_error(); } return s[index] = val; })
		
		.def("__getstate__", [](const Point2d &p) {
			return make_tuple(p[0], p[1]);
		})
		
		.def("__setstate__", [](Point2d &p, tuple t) {
			if (t.size() != 2)
				throw std::runtime_error("Invalid state!");

			new (&p) Point2d(t[0].cast<REAL>(), t[1].cast<REAL>());

		})

		.def("__iter__", [](Point2d &o) { return make_iterator(o.begin(), o.end()); },
			keep_alive<0, 1>() /* Essential: keep object alive while iterator exists */)

		.def("copy",		&Point2d::copy)
		.def("copier",		&Point2d::copier, return_value_policy::reference)
        
		.def_property("nor",	&Point2d::nor, &Point2d::set_nor)
		.def_property_readonly("nor2",	&Point2d::nor2)
			
		.def_property_readonly("uni",		&Point2d::uni)
		.def_property_readonly("iuni",		&Point2d::iuni, return_value_policy::reference)
		.def_property_readonly("nml",		&Point2d::nml)
		.def_property_readonly("inml",		&Point2d::inml, return_value_policy::reference)
		.def_property_readonly("uni_nml",	&Point2d::uni_nml)
		.def_property_readonly("iuni_nml",	&Point2d::iuni_nml, return_value_policy::reference)
		.def_property_readonly("anml",		&Point2d::anml)
		.def_property_readonly("ianml",		&Point2d::ianml, return_value_policy::reference)
		
		.def("dist",	&Point2d::dist)
		.def("dist2",	&Point2d::distSq)

		.def("rot",		&Point2d::rot)
		.def("irot",	&Point2d::irot, return_value_policy::reference)
		.def("rot_v",	&Point2d::rot_v)
		.def("irot_v",	&Point2d::irot_v, return_value_policy::reference)
		.def("iarot_v",	&Point2d::iarot_v, return_value_policy::reference)
		.def("rot_vc",	&Point2d::rot_vc)
		.def("irot_vc", &Point2d::irot_vc, return_value_policy::reference)
		.def("iarot_vc", &Point2d::iarot_vc, return_value_policy::reference)

		.def("iadd_mul", &Point2d::iadd_mul, return_value_policy::reference)
		.def("ass_mul", &Point2d::ass_mul, return_value_policy::reference)
		.def("tan_abs", &Point2d::tan_abs)
		.def("tan_abs_min", &Point2d::tan_abs_min)
		.def("angle_inf",	&Point2d::angle_inf)
		
		.def(!self)
		.def(-self)

		.def(self>=REAL())
		.def(self<=REAL())
		.def(self>REAL())
		.def(self<REAL())

		.def(REAL()>=self)
		.def(REAL()<=self)
		.def(REAL()>self)
		.def(REAL()<self)
		
		.def(self += self)
		.def(self += tuple())
		.def(self += list())
		.def(self + self)
		.def(self + tuple())
		.def(self + list())
		.def(self -= self)
		.def(self -= tuple())
		.def(self -= list())
		.def(self - self)
		.def(tuple() - self)
		.def(list() - self)
		.def(tuple() + self)
		.def(list() + self)
		
		.def(self *= REAL())
		.def(self *= int())
		.def(self * REAL())
		.def(self * int())
		.def(list() * self)
		.def(tuple() * self)
		.def(self * list())
		.def(self * tuple())
		.def(REAL() * self)
		.def(int() * self)
		.def(self * self)

		.def(self ^ self)
		.def(self /= REAL())
		.def(self /= int())
		.def(self / REAL())

		.def(self == self)
		.def(self != self)
		.def("__str__", [](Point2d const &s) { std::ostringstream os; os << s; return os.str(); })		
    ;

	VECTOR_INDEXING_SUITE(m, Point2d, "Point2dVec");
}