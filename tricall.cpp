
#include <stdio.h>
#include <stdlib.h>
#include <string>

#include <boost/format.hpp>
#include <boost/python.hpp>


/*****************************************************************************/
/*                                                                           */
/*  tests de boost python													 */
/*                                                                           */
/*****************************************************************************/

std::string tricall_main()
{
	return "";
}

std::string affiche(const boost::python::list& ns)
{
	std::string out = "";
	for (int i = 0; i < len(ns); ++i)
    {
        double arg = boost::python::extract<double>(ns[i]);
		out += boost::lexical_cast<std::string>(arg);
    }
	return out;
}

std::string affiche2(double arg[], int size)
{
	std::string out = "";
	for (int i = 0; i < size; ++i)
    {
		out += boost::lexical_cast<std::string>(arg[i]);
    }
	return out;
}

std::string afficheVec(const std::vector<double>& arg)
{
	std::string out = "";
	for (unsigned int i = 0; i < arg.size(); ++i)
    {
		out += boost::lexical_cast<std::string>(arg[i]);
    }
	return out;
}

std::vector<double> vectlist()
{ // Test
	std::vector<double> out(3);
	out[0]=1;
	out[1]=2;
	out[2]=4;
	return out;
}

struct TestClass{
	int data;
};

TestClass& testmethod( TestClass& c )
{
	c.data += 1;
	return c;
}


void export_tricall()
{
	using namespace boost::python;

	class_< TestClass >("TestClass")
		.add_property("data", &TestClass::data )
	;
	def( "testmethod", &testmethod, return_internal_reference<1>() );

	def( "affiche", &affiche );
	
	def( "afficheRealVec", &afficheVec );
	def( "vectlist", &vectlist );
	
	def("tricall", tricall_main);
}
