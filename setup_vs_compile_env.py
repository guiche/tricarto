import traceback

###
### Setup environment variables used in Visual Studio project properties
### to locate dependencies (opencv, boost, tbb)
###	

import shutil
import os
import re

LOCAL_DIR = os.path.dirname(__file__)


def main():


    DRIVE = "C:/"
    for fileName in os.listdir(DRIVE):
        if fileName.startswith('boost'):
            BOOST_DIR = DRIVE + fileName

            if "u" == input("Boost found in: %s"%(BOOST_DIR) + ". <u>se it ?").strip():
                break
    else:
        raise Exception("No Boost version found. Check out: https://sourceforge.net/projects/boost/files/latest/download?source=typ_redirect")

    LibDir = os.path.join(BOOST_DIR, "lib32-msvc-14.0")

    dlls = []
    for file in os.listdir(LibDir):
        if re.match("boost_python-.*\.dll",file):
            dlls.append(file)


    if dlls:
        for file in dlls:
            print("found dll", file)
            destPath = os.path.join(LOCAL_DIR,"tryangle", file)
            filePath = os.path.join(LibDir, file)
            if not os.path.exists(destPath):
                shutil.copy(filePath, destPath)
    else:
        print("boost python DLLs not found")

    try:
        print("setting env variables")
        EnvVars = dict( PYTHON27_DIR 	= r"C:/Python27",
                       BOOST_DIR 		= BOOST_DIR,
                       BOOST_LIB_DIR 	= LibDir,
                       #TBB_DIR 			= r"C:\tbb43_20141023oss",
                       #TBB_LIB_DIR 		= r"C:\tbb43_20141023oss\lib\ia32\vc12"
                       )

        def setenv(varName, varVal):
            """ set environment variable persistently (e.g. not just in a single cmd window scope) """
            import subprocess
            commande = 'setx -m %s "%s"'%(varName, varVal)

            print('commande', commande)
            subprocess.call(commande)


        try:
            for envName in EnvVars:
                envVal = EnvVars[envName]
                if not os.path.exists(envVal):
                    raise Exception('Path not found: ' + envVal)

                setenv(envName, envVal)
        except:
            traceback.print_exc()
            pass


        AddToPATH = [	'%PYTHON27_DIR%',
                        '%OPENCV_DIR%' + "\\bin\\",
                        '%BOOST_LIB_DIR%',
                    ]

        path = old_path = os.environ['Path']
        for pathVal in AddToPATH:
            if pathVal not in path:
                path += ';'+pathVal

        if path != old_path:
            setenv('Path', path)

    except:
        traceback.print_exc()

    if input('Appuyez sur Entree pour continuer'):
        pass


if __name__=="__main__":
    main()

