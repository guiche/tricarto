from .tryangle_py import Mesh, Point2d as Vec, Perimeter, triangle, Point2dVec as PointList
ETriSidePos = triangle.ETriSidePos

from .try_exception import TryangleException
from pygapp import sauvegarde
import os
from . import sommet
import traceback

import pygapp.media as media

suffixe = '.res'
sauve_dir = media.SAUVE_REP


def AllVertices(self, IncludeBounds=False):
    
    if self.InvalideVertices or IncludeBounds:
        
        Sommets = [self.VertexToWertex[Vert] for Vert in self.Vertices if (IncludeBounds or Vert.radius > 0)]
        if IncludeBounds:
            return Sommets
        
        self._TousSommets = Sommets
        self.InvalideVertices = False
        
    return self._TousSommets

Mesh.AllVertices = AllVertices


def mesh_getstate(self, raiseExc=True):
    
    # Validation du graphe
    try:

        try:
            self.CheckMesh()

        except TryangleException as exc:
            if 'Overlapping Vertex and Segment' in str(exc) or \
                'Overlapping Vertices by' in str(exc):
                import traceback
                traceback.print_exc()

            else:
                raise
    except:
        if raiseExc:
            raise
        else:
            print('Alerte, graphe %s invalide au moment de la sauvegarde'%self)
            traceback.print_exc()

    etat = dict(Sommets=[], Triangles=[], Segments=[])
        
    for vert in self.Vertices:
        
        Info = vert.__getstate__()
        etat['Sommets'].append(Info)

    for tri in self.Triangles:
        
        Info = []
        for i in 0,1,2:
            vert   = tri.getVert(i)
            Info  += [vert.index]
            Otri   = tri.getTri(i)
            Info  += [Otri.tri.index, Otri.orient]
            subseg = tri.getSeg(i)
            Info  += [subseg.ss.index, subseg.orient]
        
        etat['Triangles'].append(Info)

    for seg in self.Segments:
        
        Info = seg.__getstate__()
        etat['Segments'].append(Info)
                 
    return etat


def mesh_setstate(self, etat, raiseExc=False):

    Mesh.__setstate__(self, etat) # constructs the object in cpp
    InfoSommets     = etat['Sommets']
    InfoTriangles   = etat['Triangles']
    InfoSegments    = etat['Segments']
    
    # Allocations des element du graphe
    self.ResetVertices( len(InfoSommets))
    self.ResetTriangles(len(InfoTriangles))
    self.ResetSegments( len(InfoSegments))

    for Index, Data in enumerate(InfoSommets):
        
        vert = self.Vertices[Index]
        vert.__setstate__(Data, self.Triangles)
            
    if len(InfoTriangles) == 0:
        # Pas d'information sur les triangles.
        # Triangulons.
        pass # m.DoDelaunay()
        
    else:

        # Reconstruction des triangles
        for Index, Data in enumerate(InfoTriangles):
            
            Tri = self.Triangles[Index]
            
            for i in 0,1,2:
                
                DataIndex = i*5
                Tri.setVert(i, self.Vertices[Data[DataIndex]])
                TriIndex = Data[DataIndex+1]
                
                if TriIndex >= 0:
                    Tri.setTri( i, self.Triangles[TriIndex], Data[DataIndex+2])
                    
                SegIndex = Data[DataIndex+3]
                if SegIndex >= 0:
                    Tri.setSeg( i, self.Segments[SegIndex], Data[DataIndex+4] )

        ResetNeighborSegs = False
        
        # Reconstruction des segments
        for Index, Data in enumerate(InfoSegments):
            
            Seg = self.Segments[Index]
            Seg.__setstate__(Data, self.Vertices, self.Segments, self.Triangles)

    if ResetNeighborSegs:
        try:
            self.ResetNeighborSegs()
        except:
            raise
            traceback.print_exc()
    
    try:
        self.ResetDual()
        
        # Validation du graphe
        self.CheckMesh()
    except:
        if raiseExc:
            raise
        else:
            traceback.print_exc()
    
Mesh.__getstate__ = mesh_getstate
Mesh.__getstate_manages_dict__ = 1


def Chemin(self, point1, point2, *args, **kwargs):
    
    itineraire = self.PathFind(point1, point2, *args, **kwargs)
    out = []

    if itineraire:

        for passage in reversed(itineraire):

            sign = passage.sign

            if sign != 0:
                sign = 1 if sign > 0 else -1

            out.append((passage.coords, sign))

    """
    if itineraire:
      
        prevPt = itineraire[0]
        
        numPt = len(itineraire)
            
        for i, passage in enumerate(itineraire):
            
            pt = passage.coords
            sign = passage.sign
            
            if i > 0 and Pass.vert:

                #vec1 = prevPt-pt
                nextPt = itineraire[i-1].coords
                
                if i == numPt - 1:
                    prevPt = point1
                else:
                    prevPt = itineraire[i+1].coords
    
                sign = margePivot(prevPt=prevPt, vertPivot=passage.vert, nextPt=nextPt, margeSigne=sign)
                
            out.append((pt, sign))
    """
    return out 


def margePivot(prevPt, vertPivot, nextPt, margeSigne):
    """ calcul du passage le plus etroit en parcourant les voisins d'un sommet pivot """
    
    pt  = vertPivot.coords
    # on part du triangle avec org au pivot traverse par prevPt->pivot
    # puis on tourne dans le sens du pivot jusqu'a trouver le triangle traverse par pivot->nextPt
    tri = vertPivot.triSecant(prevPt)
    
    minDist = abs(margeSigne)
    
    while tri.sideposition(nextPt, False) not in (ETriSidePos.inTRI, ETriSidePos.opORG):
            
        skip = False
        
        if margeSigne > 0:
            som = tri.dest
            if not tri.apex_s.isDummy():
                skip = True
            tri = tri.oprev
        else:
            som = tri.apex
            if not tri.dest_s.isDummy():
                skip = True
            tri = tri.onext
        
        if not skip:
            
            dist = (som.coords - pt).nor
            
            if minDist > dist:
                minDist = dist

    #print 'minDist', vertPivot, margeSigne, '->', 
    if margeSigne > 0:
        margeSigne = minDist
    else:
        margeSigne = -minDist
        
    #print margeSigne
        
    return margeSigne

Mesh.Chemin = Chemin


def SelectAtPos( self, pos, tolVert=0, tolSeg=15 ):
        
    FoundTri = self.PreciseLocate( pos )
      
    if FoundTri.isDummy():
        # Click is outside the mesh        
        FoundTri = self.PreciseLocate( pos, FoundTri, BackTrack=True )
        
    Verts = FoundTri.org, FoundTri.dest, FoundTri.apex
    Segs  = FoundTri.apex_s, FoundTri.org_s, FoundTri.dest_s
    
    #FoundTri = FoundTri.tri
    FoundVerts = []
    for Vert in Verts:
                
        if hasattr(self, 'VertexToWertex'):
            vert = self.VertexToWertex.get(Vert)
        
            if vert is not None:
                v = vert._v_ * .1
                if (pos - (vert.pos - v)).nor2 <= (tolVert+vert.rayon+v.nor)**2:                    
                    # Clicked on a Vertex
                    FoundVerts.append(vert)
    if FoundVerts:
        distance_fonc = lambda vert: (pos-vert.pos).nor2
        FoundVerts.sort(key=distance_fonc)
        return None, FoundVerts[0], None
                
    for Index, (Vert, Seg) in enumerate(zip(Verts,Segs)):
        
        if not Seg.isDummy():
            
            NextVert    = Verts[(Index+1)%3]
            SegVect     = Vert.coords - NextVert.coords
            
            SegDist = abs( SegVect.uni_nml * (pos-Vert.coords) )
                    
            if SegDist < tolSeg:
                return FoundTri, None, Seg
                         
    return FoundTri, None, None

Mesh.SelectAtPos = SelectAtPos


class Reseau( Mesh ):
        
    def __init__(self, nom = 'NouveauReseau', exact=False):
        Mesh.__init__( self, exact=exact )
        self.VertexToWertex   = {}
        self.nom              = nom
        self._TousSommets     = []
        self.InvalideVertices = True

    def __str__(self):
        return self.nom
    
    def __repr__(self):
        return self.nom + ' ' + Mesh.__str__(self)
    
    def Valide(self):
        for vert in self.Vertices:
            if vert not in self.VertexToWertex:
                raise Exception('%s non immatricule'%vert)
            
        Mesh.Valide(self)

    def AddSegments( self, extremites, close=True ):
    
        Bornes = [ self.InsertVertex(pt,[0]) for pt in extremites ]
        sommet.Connecte(Bornes,cloture=close)
        return Bornes
    
    def Bornes(self):
        return [ self.VertexToWertex[Vert] for Vert in self.Vertices if Vert.radius == 0 ]
        
    def SelectVertices(self, Coords, Inverse=False, includeBornes=False):
        
        # tentative de detection de la convexite du contour pour definir un interieur
        # ou selectionner : parcours les points du contour jusqu'a en trouver 
        # trois consecutifs qui ne soient pas alignes : leur position determine alors 
        # le sens de parcours.
        
        NumPoints = len(Coords)
        
        for i in range(NumPoints-1):
            
            #determine la concavite
            point1 = Coords[i]
            point2 = Coords[i+1]
            i3 = (i+2)%NumPoints
            point3 = Coords[i3]

            Vec1 = point2-point1
            Vec2 = point3-point1
            
            orient = Vec1 ^ Vec2
            
            coordsList = Coords
            
            if orient != 0:
                if (orient < 0) ^ Inverse:
                    coordsList = reversed(coordsList)
                                        
                break
            
        if not isinstance(coordsList, PointList):
            _coordsList = PointList() 
            _coordsList.extend(pt for pt in coordsList)
            coordsList = _coordsList
                        
        return [self.VertexToWertex[Vert] for Vert in self.VerticesInPolygon(coordsList, includeAll=includeBornes)]

    def Sauvegarde( self, dialogue=False, nom=None  ):

        if not nom:
            nom = self.nom

        FoncDialogue = lambda defaut :  SelectMonde(defaut=defaut)
        
        return sauvegarde.Sauvegarde( self, nom=nom, FoncDialogue=FoncDialogue, suff=suffixe, sauve_dir=sauve_dir )

    def __getstate__(self):
        
        etat = mesh_getstate(self)
            
        etat['Unites'] = [(i, self.VertexToWertex[vert]) for i, vert in enumerate(self.Vertices) if vert in self.VertexToWertex]
        
        return etat

    def __setstate__(self, etat):

        self.VertexToWertex = {}
        mesh_setstate(self,etat)
    
        Vertices = self.Vertices
        
        for Index, Obj in etat['Unites']:
            vert = Vertices[Index]
           
            Obj.graphe = self
            Obj.set_vertex( vert )

        for Vert in Vertices:
            
            if Vert not in self.VertexToWertex:
                
                if True:
                    raise Exception("Sommet non immatricule %s"%Vert)
                else:
                    if Vert.radius == 0:                    
                        VertType = sommet.SommetBorne
                        print('conversion sommet borne', Vert)
                        
                    else:
                        raise Exception('type de sommet inconnu')
                    
                    self.VertexToWertex[Vert] = VertType(pos=Vert, Reseau=self)

    def __eq__(self, autr):
        return not self.Differe(autr, loquace=False)

    def __ne__(self, autr):
        return self.Differe(autr, loquace=False)
        
    def Differe(self, autre, loquace=True):
        
        Sommets = self.AllVertices(IncludeBounds=True)
        SommetsAutres = autre.AllVertices(IncludeBounds=True)
        
        if len(Sommets) != len(SommetsAutres):
            if loquace:
                return 'different nombre de sommets : %d vs %d'%(len(Sommets), len(SommetsAutres))
            return True
        
        else: 
            
            for v1, v2 in zip(Sommets, SommetsAutres):
                if v1.pos != v2.pos:
                    if loquace:
                        return '%s %s pos different'%(v1, v2), v1.pos, v2.pos
                    return True
                    
                if v1._v_ != v2._v_:
                    if loquace:
                        return '%s %s vitesses different'%(v1, v2), v1._v_, v2._v_
                    return True    
        
        return False

    def OteSommet(self, Sommet):
        self.InvalideVertices = True
        self.VertexToWertex.pop(Sommet._vertex)


def Existe(fileName):
    if not fileName.endswith(suffixe):
        fileName += suffixe
    return os.path.exists(os.path.join(sauve_dir,fileName))


def ListeDesMondes():
    return [ fileName.split('.')[0] for fileName in os.listdir( sauve_dir ) if fileName.endswith(suffixe) ]


def SelectMonde(defaut=None,liste=[],choixNouveau=True,valideExistant=True):
    return sauvegarde.SelectObjet( defaut=defaut, liste=ListeDesMondes(), choixNouveau=choixNouveau, valideExistant=valideExistant )

  
def Ouvrir(nom=''):
    
    if not nom:
        Info = SelectMonde(choixNouveau=False, valideExistant=False)
        if Info:
            nom = Info[0]
        else:
            return
        
    return sauvegarde.Ouvrir( nom, dossier=sauve_dir)

    
def ResauveTous():
    
    for graphe in ListeDesMondes():
        grapheObj = Ouvrir(graphe)
        grapheObj.Sauvegarde()

@property
def barycentre(self):
    bary = Vec()
    num = 0
    for perim in self:
        bary += perim.coords
        num += 1
        
    bary /= num
    
    return bary

@property
def cadrage(self):
    
    Coords = [ perim.coords for perim in self ]
    Xs = [ Coord[0] for Coord in Coords ]
    Ys = [ Coord[1] for Coord in Coords ]
    
    return [min(Xs), min(Ys)], [max(Xs),max(Ys)]

#Perimeter.barycentre = barycentre
#Perimeter.cadrage = cadrage
