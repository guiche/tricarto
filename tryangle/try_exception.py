
class TryangleException( Exception ):
    
    def __init__( self, triangleExc, *args ):
        Exception.__init__( self, *args )
        self.exc = triangleExc

    def __repr__(self):
        return str(self.exc)

    __str__ = __repr__
