from .tryangle_py import Vertex, Point2d as Vec, RealVec, Mesh
InsertVertexResult = Mesh.InsertVertexResult
from pygapp import sauvegarde
import math
import copy


def Connecte(Sommets,cloture=True):
    
    for Index, Sommet in enumerate(Sommets):
        if not cloture and Index == 0:
            continue
        if isinstance(Sommet, SommetBorne):
            Sommet.Connecte(Sommets[Index-1])
        else:
            Sommet.Connect(Sommets[Index-1])


def getstate(self):
    
    return self.coords, self.radius, self.tri.tri.index, self.tri.orient, \
            self.vel, self.mass, self.w, self.elas, self.frot


def setstate(Vert, vertData, triangles=[]):

    SauvEnPoint2d = not isinstance(vertData[0], float)

    if SauvEnPoint2d:
        Vert.coords = vertData[0]
        DataIndex = 1
    else:
        Vert.coords(vertData[0], vertData[1])
        DataIndex = 2
    
    Vert.radius = vertData[DataIndex]
    TriIndex    = vertData[DataIndex + 1]
    
    if TriIndex >= 0:
        Vert.setTri(triangles[TriIndex], vertData[DataIndex + 2])
    
    NumData = len(vertData)
    if NumData > DataIndex+3:
        
        if SauvEnPoint2d:
            Vert.vel = vertData[DataIndex + 3]
            DataIndex += 3
        else:
            Vert.vel(vertData[DataIndex + 3], vertData[DataIndex + 4])
            DataIndex += 4
        
        Vert.mass = vertData[DataIndex + 1]
        
        if NumData > DataIndex+2:
            Vert.w = vertData[DataIndex + 2]
            if NumData > DataIndex+3:
                Vert.elas = vertData[DataIndex + 3]
                Vert.frot = vertData[DataIndex + 4]

# despite the looks, Vertex is not meant to be serialized on its own, it's always part of a graph
# which provides the triangles arg needed for the de-serialization
Vertex.__getstate__ = getstate
Vertex.__setstate__ = setstate


class Wertex(sauvegarde.Phenixable):

    # les carcasses d'unites n'ont ni _vertex ni graphe.
    # apres une reouverture.    
    __exc__state__ = '_vertex', 'graphe'
    __attr_renomme__ = {} # attributs a renommer. { vieux_nom : nouv_nom, ... }
    
    surrayon = 1.1
    
    prefix_nom = 'w'
            
    def __init__(self, pos=None, reseau=None, taille=0, nom=None):
        """
            pos : PosOrVertex
            si l'argument nom est None,
            le nom du sommet sera son id de sommet dans le reseau (ordre d'instanciation).
        """

        if nom:
            self.nom = nom
        else:
            self.nom = ''
                 
        self.pos_prev   = Vec(0,0)

        self._rayon = None

        if reseau:
            self.Matricule(reseau, pos, taille)
        
        self.groupe = None
                
        self.entourage = 0
    
    def Matricule( self, Reseau, PosOrVertex, taille=10 ):
    
        assert not hasattr( self, '_vertex' ), "already registered"
        
        self.graphe = Reseau
        
        if isinstance( PosOrVertex, Vertex ):
            vert = PosOrVertex
        else:
            if not isinstance( PosOrVertex, Vec):
                PosOrVertex = Vec(*PosOrVertex)
            Attribs = RealVec()
            Attribs.append(taille)
            vert = Reseau.InsertVertex(PosOrVertex, Attribs, 0, None)
            # AFAIRE: l'insertion peut echouer et le vertex etre NULL - traiter ce cas proprement.
            
        self.set_vertex(vert)         

    def __str__(self):
        
        nom = getattr(self, 'nom', '' )
        
        if hasattr( self, '_vertex'):
            nom += '%s%d'%(self.prefix_nom,self._vertex.id)
                    
        return nom
    
    __repr__ = __str__
    #def __repr__(self):
    #    return str(self) + ' %d, %d'%(self.x,self.y)
    
    def copie(self,NovPos):
        """ afaire : __copy__ """
        NovObj = copy.deepcopy( self )
        
        if self.nom:
            NovObj.nom = 'copie de '+self.nom
        else:
            NovObj.nom= ''
            
        NovObj.Matricule(self.graphe, NovPos, taille=self.rayon)
        return NovObj

    @property
    def VerEnWer(self):
        """ racourci de syntaxe pour appeler VerEnWer sur les unites
            directement
         """
        return self.graphe.VertexToWertex

    def AfficherInfo(self, Surf, DansSelection=False, drapeaux={}):
        pass
        
    def get_vertex(self):
        """:rtype Vertex"""
        return self._vertex

    def set_vertex(self, vert):
        self._vertex = vert
        self._rayon = vert.radius
        #print('hash vert', vert, hash(vert))
        self.graphe.VertexToWertex[vert] = self
        self.graphe.InvalideVertices = True

        #print(self.graphe.Vertices)
        #print(self.graphe.AllVertices(True))
        #print(self.graphe.Vertices)
                    
    def __bool__(self):
        return getattr(self, '_vertex', None) is not None
    
    @property
    def index(self):
        return self._vertex.index

    @property
    def id(self):
        return self._vertex.id
    
    def Efface( self ):

        if self.groupe:
            self.groupe.Ote( self )
                    
        self.graphe.OteSommet(self)
        self._vertex.Delete(True)
        #self_vertex = None
        
    Delete = Efface
          
    @property
    def pos(self):
        """:rtype Vec"""
        
        return self._vertex.coords
    
    @pos.setter
    def pos(self,val):
        self.dpos = val - self._vertex.coords
    
    @property
    def dpos(self):
        """:rtype Vec"""
        return self.pos - self.pos_prev
    
    @dpos.setter
    def dpos(self,dpos):
        self.move(dpos, checkCollisions=self.racine.ValideMouvement, enforceDelaunay=True)

    barycentre = pos

    @property
    def _v_(self):
        """ vitesse 
            :rtype Vec
        """
        return self._vertex.vel
        
    @_v_.setter
    def _v_(self,val):
        """ vitesse """
        self._vertex.vel(*val)
        
    @property    
    def w(self):
        return self._vertex.w
    
    @w.setter    
    def w(self, val):
        self._vertex.w = val
    
    @property
    def frot(self):
        # coefficient de frottement de la surface de ce corps
        return self._vertex.frot

    @frot.setter
    def frot(self, val):
        self._vertex.frot = val
    
    @property    
    def elas(self):
        # coefficeient d'elasticite
        return self._vertex.elas
    
    @elas.setter
    def elas(self, val):
        self._vertex.elas = val
    
    @property
    def cadrage(self):
        a = max(self.rayon,10) * 4
        return [self.pos[0] - a, self.pos[1] - a], [self.pos[0] + a, self.pos[1] + a] 
    
    def move(self,novPos, relative=True, checkCollisions=False, enforceDelaunay=True):
        self.pos_prev.copier(self.pos)
        moveResult = self._vertex.move(novPos,relative,checkCollisions,enforceDelaunay)

        if moveResult != InsertVertexResult.SUCCESSFULVERTEX:
            msg = "Vertex move from %s"%self.pos_prev
            if relative:
                msg += " by "
            else:
                msg += " to "
            raise Exception(msg + "%s failed: %s"%(novPos, moveResult))



    @property
    def rayon(self):
        return self._rayon

    @rayon.setter
    def rayon(self,val):
        self._vertex.radius = val
        self._rayon = self._vertex.radius

    @property
    def srayon(self):
        return self._rayon * self.surrayon
    
    @property
    def poidsRatio(self):
        return self._vertex.weightRatio

    @poidsRatio.setter
    def poidsRatio(self,val):
        self._vertex.weightRatio = val
    
    def Voisins( self, Degre=0, IncludeBornes=False ):
        return ( self.graphe.VertexToWertex[Vert] for Vert in self._vertex.neighbors(Degre) if IncludeBornes or Vert.radius > 0 )
    
    def Moyennage(self):
        pos = self._vertex.coords
        #tot = [ min( Vert.rayon, max(Vert.rayon/3,Vert.entourage/math.sqrt((Vert.pos - pos).nor)*math.sqrt( self.rayon * 16 ) ) ) for Vert in self.Voisins(IncludeBornes=False) ]
        
        tot = [ Vert.rayon/math.sqrt((Vert.pos - pos).nor) for Vert in self.Voisins(IncludeBornes=True) ]
        
        self.entourage = sum(tot) * math.sqrt( 3 * self.rayon )

        tot = [ Vert.entourage/math.sqrt((Vert.pos - pos).nor) for Vert in self.Voisins(IncludeBornes=True) ]
        
        self.entourage2 = sum(tot) * math.sqrt( 3 * self.rayon )
        #self.entourage = self.rayon
        #if tot:
        #    self.entourage += sum(tot)/len(tot)
            
            
    def PremierObstacle(self, depla, frontRadius=None, sideRadius=0, relative=False, segOnly=True, detectFar=True, checkResult=False, coteTol=0):

        if frontRadius is None:
            frontRadius = self._rayon

        infoColl = self._vertex.firstObstacle(depla, frontRadius, sideRadius, relative, segOnly, detectFar, checkResult, coteTol, self.surrayon)
        Obstacles = [ self.graphe.VertexToWertex[Vert] for Vert in infoColl.Obstacles ]
        
        if infoColl.blocking: 
            
            ObstProjXs = infoColl.ObstProjXs
            
            minDist = None
            if len(ObstProjXs):
                minDist = ObstProjXs[0]

            tolDist = self.rayon * (self.surrayon-1)
                
            if infoColl.Oseg.ss:
                if minDist is None or infoColl.segAvanceX < minDist + tolDist:
                    return infoColl.Coords, [infoColl.Oseg]
                """
                elif infoColl.segAvanceX < minDist +tolDist:
                    minDist = min(infoColl.segAvanceX,minDist)
                    Obstacles.append( infoColl.Oseg )
                """
            return infoColl.Coords, Obstacles
        
        else:
            #not blocking
            if infoColl.Oseg.ss:
                Obstacles.append(infoColl.Oseg)
                
            return None, Obstacles
        
    def triSecant(self,pos):
        return self._vertex.triSecant(pos)
 
        
class SommetBorne(Wertex):
    """ Extremite de segment """

    couleur = 250,250,250
     
    const_v = Vec(0,0)
    
    prefix_nom = 'brn'
    _accelere_ = False
    
    @property
    def _v_(self):
        """:rtype Vec"""
        return self.const_v
    
    def Connecte( self, autre ):
        """:type autre: SommetBorne"""
        self._vertex.Connect(autre._vertex)
