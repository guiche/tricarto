'''
Created on 16 Aug 2012

@author: Zarastro
'''
from .tryangle_py import subseg, osub

SegType = subseg.SegType

wall_couleur       = 250,250,250 
seethrough_couleur = 180,150,230


def couleur(self):
    if self.type == SegType.WALL:
        return wall_couleur
    
    elif self.type == SegType.SEETHROUGH:
        return seethrough_couleur

subseg.couleur = couleur

@property
def barycentre(self):
    Vec = self.getVert(0) + self.getVert(1)
    Vec /= 2.
    return Vec

@property
def cadrage(self):
    p1, p2 = self.getVert(0).coords, self.getVert(1).coords
    return [min(p1[0],p2[0]), min(p1[1],p2[1])], [max(p1[0],p2[0]),max(p1[1], p2[1])]

subseg.barycentre = barycentre
subseg.cadrage = cadrage


def bloquant( self, Pos, But ):
    ''' True si [Pos,But] coupe [self.org, self.dest].
        Presuppose que Pos et But sont deux points de part et d'autre de self. 
        Verifie que les extremites du segment ne sont pas du meme cote de la droite Pos->But.
    '''

    Vect = But - Pos
    try:
        return ( Vect ^ ( self.dest.coords - Pos ) > 0 ) ^ ( Vect ^ ( self.org.coords - Pos ) > 0 )
    except:
        print(self)
        raise

osub.bloquant = bloquant
#subseg.bloquant = bloquant

def get_type(self):
    return self.ss.type

def set_type(self,val):
    self.ss.type = val

SegType.EnsembleValeurs = SegType.WALL, SegType.SEETHROUGH, SegType.GLASSWALL

setattr( osub, 'type', property(fget=get_type, fset=set_type ) )

def osub_getstate(OSub):
    return OSub.ss.mesh, OSub.ss.index, OSub.orient

def osub_setstate(OSub, Data):
    mesh, index, orient = Data
    OSub.ss      = mesh.getSeg(index)
    OSub.orient  = orient

osub.__getstate__ = osub_getstate
osub.__setstate__ = osub_setstate
osub.__getstate_manages_dict__ = 1


def getstate(Seg):

    Info = []
    
    for i in 0,1:
        Otri  = Seg.getTri(i)
        Info  += [ Otri.tri.index, Otri.orient ]
        Subseg = Seg.getColinearSeg(i)
        Info  += [ Subseg.ss.index, Subseg.orient ]
        
    for i in 0,1,2,3:
        Vert  = Seg.getVert(i)
        Info += [ Vert.index ]

    for Tag in 'Next', 'Prev':        
        for i in 0,1:
            Osub  = getattr(Seg,'get%sSeg'%Tag)(i)
            Info += [ Osub.ss.index, Osub.orient ]
            #print Seg, Tag, Osub, Osub.ss.index, Osub.orient

    Info += [ Seg.mass, Seg.elas, Seg.frot, Seg.glis ]
    
    return Info


def setstate(Seg, Data, Vertices, Segments, Triangles):
                
    for i in 1,2,3,4:
        
        Seg.setVert(4-i, Vertices[Data[12-i]])
    
    for i in 0,1:
        
        DataIndex = i*4
        TriIndex = Data[DataIndex]
        
        if TriIndex >= 0:
            Seg.setTri( i, Triangles[TriIndex], Data[DataIndex+1] )
            
        SegIndex = int(Data[DataIndex+2])
        if SegIndex >= 0:
            Seg.setColinearSeg( i, Segments[SegIndex], Data[DataIndex+3] )

    lenData = len(Data)
    if lenData > 12:
        
        for j, Tag in enumerate(('Next', 'Prev')):        
            for i in 0,1:
                DataIndex = 12 + 4*j + 2*i
                SegIndex = int(Data[DataIndex])
                if SegIndex >= 0: # SegIndex = -1 for dummyseg
                    Orient = Data[DataIndex+1]
                    getattr(Seg,'set%sSeg'%Tag)(i, Segments[SegIndex], Orient)
        
        if lenData > 20:
            Seg.mass, Seg.elas, Seg.frot, Seg.glis = Data[20:24]
                    
        ResetNeighborSegs = False
    else:
        ResetNeighborSegs = True
    
    Seg.setUnitVec()
    
    return ResetNeighborSegs

# Serialisation ne peut marcher car subseg est declare comme no_init dans boost::python
subseg.__getstate_manages_dict__ = 1 
subseg.__getstate__ = getstate
subseg.__setstate__ = setstate
