from .tryangle_py import triangle, otri, Point2d as Vec
import math


def getstate(Tri):
    
    Info = []
    for i in 0,1,2:
        Info += [Tri.getVert(i), Tri.getTri(i), Tri.getSeg(i)]

    return Info

def setstate(Tri, Data):
        
    for i in 0,1,2:
        DataIndex = i*3
        Tri.setVert(i, Data[DataIndex].index)
        
        OTri = Data[DataIndex+1]
        
        if OTri:
            Tri.setTri( i, OTri.tri.index, OTri.orient)
            
        OSub = Data[DataIndex+2]
        if OSub:
            Tri.setSeg( i, OSub.ss.index, OSub.orient )

# Serialisation ne peut marcher car triangle est declare comme no_init dans boost::python
triangle.__getstate_manages_dict__ = 1
triangle.__getstate__ = getstate
triangle.__setstate__ = setstate


def otri_getstate(OTri):
    return OTri.tri.mesh, OTri.tri.index, OTri.orient

def otri_setstate(OTri, Data):
    mesh, triIndex, orient = Data
    OTri.tri = mesh.getTri(triIndex)
    OTri.orient = orient
        
@property
def barycentre(self):
    bary = Vec()
    for i in 0,1,2:
        bary += self.getVert(i).coords
    
    bary /= 3.
    return bary

@property
def cadrage(self):
    Coords = [ self.getVert(i).coords for i in (0,1,2) ]
    Xs = [ Coord[0] for Coord in Coords ]
    Ys = [ Coord[1] for Coord in Coords ]
    
    return [min(Xs), min(Ys)], [max(Xs),max(Ys)]
                      
triangle.barycentre = barycentre
triangle.cadrage = cadrage

@property
def otri_cadrage(self):
    return self.tri.cadrage
    
otri.cadrage = otri_cadrage

@property
def otri_barycentre(self):
    return self.tri.barycentre

otri.barycentre = barycentre

otri.__getstate__ = otri_getstate
otri.__setstate__ = otri_setstate
otri.__getstate_manages_dict__ = 1

def CircumCircle( self ):
    ''' Cercle circonscrit '''
  
    (xA,yA), (xB,yB), (xC,yC) = [ (Vert.x,  Vert.y) for Vert in [ self.getVert(i) for i in range(3) ] ]
    
    uC,vC = (xA+xB)/2., (yA+yB)/2.
    uA,vA = (xC+xB)/2., (yC+yB)/2.
    nC,mC = (xA-xB), (yA-yB)
    nA,mA = (xC-xB), (yC-yB)
    
    # nC*(x-uC) + mC*(y-vC) = 0
    # nA*(x-uA) + mA*(y-vA) = 0

    if nC == 0:
        if nA == 0:
            raise "bad triangle"
            
        y = vC
        x = -mA*(y-vA)/nA + uA

    else:
        #x = uC - mC / nC * (y-vC)
                        
        if nA == 0:
            if mA == 0:
                raise "bad triangle"
            y = vA/mA
            
        else:
            y = ( - nA*mC/nC*vC + nA*uA + mA*vA - nA*uC ) / (-nA*mC/nC+mA)

        x = uC - mC / nC * (y-vC)

    radius = math.sqrt( (x-xA)**2 + (y-yA)**2 )

    return x,y, radius

triangle.CircumCircle = CircumCircle

def _CircumCircle( self ):
    return self.tri.CircumCircle()
    
otri.CircumCircle = _CircumCircle

@property
def index(self):
    return self.tri.index

otri.index = index
