from .reseau import Reseau, Perimeter
from .sommet import Wertex, SommetBorne
from .segment import osub, subseg
from .triangle import otri, triangle
from . import tryangle_py
from .tryangle_py import Point2d, Point2d as Vec, Point2dVec as PointList
import pygapp.vect2d as vect2d
from .try_exception import TryangleException
from .tryangle_py import RealVec, Mesh
PathFindAlgo = Mesh.PathFindAlgo

"""
def Point2d_getstate(self):
    return dict( vals = (self.x+1,self.y+2) )

def Point2d_setstate(self, etat):
    self(*etat['vals'])

tryangle_py.Point2d.__getstate__ = Point2d_getstate
tryangle_py.Point2d.__setstate__ = Point2d_setstate
"""

RealVec.nml  = vect2d.Vec.nml
RealVec.nor  = vect2d.Vec.nor
RealVec.nor2 = vect2d.Vec.nor2
RealVec.uni  = vect2d.Vec.uni
RealVec.uni_nml  = vect2d.Vec.uni_nml


vect2d.vectorise( RealVec )
vect2d.vectorise(tryangle_py.Point2d, faible=True)

"""
def _vec__eq__(self,other):
    return all( self[i] == other[i] for i in xrange(len(self)) )

def _vec__ne__(self,other):
    return other is None or any( self[i] != other[i] for i in xrange(len(self)) )
    
RealVec.__eq__ = _vec__eq__
RealVec.__ne__ = _vec__ne__

"""

def _getstate(self):
    return tuple(self)

def _setstate(self, etat):
    for val in etat:
        self.append(val)

RealVec.__getstate__ = _getstate
RealVec.__setstate__ = _setstate


def toPointList(iterable):
    """ Converts a list of coords to a PointList"""
    if isinstance(iterable, PointList):
        return iterable
    
    pointList = PointList()
    for p in iterable:
        if not isinstance(p, Vec):
            p = Vec(*p)
        pointList.append(p)
    
    return pointList
